import 'package:flutter/material.dart';
import 'package:light_silver/routes/route_name.dart';

import '../page/contact_us_routes.dart';
import '../page/forgot_password_routes.dart';
import '../page/home_routes.dart';
import '../page/login_or_register_routes.dart';
import '../page/medical_history_routes.dart';
import '../page/member_centre_routes.dart';
import '../page/pv/pv02.dart';
import '../page/pv/pv03.dart';
import '../page/pv/pv04.dart';
import '../page/pv/pvpdf.dart';
import '../page/pv/pv01.dart';
import '../page/record_routes.dart';
import '../page/set_up_routes.dart';
import '../page/topic/lbk/lbk01_routes.dart';
import '../page/topic/lbk/lbk02_routes.dart';
import '../page/topic/lbk/lbk03_routes.dart';
import '../page/topic/lbk/lbk04_routes.dart';
import '../page/topic/lbk/lbk05_routes.dart';
import '../page/topic/lbk/lbk11_routes.dart';
import '../page/topic/lbk/lbk12_routes.dart';
import '../page/topic/lbk/lbk13_routes.dart';
import '../page/topic/lbk/lbk14_routes.dart';
import '../page/topic/lbk/lbk15_routes.dart';
import '../page/topic/lbk/lbk160_routes.dart';
import '../page/topic/lbk/lbk16_routes.dart';
import '../page/topic/lbk/lbk17_routes.dart';
import '../page/topic/ld/ld01_routes.dart';
import '../page/topic/spe/spe01_routes.dart';
import '../page/topic/spe/spe02_routes.dart';
import '../page/topic/spe/spe03_routes.dart';
import '../page/topic/spe/spe04_routes.dart';
import '../page/topic/spe/spe05_routes.dart';
import '../page/topic/spe/spe06_routes.dart';
import '../page/topic/spe/spe07_routes.dart';
import '../page/topic/spe/spe08_routes.dart';
import '../page/topic/spe/spe09_routes.dart';
import '../page/topic/spe/spe10_routes.dart';
import '../page/topic/spe/spe11_routes.dart';
import '../page/topic/spe/spe5and6_routes.dart';
import '../page/topic/spe/spe91_routes.dart';
import '../page/topic/spe/spe92_routes.dart';
import '../page/topic/spe/spe92and93_routes.dart';
import '../page/topic/spe/spe93_routes.dart';
import '../page/topic/spe/spe94_routes.dart';
import '../page/topic/spe/spe95_routes.dart';
import '../page/topic/spe/spe96_routes.dart';
import '../page/topic/sps/sps01_routes.dart';
import '../page/topic/sps/sps02_routes.dart';
import '../page/topic/sps/sps03_routes.dart';
import '../page/topic/sps/sps04_routes.dart';
import '../page/topic/sps/sps05_routes.dart';
import '../page/topic/sps/sps06_routes.dart';
import '../page/topic/sps/sps41_routes.dart';

//路由2.0
class LSRouterDelegate extends RouterDelegate<List<RouteSettings>>
    with ChangeNotifier, PopNavigatorRouterDelegateMixin<List<RouteSettings>> {
  final List<Page> _pages = [];
  List<Page> get pages => _pages;

  @override
  GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  @override
  Widget build(BuildContext context) {
    return Navigator(
      key: navigatorKey,
      pages: List.of(_pages),
      onPopPage: _onPopPage,
    );
  }

  @override
  Future<void> setNewRoutePath(List<RouteSettings> configuration) async {
    debugPrint('setNewRoutePath ${configuration.last.name}');

    _setPath(configuration
        .map((routeSettings) => _createPage(routeSettings))
        .toList());
    return Future.value(null);
  }

  void _setPath(List<Page> pages) {
    _pages.clear();
    _pages.addAll(pages);
    if (_pages.first.name != '/') {}
    notifyListeners();
  }

  ///回上一頁
  @override
  Future<bool> popRoute() {
    if (canPop()) {
      _pages.removeLast();
      notifyListeners();
      return Future.value(true);
    }
    return _confirmExit();
  }

  ///回到第一頁
  Future<bool> popUtil() {
    if (canPop()) {
      Page pageCopy = _pages.first;
      _pages.clear();
      _pages.add(pageCopy);
      notifyListeners();
      return Future.value(true);
    }
    return _confirmExit();
  }

  ///關閉當前畫面並建立下一頁
  void popAndPushRouter({required String name, dynamic arguments}) {
    if (_pages.isNotEmpty) {
      _pages.removeAt(_pages.length - 1);
    }
    push(name: name, arguments: arguments);
  }

  bool canPop() {
    return _pages.length > 1;
  }

  bool _onPopPage(Route route, dynamic result) {
    if (!route.didPop(result)) return false;
    if (canPop()) {
      _pages.removeLast();
      return true;
    } else {
      return false;
    }
  }

  ///彈出直到
  void popUtilPage({required String name, dynamic arguments}) {
    if (_pages.any((element) => element.name == name)) {
      int lastId = _pages.indexWhere((element) => element.name == name);
      _pages.removeRange(lastId, _pages.length);
      push(name: name, arguments: arguments);
    } else {
      push(name: name, arguments: arguments);
    }
  }

  void push({required String name, dynamic arguments}) {
    _pages.add(_createPage(RouteSettings(name: name, arguments: arguments)));
    notifyListeners();
  }

  void replace({required String name, dynamic arguments}) {
    if (_pages.isNotEmpty) {
      _pages.clear();
    }
    push(name: name, arguments: arguments);
  }

  MaterialPage _createPage(RouteSettings routeSettings) {
    Widget child;
    switch (routeSettings.name) {
      case RouteName.home:
        child = HomeRoutes();
        break;
      case RouteName.singIn:
        child = LoginOrRegisterRouters();
        break;
      case RouteName.memberCentre:
        child = MemberCentreRoutes();
        break;
      case RouteName.setUp:
        child = SetUpRoutes();
        break;
      case RouteName.forgotPassword:
        child = ForgetPasswordRoutes();
        break;
      case RouteName.contactUs:
        child = ContactUsRoutes();
        break;
      case RouteName.medicalHistory:
        child = MedicalHistoryRoutes();
        break;
      case RouteName.spe01:
        child = SPe01Routes();
        break;
      case RouteName.spe02:
        child = SPe02Routes();
        break;
      case RouteName.spe03:
        child = SPe03Routes();
        break;
      case RouteName.spe04:
        child = SPe04Routes();
        break;
      case RouteName.spe05:
        child = SPe05Routes();
        break;
      case RouteName.spe06:
        child = SPe06Routes();
        break;
      case RouteName.spe5And6:
        child = SPe5And6Routes();
        break;
      case RouteName.spe07:
        child = SPe07Routes();
        break;
      case RouteName.spe08:
        child = SPe08Routes();
        break;
      case RouteName.spe09:
        child = SPe09Routes();
        break;
      case RouteName.spe10:
        child = SPe10Routes();
        break;
      case RouteName.spe11:
        child = SPe11Routes();
        break;
      case RouteName.lbk01:
        child = LBk01Routes();
        break;
      case RouteName.lbk02:
        child = LBk02Routes();
        break;
      case RouteName.lbk03:
        child = LBk03Routes();
        break;
      case RouteName.lbk04:
        child = LBk04Routes();
        break;
      case RouteName.lbk05:
        child = LBk05Routes();
        break;
      case RouteName.sps41:
        child = SPs41Routes();
        break;
      case RouteName.sps01:
        child = SPs01Routes();
        break;
      case RouteName.sps02:
        child = SPs02Routes();
        break;
      case RouteName.sps03:
        child = SPs03Routes();
        break;
      case RouteName.sps04:
        child = SPs04Routes();
        break;
      case RouteName.sps05:
        child = SPs05Routes();
        break;
      case RouteName.sps06:
        child = SPs06Routes();
        break;
      case RouteName.ld01:
        child = LD01Routes();
        break;
      case RouteName.lbk11:
        child = LBk11Routes();
        break;
      case RouteName.lbk12:
        child = LBk12Routes();
        break;
      case RouteName.lbk13:
        child = LBk13Routes();
        break;
      case RouteName.lbk14:
        child = LBk14Routes();
        break;
      case RouteName.lbk15:
        child = LBk15Routes();
        break;
      case RouteName.lbk16:
        child = LBk16Routes();
        break;
      case RouteName.lbk160:
        child = LBk160Routes();
        break;
      case RouteName.lbk17:
        child = LBk17Routes();
        break;
      case RouteName.spe91:
        child = SPe91Routes();
        break;
      case RouteName.spe92:
        child = SPe92Routes();
        break;
      case RouteName.spe93:
        child = SPe93Routes();
        break;
      case RouteName.spe92And93:
        child = SPe92And93Routes();
        break;
      case RouteName.spe94:
        child = SPe94Routes();
        break;
      case RouteName.spe95:
        child = SPe95Routes();
        break;
      case RouteName.spe96:
        child = SPe96Routes();
        break;
      case RouteName.pv01:
        child = PV01Routes();
        break;
      case RouteName.pv02:
        child = PV02Routes();
        break;
      case RouteName.pv03:
        child = PV03Routes();
        break;
      case RouteName.pv04:
        child = PV04Routes();
        break;
      case RouteName.pvPDF:
        child = PVPDFRoute();
        break;
      case RouteName.recordRoutes:
        child = RecordRoutes();
        break;
      default:
        child = const Scaffold();
    }
    return MaterialPage(
        child: SafeArea(
            top: routeSettings.name == '/splashPage' ? false : true,
            bottom: false,
            child: child),
        key: Key(routeSettings.name!) as LocalKey,
        name: routeSettings.name,
        arguments: routeSettings.arguments);
  }

  Future<bool> _confirmExit() async {
    final result = await showDialog<bool>(
        context: navigatorKey.currentContext!,
        builder: (context) {
          return AlertDialog(
            content: const Text(
              '確定要退出APP嗎?',
              textScaleFactor: 1,
            ),
            actions: [
              TextButton(
                onPressed: () => Navigator.pop(context, true),
                child: const Text(
                  '取消',
                  textScaleFactor: 1,
                ),
              ),
              TextButton(
                onPressed: () => Navigator.pop(context, false),
                child: const Text(
                  '確認',
                  textScaleFactor: 1,
                ),
              )
            ],
          );
        });
    return result ?? true;
  }
}
