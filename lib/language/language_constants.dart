import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'localization.dart';

const String LANGUAGE_CODE = 'languageCode';

//languages code
const String TAIWAN = 'TAIWAN';
const String CHINA = 'CHINA';
const String USA = 'USA';
const String JAPAN = 'JAPAN';

Future<Locale> setLocale(String languageCode) async{
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  await _prefs.setString(LANGUAGE_CODE, languageCode);
  return _locale(languageCode);
}

Future<Locale> getLocale() async {
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  String languageCode = _prefs.getString(LANGUAGE_CODE) ?? "zh";
  print('當前語系: $languageCode');
  return _locale(languageCode);
}

Locale _locale(String languageCode) {
  switch(languageCode){
    case TAIWAN:
      return Locale('zh', 'TW');
    case CHINA:
      return Locale('zh', 'CN');
    case USA:
      return Locale('en', 'US');
    case JAPAN:
      return Locale('ja', 'JP');
    default:
      return Locale('zh', 'TW');
  }
}

String getTranslated(BuildContext context, String key){
  return Localization.of(context).translate(key);
}