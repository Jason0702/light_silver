import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class Localization{
  Localization(this.locale);

  final Locale? locale;

  static Localization of(BuildContext context){
    return Localizations.of<Localization>(context, Localization)!;
  }

  Map<String, String>? _localizedValues;

  Future<void> load() async{
    String jsonStringValue = await rootBundle.loadString('assets/i18n/${locale!.languageCode}_${locale!.countryCode!}.json');
    Map<String, dynamic> mappedJson = json.decode(jsonStringValue);
    _localizedValues = mappedJson.map((key, value) => MapEntry(key, value));
  }

  String translate(String key){
    return _localizedValues![key]!;
  }

  static const LocalizationsDelegate<Localization> delegate = _LocalizationsDelegate();
}

class _LocalizationsDelegate extends LocalizationsDelegate<Localization>{
  const _LocalizationsDelegate();

  @override
  bool isSupported(Locale locale) {
    return ['zh_TW', 'zh_CN', 'en_US', 'ja_JP'].contains(locale.languageCode+'_'+locale.countryCode!);
  }

  @override
  Future<Localization> load(Locale locale) async {
    Localization localization = Localization(locale);
    await localization.load();
    return localization;
  }

  @override
  bool shouldReload(LocalizationsDelegate<Localization> old) => false;
}