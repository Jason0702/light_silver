import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:light_silver/models/patient.dart';
import 'package:path/path.dart' as p;
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseHelper{
  static final DatabaseHelper _instance = DatabaseHelper.internal();
  factory DatabaseHelper()=> _instance;

  static Database? _db;

  Future<Database> get db async{
    if(_db != null){
      return _db!;
    }
    _db = await initDb();
    return _db!;
  }

  DatabaseHelper.internal();

  initDb() async{
    Directory documentDirectory = await getApplicationDocumentsDirectory();
    String path = p.join(documentDirectory.path, 'patient.db');
    var ourDb = await openDatabase(path, version: 1, onCreate: _onCreate);
    return ourDb;
  }

  void _onCreate(Database db, int version) async{
    await db.execute(
        'CREATE TABLE Patient('
            'id INTEGER PRIMARY KEY,'
            'name TEXT,'
            'gender TEXT,'
            'age INTEGER,'
            'phone TEXT,'
            'problem TEXT,'
            'relieveSymptomsOfDiscomfort INTEGER,'
            'postureCorrection INTEGER,'
            'informationHealthEducation INTEGER,'
            'jointReduction INTEGER,'
            'coreMuscleTraining INTEGER,'
            'healthPromotion INTEGER,'
            'GT INTEGER,'
            'pad INTEGER,'
            'padHigh TEXT,'
            'IC INTEGER,'
            'ASIS INTEGER,'
            'PSIS INTEGER,'
            'flexion INTEGER,'
            'floor TEXT,'
            'gillet INTEGER,'
            'extension INTEGER,'
            'rotation_left INTEGER,'
            'rotation_right INTEGER,'
            'side_left INTEGER,'
            'side_right INTEGER,'
            'side_knee_left TEXT,'
            'side_knee_right TEXT,'
            'supine_leg_length INTEGER,'
            'adductor INTEGER,'
            'inguinal INTEGER,'
            'prone_leg_length INTEGER,'
            'add_ROM INTEGER,'
            'ext_rotator INTEGER,'
            'PSIS_left INTEGER,'
            'PSIS_right INTEGER,'
            'ILA_left INTEGER,'
            'ILA_right INTEGER,'
            'Prop_left INTEGER,'
            'Prop_right INTEGER,'
            'lumbar_left INTEGER,'
            'lumbar_right INTEGER,'
            'supine_ilium_leg_length INTEGER,'
            'Long_sit INTEGER,'
            'SLR INTEGER,'
            'Patrick INTEGER,'
            'Knee_flexor INTEGER,'
            'ASIS_umbilical INTEGER,'
            'IC_recheck INTEGER,'
            'flexion_recheck INTEGER,'
            'floor_recheck TEXT,'
            'gillet_recheck INTEGER,'
            'extension_recheck INTEGER,'
            'rotation_left_recheck INTEGER,'
            'rotation_right_recheck INTEGER,'
            'side_right_recheck INTEGER,'
            'side_left_recheck INTEGER,'
            'side_knee_right_recheck TEXT,'
            'side_knee_left_recheck TEXT)'
    );
    debugPrint('Table is created');
  }

  //insertion
  Future<int> savePatient(Patient patient) async {
    var dbClient = await db;
    int res = await dbClient.insert('Patient', patient.toMap());
    return res;
  }

  //getPatient
  Future<Patient?> getPatient(String name) async {
    var dbClient = await db;
    List<Map<String, dynamic>> maps = await dbClient.query(
      'Patient',
      where: 'name = ?',
      whereArgs: [name]
    );
    if(maps.isNotEmpty){
      return Patient.fromMap(maps.first);
    }
    return null;
  }

  //deletion
  Future<int> deletePatient(Patient patient) async {
    var dbClient = await db;
    int res = await dbClient.delete(
        'Patient',
        where: 'name = ?',
        whereArgs: [patient.name]);
    return res;
  }

  //update
  Future<int> updatePatient(Patient patient) async{
    var dbClient = await db;
    return await dbClient.update(
        'Patient',
        patient.toMap(),
        where: 'name = ?',
        whereArgs: [patient.name]
        );
  }

  //getAll
  Future<List<Patient>> getAllPatient() async{
    var dbClient = await db;
    List<Map<String, dynamic>> maps = await dbClient.query(
      'Patient',
    );
    List<Patient> p = [];
    for (var element in maps) {
      p.add(Patient.fromMap(element));
    }
    return p;
  }

  //close
  Future close() async {
    var dbClient = await db;
    dbClient.close();
  }
}