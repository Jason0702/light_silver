class CompanyInfoModel{
  String name = '';
  String subName = '';
  String taxIdNumber = '';
  String phone = '';
  String address = '';
  String email = '';
  String webSite = '';
  String facebook = '';
  String line = '';
  String instagram = '';

  CompanyInfoModel({required this.name, required this.subName, required this.taxIdNumber, required this.phone,
    required this.address, required this.email, required this.webSite, required this.facebook, required this.line, required this.instagram});

  CompanyInfoModel.fromJson(Map<String, dynamic> json){
    name = json['name'] ?? '';
    subName = json['subname'];
    taxIdNumber = json['tax_id_number'];
    phone = json['phone'];
    address = json['address'];
    email = json['email'];
    webSite = json['webSite'] ?? '';
    facebook = json['facebook'] ?? '';
    line = json['line'] ?? '';
    instagram = json['instagram'] ?? '';
  }

  Map<String, dynamic> toJson()=>{
    'name':name,
    'subName':subName,
    'taxIdNumber':taxIdNumber,
    'phone':phone,
    'address':address,
    'email':email,
    'webSite':webSite,
    'facebook':facebook,
    'line':line,
    'instagram':instagram
  };
}