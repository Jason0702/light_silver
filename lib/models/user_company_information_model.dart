class UserCompanyInformationModel{
  int id = 0;
  String name = '';
  String realName = '';
  int identity = 0;
  String operationItems = '';
  String city = '';
  String area = '';
  String address = '';
  String phone = '';
  String logo = '';
  int maxMember = 0;

  UserCompanyInformationModel({required this.id, required this.name, required this.realName, required this.identity, required this.operationItems, required this.city, required this.area,required this.address,required this.logo,required this.maxMember,required this.phone});

  UserCompanyInformationModel.fromJson(Map<String, dynamic> jsonData){
    id = jsonData['id'];
    name = jsonData['name'];
    realName = jsonData['real_name'];
    operationItems = jsonData['operation_items'];
    identity = jsonData['identity'];
    city = jsonData['city'];
    area = jsonData['area'];
    address = jsonData['address'];
    phone = jsonData['phone'];
    logo = /*jsonData['logo']*/"https://jg-hp.com/apps/adminn/assets/img/brand/logo.png";
    maxMember = jsonData['max_member'];
  }

  Map<String, dynamic> toJson() => {
    'id':id,
    'name':name,
    'realName':realName,
    'identity':identity,
    'operationItems':operationItems,
    'city':city,
    'area':area,
    'address':address,
    'phone':phone,
    'logo':logo,
    'maxMember':maxMember
  };
}