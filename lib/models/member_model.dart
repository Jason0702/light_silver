import 'dart:convert';

class MemberModel{
  int id = 0;
  String name = '';
  String title = '';
  String cellphone = '';
  String email = '';
  int usersId = 0;
  int isApprove = 0;
  List<OperationItemModel> operationItems = [];

  MemberModel({required this.id, required this.name, required this.title, required this.cellphone, required this.email, required this.operationItems, required this.usersId});

  MemberModel.fromJson(Map<String, dynamic> json){
    id = json['id'];
    name = json['name'];
    title = json['title'];
    cellphone = json['cellphone'];
    email = json['email'];
    operationItems = json['operation_items'] != '' ? List.from(jsonDecode(json['operation_items'])).map((e) => OperationItemModel.fromJson(e)).toList() : [];
    usersId = json['users_id'];
    isApprove = json['is_approve'];
  }

  Map<String, dynamic> toJson() => {
    'id':id,
    'name':name,
    'title':title,
    'cellphone':cellphone,
    'email':email,
    'operationItems':operationItems,
    'usersId':usersId
  };

}
class OperationItemModel {
  String type = '';
  String name = '';
  int isLock = 0;

  OperationItemModel({required this.type, required this.name, required this.isLock});

  factory OperationItemModel.fromJson(Map<String, dynamic> json){
    return OperationItemModel(type: json['type'], name: json['name'], isLock: json['is_lock']);
  }

  Map<String, dynamic> toJson()=>{
    'type':type,
    'name':name,
    'isLock':isLock
  };
}