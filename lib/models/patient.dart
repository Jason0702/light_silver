class Patient{
  String name = '';
  String gender = '';
  int age = 0;
  String phone = '';
  String problem = '';
  int relieveSymptomsOfDiscomfort = 0;
  int postureCorrection = 0;
  int informationHealthEducation = 0;
  int jointReduction = 0;
  int coreMuscleTraining = 0;
  int healthPromotion = 0;
  int GT = 0;
  int pad = 0;
  String padHigh = '';
  int IC = 0;
  int ASIS = 0;
  int PSIS = 0;
  int flexion = 0;
  String floor = '';
  int gillet = 0;
  int Extension = 3;
  int rotation_left = 3;
  int rotation_right = 3;
  int side_left = 3;
  int side_right = 3;
  String side_knee_left = '';
  String side_knee_right = '';
  int supine_leg_length = 0;
  int adductor = 0;
  int inguinal = 0;
  int prone_leg_length = 0;
  int add_ROM = 0;
  int ext_rotator = 0;
  int PSIS_left = 0;
  int PSIS_right = 0;
  int ILA_left = 0;
  int ILA_right = 0;
  int Prop_left = 0;
  int Prop_right = 0;
  int lumbar_left = 0;
  int lumbar_right = 0;
  int supine_ilium_leg_length = 0;
  int Long_sit = 0;
  int SLR = 0;
  int Patrick = 0;
  int Knee_flexor = 0;
  int ASIS_umbilical = 0;
  int IC_recheck = 0;
  int flexion_recheck = 0;
  String floor_recheck = '';
  int gillet_recheck = 0;
  int extension_recheck = 3;
  int rotation_left_recheck = 3;
  int rotation_right_recheck = 3;
  int side_right_recheck = 3;
  int side_left_recheck = 3;
  String side_knee_right_recheck = '';
  String side_knee_left_recheck = '';

  Patient({required this.name, required this.gender, required this.age, required this.phone, required this.problem,
    required this.relieveSymptomsOfDiscomfort,
    required this.postureCorrection,
    required this.informationHealthEducation,
    required this.jointReduction,
    required this.coreMuscleTraining,
    required this.healthPromotion,
    required this.GT,
    required this.pad,
    required this.padHigh,
    required this.IC,
    required this.ASIS,
    required this.PSIS,
    required this.flexion,
    required this.floor,
    required this.gillet,
    required this.Extension,
    required this.rotation_left,
    required this.rotation_right,
    required this.side_left,
    required this.side_right,
    required this.side_knee_left,
    required this.side_knee_right,
    required this.supine_leg_length,
    required this.adductor,
    required this.inguinal,
    required this.prone_leg_length,
    required this.add_ROM,
    required this.ext_rotator,
    required this.PSIS_left,
    required this.PSIS_right,
    required this.ILA_left,
    required this.ILA_right,
    required this.Prop_left,
    required this.Prop_right,
    required this.lumbar_left,
    required this.lumbar_right,
    required this.supine_ilium_leg_length,
    required this.Long_sit,
    required this.SLR,
    required this.Patrick,
    required this.Knee_flexor,
    required this.ASIS_umbilical,
    required this.IC_recheck,
    required this.flexion_recheck,
    required this.floor_recheck,
    required this.gillet_recheck,
    required this.extension_recheck,
    required this.rotation_left_recheck,
    required this.rotation_right_recheck,
    required this.side_right_recheck,
    required this.side_left_recheck,
    required this.side_knee_right_recheck,
    required this.side_knee_left_recheck,

  });

  Patient.fromMap(Map<String, dynamic> map){
    name = map['name'];
    gender = map['gender'];
    age = map['age'];
    phone = map['phone'];
    problem = map['problem'];
    relieveSymptomsOfDiscomfort = map['relieveSymptomsOfDiscomfort'];
    postureCorrection = map['postureCorrection'];
    informationHealthEducation = map['informationHealthEducation'];
    jointReduction = map['jointReduction'];
    coreMuscleTraining = map['coreMuscleTraining'];
    healthPromotion = map['healthPromotion'];
    GT = map['GT'];
    pad = map['pad'];
    padHigh = map['padHigh'];
    IC = map['IC'];
    ASIS = map['ASIS'];
    PSIS = map['PSIS'];
    flexion = map['flexion'];
    floor = map['floor'];
    gillet = map['gillet'];
    Extension = map['extension'];
    rotation_left = map['rotation_left'];
    rotation_right = map['rotation_right'];
    side_left = map['side_left'];
    side_right = map['side_right'];
    side_knee_left = map['side_knee_left'];
    side_knee_right = map['side_knee_right'];
    supine_leg_length = map['supine_leg_length'];
    adductor = map['adductor'];
    inguinal = map['inguinal'];
    prone_leg_length = map['prone_leg_length'];
    add_ROM = map['add_ROM'];
    ext_rotator = map['ext_rotator'];
    PSIS_left = map['PSIS_left'];
    PSIS_right = map['PSIS_right'];
    ILA_left = map['ILA_left'];
    ILA_right = map['ILA_right'];
    Prop_left = map['Prop_left'];
    Prop_right = map['Prop_right'];
    lumbar_left = map['lumbar_left'];
    lumbar_right = map['lumbar_right'];
    supine_ilium_leg_length = map['supine_ilium_leg_length'];
    Long_sit = map['Long_sit'];
    SLR = map['SLR'];
    Patrick = map['Patrick'];
    Knee_flexor = map['Knee_flexor'];
    ASIS_umbilical = map['ASIS_umbilical'];
    IC_recheck = map['IC_recheck'];
    flexion_recheck = map['flexion_recheck'];
    floor_recheck = map['floor_recheck'];
    gillet_recheck = map['gillet_recheck'];
    extension_recheck = map['extension_recheck'];
    rotation_left_recheck = map['rotation_left_recheck'];
    rotation_right_recheck = map['rotation_right_recheck'];
    side_right_recheck = map['side_right_recheck'];
    side_left_recheck = map['side_left_recheck'];
    side_knee_right_recheck = map['side_knee_right_recheck'];
    side_knee_left_recheck = map['side_knee_left_recheck'];
  }

  Map<String, dynamic> toMap(){
    return {
      'name':name,
      'gender':gender,
      'age':age,
      'phone':phone,
      'problem':problem,
      'relieveSymptomsOfDiscomfort':relieveSymptomsOfDiscomfort,
      'postureCorrection':postureCorrection,
      'informationHealthEducation':informationHealthEducation,
      'jointReduction':jointReduction,
      'coreMuscleTraining':coreMuscleTraining,
      'healthPromotion':healthPromotion,
      'GT':GT,
      'pad':pad,
      'padHigh':padHigh,
      'IC':IC,
      'ASIS':ASIS,
      'PSIS':PSIS,
      'flexion':flexion,
      'floor':floor,
      'gillet':gillet,
      'Extension':Extension,
      'rotation_left':rotation_left,
      'rotation_right':rotation_right,
      'side_left':side_left,
      'side_right':side_right,
      'side_knee_left':side_knee_left,
      'side_knee_right':side_knee_right,
      'supine_leg_length':supine_leg_length,
      'adductor':adductor,
      'inguinal':inguinal,
      'prone_leg_length':prone_leg_length,
      'add_ROM':add_ROM,
      'ext_rotator':ext_rotator,
      'PSIS_left':PSIS_left,
      'PSIS_right':PSIS_right,
      'ILA_left':ILA_left,
      'ILA_right':ILA_right,
      'Prop_left':Prop_left,
      'Prop_right':Prop_right,
      'lumbar_left':lumbar_left,
      'lumbar_right':lumbar_right,
      'supine_ilium_leg_length':supine_ilium_leg_length,
      'Long_sit':Long_sit,
      'SLR':SLR,
      'Patrick':Patrick,
      'Knee_flexor':Knee_flexor,
      'ASIS_umbilical':ASIS_umbilical,
      'IC_recheck':IC_recheck,
      'flexion_recheck':flexion_recheck,
      'floor_recheck':floor_recheck,
      'gillet_recheck':gillet_recheck,
      'extension_recheck':extension_recheck,
      'rotation_left_recheck':rotation_left_recheck,
      'rotation_right_recheck':rotation_right_recheck,
      'side_right_recheck':side_right_recheck,
      'side_left_recheck':side_left_recheck,
      'side_knee_right_recheck':side_knee_right_recheck,
      'side_knee_left_recheck':side_knee_left_recheck
    };
  }

  @override
  String toString() {
    return 'Patient'
        '{name: $name, '
        'gender: $gender, '
        'age: $age, '
        'phone: $phone, '
        'problem: $problem, '
        'relieveSymptomsOfDiscomfort: $relieveSymptomsOfDiscomfort, '
        'postureCorrection: $postureCorrection, '
        'informationHealthEducation: $informationHealthEducation, '
        'jointReduction: $jointReduction, '
        'coreMuscleTraining: $coreMuscleTraining, '
        'healthPromotion: $healthPromotion, '
        'GT: $GT, '
        'pad: $pad, '
        'padHigh: $padHigh, '
        'IC: $IC, '
        'ASIS: $ASIS, '
        'PSIS: $PSIS, '
        'flexion: $flexion, '
        'floor: $floor, '
        'gillet: $gillet, '
        'Extension: $Extension, '
        'rotation_left: $rotation_left, '
        'rotation_right: $rotation_right, '
        'side_left: $side_left, '
        'side_right: $side_right, '
        'side_knee_left: $side_knee_left, '
        'side_knee_right: $side_knee_right, '
        'supine_leg_length: $supine_leg_length, '
        'adductor: $adductor, '
        'inguinal: $inguinal, '
        'prone_leg_length: $prone_leg_length, '
        'add_ROM: $add_ROM, '
        'ext_rotator: $ext_rotator, '
        'PSIS_left: $PSIS_left, '
        'PSIS_right: $PSIS_right, '
        'ILA_left: $ILA_left, '
        'ILA_right: $ILA_right, '
        'Prop_left: $Prop_left, '
        'Prop_right: $Prop_right, '
        'lumbar_left: $lumbar_left, '
        'lumbar_right: $lumbar_right, '
        'supine_ilium_leg_length: $supine_ilium_leg_length, '
        'Long_sit: $Long_sit, '
        'SLR: $SLR, '
        'Patrick: $Patrick, '
        'Knee_flexor: $Knee_flexor, '
        'ASIS_umbilical: $ASIS_umbilical, '
        'IC_recheck: $IC_recheck, '
        'flexion_recheck: $flexion_recheck, '
        'floor_recheck: $floor_recheck, '
        'gillet_recheck: $gillet_recheck, '
        'extension_recheck: $extension_recheck, '
        'rotation_left_recheck: $rotation_left_recheck, '
        'rotation_right_recheck: $rotation_right_recheck, '
        'side_right_recheck: $side_right_recheck, '
        'side_left_recheck: $side_left_recheck, '
        'side_knee_right_recheck: $side_knee_right_recheck, '
        'side_knee_left_recheck: $side_knee_left_recheck '
        '}'
    ;
  }
}