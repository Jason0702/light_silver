import 'dart:convert';

class OperationItemsModel{
  int id = 0;
  String operationItemTypesName = '';
  String name = '';
  int isFree = 0;
  int isLock = 0;
  List<String> pics = [];
  List<Map<String, dynamic>> completePics = [];
  List<Map<String, CompleteVideo>> completeVideos = [];

  OperationItemsModel({required this.id, required this.operationItemTypesName, required this.name, required this.isFree, required this.isLock, required this.pics});

  OperationItemsModel.fromJson(Map<String, dynamic> jsonData){
    id = jsonData['id'];
    operationItemTypesName = jsonData['operation_item_types_name'];
    name = jsonData['name'];
    isFree = jsonData['is_free'];
    isLock = jsonData['is_lock'];
    pics = json.decode(jsonData['pics']).cast<String>();
    completePics = (jsonData['complete_pics'].toString().isNotEmpty ? List<Map<String, dynamic>>.from(json.decode(jsonData['complete_pics'])) : null)!;
    completeVideos = (jsonData['complete_videos'].toString().isNotEmpty ? List<Map<String, CompleteVideo>>.from(json.decode(jsonData['complete_videos']).map((x) => Map.from(x).map((k, v) => MapEntry<String, CompleteVideo>(k, CompleteVideo.fromJson(v))))) : null)!;
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'operationItemTypesName': operationItemTypesName,
    'name': name,
    'isFree': isFree,
    'isLock': isLock,
    'pics': pics,
    'completePics': completePics,
    'completeVideos': completeVideos
  };
}

class CompleteVideo{
  String thumbnail = '';
  String src = '';
  
  CompleteVideo({required this.thumbnail, required this.src});
  
  factory CompleteVideo.fromJson(Map<String, dynamic> json) => CompleteVideo(
    thumbnail: json['thumbnail'],
    src: json['src']
  );
  Map<String, dynamic> toJson() => {
    'thumbnail': thumbnail,
    'src': src
  };
}