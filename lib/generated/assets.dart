///This file is automatically generated. DO NOT EDIT, all your changes would be lost.
class Assets {
  Assets._();

  static const String assetsFonts = 'assets/fonts.zip';
  static const String databaseDatabaseHelper = 'lib/database/database_helper.dart';
  static const String dioHttpAppExceptions = 'lib/dio_http/app_exceptions.dart';
  static const String dioHttpHttpRequest = 'lib/dio_http/http_request.dart';
  static const String enumLanguage = 'lib/enum/language.dart';
  static const String enumSound = 'lib/enum/sound.dart';
  static const String fontsMsjh = 'assets/fonts/msjh.ttf';
  static const String generatedAssets = 'lib/generated/assets.dart';
  static const String i18nEnUS = 'assets/i18n/en_US.json';
  static const String i18nJaJP = 'assets/i18n/ja_JP.json';
  static const String i18nZhCN = 'assets/i18n/zh_CN.json';
  static const String i18nZhTW = 'assets/i18n/zh_TW.json';
  static const String images34 = 'assets/images/3-4.png';
  static const String images6back = 'assets/images/6back.png';
  static const String imagesBlankFile = 'assets/images/blank_file.png';
  static const String imagesBody = 'assets/images/body.png';
  static const String imagesChina = 'assets/images/china.png';
  static const String imagesCompany = 'assets/images/company.png';
  static const String imagesContactUs = 'assets/images/contact_us.png';
  static const String imagesDrawerBg = 'assets/images/drawer_bg.png';
  static const String imagesEdit = 'assets/images/edit.png';
  static const String imagesEllipse = 'assets/images/ellipse.png';
  static const String imagesEnglish = 'assets/images/english.png';
  static const String imagesFb = 'assets/images/fb.png';
  static const String imagesFile = 'assets/images/file.png';
  static const String imagesForm = 'assets/images/form.png';
  static const String imagesForward = 'assets/images/forward.png';
  static const String imagesGenuineAvatar = 'assets/images/genuine_avatar.png';
  static const String imagesGppGood = 'assets/images/gpp_good.png';
  static const String imagesHomeBg = 'assets/images/home_bg.png';
  static const String imagesImgZoomIn = 'assets/images/img_zoom_in.png';
  static const String imagesInstagram = 'assets/images/instagram.png';
  static const String imagesJapan = 'assets/images/japan.png';
  static const String imagesLaunchImage = 'assets/images/launch_image.jpg';
  static const String imagesLine = 'assets/images/line.png';
  static const String imagesLock = 'assets/images/lock.png';
  static const String imagesLogo = 'assets/images/logo.png';
  static const String imagesLogoNameBlue = 'assets/images/logo_name_blue.png';
  static const String imagesLogoNameWhite = 'assets/images/logo_name_white.png';
  static const String imagesLogout = 'assets/images/logout.png';
  static const String imagesMenu = 'assets/images/menu.png';
  static const String imagesOutputPdf = 'assets/images/output_pdf.png';
  static const String imagesPain = 'assets/images/pain.png';
  static const String imagesPainLevel = 'assets/images/pain_level.png';
  static const String imagesPeople = 'assets/images/people.png';
  static const String imagesPin = 'assets/images/pin.png';
  static const String imagesPreview = 'assets/images/preview.png';
  static const String imagesRecord = 'assets/images/record.png';
  static const String imagesRecordSheetTitle = 'assets/images/record_sheet_title.png';
  static const String imagesResultBlue = 'assets/images/result_blue.png';
  static const String imagesResultGreen = 'assets/images/result_green.png';
  static const String imagesSave = 'assets/images/save.png';
  static const String imagesShareFile = 'assets/images/share_file.png';
  static const String imagesStepImg = 'assets/images/step_img.png';
  static const String imagesTaiwan = 'assets/images/taiwan.png';
  static const String imagesTips = 'assets/images/tips.png';
  static const String imagesTrialAvatar = 'assets/images/trial_avatar.png';
  static const String imagesUrl = 'assets/images/url.png';
  static const String interceptorCache = 'lib/dio_http/interceptor/cache.dart';
  static const String interceptorError = 'lib/dio_http/interceptor/error.dart';
  static const String interceptorRequest = 'lib/dio_http/interceptor/request.dart';
  static const String interceptorRetry = 'lib/dio_http/interceptor/retry.dart';
  static const String languageLanguageConstants = 'lib/language/language_constants.dart';
  static const String languageLocalization = 'lib/language/localization.dart';
  static const String lbkLbk01Routes = 'lib/page/topic/lbk/lbk01_routes.dart';
  static const String lbkLbk02Routes = 'lib/page/topic/lbk/lbk02_routes.dart';
  static const String lbkLbk03Routes = 'lib/page/topic/lbk/lbk03_routes.dart';
  static const String lbkLbk04Routes = 'lib/page/topic/lbk/lbk04_routes.dart';
  static const String lbkLbk05Routes = 'lib/page/topic/lbk/lbk05_routes.dart';
  static const String lbkLbk11Routes = 'lib/page/topic/lbk/lbk11_routes.dart';
  static const String lbkLbk12Routes = 'lib/page/topic/lbk/lbk12_routes.dart';
  static const String lbkLbk13Routes = 'lib/page/topic/lbk/lbk13_routes.dart';
  static const String lbkLbk14Routes = 'lib/page/topic/lbk/lbk14_routes.dart';
  static const String lbkLbk15Routes = 'lib/page/topic/lbk/lbk15_routes.dart';
  static const String lbkLbk160Routes = 'lib/page/topic/lbk/lbk160_routes.dart';
  static const String lbkLbk16Routes = 'lib/page/topic/lbk/lbk16_routes.dart';
  static const String lbkLbk17Routes = 'lib/page/topic/lbk/lbk17_routes.dart';
  static const String ldLd01Routes = 'lib/page/topic/ld/ld01_routes.dart';
  static const String libApp = 'lib/app.dart';
  static const String libMain = 'lib/main.dart';
  static const String loginRegisterTitle = 'lib/widgets/login_register/title.dart';
  static const String modelErrorModel = 'lib/dio_http/model/error_model.dart';
  static const String modelsCompanyInfoModel = 'lib/models/company_info_model.dart';
  static const String modelsMemberModel = 'lib/models/member_model.dart';
  static const String modelsOperationItemModel = 'lib/models/operation_item_model.dart';
  static const String modelsPatient = 'lib/models/patient.dart';
  static const String modelsUserCompanyInformationModel = 'lib/models/user_company_information_model.dart';
  static const String newModelCarouselModel = 'lib/new_model/carousel_model.dart';
  static const String newModelCompanyInfo = 'lib/new_model/company_info.dart';
  static const String newModelGeshtStatus = 'lib/new_model/gesht_status.dart';
  static const String newModelMaintenanceTimeModel = 'lib/new_model/maintenance_time_model.dart';
  static const String newModelNoticeModel = 'lib/new_model/notice_model.dart';
  static const String newModelPaymantType = 'lib/new_model/paymant_type.dart';
  static const String newModelRepair = 'lib/new_model/repair.dart';
  static const String pageBasicSettingsPage = 'lib/page/basic_settings_page.dart';
  static const String pageChangePasswordPage = 'lib/page/change_password_page.dart';
  static const String pageContactUsRoutes = 'lib/page/contact_us_routes.dart';
  static const String pageForgotPasswordRoutes = 'lib/page/forgot_password_routes.dart';
  static const String pageHomeRoutes = 'lib/page/home_routes.dart';
  static const String pageLoginOrRegisterRoutes = 'lib/page/login_or_register_routes.dart';
  static const String pageLoginPage = 'lib/page/login_page.dart';
  static const String pageMedicalHistoryRoutes = 'lib/page/medical_history_routes.dart';
  static const String pageMemberCentreRoutes = 'lib/page/member_centre_routes.dart';
  static const String pageNetworkImagePage = 'lib/page/network_image_page.dart';
  static const String pagePersonalInformationModificationPage = 'lib/page/personal_information_modification_page.dart';
  static const String pagePreviewSettingsPage = 'lib/page/preview_settings_page.dart';
  static const String pageRecordRoutes = 'lib/page/record_routes.dart';
  static const String pageRegisterPage = 'lib/page/register_page.dart';
  static const String pageSetUpRoutes = 'lib/page/set_up_routes.dart';
  static const String pageWelcomeRouter = 'lib/page/welcome_router.dart';
  static const String pvPv01 = 'lib/page/pv/pv01.dart';
  static const String pvPv02 = 'lib/page/pv/pv02.dart';
  static const String pvPv03 = 'lib/page/pv/pv03.dart';
  static const String pvPv04 = 'lib/page/pv/pv04.dart';
  static const String pvPvpdf = 'lib/page/pv/pvpdf.dart';
  static const String routesDelegate = 'lib/routes/delegate.dart';
  static const String routesParser = 'lib/routes/parser.dart';
  static const String routesRouteName = 'lib/routes/route_name.dart';
  static const String setUpTitle = 'lib/widgets/set_up/title.dart';
  static const String speSpe01Routes = 'lib/page/topic/spe/spe01_routes.dart';
  static const String speSpe02Routes = 'lib/page/topic/spe/spe02_routes.dart';
  static const String speSpe03Routes = 'lib/page/topic/spe/spe03_routes.dart';
  static const String speSpe04Routes = 'lib/page/topic/spe/spe04_routes.dart';
  static const String speSpe05Routes = 'lib/page/topic/spe/spe05_routes.dart';
  static const String speSpe06Routes = 'lib/page/topic/spe/spe06_routes.dart';
  static const String speSpe07Routes = 'lib/page/topic/spe/spe07_routes.dart';
  static const String speSpe08Routes = 'lib/page/topic/spe/spe08_routes.dart';
  static const String speSpe09Routes = 'lib/page/topic/spe/spe09_routes.dart';
  static const String speSpe10Routes = 'lib/page/topic/spe/spe10_routes.dart';
  static const String speSpe11Routes = 'lib/page/topic/spe/spe11_routes.dart';
  static const String speSpe5and6Routes = 'lib/page/topic/spe/spe5and6_routes.dart';
  static const String speSpe91Routes = 'lib/page/topic/spe/spe91_routes.dart';
  static const String speSpe92Routes = 'lib/page/topic/spe/spe92_routes.dart';
  static const String speSpe92and93Routes = 'lib/page/topic/spe/spe92and93_routes.dart';
  static const String speSpe93Routes = 'lib/page/topic/spe/spe93_routes.dart';
  static const String speSpe94Routes = 'lib/page/topic/spe/spe94_routes.dart';
  static const String speSpe95Routes = 'lib/page/topic/spe/spe95_routes.dart';
  static const String speSpe96Routes = 'lib/page/topic/spe/spe96_routes.dart';
  static const String spsSps01Routes = 'lib/page/topic/sps/sps01_routes.dart';
  static const String spsSps02Routes = 'lib/page/topic/sps/sps02_routes.dart';
  static const String spsSps03Routes = 'lib/page/topic/sps/sps03_routes.dart';
  static const String spsSps04Routes = 'lib/page/topic/sps/sps04_routes.dart';
  static const String spsSps05Routes = 'lib/page/topic/sps/sps05_routes.dart';
  static const String spsSps06Routes = 'lib/page/topic/sps/sps06_routes.dart';
  static const String spsSps41Routes = 'lib/page/topic/sps/sps41_routes.dart';
  static const String topicVideoPalyerScreen = 'lib/page/topic/video_palyer_screen.dart';
  static const String utilsApi = 'lib/utils/api.dart';
  static const String utilsAppImage = 'lib/utils/app_image.dart';
  static const String utilsLogUtil = 'lib/utils/log_util.dart';
  static const String utilsSharedPreference = 'lib/dio_http/utils/shared_preference.dart';
  static const String utilsSharedPreferenceUtil = 'lib/utils/shared_preference_util.dart';
  static const String utilsUtils = 'lib/utils/utils.dart';
  static const String viewModelAppConfigVm = 'lib/view_model/app_config_vm.dart';
  static const String viewModelAuthVm = 'lib/view_model/auth_vm.dart';
  static const String viewModelCompanyInfoVm = 'lib/view_model/company_info_vm.dart';
  static const String viewModelLoginRegisterVm = 'lib/view_model/login_register_vm.dart';
  static const String viewModelMaintenanceTimeVm = 'lib/view_model/maintenance_time_vm.dart';
  static const String viewModelMemberVm = 'lib/view_model/member_vm.dart';
  static const String viewModelNoticeVm = 'lib/view_model/notice_vm.dart';
  static const String viewModelOperationItemVm = 'lib/view_model/operation_item_vm.dart';
  static const String viewModelPatientVm = 'lib/view_model/patient_vm.dart';
  static const String viewModelSatisfactionQuestionnaireVm = 'lib/view_model/satisfaction_questionnaire_vm.dart';
  static const String viewModelSetUpVm = 'lib/view_model/set_up_vm.dart';
  static const String viewModelUserCompanyInformationVm = 'lib/view_model/user_company_information_vm.dart';
  static const String viewModelUtilApiVm = 'lib/view_model/util_api_vm.dart';
  static const String widgetsButton = 'lib/widgets/button.dart';
  static const String widgetsHeroPhotoViewWrapper = 'lib/widgets/hero_photo_view_wrapper.dart';
  static const String widgetsLeftButton = 'lib/widgets/left_button.dart';
  static const String widgetsNoShadowScrollbehavior = 'lib/widgets/noShadowScrollbehavior.dart';
  static const String widgetsTitleWidget = 'lib/widgets/title_widget.dart';
  static const String widgetsWebviewWidget = 'lib/widgets/webview_widget.dart';

}
