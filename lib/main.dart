import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:light_silver/language/language_constants.dart';
import 'package:light_silver/routes/parser.dart';
import 'package:light_silver/routes/route_name.dart';
import 'package:light_silver/utils/api.dart';
import 'package:light_silver/view_model/app_config_vm.dart';
import 'package:light_silver/view_model/auth_vm.dart';
import 'package:light_silver/view_model/company_info_vm.dart';
import 'package:light_silver/view_model/login_register_vm.dart';
import 'package:light_silver/view_model/maintenance_time_vm.dart';
import 'package:light_silver/view_model/member_vm.dart';
import 'package:light_silver/view_model/notice_vm.dart';
import 'package:light_silver/view_model/operation_item_vm.dart';
import 'package:light_silver/view_model/patient_vm.dart';
import 'package:light_silver/view_model/satisfaction_questionnaire_vm.dart';
import 'package:light_silver/view_model/set_up_vm.dart';
import 'package:light_silver/view_model/user_company_information_vm.dart';
import 'package:light_silver/view_model/util_api_vm.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
import 'package:responsive_framework/responsive_wrapper.dart';

import 'app.dart';
import 'language/localization.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(MyApp());
  });
}

class MyApp extends StatefulWidget {
  MyApp({Key? key}):super(key: key){
    //跳轉
    delegate.replace(name: RouteName.home);
  }

  static void setLocale(BuildContext context, Locale locale){
    _MyAppState? state = context.findAncestorStateOfType<_MyAppState>();
    state!.setLocale(locale);
  }

  @override
  State createState() => _MyAppState();
}

class _MyAppState extends State<MyApp>{
  Locale? _locale;
  final List<SingleChildWidget> _providers = [];

  setLocale(Locale locale){
    setState(() {
      _locale = locale;
    });
  }

  @override
  void initState() {
    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    /*HttpManager().init(
      baseUrl: api,
      interceptors: [
        //LogInterceptor(),
      ]
    );*/
    initStore();
    initProviders();
  }

  void initStore() {
    //初始化
    httpUtils.init(baseUrl: api);
  }

  @override
  void didChangeDependencies() {
    getLocale().then((locale) {
      setState(() {
        _locale = locale;
      });
    });
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: _providers,
        child: MaterialApp.router(
          routeInformationParser: const LSRouteInformationParser(),
          routerDelegate: delegate,
        locale: _locale,
        localizationsDelegates: const [
          Localization.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        supportedLocales: const [
          Locale('zh', 'TW'),
          Locale('zh', 'CN'),
          Locale('en', 'US'),
          Locale('ja', 'JP')
        ],
        localeResolutionCallback: (locale, supportedLocales){
          for(var supportedLocale in supportedLocales){
            if(supportedLocale.languageCode == locale!.languageCode){
              return supportedLocale;
            }
          }
          return supportedLocales.first;
        },
        debugShowCheckedModeBanner: false,
        title: '光銀式',
        builder: (context, child){
          child = EasyLoading.init()(context, child);
          return ResponsiveWrapper.builder(child,
              maxWidth: 1200,
              minWidth: 480,
              defaultScale: true,
              breakpoints: [
                const ResponsiveBreakpoint.resize(100, name: MOBILE),
                const ResponsiveBreakpoint.autoScale(900, name: TABLET),
                const ResponsiveBreakpoint.autoScale(1200, name: DESKTOP),
                const ResponsiveBreakpoint.resize(2460, name: "4K"),
              ],
              background: Container(color: const Color(0xFFF5F5F5)));
        },
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  void initProviders(){
    _providers.add(ChangeNotifierProvider(create: (_) => AuthVm()));
    _providers.add(ChangeNotifierProvider(create: (_) => AppConfigVm()));
    _providers.add(ChangeNotifierProvider(create: (_) => NoticeVm()));
    _providers.add(ChangeNotifierProvider(create: (_) => MaintenanceTimeVm()));
    _providers.add(ChangeNotifierProvider(create: (_) => SatisfactionQuestionnaireVm()));
    _providers.add(ChangeNotifierProvider(create: (_) => UtilApiVm()));
    _providers.add(ChangeNotifierProvider(create: (_) => SetUpVm()));
    _providers.add(ChangeNotifierProvider(create: (_) => LoginRegisterVm()));
    _providers.add(ChangeNotifierProvider(create: (_) => PatientVm()));
    _providers.add(ChangeNotifierProvider(create: (_) => MemberVm()));
    _providers.add(ChangeNotifierProvider(create: (_) => CompanyInfoVm()));
    _providers.add(ChangeNotifierProvider(create: (_) => UserCompanyInformationVm()));
    _providers.add(ChangeNotifierProvider(create: (_) => OperationItemVm()));
  }
}