import 'package:flutter/foundation.dart';

class AuthVm with ChangeNotifier{

  //是否登入
  bool _isLogin = false;
  bool get isLogin => _isLogin;

  int? id;
  String? name;
  String? account;
  String? start;
  String? end;

  void setAuth(dynamic model){
    id = model.id;
    name = model.name;
    account = model.account;
    start = model.start;
    end = model.end;
  }

  void setLogin(bool value){
    _isLogin = value;
    notifyListeners();
  }
}