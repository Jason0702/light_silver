import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';

import '../app.dart';
import '../models/company_info_model.dart';
import '../new_model/carousel_model.dart';
import '../new_model/gesht_status.dart';
import '../new_model/paymant_type.dart';
import '../new_model/repair.dart';
import '../utils/api.dart';
import '../utils/shared_preference_util.dart';

class UtilApiVm with ChangeNotifier {
  final SharedPreferenceUtil _sharedPreferenceUtil = SharedPreferenceUtil();

  ///輪播圖
  List<CarouselModel> _carouseList = [];
  List<CarouselModel> get carouseList => _carouseList;

  ///維修項目
  List<RepairTypeModel> _repairTypeList = [];
  List<RepairTypeModel> get repairTypeList => _repairTypeList;

  ///維修位置
  List<RepairPlaceModel> _repairPlaceList = [];
  List<RepairPlaceModel> get repairPlaceList => _repairPlaceList;

  ///維修樓層
  List<RepairFloorModel> _repairFloorList = [];
  List<RepairFloorModel> get repairFloorList => _repairFloorList;

  ///要上傳的內容
  List<RepairRecordItem> _repairRecordItemList = [];
  List<RepairRecordItem> get repairRecordItemList => _repairRecordItemList;

  ///全部記錄
  List<RepairRecordModel> _repairRecordList = [];

  ///進行中
  List<RepairRecordModel> _processingRecord = [];
  List<RepairRecordModel> get processingRecord => _processingRecord;

  ///已完成
  List<RepairRecordModel> _completedRecord = [];
  List<RepairRecordModel> get completedRecord => _completedRecord;

  ///公司資料
  CompanyInfoModel? _companyInfo;
  CompanyInfoModel get companyInfo => _companyInfo!;

  bool _isGuset = false;
  bool get isGuest => _isGuset;

  ///取得輪播
  Future<void> getCarousel() async {
    try {
      dynamic response = await httpUtils.get(
        Api.carousel,
        options: Options(contentType: 'application/json', headers: {
          "authorization":
          "Bearer " + await _sharedPreferenceUtil.getAccessToken()
        }),
      );
      log('取得輪播: ${response['data']}');
      List _list = json.decode(json.encode(response['data']));
      _carouseList = _list.map((e) => CarouselModel.fromJson(e)).toList();
      _carouseList.sort((left, right) => left.sort!.compareTo(right.sort!));
      notifyListeners();
    } on DioError catch (e) {
      log('取得輪播 Error: ${e.message}');
    }
  }

  ///取得維修項目
  Future<void> getRepairType() async {
    try {
      dynamic response = await httpUtils.get(Api.repairType,
          options: Options(contentType: 'application/json', headers: {
            "authorization":
            "Bearer " + await _sharedPreferenceUtil.getAccessToken()
          }));
      log('取得維修項目: ${response['data']}');
      List _list = json.decode(json.encode(response['data']));
      _repairTypeList = _list.map((e) => RepairTypeModel.fromJson(e)).toList();
      _repairTypeList.sort((left, right) => left.sort!.compareTo(right.sort!));
    } on DioError catch (e) {
      log('取得維修項目 Error: ${e.message}');
    }
  }

  ///取得維修位置
  Future<void> getRepairPlace({required int buildId}) async {
    try {
      dynamic response = await httpUtils.get(
          '${Api.repairPlace}/byBuildId/$buildId',
          options: Options(contentType: 'application/json', headers: {
            "authorization":
            "Bearer " + await _sharedPreferenceUtil.getAccessToken()
          }));
      log('取得維修位置: $response');
      List _list = json.decode(json.encode(response));
      _repairPlaceList =
          _list.map((e) => RepairPlaceModel.fromJson(e)).toList();
      _repairPlaceList.sort((left, right) => left.sort!.compareTo(right.sort!));
    } on DioError catch (e) {
      log('取得維修位置 Error: ${e.message}');
    }
  }

  ///取得維修樓層
  Future<void> getRepairFloor({required int buildId}) async {
    try {
      dynamic response = await httpUtils.get(
          '${Api.repairFloor}/byBuildId/$buildId',
          options: Options(contentType: 'application/json', headers: {
            "authorization":
            "Bearer " + await _sharedPreferenceUtil.getAccessToken()
          }));
      log('取得維修樓層: $response');
      List _list = json.decode(json.encode(response));
      _repairFloorList =
          _list.map((e) => RepairFloorModel.fromJson(e)).toList();
      _repairFloorList.sort((left, right) => left.sort!.compareTo(right.sort!));
    } on DioError catch (e) {
      log('取得維修樓層 Error: ${e.message}');
    }
  }

  ///報修紀錄檔案上傳
  /// * [file] 檔案
  Future<String?> postRepairRecordImage({required File file}) async {
    String _path = file.path;
    String _fileName = '';

    FormData data = FormData.fromMap({
      "file": await MultipartFile.fromFile(_path, filename: _fileName)
          .catchError((e) {
        debugPrint(e);
      })
    });

    try {
      dynamic response = await httpUtils.post('${Api.repairRecord}/upload',
          data: data,
          options: Options(contentType: 'application/json', headers: {
            "authorization":
            "Bearer " + await _sharedPreferenceUtil.getAccessToken()
          }));
      log('報修紀錄檔案上傳: $response');
      return response['message'];
    } on DioError catch (e) {
      log('報修紀錄檔案上傳 Error: ${e.message}');
      return null;
    }
  }

  ///判斷是否能新增
  /// * [item] RepairRecordItem
  bool judgeAddRepairRecordItem({required RepairRecordItem item}){
    if(_repairRecordItemList.where((element) => element.repairTypeName == item.repairTypeName).isNotEmpty){
      return false;
    }
    return true;
  }

  ///新增維修資料
  /// * [item] RepairRecordItem
  void addRepairRecordItem({required RepairRecordItem item}) {
    _repairRecordItemList.add(item);
    notifyListeners();
  }

  ///清除特定項目
  /// * [item] RepairRecordItem
  void deleteRepairRecordItem({required RepairRecordItem item}){
    if(_repairRecordItemList.where((element) => element.repairTypeName == item.repairTypeName).isNotEmpty){
      _repairRecordItemList.remove(_repairRecordItemList.firstWhere((element) => element.repairTypeName == item.repairTypeName));
    }
    _repairRecordItemList.add(item);
    notifyListeners();
  }

  ///全部清除
  void deleteAllRepairRecordItem() {
    _repairRecordItemList.clear();
    notifyListeners();
  }

  ///新增報修紀錄
  /// * [model] AddRepairRecordDTO
  Future<bool> addRepairRecord({required AddRepairRecordDTO model}) async {
    bool _result = false;
    try {
      dynamic response = await httpUtils.post(Api.repairRecord,
          data: model.toJson(),
          options: Options(contentType: 'application/json', headers: {
            "authorization":
            "Bearer " + await _sharedPreferenceUtil.getAccessToken()
          }));
      log('新增報修紀錄: $response');
      _result = true;
    } on DioError catch (e) {
      log('新增報修紀錄 Error: ${e.message}');
      _result = false;
    }
    return _result;
  }

  ///取得報修紀錄
  /// * [id] MemberId
  Future<void> getRepairRecord({required int id}) async {
    try {
      dynamic response = await httpUtils.get(
          '${Api.repairRecord}/byMemberId/$id',
          options: Options(contentType: 'application/json', headers: {
            "authorization":
            "Bearer " + await _sharedPreferenceUtil.getAccessToken()
          }));
      log('取得報修紀錄: $response');
      List _list = json.decode(json.encode(response));
      _repairRecordList =
          _list.map((e) => RepairRecordModel.fromJson(e)).toList();
      _repairRecordList
          .sort((left, right) => right.createdAt!.compareTo(left.createdAt!));
      //進行中
      _processingRecord = List.from(
          _repairRecordList.where((element) => element.status != 8).toList());
      //已完成
      _completedRecord = List.from(
          _repairRecordList.where((element) => element.status == 8).toList());
      notifyListeners();
    } on DioError catch (e) {
      log('取得報修紀錄 Error: ${e.message}');
    }
  }

  ///報修記錄更新滿意度評分
  /// * [model] RepairReviewDTO
  Future<bool> postRepairReview({required RepairReviewDTO model}) async {
    bool _result = false;
    try {
      dynamic response = await httpUtils.post('${Api.repairRecord}/review',
          data: model.toJson(),
          options: Options(contentType: 'application/json', headers: {
            "authorization":
            "Bearer " + await _sharedPreferenceUtil.getAccessToken()
          }));
      log('報修記錄更新滿意度評分: $response');
      _result = true;
    } on DioError catch (e) {
      log('報修記錄更新滿意度評分 Error: ${e.message}');
      _result = false;
    }
    return _result;
  }

  ///報修記錄聲明書簽核
  /// * [id] ID
  /// * [file] 簽核文件
  Future<bool> postRepairStatementSign(
      {required int id, required File file}) async {
    bool _result = false;
    String _path = file.path;

    FormData data = FormData.fromMap({
      "id": id,
      "file":
      await MultipartFile.fromFile(_path, filename: _path).catchError((e) {
        debugPrint(e);
      })
    });
    try {
      dynamic response = await httpUtils.post(
          '${Api.repairRecord}/statement-sign',
          data: data,
          options: Options(contentType: 'application/json', headers: {
            "authorization":
            "Bearer " + await _sharedPreferenceUtil.getAccessToken()
          }));
      log('報修記錄聲明書簽核: $response');
      _result = true;
    } on DioError catch (e) {
      log('報修記錄聲明書簽核 Error: ${e.message}');
      _result = false;
    }
    return _result;
  }

  //報修記錄報價單簽核
  /// * [id] ID
  /// * [file] 簽核文件
  /// * [type] 付款狀態
  Future<bool> postRepairQuotationSign(
      {required int id, required File file, required PaymentType type}) async {
    bool _result = false;
    String _path = file.path;

    FormData data = FormData.fromMap({
      "id": id,
      "file":
      await MultipartFile.fromFile(_path, filename: _path).catchError((e) {
        debugPrint(e);
      }),
      "payment_type": type.index
    });
    try {
      dynamic response = await httpUtils.post(
          '${Api.repairRecord}/quotation-sign',
          data: data,
          options: Options(contentType: 'application/json', headers: {
            "authorization":
            "Bearer " + await _sharedPreferenceUtil.getAccessToken()
          }));
      log('報修記錄報價單簽核: $response');
      _result = true;
    } on DioError catch (e) {
      log('報修記錄報價單簽核 Error: ${e.message}');
      _result = false;
    }
    return _result;
  }

  ///公司資料
  Future<void> getCompanyInfo() async {
    try {
      dynamic response = await httpUtils.get(Api.companyInfo,
          options: Options(contentType: 'application/json', headers: {
            "authorization":
            "Bearer " + await _sharedPreferenceUtil.getAccessToken()
          }));
      log('公司資料: $response');
      _companyInfo = CompanyInfoModel.fromJson(json.decode(json.encode(response['data'])));
    } on DioError catch (e) {
      log('公司資料 Error: ${e.message}');
    }
  }
  ///訪客登入
  Future<bool> getGuestStatus() async {
    try{
      dynamic response = await httpUtils.get(
          Api.guestStatus,
          options: Options(contentType: 'application/json')
      );
      GuestStatus _guest = GuestStatus.fromJson(json.decode(json.encode(response)));
      _isGuset = _guest.guestStatus == 1 ? true : false;
      notifyListeners();
      return true;
    } on DioError catch (e){
      return false;
    }
  }
}
