import 'package:flutter/cupertino.dart';

import '../new_model/maintenance_time_model.dart';

class MaintenanceTimeVm extends ChangeNotifier{
  ///時段
  List<MaintenanceTimeModel> _maintenanceTimeList = [];
  List<MaintenanceTimeModel> get maintenanceTimeList => _maintenanceTimeList;

  ///初始化
  void maintenanceTimeInit(){
    //星期一 上午
    MaintenanceTimeModel _onMondayMorning = MaintenanceTimeModel(id: 0, period: '星期一 上午 (09:00~12:00)', isCheck: false);
    //星期一 下午
    MaintenanceTimeModel _onMondayAfternoon = MaintenanceTimeModel(id: 1, period: '星期一 下午 (13:00~17:00)', isCheck: false);
    //星期二 上午
    MaintenanceTimeModel _onTuesdayMorning = MaintenanceTimeModel(id: 2, period: '星期二 上午 (09:00~12:00)', isCheck: false);
    //星期二 下午
    MaintenanceTimeModel _onTuesdayAfternoon = MaintenanceTimeModel(id: 3, period: '星期二 下午 (13:00~17:00)', isCheck: false);
    //星期三 上午
    MaintenanceTimeModel _onWednesdayMorning = MaintenanceTimeModel(id: 4, period: '星期三 上午 (09:00~12:00)', isCheck: false);
    //星期三 下午
    MaintenanceTimeModel _onWednesdayAfternoon = MaintenanceTimeModel(id: 5, period: '星期三 下午 (13:00~17:00)', isCheck: false);
    //星期四 上午
    MaintenanceTimeModel _onThursdayMorning = MaintenanceTimeModel(id: 6, period: '星期四 上午 (09:00~12:00)', isCheck: false);
    //星期四 下午
    MaintenanceTimeModel _onThursdayAfternoon = MaintenanceTimeModel(id: 7, period: '星期四 下午 (13:00~17:00)', isCheck: false);
    //星期五 上午
    MaintenanceTimeModel _onFridayMorning = MaintenanceTimeModel(id: 8, period: '星期五 上午 (09:00~12:00)', isCheck: false);
    //星期五 下午
    MaintenanceTimeModel _onFridayAfternoon = MaintenanceTimeModel(id: 9, period: '星期五 下午 (13:00~17:00)', isCheck: false);
    //星期六 上午
    MaintenanceTimeModel _onSaturdayMorning = MaintenanceTimeModel(id: 10, period: '星期六 上午 (09:00~12:00)', isCheck: false);
    //星期六 下午
    MaintenanceTimeModel _onSaturdayAfternoon = MaintenanceTimeModel(id: 11, period: '星期六 下午 (13:00~17:00)', isCheck: false);

    _maintenanceTimeList.add(_onMondayMorning);
    _maintenanceTimeList.add(_onMondayAfternoon);
    _maintenanceTimeList.add(_onTuesdayMorning);
    _maintenanceTimeList.add(_onTuesdayAfternoon);
    _maintenanceTimeList.add(_onWednesdayMorning);
    _maintenanceTimeList.add(_onWednesdayAfternoon);
    _maintenanceTimeList.add(_onThursdayMorning);
    _maintenanceTimeList.add(_onThursdayAfternoon);
    _maintenanceTimeList.add(_onFridayMorning);
    _maintenanceTimeList.add(_onFridayAfternoon);
    _maintenanceTimeList.add(_onSaturdayMorning);
    _maintenanceTimeList.add(_onSaturdayAfternoon);
    notifyListeners();
  }
  ///設定
  /// * [id] item ID
  /// * [value] 開關
  bool setMaintenanceTime({required int id, required bool value}){
    //如果要勾選
    if(value){
      //判斷是否大於三個
      if(_maintenanceTimeList.where((element) => element.isCheck == true).toList().length <= 2){
        _maintenanceTimeList.firstWhere((element) => element.id == id).isCheck = value;
        notifyListeners();
        return true;
      }else{
        return false;
      }
    }else{
      //要取消勾選
      _maintenanceTimeList.firstWhere((element) => element.id == id).isCheck = value;
      notifyListeners();
      return true;
    }
  }
}