import 'package:flutter/cupertino.dart';

class SetUpVm with ChangeNotifier{
  String _title = '';
  String get title => _title;

  void setTitle(String value){
    _title = value;
    notifyListeners();
  }
}