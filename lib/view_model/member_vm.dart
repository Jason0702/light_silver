import 'package:flutter/material.dart';
import 'package:light_silver/models/member_model.dart';

class MemberVm with ChangeNotifier{
  MemberModel? _memberModel;
  MemberModel? get memberModel => _memberModel;

  //初始化
  void initMemberModel(){
    _memberModel = MemberModel(
        operationItems: [],
        name: '',
        email: '',
        usersId: 0,
        id: 0,
        title: '',
        cellphone: '');
    notifyListeners();
  }

  //設定會員
  void setMemberModel(MemberModel model){
    _memberModel = model;
    notifyListeners();
  }
}