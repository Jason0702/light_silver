import 'dart:convert';
import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';

import '../app.dart';
import '../new_model/notice_model.dart';
import '../utils/api.dart';
import '../utils/shared_preference_util.dart';

class NoticeVm with ChangeNotifier{
  final SharedPreferenceUtil _sharedPreferenceUtil = SharedPreferenceUtil();

  int _unreadCount = 0;
  int get unreadCount => _unreadCount;

  ///取得推播紀錄
  /// * [id] 用戶ID
  Future<List<NoticeModel>> getNotice({required int id}) async {
    try{
      dynamic response = await httpUtils.get(
        '${Api.notification}/byMemberId/$id',
        options: Options(contentType: 'application/json', headers: {
          "authorization":
          "Bearer " + await _sharedPreferenceUtil.getAccessToken()
        }),
      );
      log('取得推播紀錄: ${response['data']}');
      List _list = json.decode(json.encode(response['data']));
      if(_list.isNotEmpty){
        List<NoticeModel> _model = _list.map((e) => NoticeModel.fromJson(e)).toList();
        _model.sort((left, right) => right.createdAt!.compareTo(left.createdAt!));
        return _model;
      }
      return [];
    } on DioError catch(e){
      log('取得推播紀錄 Error: ${e.message}');
      return [];
    } on Error catch(e){
      log('e: ${e.toString()}');
      return [];
    }
  }

  ///取得推播通知未讀數
  /// * [id] 用戶ID
  Future<void> getNotificationUnreadCount(int id) async {
    try{
      dynamic response = await httpUtils.get(
        '${Api.notificationUnreadCount}/byMemberId/$id',
        options: Options(contentType: 'application/json', headers: {
          "authorization":
          "Bearer " + await _sharedPreferenceUtil.getAccessToken()
        }),
      );
      log('取得推播通知未讀數: ${response['data']}');
      _unreadCount = response['data'];
      notifyListeners();
    } on DioError catch(e){
      log('取得推播通知未讀數 Error: ${e.message}');
    } on Error catch(e){
      log('e: ${e.toString()}');
    }
  }

  ///已讀所有推播通知
  /// * [id] 用戶ID
  Future<void> putNotificationReadAll(int id) async {
    try{
      dynamic response = await httpUtils.put(
        '${Api.notificationReadAll}/byMemberId/$id',
        options: Options(contentType: 'application/json', headers: {
          "authorization":
          "Bearer " + await _sharedPreferenceUtil.getAccessToken()
        }),
      );
      log('已讀所有推播通知: ${response['data']}');
      _unreadCount = 0;
      notifyListeners();
    } on DioError catch(e){
      log('已讀所有推播通知 Error: ${e.message}');
    } on Error catch(e){
      log('e: ${e.toString()}');
    }
  }
}