import 'dart:developer';

import 'package:flutter/cupertino.dart';

class SatisfactionQuestionnaireVm extends ChangeNotifier{
  int _topic1 = 0;
  int _topic2 = 0;
  int _topic3 = 0;
  int _topic4 = 0;
  int _topic5 = 0;
  int _topic6 = 0;
  int _topic7 = 0;

  ///設定星級評分
  /// * [quest] 問題
  /// * [index] 分數
  void setQuestAndStar({required int quest, required int index}){
    log('設定星級評分 第$quest題 $index分');
    switch(quest){
      case 0:
        _topic1 = index;
        break;
      case 1:
        _topic2 = index;
        break;
      case 2:
        _topic3 = index;
        break;
      case 3:
        _topic4 = index;
        break;
      case 4:
        _topic5 = index;
        break;
      case 5:
        _topic6 = index;
        break;
      case 6:
        _topic7 = index;
        break;
    }
    notifyListeners();
  }

  /// 取得目前分數
  /// * [quest] 題目
  int getQuestAndStar({required int quest}){
    switch(quest){
      case 0:
        return _topic1;
      case 1:
        return _topic2;
      case 2:
        return _topic3;
      case 3:
        return _topic4;
      case 4:
        return _topic5;
      case 5:
        return _topic6;
      case 6:
        return _topic7;
      default:
        return 0;
    }
  }

  ///歸0
  void clear(){
    _topic1 = 0;
    _topic2 = 0;
    _topic3 = 0;
    _topic4 = 0;
    _topic5 = 0;
    _topic6 = 0;
    _topic7 = 0;
    notifyListeners();
  }

  ///取全部分數
  List<int> getAllScore(){
    return [_topic1, _topic2, _topic3, _topic4, _topic5, _topic6, _topic7];
  }
}