import 'package:flutter/cupertino.dart';

import '../models/operation_item_model.dart';

class OperationItemVm with ChangeNotifier{
  List<OperationItemsModel>? _operationItemsModelList;
  List<OperationItemsModel>? get operationItemsModelList => _operationItemsModelList;
  //設定List
  void setList(List<OperationItemsModel> list){
    _operationItemsModelList = List.from(list);
    notifyListeners();
  }
}