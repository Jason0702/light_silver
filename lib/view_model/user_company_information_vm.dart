import 'package:flutter/material.dart';

import '../models/user_company_information_model.dart';

class UserCompanyInformationVm with ChangeNotifier{
  UserCompanyInformationModel? _userCompanyInformationModel;
  UserCompanyInformationModel? get userCompanyInformationModel => _userCompanyInformationModel;

  //初始化
  void initModel(){
    _userCompanyInformationModel = UserCompanyInformationModel(address: '', operationItems: '', phone: '', identity: 0, area: '', id: 0, maxMember: 0, name: '', realName: '', city: '', logo: '');
    notifyListeners();
  }

  //設定Model
  void setModel(UserCompanyInformationModel model){
    _userCompanyInformationModel = model;
    notifyListeners();
  }

  //設定名稱
  void setName(String name){
    _userCompanyInformationModel!.name = name;
    notifyListeners();
  }

  //設定手機
  void setPhone(String phone){
    _userCompanyInformationModel!.phone = phone;
    notifyListeners();
  }

  //設定城市
  void setCity(String city){
    _userCompanyInformationModel!.city = city;
    notifyListeners();
  }
  //設定區域
  void setArea(String area){
    _userCompanyInformationModel!.area = area;
    notifyListeners();
  }
  //設定地址
  void setAddress(String address){
    _userCompanyInformationModel!.address = address;
    notifyListeners();
  }
  //設定Logo
  void setLogo(String logo){
    _userCompanyInformationModel!.logo = logo;
    notifyListeners();
  }
}