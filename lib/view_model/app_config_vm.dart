import 'package:flutter/foundation.dart';

class AppConfigVm extends ChangeNotifier{
  double _width = 0.0;
  double get width => _width;

  double _height = 0.0;
  double get height => _height;

  String _version = '';
  String get version => _version;

  void setWidthAndHeight(double vw, double vh){
    _width = vw;
    _height = vh;
  }

  void setAppVersion(String value){
    _version = value;
  }
}