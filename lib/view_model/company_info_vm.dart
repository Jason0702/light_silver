import 'package:flutter/material.dart';

import '../models/company_info_model.dart';

class CompanyInfoVm with ChangeNotifier{
  CompanyInfoModel? _companyInfoModel;
  CompanyInfoModel? get companyInfoModel => _companyInfoModel;

  //設定
  void setCompanyInfoModel(CompanyInfoModel model){
    _companyInfoModel = model;
    notifyListeners();
  }
}