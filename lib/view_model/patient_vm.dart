import 'package:flutter/cupertino.dart';
import 'package:light_silver/database/database_helper.dart';
import 'package:light_silver/utils/shared_preference_util.dart';

import '../models/patient.dart';

class PatientVm with ChangeNotifier{
  final SharedPreferenceUtil _sharedPreferenceUtil = SharedPreferenceUtil();
  DatabaseHelper dbHelper = DatabaseHelper();

  Patient? _patient;
  Patient? get patient => _patient;

  void getPatient() async {
    String sp = await _sharedPreferenceUtil.getPatientName();
    if(sp.isNotEmpty){
      _patient = await dbHelper.getPatient(sp);
      notifyListeners();
    }
  }

  Future<bool> isSavePatient(String name) async{
    if(await dbHelper.getPatient(name) == null){
      return true;
    }
    return false;
  }

  Future<void> savePatient(Patient value) async {
    _sharedPreferenceUtil.savePatientName(value.name);
    _patient = value;
    await dbHelper.savePatient(value);
    notifyListeners();
  }

  Future<void> editPatient(Patient value) async {
    _patient = value;
    await dbHelper.updatePatient(value);
    notifyListeners();
  }

  void delPatient(){
    _sharedPreferenceUtil.delPatientName();
    _patient = null;
    notifyListeners();
  }
}