import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:light_silver/app.dart';
import 'package:light_silver/language/language_constants.dart';
import 'package:light_silver/utils/api.dart';
import 'package:light_silver/utils/utils.dart';
import 'package:light_silver/view_model/member_vm.dart';
import 'package:light_silver/widgets/noShadowScrollbehavior.dart';
import 'package:provider/provider.dart';

class ChangePasswordPage extends StatefulWidget {
  @override
  State createState() => _ChangePasswordPageState();
}

class _ChangePasswordPageState extends State<ChangePasswordPage> {
  final TextEditingController _oldPassword = TextEditingController();
  final TextEditingController _newPassword = TextEditingController();
  final TextEditingController _checkPassword = TextEditingController();

  final _bg = BoxDecoration(
      borderRadius: const BorderRadius.all(Radius.circular(10.0)),
      gradient: LinearGradient(colors: [
        HexColor.fromHex('#0067B4'),
        HexColor.fromHex('#003457'),
      ], begin: Alignment.topCenter, end: Alignment.bottomCenter));

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      resizeToAvoidBottomInset: true,
      body: SafeArea(
        child: ScrollConfiguration(
          behavior: NoShadowScrollBehavior(),
          child: SingleChildScrollView(
            padding:
                const EdgeInsets.only(top: 23, left: 23, right: 23, bottom: 23),
            child: buildIndexTextForm(),
          ),
        ),
      ),
    );
  }

  Widget buildIndexTextForm() {
    return Container(
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(8)),
        color: Colors.white,
      ),
      height: MediaQuery.of(context).size.height / 2,
      child: Column(
        children: [
          const SizedBox(
            height: 20,
          ),
          Container(
            alignment: Alignment.centerLeft,
            child: FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                getTranslated(context, 'Current_password'),
                textScaleFactor: 1,
                style: TextStyle(
                    fontSize: 20,
                    color: HexColor.fromHex('#1E2C39'),
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
          _buildOldPassword(),
          const SizedBox(
            height: 20,
          ),
          Container(
            alignment: Alignment.centerLeft,
            child: FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                getTranslated(context, 'New_password'),
                textScaleFactor: 1,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: HexColor.fromHex('#1E2C39'),
                  fontSize: 20,
                ),
              ),
            ),
          ),
          _buildNewPassword(),
          const SizedBox(
            height: 20,
          ),
          Container(
            alignment: Alignment.centerLeft,
            child: FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                getTranslated(context, 'New_key_confirmation'),
                textScaleFactor: 1,
                style: TextStyle(
                    fontSize: 20,
                    color: HexColor.fromHex('#1E2C39'),
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
          _buildCheckPassword(),
          Expanded(
              child: Container(
            alignment: Alignment.center,
            child: ButtonTheme(
              height: 110,
              child: Consumer<MemberVm>(
                builder: (context, vm, _) {
                  return ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        foregroundColor: Colors.white,
                        backgroundColor: Colors.transparent,
                        shape: const RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.0))),
                        padding: const EdgeInsets.all(0.0)),
                    onPressed: () => _confirmOnTap(vm.memberModel!.id),
                    child: Container(
                        height: 60,
                        width: 750,
                        alignment: Alignment.center,
                        decoration: _bg,
                        child: FittedBox(
                          fit: BoxFit.scaleDown,
                          child: Text(
                            getTranslated(context, 'Confirm_the_changes'),
                            textScaleFactor: 1,
                            style: const TextStyle(
                                color: Colors.white, fontSize: 23),
                          ),
                        )),
                  );
                },
              ),
            ),
          ))
        ],
      ),
    );
  }

  Widget _buildOldPassword() {
    return TextField(
      style: const TextStyle(color: Colors.black),
      decoration: InputDecoration(
          errorText: null,
          contentPadding: const EdgeInsets.all(10.0),
          enabledBorder: OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.black),
              borderRadius: BorderRadius.circular(10.0)),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
          filled: true,
          fillColor: Colors.white,
          hintText: getTranslated(context, 'Please_enter_the_current_password'),
          hintStyle: const TextStyle(color: Colors.grey)),
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _oldPassword,
      obscureText: true,
    );
  }

  Widget _buildNewPassword() {
    return TextField(
      style: const TextStyle(color: Colors.black),
      decoration: InputDecoration(
          errorText: null,
          contentPadding: const EdgeInsets.all(10.0),
          enabledBorder: OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.black),
              borderRadius: BorderRadius.circular(10.0)),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
          filled: true,
          fillColor: Colors.white,
          hintText: getTranslated(context, 'Please_enter_a_new_password'),
          hintStyle: const TextStyle(color: Colors.grey)),
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _newPassword,
      obscureText: true,
    );
  }

  Widget _buildCheckPassword() {
    return TextField(
      style: const TextStyle(color: Colors.black),
      decoration: InputDecoration(
          errorText: null,
          contentPadding: const EdgeInsets.all(10.0),
          enabledBorder: OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.black),
              borderRadius: BorderRadius.circular(10.0)),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
          filled: true,
          fillColor: Colors.white,
          hintText:
              getTranslated(context, 'Please_enter_the_new_password_again'),
          hintStyle: const TextStyle(color: Colors.grey)),
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _checkPassword,
      obscureText: true,
    );
  }

  void _confirmOnTap(int id) async {
    debugPrint('確認修改');
    if (_newPassword.text != _checkPassword.text) {
      EasyLoading.showError('新密碼確認與新密碼不一樣');
    } else if (_oldPassword.text == _newPassword.text) {
      EasyLoading.showError('舊密碼與新密碼一樣');
    } else {
      Map<String, dynamic> map = {
        'id': id,
        'password': _oldPassword.text,
        'new_password': _newPassword.text,
      };
      try {
        dynamic response = await httpUtils.put(
          '$putNewPassword/$id/password',
          options: Options(
              contentType: 'application/json',
              responseType: ResponseType.json,
              headers: {"authorization": "Bearer $accessToken"}),
        );
        debugPrint('發送更新密碼: $response');
      } on DioError catch (e) {
        debugPrint(e.message);
      }
    }
  }
}
