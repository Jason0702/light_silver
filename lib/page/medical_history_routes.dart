import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:light_silver/language/language_constants.dart';
import 'package:light_silver/models/patient.dart';
import 'package:light_silver/utils/utils.dart';
import 'package:light_silver/widgets/noShadowScrollbehavior.dart';
import 'package:provider/provider.dart';

import '../app.dart';
import '../routes/route_name.dart';
import '../view_model/patient_vm.dart';
import '../widgets/button.dart';

class MedicalHistoryRoutes extends StatefulWidget {
  @override
  State createState() => _MedicalHistoryRoutesState();
}

class _MedicalHistoryRoutesState extends State<MedicalHistoryRoutes>
    with WidgetsBindingObserver {
  PatientVm? _patientVm;
  final TextEditingController _name = TextEditingController();
  final TextEditingController _gender = TextEditingController();
  final TextEditingController _age = TextEditingController();
  final TextEditingController _phone = TextEditingController();
  final TextEditingController _problem = TextEditingController();

  //減緩不適症狀
  bool _relieveSymptomsOfDiscomfort = false;

  //姿勢矯正
  bool _postureCorrection = false;

  //資訊衛教
  bool _informationHealthEducation = false;

  //關節復位
  bool _jointReduction = false;

  //核心肌群訓練
  bool _coreMuscleTraining = false;

  //健康促進
  bool _healthPromotion = false;

  List<String> items = ["F", "M"];

  //漸層背景
  final _homeBg = const BoxDecoration(
      image: DecorationImage(
          image: AssetImage('assets/images/home_bg.png'), fit: BoxFit.fill));

  final _titleBarBG = BoxDecoration(
      gradient: LinearGradient(colors: [
    HexColor.fromHex('#CB2020'),
    HexColor.fromHex('#DC6363'),
    HexColor.fromHex('#CB2020'),
  ], begin: Alignment.topCenter, end: Alignment.bottomCenter));

  //中間背景
  final _centerBG = const BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.all(Radius.circular(10)),
      boxShadow: [
        BoxShadow(
            color: Colors.black54,
            offset: Offset(5.0, 5.0),
            blurRadius: 10.0,
            spreadRadius: 2.0)
      ]);

  _onLayoutDone(_) {
    _dataInit(_patientVm!.patient);
  }

  void _dataInit(Patient? patient) {
    if (patient == null) {
      return;
    }
    _name.text = patient.name;
    _gender.text = patient.gender;
    _age.text = patient.age.toString();
    _phone.text = patient.phone;
    _problem.text = patient.problem;
    _relieveSymptomsOfDiscomfort =
        patient.relieveSymptomsOfDiscomfort == 0 ? false : true;
    _postureCorrection = patient.postureCorrection == 0 ? false : true;
    _informationHealthEducation =
        patient.informationHealthEducation == 0 ? false : true;
    _jointReduction = patient.jointReduction == 0 ? false : true;
    _coreMuscleTraining = patient.coreMuscleTraining == 0 ? false : true;
    _healthPromotion = patient.healthPromotion == 0 ? false : true;
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  Widget build(BuildContext context) {
    _patientVm = Provider.of<PatientVm>(context);
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: true,
      body: GestureDetector(
        onTap: _clickOnTap,
        child: SafeArea(
          child: Column(
            children: [
              Expanded(flex: 1, child: _titleBar()),
              Expanded(flex: 10, child: _bodyWidget()),
            ],
          ),
        ),
      ),
    );
  }

  void _clickOnTap() {
    debugPrint('點擊其他區域');
    FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus && currentFocus.focusedChild != null) {
      FocusManager.instance.primaryFocus!.unfocus();
    }
  }

  //最上層標題與按鈕
  Widget _titleBar() {
    return Container(
      decoration: _titleBarBG,
      child: Row(
        children: [
          Expanded(flex: 1, child: _leftTopButton()),
          Expanded(
              flex: 3,
              child: Container(
                alignment: Alignment.center,
                child: FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    getTranslated(context, 'Medical_history'),
                    textScaleFactor: 1,
                    style: const TextStyle(fontSize: 25, color: Colors.white),
                  ),
                ),
              )),
          const Expanded(flex: 1, child: SizedBox())
        ],
      ),
    );
  }

  //左側按鈕
  Widget _leftTopButton() {
    return InkWell(
      onTap: () {
        _saveOnTap(isShowToast: false);
        Navigator.of(context).pop(true);
      },
      child: const Icon(
        Icons.arrow_back_rounded,
        color: Colors.white,
        size: 45,
      ),
    );
  }

  //主體
  Widget _bodyWidget() {
    return ScrollConfiguration(
      behavior: NoShadowScrollBehavior(),
      child: SingleChildScrollView(
        child: Container(
          decoration: _homeBg,
          alignment: Alignment.center,
          child: Container(
            width: MediaQuery.of(context).size.width / 1.1,
            height: MediaQuery.of(context).size.height * 1.5,
            margin: const EdgeInsets.symmetric(vertical: 10),
            decoration: _centerBG,
            child: Column(
              children: [
                const SizedBox(
                  height: 15,
                ),
                //病史諮詢
                FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    getTranslated(context, 'Medical_history_text'),
                    textScaleFactor: 1,
                    style: const TextStyle(
                        fontSize: 23.5, fontWeight: FontWeight.bold),
                  ),
                ),
                const SizedBox(height: 15),
                Container(
                  height: 1.5,
                  margin: const EdgeInsets.only(left: 25, right: 25),
                  color: Colors.red,
                ),
                const SizedBox(height: 15),
                //基本資料
                Container(
                  margin: const EdgeInsets.only(left: 25),
                  alignment: Alignment.centerLeft,
                  child: FittedBox(
                    fit: BoxFit.scaleDown,
                    child: Text(
                      getTranslated(context, 'Basic_information'),
                      textScaleFactor: 1,
                      style: const TextStyle(
                          color: Colors.blue,
                          fontWeight: FontWeight.bold,
                          fontSize: 20),
                    ),
                  ),
                ),
                const SizedBox(height: 5),
                //姓名
                Container(
                  margin: const EdgeInsets.only(left: 25),
                  alignment: Alignment.centerLeft,
                  child: FittedBox(
                    fit: BoxFit.scaleDown,
                    child: Text(
                      getTranslated(context, 'Name'),
                      textScaleFactor: 1,
                      style: const TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 20),
                    ),
                  ),
                ),
                _buildName(),
                const SizedBox(height: 5),
                //性別年齡
                SizedBox(
                  height: 90,
                  child: Container(
                    margin: const EdgeInsets.only(left: 25, right: 25),
                    child: Row(
                      children: [
                        //性別
                        Expanded(
                            flex: 1,
                            child: Column(
                              children: [
                                Container(
                                  alignment: Alignment.centerLeft,
                                  child: FittedBox(
                                    fit: BoxFit.scaleDown,
                                    child: Text(
                                      getTranslated(context, 'Gender'),
                                      textScaleFactor: 1,
                                      style: const TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ),
                                Expanded(child: _buildGender()),
                              ],
                            )),
                        const SizedBox(
                          width: 10,
                        ),
                        //年齡
                        Expanded(
                          flex: 1,
                          child: Column(
                            children: [
                              Container(
                                alignment: Alignment.centerLeft,
                                child: FittedBox(
                                  fit: BoxFit.scaleDown,
                                  child: Text(
                                    getTranslated(context, 'Age'),
                                    textScaleFactor: 1,
                                    style: const TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                              Expanded(child: _buildAge())
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                //電話
                Container(
                  margin: const EdgeInsets.only(left: 25),
                  alignment: Alignment.centerLeft,
                  child: FittedBox(
                    fit: BoxFit.scaleDown,
                    child: Text(
                      getTranslated(context, 'Phone'),
                      textScaleFactor: 1,
                      style: const TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 20),
                    ),
                  ),
                ),
                _buildPhone(),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 20),
                  height: 1.5,
                  color: Colors.grey[400],
                ),
                //問題
                Container(
                  margin: const EdgeInsets.only(left: 25),
                  alignment: Alignment.centerLeft,
                  child: FittedBox(
                    fit: BoxFit.scaleDown,
                    child: Text(
                      getTranslated(context, 'Problem'),
                      textScaleFactor: 1,
                      style: const TextStyle(
                          color: Colors.blue,
                          fontWeight: FontWeight.bold,
                          fontSize: 20),
                    ),
                  ),
                ),
                const SizedBox(height: 5),
                _buildProblem(),
                const SizedBox(height: 15),
                _buildSaveButton(),
                const SizedBox(height: 15),
                Container(
                  height: 1.5,
                  color: Colors.grey[400],
                ),
                const SizedBox(height: 15),
                //目標
                Container(
                  margin: const EdgeInsets.only(left: 25),
                  alignment: Alignment.centerLeft,
                  child: FittedBox(
                    fit: BoxFit.scaleDown,
                    child: Text(
                      getTranslated(context, 'The_goal'),
                      textScaleFactor: 1,
                      style: const TextStyle(
                          color: Colors.blue,
                          fontWeight: FontWeight.bold,
                          fontSize: 20),
                    ),
                  ),
                ),
                const SizedBox(height: 10),
                Expanded(flex: 4, child: _buildColumnList()),
                Expanded(child: _buildButton())
              ],
            ),
          ),
        ),
      ),
    );
  }

  //姓名
  Widget _buildName() {
    return Container(
      margin: const EdgeInsets.only(left: 25, right: 25),
      child: TextField(
        style: const TextStyle(color: Colors.black),
        decoration: InputDecoration(
            errorText: null,
            contentPadding: const EdgeInsets.all(5.0),
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.grey[400]!),
                borderRadius: BorderRadius.circular(10.0)),
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(10.0)),
            filled: true,
            fillColor: Colors.white,
            hintText: getTranslated(context, 'Please_type_in_your_name'),
            hintStyle: const TextStyle(color: Colors.grey)),
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.next,
        controller: _name,
      ),
    );
  }

  //電話
  Widget _buildPhone() {
    return Container(
      margin: const EdgeInsets.only(left: 25, right: 25),
      child: TextField(
        style: const TextStyle(color: Colors.black),
        decoration: InputDecoration(
            errorText: null,
            contentPadding: const EdgeInsets.all(5.0),
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.grey[400]!),
                borderRadius: BorderRadius.circular(10.0)),
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(10.0)),
            filled: true,
            fillColor: Colors.white,
            hintText: getTranslated(context, 'Please_enter_your_phone_number'),
            hintStyle: const TextStyle(color: Colors.grey)),
        keyboardType: TextInputType.phone,
        textInputAction: TextInputAction.next,
        controller: _phone,
      ),
    );
  }

  //性別
  Widget _buildGender() {
    return TextField(
      readOnly: true,
      style: const TextStyle(color: Colors.black),
      decoration: InputDecoration(
        suffixIcon: PopupMenuButton<String>(
          offset: const Offset(-100, 70),
          icon: const Icon(
            Icons.arrow_drop_down,
            size: 35,
          ),
          onSelected: (String value) {
            _gender.text = value;
          },
          itemBuilder: (BuildContext context) {
            return items.map<PopupMenuItem<String>>((String value) {
              return PopupMenuItem(
                value: value,
                child: FittedBox(fit: BoxFit.scaleDown, child: Text(value)),
              );
            }).toList();
          },
        ),
        contentPadding: const EdgeInsets.all(5),
        enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey[400]!),
            borderRadius: BorderRadius.circular(10)),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
        filled: true,
        fillColor: Colors.white,
        hintText: getTranslated(context, 'Please_enter_gender'),
        hintStyle: TextStyle(color: HexColor.fromHex('#919191')),
      ),
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _gender,
    );
  }

  //年齡
  Widget _buildAge() {
    return SizedBox(
      width: 400,
      child: TextField(
        style: const TextStyle(color: Colors.black),
        decoration: InputDecoration(
            errorText: null,
            contentPadding: const EdgeInsets.all(5.0),
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.grey[400]!),
                borderRadius: BorderRadius.circular(10)),
            border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
            filled: true,
            fillColor: Colors.white,
            hintText: getTranslated(context, 'Please_enter_age'),
            hintStyle: const TextStyle(color: Colors.grey)),
        keyboardType: TextInputType.number,
        textInputAction: TextInputAction.done,
        controller: _age,
      ),
    );
  }

  //問題
  Widget _buildProblem() {
    return Container(
      margin: const EdgeInsets.only(left: 25, right: 25),
      child: TextField(
        maxLines: 8,
        maxLength: 500,
        decoration: InputDecoration(
            errorText: null,
            contentPadding: const EdgeInsets.all(5.0),
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.grey[400]!),
                borderRadius: BorderRadius.circular(10)),
            border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
            filled: true,
            fillColor: Colors.white,
            hintText: '',
            hintStyle: const TextStyle(color: Colors.grey)),
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.done,
        controller: _problem,
      ),
    );
  }

  //完成存檔
  Widget _buildSaveButton() {
    return Container(
      alignment: Alignment.centerRight,
      margin: const EdgeInsets.only(right: 25),
      child: ButtonTheme(
        height: 110,
        child: ElevatedButton(
            style: ElevatedButton.styleFrom(
                foregroundColor: Colors.white,
                backgroundColor: Colors.transparent,
                shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0))),
                padding: const EdgeInsets.all(0.0)),
            onPressed: _saveOnTap,
            child: Container(
              width: 120,
              height: 50,
              alignment: Alignment.center,
              decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Colors.red),
              child: FittedBox(
                fit: BoxFit.scaleDown,
                child: Text(
                  getTranslated(context, 'Complete_archive'),
                  textScaleFactor: 1,
                  style: const TextStyle(fontSize: 20),
                ),
              ),
            )),
      ),
    );
  }

  //核取方塊
  Widget _buildColumnList() {
    return Container(
      margin: const EdgeInsets.only(left: 25),
      child: Column(
        children: [
          Expanded(
              child: Row(
            children: [
              Transform.scale(
                scale: 1.5,
                child: Checkbox(
                  value: _relieveSymptomsOfDiscomfort,
                  onChanged: (value) {
                    setState(() {
                      _relieveSymptomsOfDiscomfort = value!;
                    });
                  },
                ),
              ),
              FittedBox(
                fit: BoxFit.scaleDown,
                child: Text(
                  getTranslated(context, 'Relieve_symptoms_of_discomfort'),
                  textScaleFactor: 1,
                  style: const TextStyle(
                      fontSize: 20, fontWeight: FontWeight.bold),
                ),
              ),
            ],
          )),
          const SizedBox(
            height: 20,
          ),
          Expanded(
              child: Row(
            children: [
              Transform.scale(
                scale: 1.5,
                child: Checkbox(
                  value: _postureCorrection,
                  onChanged: (value) {
                    setState(() {
                      _postureCorrection = value!;
                    });
                  },
                ),
              ),
              FittedBox(
                fit: BoxFit.scaleDown,
                child: Text(
                  getTranslated(context, 'Posture_correction'),
                  textScaleFactor: 1,
                  style: const TextStyle(
                      fontSize: 20, fontWeight: FontWeight.bold),
                ),
              )
            ],
          )),
          const SizedBox(
            height: 20,
          ),
          Expanded(
              child: Row(
            children: [
              Transform.scale(
                scale: 1.5,
                child: Checkbox(
                  value: _informationHealthEducation,
                  onChanged: (value) {
                    setState(() {
                      _informationHealthEducation = value!;
                    });
                  },
                ),
              ),
              FittedBox(
                fit: BoxFit.scaleDown,
                child: Text(
                  getTranslated(context, 'Information_Health_Education'),
                  textScaleFactor: 1,
                  style: const TextStyle(
                      fontSize: 20, fontWeight: FontWeight.bold),
                ),
              )
            ],
          )),
          const SizedBox(
            height: 20,
          ),
          Expanded(
              child: Row(
            children: [
              Transform.scale(
                scale: 1.5,
                child: Checkbox(
                  value: _jointReduction,
                  onChanged: (value) {
                    setState(() {
                      _jointReduction = value!;
                    });
                  },
                ),
              ),
              FittedBox(
                fit: BoxFit.scaleDown,
                child: Text(
                  getTranslated(context, 'Joint_reduction'),
                  textScaleFactor: 1,
                  style: const TextStyle(
                      fontSize: 20, fontWeight: FontWeight.bold),
                ),
              )
            ],
          )),
          const SizedBox(
            height: 20,
          ),
          Expanded(
              child: Row(
            children: [
              Transform.scale(
                scale: 1.5,
                child: Checkbox(
                  value: _coreMuscleTraining,
                  onChanged: (value) {
                    setState(() {
                      _coreMuscleTraining = value!;
                    });
                  },
                ),
              ),
              FittedBox(
                fit: BoxFit.scaleDown,
                child: Text(
                  getTranslated(context, 'Core_muscle_training'),
                  textScaleFactor: 1,
                  style: const TextStyle(
                      fontSize: 20, fontWeight: FontWeight.bold),
                ),
              )
            ],
          )),
          const SizedBox(
            height: 20,
          ),
          Expanded(
              child: Row(
            children: [
              Transform.scale(
                scale: 1.5,
                child: Checkbox(
                  value: _healthPromotion,
                  onChanged: (value) {
                    setState(() {
                      _healthPromotion = value!;
                    });
                  },
                ),
              ),
              FittedBox(
                fit: BoxFit.scaleDown,
                child: Text(
                  getTranslated(context, 'Health_promotion'),
                  textScaleFactor: 1,
                  style: const TextStyle(
                      fontSize: 20, fontWeight: FontWeight.bold),
                ),
              )
            ],
          )),
        ],
      ),
    );
  }

  //按鈕
  Widget _buildButton() {
    return Container(
      padding: const EdgeInsets.only(right: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          //清除
          ClearButton(
            click: _clearOnTap,
            buttonText: getTranslated(context, 'Clear'),
          ),
          const SizedBox(
            width: 20,
          ),
          //下一步
          NextButton(
              click: _nextOnTap,
              decoration: Style.medicalHistoryBG,
              buttonText: getTranslated(context, 'Next_step'),
          )
        ],
      ),
    );
  }

  //完成存檔
  void _saveOnTap({bool isShowToast = true}) async {
    if(await _patientVm!.isSavePatient(_name.text)){
      addPatient(isShowToast: isShowToast);
    }else{
      editPatient(_patientVm!.patient, isShowToast: isShowToast);
    }
  }

  void addPatient({required bool isShowToast}) async {
    Patient _patient = Patient(
        name: _name.text,
        gender: _gender.text,
        age: _age.text.isNotEmpty ? int.parse(_age.text) : 0,
        phone: _phone.text,
        problem: _problem.text,
        relieveSymptomsOfDiscomfort: _relieveSymptomsOfDiscomfort ? 1 : 0,
        postureCorrection: _postureCorrection ? 1 : 0,
        informationHealthEducation: _informationHealthEducation ? 1 : 0,
        jointReduction: _jointReduction ? 1 : 0,
        coreMuscleTraining: _coreMuscleTraining ? 1 : 0,
        healthPromotion: _healthPromotion ? 1 : 0,
        GT: 0,
        pad: 0,
        padHigh: '',
        IC: 0,
        ASIS: 0,
        PSIS: 0,
        flexion: 0,
        floor: '',
        gillet: 0,
        Extension: 3,
        rotation_right: 3,
        rotation_left: 3,
        side_left: 3,
        side_right: 3,
        side_knee_left: '',
        side_knee_right: '',
        supine_leg_length: 0,
        adductor: 0,
        inguinal: 0,
        prone_leg_length: 0,
        add_ROM: 0,
        ext_rotator: 0,
        PSIS_left: 0,
        PSIS_right: 0,
        ILA_left: 0,
        ILA_right: 0,
        Prop_left: 0,
        Prop_right: 0,
        lumbar_left: 0,
        lumbar_right: 0,
        supine_ilium_leg_length: 0,
        Long_sit: 0,
        SLR: 0,
        Patrick: 0,
        Knee_flexor: 0,
        ASIS_umbilical: 0,
        IC_recheck: 0,
        flexion_recheck: 0,
        floor_recheck: '',
        gillet_recheck: 0,
        extension_recheck: 3,
        rotation_left_recheck: 3,
        rotation_right_recheck: 3,
        side_left_recheck: 3,
        side_right_recheck: 3,
        side_knee_left_recheck: '',
        side_knee_right_recheck: '');

    await _patientVm!.savePatient(_patient);
    EasyLoading.showSuccess('儲存完成');
  }

  void editPatient(Patient? patient, {required bool isShowToast}) async {
    if(patient == null){
      return;
    }
    Patient _patient = Patient(
        name: _name.text,
        gender: _gender.text,
        age: _age.text.isNotEmpty ? int.parse(_age.text) : 0,
        phone: _phone.text,
        problem: _problem.text,
        relieveSymptomsOfDiscomfort: _relieveSymptomsOfDiscomfort ? 1 : 0,
        postureCorrection: _postureCorrection ? 1 : 0,
        informationHealthEducation: _informationHealthEducation ? 1 : 0,
        jointReduction: _jointReduction ? 1 : 0,
        coreMuscleTraining: _coreMuscleTraining ? 1 : 0,
        healthPromotion: _healthPromotion ? 1 : 0,
        GT: patient.GT,
        pad: patient.pad,
        padHigh: patient.padHigh,
        IC: patient.IC,
        ASIS: patient.ASIS,
        PSIS: patient.PSIS,
        flexion: patient.flexion,
        floor: patient.floor,
        gillet: patient.gillet,
        Extension: patient.Extension,
        rotation_right: patient.rotation_right,
        rotation_left: patient.rotation_left,
        side_left: patient.side_left,
        side_right: patient.side_right,
        side_knee_left: patient.side_knee_left,
        side_knee_right: patient.side_knee_right,
        supine_leg_length: patient.supine_leg_length,
        adductor: patient.adductor,
        inguinal: patient.inguinal,
        prone_leg_length: patient.prone_leg_length,
        add_ROM: patient.add_ROM,
        ext_rotator: patient.ext_rotator,
        PSIS_left: patient.PSIS_left,
        PSIS_right: patient.PSIS_right,
        ILA_left: patient.ILA_left,
        ILA_right: patient.ILA_right,
        Prop_left: patient.Prop_left,
        Prop_right: patient.Prop_right,
        lumbar_left: patient.lumbar_left,
        lumbar_right: patient.lumbar_right,
        supine_ilium_leg_length: patient.supine_ilium_leg_length,
        Long_sit: patient.Long_sit,
        SLR: patient.SLR,
        Patrick: patient.Patrick,
        Knee_flexor: patient.Knee_flexor,
        ASIS_umbilical: patient.ASIS_umbilical,
        IC_recheck: patient.IC_recheck,
        flexion_recheck: patient.flexion_recheck,
        floor_recheck: patient.floor_recheck,
        gillet_recheck: patient.gillet_recheck,
        extension_recheck: patient.extension_recheck,
        rotation_left_recheck: patient.rotation_left_recheck,
        rotation_right_recheck: patient.rotation_right_recheck,
        side_left_recheck: patient.side_left_recheck,
        side_right_recheck: patient.side_right_recheck,
        side_knee_left_recheck: patient.side_knee_left_recheck,
        side_knee_right_recheck: patient.side_knee_right_recheck);

    await _patientVm!.editPatient(_patient);

    EasyLoading.showSuccess('儲存完成');
  }

  //清除
  void _clearOnTap() {
    _name.text = '';
    _gender.text = '';
    _age.text = '';
    _phone.text = '';
    _problem.text = '';
    _relieveSymptomsOfDiscomfort = false;
    _postureCorrection = false;
    _informationHealthEducation = false;
    _jointReduction = false;
    _coreMuscleTraining = false;
    _healthPromotion = false;
    _patientVm!.delPatient();
    setState(() {});
  }

  //下一步
  void _nextOnTap() {
    _saveOnTap(isShowToast: false);
    delegate.push(name: RouteName.spe01);
  }
}
