import 'package:flutter/material.dart';
import 'package:light_silver/language/language_constants.dart';
import 'package:light_silver/utils/utils.dart';
import 'package:light_silver/widgets/left_button.dart';
import 'package:provider/provider.dart';

import '../app.dart';
import '../view_model/login_register_vm.dart';
import '../widgets/login_register/title.dart';
import 'login_page.dart';
import 'register_page.dart';

class LoginOrRegisterRouters extends StatefulWidget {
  @override
  State createState() => _LoginOrRegisterRoutersState();
}

class _LoginOrRegisterRoutersState extends State<LoginOrRegisterRouters>
    with WidgetsBindingObserver {
  LoginRegisterVm? _loginRegisterVm;

  late PageController _pageController;
  late PageView _pageView;
  int _currentPage = 0;

  //漸層背景
  final _homeBG = const BoxDecoration(
      image: DecorationImage(
          image: AssetImage('assets/images/home_bg.png'), fit: BoxFit.fill));

  final _titleBarBG = BoxDecoration(
      gradient: LinearGradient(
          colors: [HexColor.fromHex('#0067B4'), HexColor.fromHex('#003457')],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter));
  //中間背景
  final _centerBG = const BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.all(Radius.circular(10)),
      boxShadow: [
        BoxShadow(
            color: Colors.black54,
            offset: Offset(5.0, 5.0),
            blurRadius: 10.0,
            spreadRadius: 2.0),
      ]);

  _onLayoutDone(_) {
    _loginRegisterVm!.setTitle(getTranslated(context, 'Sing_in'));
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
    _pageController = PageController();
    _pageView = PageView(
      controller: _pageController,
      children: [LoginPage(), RegisterPage()],
      onPageChanged: (index) {
        if (index == 1) {
          _loginRegisterVm!.setTitle(getTranslated(context, 'Register'));
        } else {
          _loginRegisterVm!.setTitle(getTranslated(context, 'Sing_in'));
        }
        setState(() {
          _currentPage = index;
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    _loginRegisterVm = Provider.of<LoginRegisterVm>(context);
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: false,
      body: GestureDetector(
        onTap: _clickOnTap,
        child: SafeArea(
          child: Column(
            children: [
              Expanded(flex: 1, child: _titleBar()),
              Expanded(flex: 10, child: _bodyWidget()),
            ],
          ),
        ),
      ),
    );
  }

  void _clickOnTap() {
    debugPrint('點擊其他區域');
    FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus && currentFocus.focusedChild != null) {
      FocusManager.instance.primaryFocus!.unfocus();
    }
  }

  //最上層標題與按鈕
  Widget _titleBar() {
    return Container(
      decoration: _titleBarBG,
      child: Row(
        children: [
          const Expanded(flex: 1, child: LeftButton()),
          Expanded(
              flex: 5,
              child: Container(
                alignment: Alignment.center,
                child: const TitleTextWidget(),
              )),
          const Expanded(
              flex: 1,
              child: SizedBox(
                width: 50,
              ))
        ],
      ),
    );
  }

  //主體
  Widget _bodyWidget() {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      decoration: _homeBG,
      alignment: Alignment.center,
      child: Container(
        width: MediaQuery.of(context).size.width / 1.1,
        height: MediaQuery.of(context).size.height / 1.3,
        decoration: _centerBG,
        child: Column(
          children: [
            Expanded(
                child: Container(
              alignment: Alignment.center,
              child: Image(
                image: const AssetImage('assets/images/logo_name_blue.png'),
                width: MediaQuery.of(context).size.width / 2,
              ),
            )),
            Container(
              height: 60,
              margin: const EdgeInsets.symmetric(horizontal: 10),
              padding: const EdgeInsets.all(5),
              decoration: BoxDecoration(
                  borderRadius: const BorderRadius.all(Radius.circular(10)),
                  color: HexColor.fromHex('#F5F5F5'),
                  boxShadow: const [
                    BoxShadow(
                        color: Colors.black38,
                        offset: Offset(5.0, 5.0),
                        blurRadius: 10.0,
                        spreadRadius: 2.0)
                  ]),
              child: Row(
                children: [
                  Expanded(
                      child: Container(
                        decoration: _currentPage == 0
                        ? BoxDecoration(
                            borderRadius: const BorderRadius.all(Radius.circular(10)),
                            gradient: LinearGradient(
                                colors: [
                                  HexColor.fromHex('#0067B4'),
                                  HexColor.fromHex('#003457'),
                                ],
                                begin: Alignment.topCenter,
                                end: Alignment.bottomCenter))
                        : null,
                    child: Center(
                      child: TextButton(
                        onPressed: () {
                          _pageController.animateToPage(0,
                              duration: const Duration(milliseconds: 300),
                              curve: Curves.decelerate);
                        },
                        child: FittedBox(
                          fit: BoxFit.scaleDown,
                          child: Text(getTranslated(context, 'Sing_in'),
                              textScaleFactor: 1,
                              style: TextStyle(
                                  fontSize: 20,
                                  color: _currentPage == 0

                                      ? Colors.white
                                      : HexColor.fromHex('#032137'))),
                        ),
                      ),
                    ),
                  )),
                  Expanded(
                      child: Container(
                    decoration: _currentPage == 1
                        ? BoxDecoration(
                            borderRadius: const BorderRadius.all(Radius.circular(10)),
                            gradient: LinearGradient(
                                colors: [
                                  HexColor.fromHex('#0067B4'),
                                  HexColor.fromHex('#003457'),
                                ],
                                begin: Alignment.topCenter,
                                end: Alignment.bottomCenter))
                        : null,
                    child: Center(
                      child: TextButton(
                        onPressed: () {
                          _pageController.animateToPage(1,
                              duration: const Duration(milliseconds: 300),
                              curve: Curves.decelerate);
                        },
                        child: FittedBox(
                          fit: BoxFit.scaleDown,
                          child: Text(
                            getTranslated(context, 'Register'),
                            textScaleFactor: 1,
                            style: TextStyle(
                                fontSize: 20,
                                color: _currentPage == 1
                                    ? Colors.white
                                    : HexColor.fromHex('#032137')),
                          ),
                        ),
                      ),
                    ),
                  )),
                ],
              ),
            ),
            Expanded(flex: 7, child: _pageView)
          ],
        ),
      ),
    );
  }
}
