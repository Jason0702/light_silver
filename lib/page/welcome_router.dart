import 'package:flutter/material.dart';
import 'package:light_silver/app.dart';

import '../routes/route_name.dart';
import '../utils/app_image.dart';

class WelcomeRouter extends StatefulWidget{

  @override
  State createState() => _WelcomeRouterState();
}

class _WelcomeRouterState extends State<WelcomeRouter> with WidgetsBindingObserver{

  _onLayoutDone(_){
   Future.delayed(const Duration(seconds: 2),(){
     delegate.replace(name: RouteName.home);
   });
  }


  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: SizedBox(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Image.asset(AppImage.launch, fit: BoxFit.fill,),
        ),
      ),
    );
  }
}