import 'package:flutter/material.dart';
import 'package:light_silver/language/language_constants.dart';
import 'package:light_silver/page/change_password_page.dart';
import 'package:light_silver/utils/utils.dart';
import 'package:light_silver/view_model/member_vm.dart';
import 'package:provider/provider.dart';

import '../widgets/left_button.dart';
import 'personal_information_modification_page.dart';

class MemberCentreRoutes extends StatefulWidget{

  @override
  State createState() => _MemberCentreRoutesState();
}

class _MemberCentreRoutesState extends State<MemberCentreRoutes> with WidgetsBindingObserver{

  late PageController _pageController;
  late PageView _pageView;
  int _currentPage = 0;

  //漸層背景
  final _homeBG = const BoxDecoration(
      image: DecorationImage(
          image: AssetImage('assets/images/home_bg.png'), fit: BoxFit.fill));

  //上半部背景
  final _titleBG = const BoxDecoration(
    image: DecorationImage(
      image: AssetImage('assets/images/drawer_bg.png'),
      fit: BoxFit.fill
    )
  );

  //中間背景
  final _centerBG = const BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.all(Radius.circular(10)),
      boxShadow: [
        BoxShadow(
            color: Colors.black54,
            offset: Offset(5.0, 5.0),
            blurRadius: 10.0,
            spreadRadius: 2.0),
      ]);

  _onLayoutDone(_){

  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
    _pageController = PageController();
    _pageView = PageView(
      controller: _pageController,
      children: [
        PersonalInformationModificationPage(),
        ChangePasswordPage()
      ],
      onPageChanged: (index){
        setState(() {
          _currentPage = index;
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: false,
      body: GestureDetector(
        onTap: _clickOnTap,
        child: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Expanded(flex: 2, child: _titleBar()),
              Expanded(flex: 7, child: _bodyWidget()),
            ],
          ),
        ),
      ),
    );
  }

  void _clickOnTap(){
    debugPrint('點擊其他區域');
    FocusScopeNode currentFocus = FocusScope.of(context);
    if(!currentFocus.hasPrimaryFocus && currentFocus.focusedChild != null){
      FocusManager.instance.primaryFocus!.unfocus();
    }
  }

  //最上層標題與按鈕
  Widget _titleBar(){
    return Container(
      decoration: _titleBG,
      child: Column(
        children: [
          Expanded(flex: 1,child: Container(
            padding: const EdgeInsets.only(top: 10),
            child: Row(
              children: [
                const Expanded(flex: 1, child: LeftButton()),
                Expanded(flex: 5, child: Container(
                  alignment: Alignment.center,
                  child: FittedBox(
                    fit: BoxFit.scaleDown,
                    child: Text(
                      getTranslated(context, 'Member_Centre'),
                      textScaleFactor: 1,
                      style: const TextStyle(
                          color: Colors.white,
                          fontSize: 25,
                          fontWeight: FontWeight.bold
                      ),
                    ),
                  ),
                )),
                const Expanded(
                    flex: 1,
                    child: SizedBox(
                      width: 50,
                    ))
              ],
            ),
          )),
          Expanded(flex: 2,child: Row(
            children: [
              Container(
                margin: const EdgeInsets.only(left: 20, right: 20),
                width: 100,
                height: 100,
                child: Image.asset('assets/images/genuine_avatar.png'),
              ),
              Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      //使用者名稱
                      FittedBox(
                        fit: BoxFit.scaleDown,
                        child: Consumer<MemberVm>(builder: (context, vm, _){
                          return Text(
                            vm.memberModel != null ? vm.memberModel!.name : '林大名',
                            textScaleFactor: 1,
                            style: const TextStyle(
                                fontSize: 25,
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                          );
                        },),
                      ),
                      FittedBox(
                        fit: BoxFit.scaleDown,
                        child: Consumer<MemberVm>(builder: (context, vm, _){
                          return Text(
                            vm.memberModel!.isApprove == 0 ? '試用版' : '正式版(已授權)',
                            textScaleFactor: 1,
                            style: const TextStyle(fontSize: 23, color: Colors.white),
                          );
                        },),
                      )
                    ],
                  ))
            ],
          ))
        ],
      ),
    );
  }

  //主體
  Widget _bodyWidget(){
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      decoration: _homeBG,
      alignment: Alignment.center,
      child: Container(
        margin: const EdgeInsets.only(top:20, bottom: 20),
        width: MediaQuery.of(context).size.width / 1.1,
        height: MediaQuery.of(context).size.height,
        decoration: _centerBG,
        child: Column(
          children: [
            Container(
              width: 900,
              height: 60,
              margin: const EdgeInsets.symmetric(horizontal: 10,vertical: 10),
              padding: const EdgeInsets.all(5),
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(Radius.circular(10)),
                color: HexColor.fromHex('#F5F5F5'),
                boxShadow: const [
                  BoxShadow(
                    color: Colors.black38,
                    offset: Offset(5.0, 5.0),
                    blurRadius: 10.0,
                    spreadRadius: 2.0)
                ]),
              child: Row(
                children: [
                  Expanded(child: Container(
                    decoration: _currentPage == 0
                      ? BoxDecoration(
                      borderRadius: const BorderRadius.all(Radius.circular(10)),
                      gradient: LinearGradient(
                        colors: [
                          HexColor.fromHex('#0067B4'),
                          HexColor.fromHex('#003457'),
                        ],
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter))
                        : null,
                    child: Center(
                      child: TextButton(
                        onPressed: (){
                          _pageController.animateToPage(0, duration: const Duration(milliseconds: 300), curve: Curves.decelerate);
                        },
                        child: FittedBox(
                          fit: BoxFit.scaleDown,
                          child: Text(
                            getTranslated(context, 'Personal_information_modification'),
                            textScaleFactor: 1,
                            style: TextStyle(
                              color: _currentPage == 0
                                  ? Colors.white
                                  : HexColor.fromHex('#032137'))),
                        ),
                      ),
                    ),
                  )),
                  Expanded(child: Container(
                    decoration: _currentPage == 1
                      ? BoxDecoration(
                      borderRadius: const BorderRadius.all(Radius.circular(10)),
                      gradient: LinearGradient(
                        colors: [
                          HexColor.fromHex('#0067B4'),
                          HexColor.fromHex('#003457'),
                        ],
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter))
                        : null,
                    child: Center(
                      child: TextButton(
                        onPressed: (){
                          _pageController.animateToPage(1, duration: const Duration(milliseconds: 300), curve: Curves.decelerate);
                        },
                        child: FittedBox(
                          fit: BoxFit.scaleDown,
                          child: Text(
                            getTranslated(context, 'Change_Password'),
                            textScaleFactor: 1,
                            style: TextStyle(
                              color: _currentPage == 1
                                  ? Colors.white
                                  : HexColor.fromHex('#032137')
                            ),
                          ),
                        ),
                      ),
                    ),
                  ))
                ],
              ),
            ),
            Expanded(flex: 5, child: _pageView)
          ],
        ),
      ),
    );
  }
}