import 'dart:convert';
import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:light_silver/language/language_constants.dart';
import 'package:light_silver/models/member_model.dart';
import 'package:light_silver/utils/api.dart';
import 'package:light_silver/utils/utils.dart';
import 'package:light_silver/widgets/noShadowScrollbehavior.dart';
import 'package:provider/provider.dart';

import '../app.dart';
import '../view_model/member_vm.dart';

class PersonalInformationModificationPage extends StatefulWidget{

  @override
  State createState() => _PersonalInformationModificationPageState();
}

class _PersonalInformationModificationPageState extends State<PersonalInformationModificationPage>{
  MemberVm? _memberVm;
  final TextEditingController _name = TextEditingController();
  final TextEditingController _job = TextEditingController();
  final TextEditingController _phone = TextEditingController();
  final TextEditingController _mail = TextEditingController();

  final _bg = BoxDecoration(
    borderRadius: const BorderRadius.all(Radius.circular(10.0)),
    gradient: LinearGradient(colors: [
      HexColor.fromHex('#0067B4'),
      HexColor.fromHex('#003457'),
    ], begin: Alignment.topCenter, end: Alignment.bottomCenter));


  _onLayoutDone(_){
    if(_memberVm!.memberModel != null){
      _name.text = _memberVm!.memberModel!.name;
      _job.text = _memberVm!.memberModel!.title;
      _phone.text = _memberVm!.memberModel!.cellphone;
      _mail.text = _memberVm!.memberModel!.email;
    }
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  Widget build(BuildContext context) {
    _memberVm = Provider.of<MemberVm>(context);
    return Scaffold(
      backgroundColor: Colors.transparent,
      resizeToAvoidBottomInset: true,
      body: SafeArea(
        child: ScrollConfiguration(
          behavior: NoShadowScrollBehavior(),
          child: SingleChildScrollView(
            padding: const EdgeInsets.only(top: 23, left: 23, right: 23, bottom: 23),
            child: buildIndexTextForm(),
          ),
        ),
      ),
    );
  }

  Widget buildIndexTextForm(){
    return Container(
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(8)),
        color: Colors.white,
      ),
      height: MediaQuery.of(context).size.height / 1.7,
      child: Column(mainAxisAlignment: MainAxisAlignment.start,
        children: [
          const SizedBox(
            height: 5,
          ),
          Container(
            alignment: Alignment.centerLeft,
            child: FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                getTranslated(context, 'Name'),
                textScaleFactor: 1,
                style: TextStyle(
                  fontSize: 20,
                  color: HexColor.fromHex('#1E2C39'),
                  fontWeight: FontWeight.bold
                ),
              ),
            ),
          ),
          _buildName(),
          const SizedBox(
            height: 5,
          ),
          Container(
            alignment: Alignment.centerLeft,
            child: FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                getTranslated(context, 'JobTitle'),
                textScaleFactor: 1,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: HexColor.fromHex('#1E2C39'),
                  fontSize: 20,
                ),
              ),
            ),
          ),
          _buildJob(),
          const SizedBox(
            height: 5,
          ),
          Container(
            alignment: Alignment.centerLeft,
            child: FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                getTranslated(context, 'Phone'),
                textScaleFactor: 1,
                style: TextStyle(
                    fontSize: 20,
                  color: HexColor.fromHex('#1E2C39'),
                  fontWeight: FontWeight.bold
                ),
              ),
            ),
          ),
          _buildPhone(),
          const SizedBox(
            height: 5,
          ),
          Container(
            alignment: Alignment.centerLeft,
            child: FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                getTranslated(context, 'Mail'),
                textScaleFactor: 1,
                style: TextStyle(
                    fontSize: 20,
                  color: HexColor.fromHex('#1E2C39'),
                  fontWeight: FontWeight.bold
                ),
              ),
            ),
          ),
          _buildMail(),
          Container(
            margin: const EdgeInsets.only(top:40),
            alignment: Alignment.center,
            child: ButtonTheme(
              height: 110,
              child: Consumer<MemberVm>(builder: (context, vm, _){
                return ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      foregroundColor: Colors.white,
                      backgroundColor: Colors.transparent,
                      shape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10.0))),
                      padding: const EdgeInsets.all(0.0)
                  ),
                  onPressed: () => _confirmOnTap(vm.memberModel!.id),
                  child: Container(
                      height: 60,
                      width: 750,
                      alignment: Alignment.center,
                      decoration: _bg,
                      child: FittedBox(
                        fit: BoxFit.scaleDown,
                        child: Text(
                          getTranslated(context, 'Confirm_the_changes'),
                          textScaleFactor: 1,
                          style: const TextStyle(color: Colors.white, fontSize: 23),
                        ),
                      )),
                );
              },),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildName(){
    return TextField(
      style: const TextStyle(color: Colors.black),
      decoration: InputDecoration(
        errorText: null,
        contentPadding: const EdgeInsets.all(5.0),
        enabledBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Colors.black),
          borderRadius: BorderRadius.circular(10.0)),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(10.0)),
        filled: true,
        fillColor: Colors.white,
        hintText: getTranslated(context, 'Please_type_in_your_name'),
        hintStyle: const TextStyle(color: Colors.grey)),
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _name,
    );
  }
  Widget _buildJob(){
    return TextField(
      style: const TextStyle(color: Colors.black),
      decoration: InputDecoration(
          errorText: null,
          contentPadding: const EdgeInsets.all(5.0),
          enabledBorder: OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.black),
              borderRadius: BorderRadius.circular(10.0)),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10.0)),
          filled: true,
          fillColor: Colors.white,
          hintText: getTranslated(context, 'Please_enter_a_title'),
          hintStyle: const TextStyle(color: Colors.grey)),
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _job,
    );
  }
  Widget _buildPhone(){
    return TextField(
      style: const TextStyle(color: Colors.black),
      decoration: InputDecoration(
          errorText: null,
          contentPadding: const EdgeInsets.all(5.0),
          enabledBorder: OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.black),
              borderRadius: BorderRadius.circular(10.0)),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10.0)),
          filled: true,
          fillColor: Colors.white,
          hintText: getTranslated(context, 'Please_enter_mobile_phone'),
          hintStyle: const TextStyle(color: Colors.grey)),
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _phone,
    );
  }
  Widget _buildMail(){
    return TextField(
      style: const TextStyle(color: Colors.black),
      decoration: InputDecoration(
          errorText: null,
          contentPadding: const EdgeInsets.all(5.0),
          enabledBorder: OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.black),
              borderRadius: BorderRadius.circular(10.0)),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10.0)),
          filled: true,
          fillColor: Colors.white,
          hintText: getTranslated(context, 'Please_enter_mail'),
          hintStyle: const TextStyle(color: Colors.grey)),
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _mail,
    );
  }

  void _confirmOnTap(int id) async {
    EasyLoading.show(status: '更新中');
    debugPrint('確認修改');
    Map<String, dynamic> map = {
      'name':_name.text,
      'title':_job.text,
      'cellphone':_phone.text,
      'email':_mail.text
    };
    try{
      dynamic response = await httpUtils.put(
        '$getMember$id',
        data: map,
        options: Options(
            contentType: 'application/json',
            responseType: ResponseType.json,
            headers: {"authorization": "Bearer $accessToken"}),
      );
      EasyLoading.dismiss(animation: true);
      EasyLoading.showSuccess('更新完成');
      _getMember(id);
    } on DioError catch(e){
      EasyLoading.dismiss(animation: true);
      EasyLoading.showError('更新失敗');
    }
  }
  //取得用戶資料
  void _getMember(int id) async {
    try{
      dynamic response = await httpUtils.get(
          '$getMember$id',
        options: Options(
            contentType: 'application/json',
            responseType: ResponseType.json,
            headers: {"authorization": "Bearer $accessToken"}),
      );
      log('用戶資訊 $response');
      _memberVm!.setMemberModel(MemberModel.fromJson(json.decode(json.encode(response))));
      delegate.popRoute();
    }on DioError catch (e){
      EasyLoading.dismiss();
      EasyLoading.showError(e.message);
      debugPrint('用戶資料Error: ${e.message}');
    }
  }
}