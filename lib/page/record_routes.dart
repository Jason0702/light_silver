import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:light_silver/database/database_helper.dart';
import 'package:light_silver/language/language_constants.dart';
import 'package:light_silver/models/patient.dart';
import 'package:light_silver/utils/utils.dart';

import '../widgets/left_button.dart';

class RecordRoutes extends StatefulWidget{

  @override
  State createState() => _RecordRoutesState();
}
class _RecordRoutesState extends State<RecordRoutes>{

  late DatabaseHelper dbHelper;
  List<Patient> patient =[];

  final _titleBarBG = BoxDecoration(
      gradient: LinearGradient(
          colors: [HexColor.fromHex('#0067B4'), HexColor.fromHex('#003457')],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter));



  _onLayoutDone(_){
    dbHelper.getAllPatient().then((value) => {
      _dataInit(value)
    });
  }

  void _dataInit(List<Patient> value){
    patient = value;
    setState(() {

    });
    for (var element in patient) {
      log(element.toString());
    }
  }

  @override
  void initState() {
    super.initState();
    dbHelper = DatabaseHelper();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Column(
          children: [
            Expanded(flex: 1, child: _titleBar()),
            Expanded(flex: 10, child: _bodyWidget()),
          ],
        ),
      ),
    );
  }

  //最上層標題與左側返回
  Widget _titleBar(){
    return Container(
      decoration: _titleBarBG,
      child: Row(
        children: [
          const Expanded(flex: 1, child: LeftButton()),
          Expanded(flex: 5, child: Container(
            alignment: Alignment.center,
            child: FittedBox(fit: BoxFit.scaleDown,child: Text(getTranslated(context, 'Record'), textScaleFactor: 1, style: const TextStyle(color: Colors.white, fontSize: 25),)),
          )),
          Expanded(flex: 1,
              child: Container()),
        ],
      ),
    );
  }

  //主體
  Widget _bodyWidget(){
    return Container(
      color: Colors.white,
      child: ListView.builder(
          itemCount: patient.length,
          itemBuilder: (context, index){
            return ListTile(
              leading: Image.asset('assets/images/blank_file.png', width: 30,),
              title: FittedBox(fit: BoxFit.scaleDown,child: Text(patient[index].name)),
            );
          }
      ),
    );
  }
}