import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:light_silver/language/language_constants.dart';
import 'package:light_silver/utils/shared_preference_util.dart';
import 'package:light_silver/view_model/user_company_information_vm.dart';
import 'package:light_silver/widgets/noShadowScrollbehavior.dart';
import 'package:provider/provider.dart';

import '../view_model/member_vm.dart';

class PreviewSettingsPage extends StatefulWidget {
  @override
  State createState() => _PreviewSettingsPageState();
}

class _PreviewSettingsPageState extends State<PreviewSettingsPage> {
  MemberVm? _memberVm;
  UserCompanyInformationVm? _userCompanyInformationVm;
  final TextEditingController _companyName = TextEditingController();
  final TextEditingController _operator = TextEditingController();
  final TextEditingController _companyAddress = TextEditingController();
  final TextEditingController _companyPhone = TextEditingController();
  String _companyLOGO = '';

  late SharedPreferenceUtil _sharedPreferenceUtil;

  _onLayoutDone(_) {
    _companyName.text = _userCompanyInformationVm!.userCompanyInformationModel!.name;
    _companyAddress.text =
        '${_userCompanyInformationVm!.userCompanyInformationModel!.city}${_userCompanyInformationVm!.userCompanyInformationModel!.area}${_userCompanyInformationVm!.userCompanyInformationModel!.address}';
    _companyPhone.text = _userCompanyInformationVm!.userCompanyInformationModel!.phone;
    _companyLOGO = _userCompanyInformationVm!.userCompanyInformationModel!.logo;
    _operator.text = _memberVm!.memberModel!.name;
  }

  @override
  void initState() {
    super.initState();
    _sharedPreferenceUtil = SharedPreferenceUtil();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  Widget build(BuildContext context) {
    _memberVm = Provider.of<MemberVm>(context);
    _userCompanyInformationVm = Provider.of<UserCompanyInformationVm>(context);
    return Scaffold(
      backgroundColor: Colors.transparent,
      resizeToAvoidBottomInset: true,
      body: SafeArea(
        child: ScrollConfiguration(
          behavior: NoShadowScrollBehavior(),
          child: SingleChildScrollView(
            padding:
                const EdgeInsets.only(top: 23, left: 23, right: 23, bottom: 23),
            child: buildIndexTextFrom(),
          ),
        ),
      ),
    );
  }

  Widget buildIndexTextFrom() {
    return Container(
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(8)),
        color: Colors.white,
      ),
      height: MediaQuery.of(context).size.height / 1,
      child: Column(
        children: [
          Container(
            alignment: Alignment.centerLeft,
            child: FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                getTranslated(context, 'Company_name'),
                textScaleFactor: 1,
                style:
                    const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
            ),
          ),
          _buildTheCompanyName(),
          const SizedBox(
            height: 20,
          ),
          Container(
            alignment: Alignment.centerLeft,
            child: FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                getTranslated(context, 'Operator'),
                textScaleFactor: 1,
                style:
                    const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
            ),
          ),
          _buildName(),
          const SizedBox(
            height: 20,
          ),
          Container(
            alignment: Alignment.centerLeft,
            child: FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                getTranslated(context, 'Company_address'),
                textScaleFactor: 1,
                style:
                    const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
            ),
          ),
          _buildTheCompanyAddress(),
          const SizedBox(
            height: 20,
          ),
          Container(
            alignment: Alignment.centerLeft,
            child: FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                getTranslated(context, 'Work_phone'),
                textScaleFactor: 1,
                style:
                    const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
            ),
          ),
          _buildTheCompanyPhone(),
          const SizedBox(
            height: 20,
          ),
          Container(
            alignment: Alignment.centerLeft,
            child: FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                getTranslated(context, 'Company_LOGO'),
                textScaleFactor: 1,
                style:
                    const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
            ),
          ),
          _buildTheCompanyLOGO(),
        ],
      ),
    );
  }

  //公司名稱
  Widget _buildTheCompanyName() {
    return TextField(
      readOnly: true,
      style: const TextStyle(color: Colors.black),
      decoration: InputDecoration(
          errorText: null,
          contentPadding: const EdgeInsets.all(5.0),
          enabledBorder: OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.black),
              borderRadius: BorderRadius.circular(10.0)),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10.0)),
          filled: true,
          fillColor: Colors.white,
          hintText: getTranslated(context, 'Please_enter_company_name'),
          hintStyle: const TextStyle(color: Colors.grey)),
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _companyName,
    );
  }

  //操作人員
  Widget _buildName() {
    return TextField(
      readOnly: true,
      style: const TextStyle(color: Colors.black),
      decoration: InputDecoration(
          errorText: null,
          contentPadding: const EdgeInsets.all(5.0),
          enabledBorder: OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.black),
              borderRadius: BorderRadius.circular(10.0)),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10.0)),
          filled: true,
          fillColor: Colors.white,
          hintText:
              getTranslated(context, 'Please_enter_the_name_of_the_operator'),
          hintStyle: const TextStyle(color: Colors.grey)),
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _operator,
    );
  }

  //公司地址
  Widget _buildTheCompanyAddress() {
    return TextField(
      readOnly: true,
      style: const TextStyle(color: Colors.black),
      decoration: InputDecoration(
          errorText: null,
          contentPadding: const EdgeInsets.all(5.0),
          enabledBorder: OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.black),
              borderRadius: BorderRadius.circular(10.0)),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10.0)),
          filled: true,
          fillColor: Colors.white,
          hintText: getTranslated(context, 'Please_enter_the_address'),
          hintStyle: const TextStyle(color: Colors.grey)),
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _companyAddress,
    );
  }

  //公司電話
  Widget _buildTheCompanyPhone() {
    return TextField(
      readOnly: true,
      style: const TextStyle(color: Colors.black),
      decoration: InputDecoration(
          errorText: null,
          contentPadding: const EdgeInsets.all(5.0),
          enabledBorder: OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.black),
              borderRadius: BorderRadius.circular(10.0)),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10.0)),
          filled: true,
          fillColor: Colors.white,
          hintText: getTranslated(context, 'Please_enter_company_phone'),
          hintStyle: const TextStyle(color: Colors.grey)),
      keyboardType: TextInputType.phone,
      textInputAction: TextInputAction.next,
      controller: _companyPhone,
    );
  }

  //公司LOGO
  Widget _buildTheCompanyLOGO() {
    log('公司LOGO ${_userCompanyInformationVm!.userCompanyInformationModel!.logo}');
    return Image.network(_userCompanyInformationVm!.userCompanyInformationModel?.logo ?? '');
  }

  void _saveOnTap() {
    debugPrint('儲存');
    if (_companyName.text.isNotEmpty) {
      _sharedPreferenceUtil.saveCompanyName(_companyName.text);
    }
    if (_operator.text.isNotEmpty) {
      _sharedPreferenceUtil.saveOperator(_operator.text);
    }
    if (_companyPhone.text.isNotEmpty) {
      _sharedPreferenceUtil.saveCompanyPhone(_companyPhone.text);
    }
    if (_companyAddress.text.isNotEmpty) {
      _sharedPreferenceUtil.saveCompanyAddress(_companyAddress.text);
    }
    if (_companyLOGO.isNotEmpty) {
      _sharedPreferenceUtil.saveCompanyLOGO(_companyLOGO);
    }
    EasyLoading.showToast(getTranslated(context, 'success'));
  }
}
