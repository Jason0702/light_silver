import 'package:flutter/material.dart';
import 'package:light_silver/language/language_constants.dart';
import 'package:light_silver/utils/utils.dart';
import 'package:light_silver/widgets/noShadowScrollbehavior.dart';

import '../enum/language.dart';
import '../enum/sound.dart';

class BasicSettingsPage extends StatefulWidget{

  @override
  State createState() => _BasicSettingsPageState();
}
class _BasicSettingsPageState extends State<BasicSettingsPage>{

  Language _language = Language.TraditionalChinese;
  Sound _sound = Sound.TraditionalChinese;

  final _bg = BoxDecoration(
      borderRadius: const BorderRadius.all(Radius.circular(10.0)),
      gradient: LinearGradient(colors: [
        HexColor.fromHex('#0067B4'),
        HexColor.fromHex('#003457'),
      ], begin: Alignment.topCenter, end: Alignment.bottomCenter));

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: buildIndexTextFrom(),
      ),
    );
  }

  Widget buildIndexTextFrom(){
    return ScrollConfiguration(
      behavior: NoShadowScrollBehavior(),
      child: SingleChildScrollView(
        child: Container(
          margin: const EdgeInsets.only(top: 0,left: 25, right: 25),
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(8)),
            color: Colors.transparent,
          ),
          height: MediaQuery.of(context).size.height / 1.2,
          child: Column(
            children: [
              //語系選擇
              Container(
                alignment: Alignment.centerLeft,
                child: FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    getTranslated(context, 'Language_selection'),
                    textScaleFactor: 1,
                    style: const TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold
                    ),
                  ),
                ),
              ),
              Column(
                children: [
                  _languageWidget(Language.TraditionalChinese, 'assets/images/taiwan.png', 'Traditional_Chinese'),
                  _languageWidget(Language.SimplifiedChinese, 'assets/images/china.png', 'Simplified_Chinese'),
                  _languageWidget(Language.English, 'assets/images/english.png', 'English'),
                  _languageWidget(Language.Japanese, 'assets/images/japan.png', 'Japanese')
                ],
              ),
              //聲音
              Container(
                alignment: Alignment.centerLeft,
                child: FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    getTranslated(context, 'Sound_language'),
                    textScaleFactor: 1,
                    style: const TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold
                    ),
                  ),
                ),
              ),
              Column(
                children: [
                  _soundWidget(Sound.TraditionalChinese, 'assets/images/taiwan.png', 'Chinese'),
                  _soundWidget(Sound.SimplifiedChinese, 'assets/images/china.png', 'Mandarin'),
                  _soundWidget(Sound.English, 'assets/images/english.png', 'English'),
                  _soundWidget(Sound.Japanese, 'assets/images/japan.png', 'Japanese')
                ],
              ),
              const SizedBox(height: 10,),
              //按鈕
              Container(
                alignment: Alignment.center,
                child: ButtonTheme(
                  height: 110,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        foregroundColor: Colors.white,
                        backgroundColor: Colors.transparent,
                        shape: const RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(10.0))),
                        padding: const EdgeInsets.all(0.0)
                    ),
                    onPressed: _saveOnTap,
                    child: Container(
                        height: 55,
                        width: 750,
                        alignment: Alignment.center,
                        decoration: _bg,
                        child: FittedBox(
                          fit: BoxFit.scaleDown,
                          child: Text(
                            getTranslated(context, 'Save'),
                            textScaleFactor: 1,
                            style: const TextStyle(color: Colors.white, fontSize: 20),
                          ),
                        )),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
  //語言選擇按鈕
  Widget _languageWidget(Language value, String image, String key){
    return Row(
      children: [
        Expanded(flex: 1, child: Radio(
          activeColor: HexColor.fromHex('#1E2C39'),
          value: value,
          groupValue: _language,
          onChanged: (Language? languageValue){
            setState(() {
              _language = languageValue!;
            });
          },
        )),
        Expanded(flex: 1, child: SizedBox(
          width: 40,
          child: Image.asset(image),
        )),
        Expanded(flex: 5,
            child: FittedBox(
              alignment: Alignment.centerLeft,
              fit: BoxFit.scaleDown,
              child: Text(
                ' ${getTranslated(context, key)}(${key.replaceAll('_', ' ')})',
                textScaleFactor: 1,
                style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold, color: HexColor.fromHex('#1E2C39')),),
            ))
      ],
    );
  }
  //聲音選擇按鈕
  Widget _soundWidget(Sound value, String image, String key){
    return Row(
      children: [
        Expanded(flex: 1, child: Radio(
          activeColor: HexColor.fromHex('#1E2C39'),
          value: value,
          groupValue: _sound,
          onChanged: (Sound? soundValue){
            setState(() {
              _sound = soundValue!;
            });
          },
        )),
        Expanded(flex: 1, child: SizedBox(
          width: 40,
          child: Image.asset(image),
        )),
        Expanded(flex: 5,
            child: FittedBox(
              alignment: Alignment.centerLeft,
              fit: BoxFit.scaleDown,
              child: Text(
                ' ${getTranslated(context, key)}($key)',
                textScaleFactor: 1,
                style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold, color: HexColor.fromHex('#1E2C39'),),),
            )
            )
      ],
    );
  }

  void _saveOnTap(){
    debugPrint('儲存');
  }
}