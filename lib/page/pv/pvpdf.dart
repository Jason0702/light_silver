import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:light_silver/database/database_helper.dart';
import 'package:light_silver/language/language_constants.dart';
import 'package:light_silver/models/patient.dart';
import 'package:light_silver/utils/shared_preference_util.dart';
import 'package:light_silver/utils/utils.dart';
import 'package:light_silver/view_model/user_company_information_vm.dart';
import 'package:light_silver/widgets/noShadowScrollbehavior.dart';

import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:printing/printing.dart';
import 'package:provider/provider.dart';
import 'dart:ui' as dart_ui;

import '../../view_model/member_vm.dart';
import '../../view_model/patient_vm.dart';
import '../../widgets/left_button.dart';
import '../../widgets/title_widget.dart';

class PVPDFRoute extends StatefulWidget {
  @override
  State createState() => _PVPDFRouteState();
}

class _PVPDFRouteState extends State<PVPDFRoute> {
  PatientVm? _patientVm;
  MemberVm? _memberVm;
  UserCompanyInformationVm? _userCompanyInformationVm;
  final GlobalKey _printKey = GlobalKey();

  //資料框
  final _inputBg = BoxDecoration(
      border: Border.all(color: Colors.grey, width: 1.5),
      borderRadius: const BorderRadius.all(Radius.circular(10)));
  //資料title
  final _dataTitleBg = BoxDecoration(
      borderRadius: const BorderRadius.only(
          topLeft: Radius.circular(10), topRight: Radius.circular(10)),
      color: HexColor.fromHex('#096BC7'));


  String _companyName = '載入中';
  String _companyPhone = '載入中';
  String _companyAddress = '載入中';
  String _companyLOGO = '';

  final TextEditingController _patientName = TextEditingController();
  final TextEditingController _patientGender = TextEditingController();
  final TextEditingController _patientAge = TextEditingController();
  final TextEditingController _patientPhone = TextEditingController();
  final TextEditingController _patientProblem = TextEditingController();

  final TextEditingController _problemText = TextEditingController();
  final TextEditingController _problemSideText = TextEditingController();
  bool _pad = false;
  final TextEditingController _padHigh = TextEditingController();

  final TextEditingController _floorResult = TextEditingController();
  final TextEditingController _sideKneeLeftResult = TextEditingController();
  final TextEditingController _sideKneeRightResult = TextEditingController();

  final TextEditingController _summary1 = TextEditingController();

  final TextEditingController _floorRecheckResult = TextEditingController();
  final TextEditingController _sideKneeLeftRecheckResult = TextEditingController();
  final TextEditingController _sideKneeRightRecheckResult = TextEditingController();

  final TextEditingController _summary2 = TextEditingController();

  //減緩不適症狀
  bool _relieveSymptomsOfDiscomfort = false;
  //姿勢矯正
  bool _postureCorrection = false;
  //資訊衛教
  bool _informationHealthEducation = false;
  //關節復位
  bool _jointReduction = false;
  //核心肌群訓練
  bool _coreMuscleTraining = false;
  //健康促進
  bool _healthPromotion = false;

  List<String> items = ["F", "M"];

  _onLayoutDone(_) {
    _dataInit(_patientVm!.patient);
  }

  void _dataInit(Patient? patient) {
    if (patient == null) {
      return;
    }
    log('取得 預覽(1): $patient');
    _companyName = _userCompanyInformationVm!.userCompanyInformationModel!.name;
    _companyPhone = _userCompanyInformationVm!.userCompanyInformationModel!.phone.isEmpty ? '無' : _userCompanyInformationVm!.userCompanyInformationModel!.phone;
    _companyAddress =
        '${_userCompanyInformationVm!.userCompanyInformationModel!.city} ${_userCompanyInformationVm!.userCompanyInformationModel!.area}${_userCompanyInformationVm!.userCompanyInformationModel!.address}';
    _companyLOGO = _userCompanyInformationVm!.userCompanyInformationModel!.logo;

    _patientName.text = _patientVm!.patient!.name;
    _patientGender.text = _patientVm!.patient!.gender;
    _patientAge.text = _patientVm!.patient!.age.toString();
    _patientPhone.text = _patientVm!.patient!.phone;
    _patientProblem.text = _patientVm!.patient!.problem;
    _relieveSymptomsOfDiscomfort =
    _patientVm!.patient!.relieveSymptomsOfDiscomfort == 1 ? true : false;
    _postureCorrection = _patientVm!.patient!.postureCorrection == 1 ? true : false;
    _informationHealthEducation =
    _patientVm!.patient!.informationHealthEducation == 1 ? true : false;
    _jointReduction = _patientVm!.patient!.jointReduction == 1 ? true : false;
    _coreMuscleTraining = _patientVm!.patient!.coreMuscleTraining == 1 ? true : false;
    _healthPromotion = _patientVm!.patient!.healthPromotion == 1 ? true : false;
    switch (_patientVm!.patient!.GT) {
      case 0:
        _problemText.text =
        '${getTranslated(context, 'Foot_length')}${getTranslated(context, 'Equal_length')}';
        break;
      case 1:
        _problemText.text =
        '${getTranslated(context, 'Foot_length')}${getTranslated(context, 'Unequal_length')}';
        break;
      case 2:
        _problemText.text =
        '${getTranslated(context, 'Foot_length')}${getTranslated(context, 'Equal_length')}';
        break;
      case 3:
        _problemText.text =
        '${getTranslated(context, 'Foot_length')}${getTranslated(context, 'Unequal_length')}';
        break;
    }
    switch (_patientVm!.patient!.pad) {
      case 0:
        _problemSideText.text = '';
        break;
      case 1:
        _problemSideText.text = getTranslated(context, 'left');
        break;
      case 2:
        _problemSideText.text = '';
        break;
      case 3:
        _problemSideText.text = getTranslated(context, 'right');
        break;
    }
    if (_patientVm!.patient!.pad != 2) {
      _pad = false;
    } else {
      _pad = true;
    }
    _padHigh.text = _patientVm!.patient!.padHigh;

    _floorResult.text = _patientVm!.patient!.floor;
    _sideKneeLeftResult.text = _patientVm!.patient!.side_knee_left;
    _sideKneeRightResult.text = _patientVm!.patient!.side_knee_right;

    _floorRecheckResult.text = _patientVm!.patient!.floor_recheck;
    _sideKneeLeftRecheckResult.text = _patientVm!.patient!.side_knee_left_recheck;
    _sideKneeRightRecheckResult.text = _patientVm!.patient!.side_knee_right_recheck;
    setState(() {});
    Future.delayed(const Duration(seconds: 3), () {
      _print();
    });
  }

  Future<Uint8List> _renderCartesianImage() async {
    RenderRepaintBoundary? _cartesianKey =
        _printKey.currentContext!.findRenderObject() as RenderRepaintBoundary?;
    dart_ui.Image data = await _cartesianKey!.toImage(pixelRatio: 3.0);
    final bytes = await data.toByteData(format: dart_ui.ImageByteFormat.png);
    return bytes!.buffer.asUint8List(bytes.offsetInBytes, bytes.lengthInBytes);
  }

  void _print() async {
    Uint8List chartUint8 = await _renderCartesianImage();

    Uint8List _titleBg =
        (await rootBundle.load('assets/images/record_sheet_title.png'))
            .buffer
            .asUint8List();
    Uint8List _bodyImage = (await rootBundle.load('assets/images/body.png')).buffer.asUint8List();
    Uint8List _painImage = (await rootBundle.load('assets/images/pain_level.png')).buffer.asUint8List();
    Uint8List _resultImage = (await rootBundle.load('assets/images/result_blue.png')).buffer.asUint8List();
    Printing.layoutPdf(onLayout: (PdfPageFormat format) async {
      final doc = pw.Document();
      doc.addPage(pw.MultiPage(
               maxPages: 9,
          theme: pw.ThemeData.withFont(
            base: pw.Font.ttf(await rootBundle.load("assets/fonts/msjh.ttf")),
          ),
          crossAxisAlignment: pw.CrossAxisAlignment.start,
          //region 頁首
          header: (pw.Context context) {
            if (context.pageNumber != 1) {
              return pw.Container();
            }
            return pw.Container(
                height: 70,
                alignment: pw.Alignment.center,
                decoration: pw.BoxDecoration(
                    image: pw.DecorationImage(
                  image: pw.MemoryImage(_titleBg),
                )),
                child: pw.Text('光銀式手法評估與調整紀錄',
                    style: pw.Theme.of(context)
                        .defaultTextStyle
                        .copyWith(fontSize: 30, color: PdfColors.white)));
          },
          //endregion
          //region 頁尾
          footer: (pw.Context context) {
            return pw.Column(
                crossAxisAlignment: pw.CrossAxisAlignment.center,
                mainAxisAlignment: pw.MainAxisAlignment.center,
                children: [
                  pw.Center(
                      child: pw.Text(
                          '${context.pageNumber}/${context.pagesCount}'))
                ]);
          },
          //endregion
          build: (pw.Context context) => [
              //region 第一列
              pw.Row(
                  mainAxisAlignment: pw.MainAxisAlignment.spaceAround,
                children: [
                  //region 公司資訊
                  pw.Expanded(
                    child: pw.Container(
                      height: 70,
                      margin: const pw.EdgeInsets.only(top: 8),
                      decoration: pw.BoxDecoration(
                        border: pw.Border.all(color: PdfColors.grey, width: 1.5),
                        borderRadius: const pw.BorderRadius.all(pw.Radius.circular(10)),
                      ),
                      child: pw.Row(
                        children: [
                          pw.Container(
                            margin: const pw.EdgeInsets.only(left: 8),
                            child: pw.Column(
                              mainAxisAlignment: pw.MainAxisAlignment.center,
                              crossAxisAlignment: pw.CrossAxisAlignment.start,
                              children: [
                                pw.Text(
                                  _companyName,
                                  textScaleFactor: 1,
                                ),
                                pw.Text(
                                  _companyPhone,
                                  textScaleFactor: 1,
                                ),
                                pw.Text(
                                  _companyAddress,
                                  textScaleFactor: 1,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  //endregion
                  //region 診察與治療日期
                  pw.Container()
                  //endregion
                ]
              ),
              //endregion
              //region 第二列
              pw.Row(
                mainAxisAlignment: pw.MainAxisAlignment.spaceAround,
                children: [
                  //region 病人姓名
                  pw.Expanded(
                    child: pw.Container(
                        margin: const pw.EdgeInsets.only(top: 8),
                        child: pw.Column(
                          crossAxisAlignment: pw.CrossAxisAlignment.start,
                          children: [
                            pw.Container(
                              child: pw.Text(
                                '姓名',
                                style: pw.Theme.of(context)
                                    .defaultTextStyle
                                    .copyWith(fontSize: 20, color: PdfColors.black),
                              ),
                            ),
                            pw.Container(
                                alignment: pw.Alignment.centerLeft,
                                padding: const pw.EdgeInsets.only(top: 5, bottom: 5, left: 10, right: 10),
                                decoration: pw.BoxDecoration(
                                  border: pw.Border.all(color: PdfColors.grey, width: 1.5),
                                  borderRadius: const pw.BorderRadius.all(pw.Radius.circular(10)),),
                                child: pw.Text(_patientName.text,style: pw.Theme.of(context)
                                    .defaultTextStyle
                                    .copyWith(fontSize: 15, color: PdfColors.black))
                            ),
                          ],
                        )),
                  ),

                  //endregion
                  //region 病人性別
                  pw.Expanded(
                    child: pw.Container(
                        margin: const pw.EdgeInsets.only(top: 8),
                        child: pw.Column(
                          crossAxisAlignment: pw.CrossAxisAlignment.start,
                          children: [
                            pw.Container(
                              child: pw.Text(
                                '性別',
                                style: pw.Theme.of(context)
                                    .defaultTextStyle
                                    .copyWith(fontSize: 20, color: PdfColors.black),
                              ),
                            ),
                            pw.Container(
                                alignment: pw.Alignment.centerLeft,
                                padding: const pw.EdgeInsets.only(top: 5, bottom: 5, left: 10, right: 10),
                                decoration: pw.BoxDecoration(
                                  border: pw.Border.all(color: PdfColors.grey, width: 1.5),
                                  borderRadius: const pw.BorderRadius.all(pw.Radius.circular(10)),),
                                child: pw.Text(_patientGender.text,style: pw.Theme.of(context)
                                    .defaultTextStyle
                                    .copyWith(fontSize: 15, color: PdfColors.black))
                            ),
                          ],
                        )),
                  )

                  //endregion
                ]
              ),
            //endregion
              //region 第三列
            pw.Row(
                mainAxisAlignment: pw.MainAxisAlignment.spaceAround,
                children: [
                  //region 病人年齡
                  pw.Expanded(
                    child: pw.Container(
                        margin: const pw.EdgeInsets.only(top: 8),
                        child: pw.Column(
                          crossAxisAlignment: pw.CrossAxisAlignment.start,
                          children: [
                            pw.Container(
                              child: pw.Text(
                                '年齡',
                                style: pw.Theme.of(context)
                                    .defaultTextStyle
                                    .copyWith(fontSize: 20, color: PdfColors.black),
                              ),
                            ),
                            pw.Container(
                                alignment: pw.Alignment.centerLeft,
                                padding: const pw.EdgeInsets.only(top: 5, bottom: 5, left: 10, right: 10),
                                decoration: pw.BoxDecoration(
                                  border: pw.Border.all(color: PdfColors.grey, width: 1.5),
                                  borderRadius: const pw.BorderRadius.all(pw.Radius.circular(10)),),
                                child: pw.Text(_patientAge.text,style: pw.Theme.of(context)
                                    .defaultTextStyle
                                    .copyWith(fontSize: 15, color: PdfColors.black))
                            ),
                          ],
                        )),
                  ),
                  //endregion
                  //region 病人電話
                  pw.Expanded(
                    child: pw.Container(
                        margin: const pw.EdgeInsets.only(top: 8),
                        child: pw.Column(
                          crossAxisAlignment: pw.CrossAxisAlignment.start,
                          children: [
                            pw.Container(
                              child: pw.Text(
                                '電話',
                                style: pw.Theme.of(context)
                                    .defaultTextStyle
                                    .copyWith(fontSize: 20, color: PdfColors.black),
                              ),
                            ),
                            pw.Container(
                                alignment: pw.Alignment.centerLeft,
                                padding: const pw.EdgeInsets.only(top: 5, bottom: 5, left: 10, right: 10),
                                decoration: pw.BoxDecoration(
                                  border: pw.Border.all(color: PdfColors.grey, width: 1.5),
                                  borderRadius: const pw.BorderRadius.all(pw.Radius.circular(10)),),
                                child: pw.Text(_patientPhone.text,style: pw.Theme.of(context)
                                    .defaultTextStyle
                                    .copyWith(fontSize: 15, color: PdfColors.black))
                            ),
                          ],
                        )),
                  ),
                  //endregion
                ]
            ),
            //endregion
              //region 問題
            pw.Container(
              height: 90,
              margin: const pw.EdgeInsets.only(top: 8),
              child: pw.Column(
                children: [
                  pw.Expanded(
                    flex: 1,
                    child: pw.Container(
                      alignment: pw.Alignment.center,
                      decoration: pw.BoxDecoration(
                          borderRadius: const pw.BorderRadius.only(
                              topLeft: pw.Radius.circular(10),
                              topRight: pw.Radius.circular(10)),
                          color: PdfColor.fromHex('#096BC7')),
                      child: pw.Text(
                        '問題',
                        textScaleFactor: 1,
                        style:
                        const pw.TextStyle(fontSize: 25,color: PdfColors.white),
                      ),
                    ),
                  ),
                  pw.Expanded(
                    flex: 2,
                    child: pw.Container(
                        alignment: pw.Alignment.topLeft,
                        decoration: pw.BoxDecoration(
                            borderRadius: const pw.BorderRadius.only(
                              bottomRight: pw.Radius.circular(10),
                              bottomLeft: pw.Radius.circular(10)
                            ),
                            border: pw.Border.all(color: PdfColors.grey)),
                        child: pw.Text(_patientProblem.text, style: const pw.TextStyle(fontSize: 15))
                    ),
                  ),
                ],
              ),
            ),
              //endregion
              //region 目標
            pw.Container(
              height:  130,
              margin: const pw.EdgeInsets.only(top: 8),
              child: pw.Column(
                mainAxisAlignment: pw.MainAxisAlignment.spaceAround,
                children: [
                  pw.Expanded(
                    flex: 1,
                    child: pw.Container(
                      alignment: pw.Alignment.center,
                      decoration: pw.BoxDecoration(
                          borderRadius: const pw.BorderRadius.only(
                              topLeft: pw.Radius.circular(10),
                              topRight: pw.Radius.circular(10)),
                          color: PdfColor.fromHex('#096BC7')),
                      child: pw.Text(
                        '目標',
                        textScaleFactor: 1,
                        style:
                        const pw.TextStyle(fontSize: 25,color: PdfColors.white),
                      ),
                    ),
                  ),
                  pw.Expanded(
                    flex: 3,
                    child: pw.Container(
                        padding: const pw.EdgeInsets.all(8),
                        alignment: pw.Alignment.topLeft,
                        decoration: pw.BoxDecoration(
                            borderRadius: const pw.BorderRadius.only(
                                bottomRight: pw.Radius.circular(10),
                                bottomLeft: pw.Radius.circular(10)
                            ),
                            border: pw.Border.all(color: PdfColors.grey)),
                        child: pw.Column(
                          children: [
                            //region 減緩不適症狀 姿勢矯正
                            pw.Row(
                              mainAxisAlignment: pw.MainAxisAlignment.spaceAround,
                              children: [
                                //region 減緩不適症狀
                                pw.Expanded(
                                  child: pw.Row(
                                    children: [
                                      pw.Transform.scale(
                                        scale: 1.5,
                                        child: pw.Checkbox(
                                          name: '',
                                          value: _relieveSymptomsOfDiscomfort,
                                        ),
                                      ),
                                      pw.SizedBox(width: 5),
                                      pw.Text(
                                        '減緩不適症狀',
                                        textScaleFactor: 1,
                                        style: const pw.TextStyle(
                                            fontSize: 15),
                                      )
                                    ],
                                  ),
                                ),
                                //endregion
                                //region 姿勢矯正
                                pw.Expanded(
                                  child: pw.Row(
                                    children: [
                                      pw.Transform.scale(
                                        scale: 1.5,
                                        child: pw.Checkbox(
                                          name: '',
                                          value: _postureCorrection,
                                        ),
                                      ),
                                      pw.SizedBox(width: 5),
                                      pw.Text(
                                        '姿勢矯正',
                                        textScaleFactor: 1,
                                        style: pw.TextStyle(
                                          fontSize: 20,),
                                      )
                                    ],
                                  ),
                                ),
                                //endregion
                              ]
                            ),
                            //endregion
                            //region 資訊衛教 關節復位
                            pw.Row(
                              mainAxisAlignment: pw.MainAxisAlignment.spaceAround,
                              children: [
                                //資訊衛教
                                pw.Expanded(
                                  child: pw.Row(
                                    children: [
                                      pw.Transform.scale(
                                        scale: 1.5,
                                        child: pw.Checkbox(
                                          name: '',
                                          value: _informationHealthEducation,
                                        ),
                                      ),
                                      pw.SizedBox(width: 5),
                                      pw.Text(
                                        '資訊衛教',
                                        textScaleFactor: 1,
                                        style: pw.TextStyle(
                                            fontSize: 20),
                                      )
                                    ],
                                  ),
                                ),
                                //關節復位
                                pw.Expanded(
                                  child: pw.Row(
                                    children: [
                                      pw.Transform.scale(
                                        scale: 1.5,
                                        child: pw.Checkbox(
                                          name: '',
                                          value: _jointReduction,
                                        ),
                                      ),
                                      pw.SizedBox(width: 5),
                                      pw.Text(
                                        '關節復位',
                                        textScaleFactor: 1,
                                        style: pw.TextStyle(
                                            fontSize: 20),
                                      )
                                    ],
                                  ),
                                ),
                              ]
                            ),
                            //endregion
                            //region 核心肌群訓練 健康促進
                            pw.Row(
                              mainAxisAlignment: pw.MainAxisAlignment.spaceAround,
                              children: [
                                //核心肌群訓練
                                pw.Expanded(
                                  child: pw.Row(
                                    children: [
                                      pw.Transform.scale(
                                        scale: 1.5,
                                        child: pw.Checkbox(
                                          name: '',
                                          value: _coreMuscleTraining,
                                        ),
                                      ),
                                      pw.SizedBox(width: 5),
                                      pw.Text(
                                        '核心肌群訓練',
                                        textScaleFactor: 1,
                                        style: pw.TextStyle(
                                            fontSize: 20),
                                      )
                                    ],
                                  ),
                                ),
                                //健康促進
                                pw.Expanded(
                                  child: pw.Row(
                                    children: [
                                      pw.Transform.scale(
                                        scale: 1.5,
                                        child: pw.Checkbox(
                                          name: '',
                                          value: _healthPromotion,
                                        ),
                                      ),
                                      pw.SizedBox(width: 5),
                                      pw.Text(
                                        '健康促進',
                                        textScaleFactor: 1,
                                        style: pw.TextStyle(
                                            fontSize: 20),
                                      )
                                    ],
                                  )
                                )
                              ]
                            )
                            //endregion
                          ],
                        )
                    ),
                  ),
                ],
              ),
            ),
            //endregion
              //region 長短腳問題列表與調整紀錄
            pw.Container(
              height: 170,
              margin: const pw.EdgeInsets.only(top: 8),
              child: pw.Column(
                children: [
                  pw.Expanded(
                    flex: 1,
                    child: pw.Container(
                      alignment: pw.Alignment.center,
                      decoration: pw.BoxDecoration(
                          borderRadius: const pw.BorderRadius.only(
                              topLeft: pw.Radius.circular(10),
                              topRight: pw.Radius.circular(10)),
                          color: PdfColor.fromHex('#096BC7')),
                      child: pw.Text(
                        '長短腳問題列表與調整紀錄',
                        textScaleFactor: 1,
                        style:
                        const pw.TextStyle(fontSize: 25,color: PdfColors.white),
                      ),
                    ),
                  ),
                  pw.Expanded(
                    flex: 3,
                    child: pw.Container(
                      child: pw.Column(
                        children: [
                          pw.Row(
                            mainAxisAlignment: pw.MainAxisAlignment.spaceAround,
                            crossAxisAlignment: pw.CrossAxisAlignment.start,
                            children:[
                              pw.Column(
                                  mainAxisAlignment: pw.MainAxisAlignment.start,
                                children: [
                                  pw.Container(
                                    margin: const pw.EdgeInsets.only(top: 8),
                                    alignment: pw.Alignment.centerLeft,
                                    child: pw.Text(
                                      '問題',
                                      style: const pw.TextStyle(fontSize: 20),
                                    ),
                                  ),
                                  pw.Container(
                                      padding: const pw.EdgeInsets.only(top: 5, bottom: 5, left: 10, right: 10),
                                      decoration: pw.BoxDecoration(
                                        border: pw.Border.all(color: PdfColors.grey, width: 1.5),
                                        borderRadius: const pw.BorderRadius.all(pw.Radius.circular(10)),),
                                      child: pw.Text(_problemText.text, style: const pw.TextStyle(fontSize: 15))
                                  ),
                                ]
                              ),
                              pw.Column(
                                  mainAxisAlignment: pw.MainAxisAlignment.start,
                                children: [
                                  pw.Container(
                                    margin: const pw.EdgeInsets.only(top: 8),
                                    alignment: pw.Alignment.centerLeft,
                                    child: pw.Text(
                                      '表徵',
                                      style: const pw.TextStyle(fontSize: 20),
                                    ),
                                  ),
                                  pw.Container(
                                    padding: const pw.EdgeInsets.only(top: 5, bottom: 5, left: 10, right: 10),
                                    decoration: pw.BoxDecoration(
                                      border: pw.Border.all(color: PdfColors.grey, width: 1.5),
                                      borderRadius: pw.BorderRadius.all(const pw.Radius.circular(10)),),
                                    child: pw.Text('腳長${_problemSideText.text}側腳長較短', style: const pw.TextStyle(fontSize: 15)),
                                  ),
                                ]
                              ),
                            ],
                          ),
                          pw.Container(
                            margin: const pw.EdgeInsets.only(top: 8),
                            alignment: pw.Alignment.centerLeft,
                            child: pw.Text(
                              '結果',
                              style: const pw.TextStyle(fontSize: 15),
                            ),
                          ),
                          pw.Container(
                            margin: const pw.EdgeInsets.only(top: 8),
                            child: pw.Row(
                              children: [
                                pw.Transform.scale(
                                  scale: 1.5,
                                  child: pw.Checkbox(name: '',value: _pad),
                                ),
                                pw.SizedBox(width: 10),
                                pw.Text(
                                  '建議穿著/購置鞋墊',
                                  textScaleFactor: 1,
                                  style: const pw.TextStyle(fontSize: 15),
                                ),
                                pw.Container(
                                  height: 20,
                                  width: 50,
                                  padding: const pw.EdgeInsets.only(top: 5, bottom: 5, left: 10, right: 10),
                                  decoration: pw.BoxDecoration(
                                    border: pw.Border.all(color: PdfColors.grey, width: 1.5),
                                    borderRadius: const pw.BorderRadius.all(pw.Radius.circular(10)),),
                                  child: pw.Text(_padHigh.text, style: const pw.TextStyle(fontSize: 15)),
                                ),
                                pw.Text(
                                  '公分',
                                  textScaleFactor: 1,
                                  style: const pw.TextStyle(
                                    fontSize: 15
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
              //endregion
              //region 標語
              pw.Container(
                alignment: pw.Alignment.centerLeft,
                decoration: pw.BoxDecoration(color: PdfColor.fromHex('#609AD1')),
                padding: const pw.EdgeInsets.only(left: 8),
                child: pw.Text(
                  '光銀式手法版權所有，翻版必究',
                  style: const pw.TextStyle(
                      color: PdfColors.white),
                ),
              ),
            //endregion
              //region 介入調整前
              pw.Container(
              margin: const pw.EdgeInsets.only(top: 8),
              child: pw.Column(
                children: [
                  //標題
                  pw.Container(
                    height: 40,
                    alignment: pw.Alignment.center,
                    decoration: pw.BoxDecoration(
                      borderRadius: const pw.BorderRadius.only(
                          topLeft: pw.Radius.circular(10), topRight: pw.Radius.circular(10)),
                      color: PdfColor.fromHex('#096BC7')),
                    child: pw.Text(
                      '介入調整前',
                      textScaleFactor: 1,
                      style:
                      const pw.TextStyle(color: PdfColors.white),
                    ),
                  ),
                  pw.Row(
                    children:[
                      //region 身體圖片
                      pw.Container(
                        width: 200,
                        alignment: pw.Alignment.centerRight,
                        child: pw.Stack(
                          children: [
                            pw.Container(
                              height: 200,
                              alignment: pw.Alignment.center,
                              child: pw.Image(pw.MemoryImage(_bodyImage), fit: pw.BoxFit.contain),
                            ),
                            pw.Positioned(
                                bottom: 1,
                                right: 1,
                                child: pw.Container(
                                  alignment: pw.Alignment.centerRight,
                                  child: pw.Column(
                                    crossAxisAlignment: pw.CrossAxisAlignment.end,
                                    children: [
                                      pw.Row(
                                        children: [
                                          pw.Image(pw.MemoryImage(_painImage),width: 10),
                                          pw.SizedBox(
                                            width: 5,
                                          ),
                                          pw.Text(
                                            '輕微受損',
                                            textScaleFactor: 1,
                                            style: const pw.TextStyle(
                                              color: PdfColors.red,),
                                          )
                                        ],
                                      ),
                                      pw.Row(
                                        children: [
                                          pw.Image(pw.MemoryImage(_painImage),width: 10),
                                          pw.Image(pw.MemoryImage(_painImage),width: 10),
                                          pw.SizedBox(
                                            width: 5,
                                          ),
                                          pw.Text(
                                            '嚴重受損',
                                            textScaleFactor: 1,
                                            style: const pw.TextStyle(
                                                color: PdfColors.red),
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                )),
                          ],
                        ),
                      ),
                      //endregion
                      //region底部身體問題圖
                      pw.Container(
                        width: 300,
                        height: 200,
                        child: pw.Stack(
                          children: [
                            //文字
                            pw.Container(
                              margin: const pw.EdgeInsets.only(top: 10, left: 15, right: 15),
                              child: pw.Column(
                                children: [
                                  //身體前屈
                                  pw.Column(
                                    children: [
                                      pw.Text(
                                        '身體前屈 (指尖到地板距離)',
                                        textScaleFactor: 1,
                                      ),
                                      pw.Row(
                                        mainAxisAlignment: pw.MainAxisAlignment.center,
                                        children: [
                                          pw.Container(
                                            margin: const pw.EdgeInsets.all(8),
                                            width: 10,
                                            child: pw.Text(_floorResult.text),
                                          ),
                                          pw.Text(
                                            '公分',
                                            textScaleFactor: 1,
                                            style: const pw.TextStyle(
                                              fontSize: 15,
                                              color: PdfColors.black,),
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                  //左側
                                  pw.Row(
                                    mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
                                    children: [
                                      //左側彎
                                      pw.Column(
                                        children: [
                                          pw.Container(
                                            child: pw.Text(
                                              '左側彎',
                                            ),
                                          ),
                                          pw.Container(
                                            child: pw.Row(
                                              mainAxisAlignment: pw.MainAxisAlignment.center,
                                              children: [
                                                pw.Container(
                                                  margin: const pw.EdgeInsets.all(8),
                                                  width: 10,
                                                  child: pw.Text(_sideKneeLeftResult.text),
                                                ),
                                                pw.Text(
                                                  '公分',
                                                  textScaleFactor: 1,
                                                  style: const pw.TextStyle(
                                                    fontSize: 15,
                                                    color: PdfColors.black,),
                                                )
                                              ],
                                            ),
                                          ),
                                          //左側彎XX
                                          pw.Container(
                                            margin: const pw.EdgeInsets.only(top: 5, left: 10),
                                            child: pw.Row(
                                              children: [
                                                _patientVm!.patient!.side_left != 0 || _patientVm!.patient!.side_left == 2
                                                    ? pw.Image(pw.MemoryImage(_painImage),width: 10)
                                                    : pw.Container(),
                                                _patientVm!.patient!.side_left != 0
                                                    ? pw.Image(pw.MemoryImage(_painImage),width: 10)
                                                    : pw.Container(),
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                                      //身體前屈XX
                                      pw.Row(
                                        children: [
                                          int.parse(_patientVm!.patient!.floor) <= 5 &&
                                              int.parse(_patientVm!.patient!.floor) != 100 &&
                                              int.parse(_patientVm!.patient!.floor) != 0
                                              ? pw.Image(pw.MemoryImage(_painImage),width: 10)
                                              : pw.Container(),
                                          int.parse(_patientVm!.patient!.floor) > 5 &&
                                              int.parse(_patientVm!.patient!.floor) != 0 &&
                                              int.parse(_patientVm!.patient!.floor) != 100
                                              ? pw.Image(pw.MemoryImage(_painImage),width: 10)
                                              : pw.Container(),
                                          int.parse(_patientVm!.patient!.floor) == 100
                                              ? pw.Text(
                                            '預設值',
                                            style: const pw.TextStyle(color: PdfColors.red),
                                          )
                                              : pw.Container(),
                                        ],
                                      ),
                                      //右側彎
                                      pw.Column(
                                        children: [
                                          pw.Container(
                                            child: pw.Text(
                                              '右側彎',
                                            ),
                                          ),
                                          pw.Row(
                                            mainAxisAlignment: pw.MainAxisAlignment.center,
                                            children: [
                                              pw.Container(
                                                margin: pw.EdgeInsets.all(8),
                                                width: 10,
                                                child: pw.Text(_sideKneeRightResult.text),
                                              ),
                                              pw.Text(
                                                '公分',
                                                textScaleFactor: 1,
                                                style: const pw.TextStyle(
                                                  fontSize: 15,
                                                  color: PdfColors.black,),
                                              )
                                            ],
                                          ),
                                          //右側彎XX
                                          pw.Container(
                                            margin: const pw.EdgeInsets.only(top: 5, right: 30),
                                            child: pw.Row(
                                              children: [
                                                _patientVm!.patient!.side_right != 0 && _patientVm!.patient!.side_right == 2
                                                    ? pw.Image(pw.MemoryImage(_painImage),width: 10)
                                                    : pw.Container(),
                                                _patientVm!.patient!.side_right != 0
                                                    ? pw.Image(pw.MemoryImage(_painImage),width: 10)
                                                    : pw.Container(),
                                              ],
                                            ),
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                  pw.SizedBox(
                                    height: 5,
                                  ),
                                  pw.Row(
                                    mainAxisAlignment: pw.MainAxisAlignment.spaceAround,
                                    children: [
                                      pw.Column(
                                        children: [
                                          //左旋轉XX
                                          pw.Container(
                                            child: pw.Row(
                                              children: [
                                                _patientVm!.patient!.rotation_left != 0 &&
                                                    _patientVm!.patient!.rotation_left == 2
                                                    ? pw.Image(pw.MemoryImage(_painImage),width: 10)
                                                    : pw.Container(),
                                                _patientVm!.patient!.rotation_left != 0
                                                    ? pw.Image(pw.MemoryImage(_painImage),width: 10)
                                                    : pw.Container(),
                                              ],
                                            ),
                                          ),
                                          pw.SizedBox(
                                            height: 5,
                                          ),
                                          pw.Container(
                                            alignment: pw.Alignment.center,
                                            child: pw.Text(
                                              '左旋轉',
                                              style: const pw.TextStyle(fontSize: 15),
                                            ),
                                          )
                                        ],
                                      ),
                                      pw.Column(
                                        children: [
                                          //右旋轉XX
                                          pw.Container(
                                            child: pw.Row(
                                              children: [
                                                _patientVm!.patient!.rotation_right != 0 &&
                                                    _patientVm!.patient!.rotation_right == 2
                                                    ? pw.Image(pw.MemoryImage(_painImage),width: 10)
                                                    : pw.Container(),
                                                _patientVm!.patient!.rotation_right != 0
                                                    ? pw.Image(pw.MemoryImage(_painImage),width: 10)
                                                    : pw.Container(),
                                              ],
                                            ),
                                          ),
                                          pw.SizedBox(
                                            height: 5,
                                          ),
                                          pw.Container(
                                            alignment: pw.Alignment.center,
                                            child: pw.Text(
                                              '右旋轉',
                                              textScaleFactor: 1,
                                              style: const pw.TextStyle(fontSize: 15),
                                            ),
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                  pw.Container(
                                    child: pw.Row(
                                      mainAxisAlignment: pw.MainAxisAlignment.center,
                                      children: [
                                        _patientVm!.patient!.Extension != 0 && _patientVm!.patient!.Extension == 2
                                            ? pw.Image(pw.MemoryImage(_painImage),width: 10)
                                            : pw.Container(),
                                        _patientVm!.patient!.Extension != 0
                                            ? pw.Image(pw.MemoryImage(_painImage),width: 10)
                                            : pw.Container(),
                                      ],
                                    ),
                                  ),
                                  pw.SizedBox(
                                    height: 5,
                                  ),
                                  pw.Container(
                                    alignment: pw.Alignment.center,
                                    child: pw.Text(
                                      '身體伸直',
                                      textScaleFactor: 1,
                                      style:
                                      const pw.TextStyle(fontSize: 15),
                                    ),
                                  )
                                ],
                              ),
                            ),
                            //圖
                            pw.Container(
                                alignment: pw.Alignment.bottomCenter,
                                margin: const pw.EdgeInsets.only(bottom: 15),
                                child: pw.Image(pw.MemoryImage(_resultImage),width: 250, height: 250)
                            )
                          ],
                        ),
                      ),
                      //endregion
                    ]
                  ),

                  //備註
                  pw.Column(
                    children: [
                      pw.Container(
                        alignment: pw.Alignment.centerLeft,
                        child: pw.Text(
                          '備註',
                          textScaleFactor: 1,
                        ),
                      ),
                      pw.Container(
                        height: 70,
                        margin: const pw.EdgeInsets.only(left: 10, right: 10),
                        alignment: pw.Alignment.topLeft,
                        decoration: pw.BoxDecoration(
                          border: pw.Border.all(color: PdfColors.grey, width: 1.5),
                          borderRadius: const pw.BorderRadius.all(pw.Radius.circular(10)),
                        ),
                        child: pw.Text(_summary1.text,),
                      )
                    ],
                  )
                ],
              ),
            ),
              //endregion

              //region 介入調整後
            pw.Container(
              margin: const pw.EdgeInsets.only(top: 8),
              child: pw.Column(
                children: [
                  //標題
                  pw.Container(
                    height: 40,
                    alignment: pw.Alignment.center,
                    decoration: pw.BoxDecoration(
                        borderRadius: const pw.BorderRadius.only(
                            topLeft: pw.Radius.circular(10), topRight: pw.Radius.circular(10)),
                        color: PdfColor.fromHex('#096BC7')),
                    child: pw.Text(
                      '介入調整後',
                      textScaleFactor: 1,
                      style:
                      const pw.TextStyle(color: PdfColors.white),
                    ),
                  ),
                  pw.Row(
                      children:[
                        //region 身體圖片
                        pw.Container(
                          width: 200,
                          alignment: pw.Alignment.centerRight,
                          child: pw.Stack(
                            children: [
                              pw.Container(
                                height: 200,
                                alignment: pw.Alignment.center,
                                child: pw.Image(pw.MemoryImage(_bodyImage), fit: pw.BoxFit.contain),
                              ),
                              pw.Positioned(
                                  bottom: 1,
                                  right: 1,
                                  child: pw.Container(
                                    alignment: pw.Alignment.centerRight,
                                    child: pw.Column(
                                      crossAxisAlignment: pw.CrossAxisAlignment.end,
                                      children: [
                                        pw.Row(
                                          children: [
                                            pw.Image(pw.MemoryImage(_painImage),width: 10),
                                            pw.SizedBox(
                                              width: 5,
                                            ),
                                            pw.Text(
                                              '輕微受損',
                                              textScaleFactor: 1,
                                              style: const pw.TextStyle(
                                                color: PdfColors.red,),
                                            )
                                          ],
                                        ),
                                        pw.Row(
                                          children: [
                                            pw.Image(pw.MemoryImage(_painImage),width: 10),
                                            pw.Image(pw.MemoryImage(_painImage),width: 10),
                                            pw.SizedBox(
                                              width: 5,
                                            ),
                                            pw.Text(
                                              '嚴重受損',
                                              textScaleFactor: 1,
                                              style: const pw.TextStyle(
                                                  color: PdfColors.red),
                                            )
                                          ],
                                        )
                                      ],
                                    ),
                                  )),
                            ],
                          ),
                        ),
                        //endregion
                        //region底部身體問題圖
                        pw.Container(
                          width: 300,
                          height: 200,
                          child: pw.Stack(
                            children: [
                              //文字
                              pw.Container(
                                margin: const pw.EdgeInsets.only(top: 10, left: 15, right: 15),
                                child: pw.Column(
                                  children: [
                                    //身體前屈
                                    pw.Column(
                                      children: [
                                        pw.Text(
                                          '身體前屈 (指尖到地板距離)',
                                          textScaleFactor: 1,
                                        ),
                                        pw.Row(
                                          mainAxisAlignment: pw.MainAxisAlignment.center,
                                          children: [
                                            pw.Container(
                                              margin: const pw.EdgeInsets.all(8),
                                              width: 10,
                                              child: pw.Text(_floorRecheckResult.text),
                                            ),
                                            pw.Text(
                                              '公分',
                                              textScaleFactor: 1,
                                              style: const pw.TextStyle(
                                                fontSize: 15,
                                                color: PdfColors.black,),
                                            )
                                          ],
                                        )
                                      ],
                                    ),
                                    //左側
                                    pw.Row(
                                      mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
                                      children: [
                                        //左側彎
                                        pw.Column(
                                          children: [
                                            pw.Container(
                                              child: pw.Text(
                                                '左側彎',
                                              ),
                                            ),
                                            pw.Container(
                                              child: pw.Row(
                                                mainAxisAlignment: pw.MainAxisAlignment.center,
                                                children: [
                                                  pw.Container(
                                                    margin: const pw.EdgeInsets.all(8),
                                                    width: 10,
                                                    child: pw.Text(_sideKneeLeftRecheckResult.text),
                                                  ),
                                                  pw.Text(
                                                    '公分',
                                                    textScaleFactor: 1,
                                                    style: const pw.TextStyle(
                                                      fontSize: 15,
                                                      color: PdfColors.black,),
                                                  )
                                                ],
                                              ),
                                            ),
                                            //左側彎XX
                                            pw.Container(
                                              margin: const pw.EdgeInsets.only(top: 5, left: 10),
                                              child: pw.Row(
                                                children: [
                                                  _patientVm!.patient!.side_left_recheck != 0 || _patientVm!.patient!.side_left_recheck == 2
                                                      ? pw.Image(pw.MemoryImage(_painImage),width: 10)
                                                      : pw.Container(),
                                                  _patientVm!.patient!.side_left_recheck != 0
                                                      ? pw.Image(pw.MemoryImage(_painImage),width: 10)
                                                      : pw.Container(),
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                        //身體前屈XX
                                        pw.Row(
                                          children: [
                                            int.parse(_patientVm!.patient!.floor_recheck) <= 5 &&
                                                int.parse(_patientVm!.patient!.floor_recheck) != 100 &&
                                                int.parse(_patientVm!.patient!.floor_recheck) != 0
                                                ? pw.Image(pw.MemoryImage(_painImage),width: 10)
                                                : pw.Container(),
                                            int.parse(_patientVm!.patient!.floor_recheck) > 5 &&
                                                int.parse(_patientVm!.patient!.floor_recheck) != 0 &&
                                                int.parse(_patientVm!.patient!.floor_recheck) != 100
                                                ? pw.Image(pw.MemoryImage(_painImage),width: 10)
                                                : pw.Container(),
                                            int.parse(_patientVm!.patient!.floor_recheck) == 100
                                                ? pw.Text(
                                              '預設值',
                                              style: const pw.TextStyle(color: PdfColors.red),
                                            )
                                                : pw.Container(),
                                          ],
                                        ),
                                        //右側彎
                                        pw.Column(
                                          children: [
                                            pw.Container(
                                              child: pw.Text(
                                                '右側彎',
                                              ),
                                            ),
                                            pw.Row(
                                              mainAxisAlignment: pw.MainAxisAlignment.center,
                                              children: [
                                                pw.Container(
                                                  margin: const pw.EdgeInsets.all(8),
                                                  width: 10,
                                                  child: pw.Text(_sideKneeRightRecheckResult.text),
                                                ),
                                                pw.Text(
                                                  '公分',
                                                  textScaleFactor: 1,
                                                  style: const pw.TextStyle(
                                                    fontSize: 15,
                                                    color: PdfColors.black,),
                                                )
                                              ],
                                            ),
                                            //右側彎XX
                                            pw.Container(
                                              margin: const pw.EdgeInsets.only(top: 5, right: 30),
                                              child: pw.Row(
                                                children: [
                                                  _patientVm!.patient!.side_right_recheck != 0 && _patientVm!.patient!.side_right_recheck == 2
                                                      ? pw.Image(pw.MemoryImage(_painImage),width: 10)
                                                      : pw.Container(),
                                                  _patientVm!.patient!.side_right_recheck != 0
                                                      ? pw.Image(pw.MemoryImage(_painImage),width: 10)
                                                      : pw.Container(),
                                                ],
                                              ),
                                            ),
                                          ],
                                        )
                                      ],
                                    ),
                                    pw.SizedBox(
                                      height: 5,
                                    ),
                                    pw.Row(
                                      mainAxisAlignment: pw.MainAxisAlignment.spaceAround,
                                      children: [
                                        pw.Column(
                                          children: [
                                            //左旋轉XX
                                            pw.Container(
                                              child: pw.Row(
                                                children: [
                                                  _patientVm!.patient!.rotation_left_recheck != 0 &&
                                                      _patientVm!.patient!.rotation_left_recheck == 2
                                                      ? pw.Image(pw.MemoryImage(_painImage),width: 10)
                                                      : pw.Container(),
                                                  _patientVm!.patient!.rotation_left_recheck != 0
                                                      ? pw.Image(pw.MemoryImage(_painImage),width: 10)
                                                      : pw.Container(),
                                                ],
                                              ),
                                            ),
                                            pw.SizedBox(
                                              height: 5,
                                            ),
                                            pw.Container(
                                              alignment: pw.Alignment.center,
                                              child: pw.Text(
                                                '左旋轉',
                                                style: const pw.TextStyle(fontSize: 15),
                                              ),
                                            )
                                          ],
                                        ),
                                        pw.Column(
                                          children: [
                                            //右旋轉XX
                                            pw.Container(
                                              child: pw.Row(
                                                children: [
                                                  _patientVm!.patient!.rotation_right_recheck != 0 &&
                                                      _patientVm!.patient!.rotation_right_recheck == 2
                                                      ? pw.Image(pw.MemoryImage(_painImage),width: 10)
                                                      : pw.Container(),
                                                  _patientVm!.patient!.rotation_right_recheck != 0
                                                      ? pw.Image(pw.MemoryImage(_painImage),width: 10)
                                                      : pw.Container(),
                                                ],
                                              ),
                                            ),
                                            pw.SizedBox(
                                              height: 5,
                                            ),
                                            pw.Container(
                                              alignment: pw.Alignment.center,
                                              child: pw.Text(
                                                '右旋轉',
                                                textScaleFactor: 1,
                                                style: const pw.TextStyle(fontSize: 15),
                                              ),
                                            )
                                          ],
                                        )
                                      ],
                                    ),
                                    pw.Container(
                                      child: pw.Row(
                                        mainAxisAlignment: pw.MainAxisAlignment.center,
                                        children: [
                                          _patientVm!.patient!.extension_recheck != 0 && _patientVm!.patient!.extension_recheck == 2
                                              ? pw.Image(pw.MemoryImage(_painImage),width: 10)
                                              : pw.Container(),
                                          _patientVm!.patient!.extension_recheck != 0
                                              ? pw.Image(pw.MemoryImage(_painImage),width: 10)
                                              : pw.Container(),
                                        ],
                                      ),
                                    ),
                                    pw.SizedBox(
                                      height: 5,
                                    ),
                                    pw.Container(
                                      alignment: pw.Alignment.center,
                                      child: pw.Text(
                                        '身體伸直',
                                        textScaleFactor: 1,
                                        style:
                                        const pw.TextStyle(fontSize: 15),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              //圖
                              pw.Container(
                                  alignment: pw.Alignment.bottomCenter,
                                  margin: const pw.EdgeInsets.only(bottom: 15),
                                  child: pw.Image(pw.MemoryImage(_resultImage),width: 250, height: 250)
                              )
                            ],
                          ),
                        ),
                        //endregion
                      ]
                  ),

                  //備註
                  pw.Column(
                    children: [
                      pw.Container(
                        alignment: pw.Alignment.centerLeft,
                        child: pw.Text(
                          '備註',
                          textScaleFactor: 1,
                        ),
                      ),
                      pw.Container(
                        height: 70,
                        margin: const pw.EdgeInsets.only(left: 10, right: 10),
                        alignment: pw.Alignment.topLeft,
                        decoration: pw.BoxDecoration(
                          border: pw.Border.all(color: PdfColors.grey, width: 1.5),
                          borderRadius: const pw.BorderRadius.all(pw.Radius.circular(10)),
                        ),
                        child: pw.Text(_summary1.text,),
                      )
                    ],
                  )
                ],
              ),
            ),
              //endregion
              //region
              pw.SizedBox(height: 8),
              //endregion
              //region 評估者
              pw.Container(
                alignment: pw.Alignment.centerRight,
                child: pw.Container(
                    padding: const pw.EdgeInsets.all(10),
                    decoration: pw.BoxDecoration(
                        borderRadius: pw.BorderRadius.circular(10),
                        border: pw.Border.all(color: PdfColors.red, width: 1)),
                    child: pw.Text(
                      _memberVm!.memberModel!.name,
                      textScaleFactor: 1,
                      style: pw.TextStyle(
                          color: PdfColors.red,
                          fontSize: 30,),
                    )),
              )
              //endregion
      ]));


      return doc.save();
    });
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  Widget build(BuildContext context) {
    _patientVm = Provider.of<PatientVm>(context);
    _memberVm = Provider.of<MemberVm>(context);
    _userCompanyInformationVm = Provider.of<UserCompanyInformationVm>(context);
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: true,
      body: SafeArea(
          child: Column(
        children: [
          Expanded(flex: 1, child: _titleBar()),
          Expanded(
            flex: 12,
            child: InteractiveViewer(
              panEnabled: true,
              scaleEnabled: true,
              child: _mainWidget(),
            ),
          )
        ],
      )),
    );
  }

  //Title
  Widget _titleBar() {
    return PvTitleWidget(
      titleText: getTranslated(context, 'Preview'),
      leftButton: const LeftButton(),
      rightButton: Container(),
    );
  }

  //主體
  Widget _mainWidget() {
    return Container(
      decoration: Style.homeBg,
      alignment: Alignment.center,
      child: Container(
        width: MediaQuery.of(context).size.width / 1.1,
        height: MediaQuery.of(context).size.height / 1.2,
        decoration: Style.centerBG,
        padding: const EdgeInsets.only(top: 15, left: 10, right: 10, bottom: 15),
        child: buildIndexFrom(),
      ),
    );
  }

  //表單
  Widget buildIndexFrom() {
    return RepaintBoundary(
      key: _printKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(flex: 1, child: fromHeader()),
          SizedBox(
            height: 10,
          ),
          Expanded(flex: 9, child: fromBody()),
        ],
      ),
    );
  }

  //表單頭
  Widget fromHeader() {
    return Container(
      width: MediaQuery.of(context).size.width,
      decoration: const BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/images/record_sheet_title.png'),
              fit: BoxFit.fill)),
      alignment: Alignment.center,
      child: FittedBox(
        fit:BoxFit.scaleDown,
        child: Text(
          getTranslated(
              context, 'Guangyin_Method_Evaluation_and_Adjustment_Record_Sheet'),
          textScaleFactor: 1,
          style: TextStyle(
              color: Colors.white, fontSize: 23, fontWeight: FontWeight.bold),
        ),
      ),
    );
  }

  //表單身
  Widget fromBody() {
    return ScrollConfiguration(
        behavior: NoShadowScrollBehavior(),
        child: SingleChildScrollView(
          child: Column(
            children: [
              //公司資訊
              _companyInfoWidget(),
              //診察與治療日期
              _dateWidget(),
              //病人姓名
              _patientNameWidget(),
              //病人性別/年齡
              _patientGenderAndAgeWidget(),
              //病人電話
              _patientPhoneWidget(),
              //病人問題
              _patientProblemWidget(),
              //病人目標
              _patientTheGoalWidget(),
              //長短腳問題
              _longAndShortFeetWidget(),
              SizedBox(
                height: 15,
              ),
              //標語
              Container(
                height: 50,
                alignment: Alignment.centerLeft,
                decoration: BoxDecoration(color: HexColor.fromHex('#609AD1')),
                padding: const EdgeInsets.only(left: 8),
                child: FittedBox(
                  fit:BoxFit.scaleDown,
                  child: Text(
                    getTranslated(context,
                        'All_rights_reserved_by_the_light_silver_technique_piracy_must_be_punished'),
                    textScaleFactor: 1,
                    style: const TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              /*--------------------------------*/
              //介入調整前
              _beforeInterventionAdjustmentWidget(),
              //介入調整後
              _afterInterventionAndAdjustmentWidget(),
              SizedBox(
                height: 15,
              ),
              //評估者
              Container(
                alignment: Alignment.centerRight,
                child: Container(
                    padding: const EdgeInsets.all(10),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(color: Colors.red, width: 1)),
                    child: FittedBox(
                      fit:BoxFit.scaleDown,
                      child: Consumer<MemberVm>(builder: (context, vm, _){
                        return Text(
                          vm.memberModel!.name,
                          textScaleFactor: 1,
                          style: TextStyle(
                              color: Colors.red,
                              fontSize: 23,
                              fontWeight: FontWeight.bold),
                        );
                      },),
                    )),
              ),
              //標語
              Container(
                height: 50,
                alignment: Alignment.centerLeft,
                decoration: BoxDecoration(color: HexColor.fromHex('#609AD1')),
                padding: const EdgeInsets.only(left: 8),
                child: FittedBox(
                  fit:BoxFit.scaleDown,
                  child: Text(
                    getTranslated(context,
                        'All_rights_reserved_by_the_light_silver_technique_piracy_must_be_punished'),
                    textScaleFactor: 1,
                    style: const TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ],
          ),
        ));
  }

  //公司資訊
  Widget _companyInfoWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 8),
      decoration: _inputBg,
      child: Row(
        children: [
          Container(
            margin: const EdgeInsets.only(left: 8),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                FittedBox(
                  fit:BoxFit.scaleDown,
                  child: Text(
                    _companyName,
                    textScaleFactor: 1,
                    style: const TextStyle(),
                  ),
                ),
                FittedBox(
                  fit:BoxFit.scaleDown,
                  child: Text(
                    _companyPhone,
                    textScaleFactor: 1,
                    style: const TextStyle(),
                  ),
                ),
                FittedBox(
                  fit:BoxFit.scaleDown,
                  child: Text(
                    _companyAddress,
                    textScaleFactor: 1,
                    style: const TextStyle(),
                  ),
                ),
              ],
            ),
          ),
          Container()
        ],
      ),
    );
  }

  //診察與治療日期
  Widget _dateWidget() {
    return Container();
  }

  //病人姓名
  Widget _patientNameWidget() {
    return Container(
        margin: const EdgeInsets.only(top: 8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            FittedBox(
              fit:BoxFit.scaleDown,
              child: Text(
                getTranslated(context, 'Name'),
                textScaleFactor: 1,
                style: const TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            TextField(
              style: const TextStyle(color: Colors.black),
              decoration: InputDecoration(
                  errorText: null,
                  contentPadding: const EdgeInsets.all(5.0),
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey[400]!),
                      borderRadius: BorderRadius.circular(10.0)),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                  filled: true,
                  fillColor: Colors.white,
                  hintText: getTranslated(context, 'Please_type_in_your_name'),
                  hintStyle: const TextStyle(color: Colors.grey)),
              keyboardType: TextInputType.text,
              textInputAction: TextInputAction.next,
              controller: _patientName,
            ),
          ],
        ));
  }

  //病人性別/年齡
  Widget _patientGenderAndAgeWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 8),
      child: Row(
        children: [
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              FittedBox(
                fit:BoxFit.scaleDown,
                child: Text(
                  getTranslated(context, 'Gender'),
                  textScaleFactor: 1,
                  style: const TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
              _buildGender(),
            ],
          )),
          SizedBox(
            width: 10,
          ),
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              FittedBox(
                fit:BoxFit.scaleDown,
                child: Text(
                  getTranslated(context, 'Age'),
                  textScaleFactor: 1,
                  style: const TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
              _buildAge(),
            ],
          )),
        ],
      ),
    );
  }

  //性別
  Widget _buildGender() {
    return TextField(
      readOnly: true,
      style: const TextStyle(color: Colors.black),
      decoration: InputDecoration(
        suffixIcon: PopupMenuButton<String>(
          offset: const Offset(-100, 70),
          icon: const Icon(
            Icons.arrow_drop_down,
            size: 35,
          ),
          onSelected: (String value) {
            _patientGender.text = value;
          },
          itemBuilder: (BuildContext context) {
            return items.map<PopupMenuItem<String>>((String value) {
              return PopupMenuItem(
                child: FittedBox(fit:BoxFit.scaleDown,child: Text(value)),
                value: value,
              );
            }).toList();
          },
        ),
        contentPadding: const EdgeInsets.all(5),
        enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey[400]!),
            borderRadius: BorderRadius.circular(10)),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
        filled: true,
        fillColor: Colors.white,
        hintText: getTranslated(context, 'Please_enter_gender'),
        hintStyle: TextStyle(color: HexColor.fromHex('#919191')),
      ),
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _patientGender,
    );
  }

  //年齡
  Widget _buildAge() {
    return TextField(
      style: const TextStyle(color: Colors.black),
      decoration: InputDecoration(
          errorText: null,
          contentPadding: const EdgeInsets.all(5.0),
          enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.grey[400]!),
              borderRadius: BorderRadius.circular(10)),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
          filled: true,
          fillColor: Colors.white,
          hintText: getTranslated(context, 'Please_enter_age'),
          hintStyle: const TextStyle(color: Colors.grey)),
      keyboardType: TextInputType.number,
      textInputAction: TextInputAction.done,
      controller: _patientAge,
    );
  }

  //病人電話
  Widget _patientPhoneWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          FittedBox(
            fit:BoxFit.scaleDown,
            child: Text(
              getTranslated(context, 'Phone'),
              textScaleFactor: 1,
              style: const TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          TextField(
            style: const TextStyle(color: Colors.black),
            decoration: InputDecoration(
                errorText: null,
                contentPadding: const EdgeInsets.all(5.0),
                enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey[400]!),
                    borderRadius: BorderRadius.circular(10.0)),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0)),
                filled: true,
                fillColor: Colors.white,
                hintText:
                    getTranslated(context, 'Please_enter_your_phone_number'),
                hintStyle: const TextStyle(color: Colors.grey)),
            keyboardType: TextInputType.phone,
            textInputAction: TextInputAction.next,
            controller: _patientPhone,
          ),
        ],
      ),
    );
  }

  //病人問題
  Widget _patientProblemWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 8),
      child: Column(
        children: [
          Container(
            height: 50,
            alignment: Alignment.center,
            decoration: BoxDecoration(
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(10),
                    topRight: Radius.circular(10)),
                color: HexColor.fromHex('#096BC7')),
            child: FittedBox(
              fit:BoxFit.scaleDown,
              child: Text(
                getTranslated(context, 'Problem'),
                textScaleFactor: 1,
                style:
                    const TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
              ),
            ),
          ),
          TextField(
            maxLines: 5,
            maxLength: 500,
            decoration: InputDecoration(
                counterText: '',
                errorText: null,
                contentPadding: const EdgeInsets.all(5.0),
                enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey[400]!),
                    borderRadius: const BorderRadius.only(
                        bottomRight: Radius.circular(10),
                        bottomLeft: Radius.circular(10))),
                border: const OutlineInputBorder(
                    borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(10),
                        bottomLeft: Radius.circular(10))),
                filled: true,
                fillColor: Colors.white,
                hintText: '',
                hintStyle: const TextStyle(color: Colors.grey)),
            keyboardType: TextInputType.text,
            textInputAction: TextInputAction.done,
            controller: _patientProblem,
          ),
        ],
      ),
    );
  }

  //病人目標
  Widget _patientTheGoalWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 8),
      child: Column(
        children: [
          Container(
            height: 50,
            alignment: Alignment.center,
            decoration: BoxDecoration(
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(10),
                    topRight: Radius.circular(10)),
                color: HexColor.fromHex('#096BC7')),
            child: FittedBox(
              fit:BoxFit.scaleDown,
              child: Text(
                getTranslated(context, 'The_goal'),
                textScaleFactor: 1,
                style:
                    const TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
              ),
            ),
          ),
          Container(
            decoration: BoxDecoration(
                borderRadius: const BorderRadius.only(
                    bottomRight: Radius.circular(10),
                    bottomLeft: Radius.circular(10)),
                border: Border.all(color: Colors.grey, width: 1.5)),
            child: Column(
              children: [
                //減緩不適症狀
                Row(
                  children: [
                    Transform.scale(
                      scale: 1.5,
                      child: Checkbox(
                        value: _relieveSymptomsOfDiscomfort,
                        onChanged: (value) {
                          setState(() {
                            _relieveSymptomsOfDiscomfort = value!;
                          });
                        },
                      ),
                    ),
                    FittedBox(
                      fit:BoxFit.scaleDown,
                      child: Text(
                        getTranslated(context, 'Relieve_symptoms_of_discomfort'),
                        textScaleFactor: 1,
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                    )
                  ],
                ),
                //姿勢矯正
                Row(
                  children: [
                    Transform.scale(
                      scale: 1.5,
                      child: Checkbox(
                        value: _postureCorrection,
                        onChanged: (value) {
                          setState(() {
                            _postureCorrection = value!;
                          });
                        },
                      ),
                    ),
                    FittedBox(
                      fit:BoxFit.scaleDown,
                      child: Text(
                        getTranslated(context, 'Posture_correction'),
                        textScaleFactor: 1,
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                    )
                  ],
                ),
                //資訊衛教
                Row(
                  children: [
                    Transform.scale(
                      scale: 1.5,
                      child: Checkbox(
                        value: _informationHealthEducation,
                        onChanged: (value) {
                          setState(() {
                            _informationHealthEducation = value!;
                          });
                        },
                      ),
                    ),
                    FittedBox(
                      fit:BoxFit.scaleDown,
                      child: Text(
                        getTranslated(context, 'Information_Health_Education'),
                        textScaleFactor: 1,
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                    )
                  ],
                ),
                //關節復位
                Row(
                  children: [
                    Transform.scale(
                      scale: 1.5,
                      child: Checkbox(
                        value: _jointReduction,
                        onChanged: (value) {
                          setState(() {
                            _jointReduction = value!;
                          });
                        },
                      ),
                    ),
                    FittedBox(
                      fit:BoxFit.scaleDown,
                      child: Text(
                        getTranslated(context, 'Joint_reduction'),
                        textScaleFactor: 1,
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                    )
                  ],
                ),
                //核心肌群訓練
                Row(
                  children: [
                    Transform.scale(
                      scale: 1.5,
                      child: Checkbox(
                        value: _coreMuscleTraining,
                        onChanged: (value) {
                          setState(() {
                            _coreMuscleTraining = value!;
                          });
                        },
                      ),
                    ),
                    FittedBox(
                      fit:BoxFit.scaleDown,
                      child: Text(
                        getTranslated(context, 'Core_muscle_training'),
                        textScaleFactor: 1,
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                    )
                  ],
                ),
                //健康促進
                Row(
                  children: [
                    Transform.scale(
                      scale: 1.5,
                      child: Checkbox(
                        value: _healthPromotion,
                        onChanged: (value) {
                          setState(() {
                            _healthPromotion = value!;
                          });
                        },
                      ),
                    ),
                    FittedBox(
                      fit:BoxFit.scaleDown,
                      child: Text(
                        getTranslated(context, 'Health_promotion'),
                        textScaleFactor: 1,
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                    )
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  //長短腳問題
  Widget _longAndShortFeetWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 8),
      child: Column(
        children: [
          Container(
            height: 80,
            alignment: Alignment.center,
            decoration: BoxDecoration(
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(10),
                    topRight: Radius.circular(10)),
                color: HexColor.fromHex('#096BC7')),
            child: FittedBox(
              fit:BoxFit.scaleDown,
              child: Text(
                getTranslated(context,
                    'List_of_long_and_short_feet_problems_and_adjustment_records'),
                textScaleFactor: 1,
                style:
                    const TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
              ),
            ),
          ),
          Column(
            children: [
              Container(
                alignment: Alignment.centerLeft,
                child: FittedBox(
                  fit:BoxFit.scaleDown,
                  child: Text(
                    getTranslated(context, 'Problem'),
                    textScaleFactor: 1,
                    style: const TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              TextField(
                readOnly: true,
                style: const TextStyle(color: Colors.black),
                decoration: InputDecoration(
                    errorText: null,
                    contentPadding: const EdgeInsets.all(5.0),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey[400]!),
                        borderRadius: BorderRadius.circular(10.0)),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0)),
                    filled: true,
                    fillColor: Colors.white,
                    hintText: '',
                    hintStyle: const TextStyle(color: Colors.grey)),
                keyboardType: TextInputType.text,
                textInputAction: TextInputAction.next,
                controller: _problemText,
              ),
              Container(
                alignment: Alignment.centerLeft,
                child: FittedBox(
                  fit:BoxFit.scaleDown,
                  child: Text(
                    getTranslated(context, 'Characterization'),
                    textScaleFactor: 1,
                    style: const TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              TextField(
                readOnly: true,
                style: const TextStyle(color: Colors.black),
                decoration: InputDecoration(
                    errorText: null,
                    contentPadding: const EdgeInsets.all(5.0),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey[400]!),
                        borderRadius: BorderRadius.circular(10.0)),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0)),
                    filled: true,
                    fillColor: Colors.white,
                    hintText: '',
                    hintStyle: const TextStyle(color: Colors.grey)),
                keyboardType: TextInputType.text,
                textInputAction: TextInputAction.next,
                controller: _problemSideText,
              ),
              Container(
                alignment: Alignment.centerLeft,
                child: FittedBox(
                  fit:BoxFit.scaleDown,
                  child: Text(
                    getTranslated(context, 'result'),
                    textScaleFactor: 1,
                    style: const TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              Row(
                children: [
                  Transform.scale(
                    scale: 1.5,
                    child: Checkbox(value: _pad, onChanged: null),
                  ),
                  FittedBox(
                    fit:BoxFit.scaleDown,
                    child: Text(
                      getTranslated(context,
                          'It_is_recommended_to_wear_purchase_insoles'),
                      textScaleFactor: 1,
                      style: const TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                  SizedBox(
                    width: 80,
                    child: TextField(
                      readOnly: true,
                      style: const TextStyle(color: Colors.black),
                      decoration: InputDecoration(
                        suffixIcon: PopupMenuButton<String>(
                          offset: const Offset(-100, 70),
                          icon: const Icon(
                            Icons.arrow_drop_down,
                            size: 25,
                          ),
                          onSelected: null,
                          itemBuilder: (BuildContext context) {
                            return [];
                          },
                        ),
                        contentPadding: const EdgeInsets.all(5),
                        enabledBorder: const OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.black)),
                        filled: true,
                        fillColor: Colors.white,
                      ),
                      keyboardType: TextInputType.text,
                      textInputAction: TextInputAction.done,
                      controller: _padHigh,
                    ),
                  ),
                  FittedBox(
                    fit:BoxFit.scaleDown,
                    child: Text(
                      getTranslated(context, 'Cm'),
                      textScaleFactor: 1,
                      style: const TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
        ],
      ),
    );
  }

  //介入調整前
  Widget _beforeInterventionAdjustmentWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 8),
      child: Column(
        children: [
          //標題
          Container(
            height: 50,
            alignment: Alignment.center,
            decoration: _dataTitleBg,
            child: FittedBox(
              fit:BoxFit.scaleDown,
              child: Text(
                getTranslated(context, 'Before_intervention_adjustment'),
                textScaleFactor: 1,
                style:
                    const TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
              ),
            ),
          ),
          //圖片
          Container(
            alignment: Alignment.centerRight,
            child: Stack(
              children: [
                Container(
                  alignment: Alignment.center,
                  child: Image.asset(
                    'assets/images/body.png',
                    fit: BoxFit.fill,
                  ),
                ),
                Positioned(
                    bottom: 1,
                    right: 1,
                    child: Container(
                      alignment: Alignment.centerRight,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Row(
                            children: [
                              Image.asset(
                                'assets/images/pain_level.png',
                                width: 20,
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              FittedBox(
                                fit:BoxFit.scaleDown,
                                child: const Text(
                                  '輕微受損',
                                  textScaleFactor: 1,
                                  style: TextStyle(
                                      color: Colors.red,
                                      fontWeight: FontWeight.bold),
                                ),
                              )
                            ],
                          ),
                          Row(
                            children: [
                              Image.asset(
                                'assets/images/pain_level.png',
                                width: 20,
                              ),
                              Image.asset(
                                'assets/images/pain_level.png',
                                width: 20,
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              FittedBox(
                                fit:BoxFit.scaleDown,
                                child: const Text(
                                  '嚴重受損',
                                  textScaleFactor: 1,
                                  style: TextStyle(
                                      color: Colors.red,
                                      fontWeight: FontWeight.bold),
                                ),
                              )
                            ],
                          )
                        ],
                      ),
                    )),
              ],
            ),
          ),
          //底部身體問題圖
          _beforeBodyImageWidget(),
          //備註
          Column(
            children: [
              Container(
                alignment: Alignment.centerLeft,
                child: FittedBox(
                  fit:BoxFit.scaleDown,
                  child: const Text(
                    '備註',
                    textScaleFactor: 1,
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(left: 25, right: 25),
                child: TextField(
                  maxLines: 5,
                  maxLength: 500,
                  decoration: InputDecoration(
                      errorText: null,
                      contentPadding: const EdgeInsets.all(5.0),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey[400]!),
                          borderRadius: BorderRadius.circular(10)),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10)),
                      filled: true,
                      fillColor: Colors.white,
                      hintText: '',
                      hintStyle: const TextStyle(color: Colors.grey)),
                  keyboardType: TextInputType.text,
                  textInputAction: TextInputAction.done,
                  controller: _summary1,
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  //介入前身體圖
  Widget _beforeBodyImageWidget() {
    return SizedBox(
      width: 950,
      height: 390,
      child: Stack(
        children: [
          //文字
          Container(
            margin: const EdgeInsets.only(top: 10, left: 15, right: 15),
            child: Column(
              children: [
                //身體前屈
                Column(
                  children: [
                    FittedBox(
                      fit:BoxFit.scaleDown,
                      child: Text(
                        getTranslated(context, 'Bend_forward'),
                        textScaleFactor: 1,
                        style: const TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          margin: const EdgeInsets.all(8),
                          width: 80,
                          child: TextField(
                            readOnly: true,
                            textAlign: TextAlign.center,
                            style: const TextStyle(color: Colors.black),
                            decoration: const InputDecoration(
                                contentPadding: EdgeInsets.all(5),
                                enabledBorder: OutlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.black)),
                                filled: true,
                                fillColor: Colors.white),
                            keyboardType: TextInputType.number,
                            textInputAction: TextInputAction.done,
                            controller: _floorResult,
                          ),
                        ),
                        FittedBox(
                          fit:BoxFit.scaleDown,
                          child: Text(
                            getTranslated(context, 'Cm'),
                            textScaleFactor: 1,
                            style: TextStyle(
                                fontSize: 20,
                                color: Colors.black,
                                fontWeight: FontWeight.bold),
                          ),
                        )
                      ],
                    )
                  ],
                ),
                //左側
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    //左側彎
                    Column(
                      children: [
                        FittedBox(
                          fit:BoxFit.scaleDown,
                          child: Text(
                            getTranslated(context, 'Left_side_bend'),
                            textScaleFactor: 1,
                            style: const TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              margin: const EdgeInsets.all(8),
                              width: 80,
                              child: TextField(
                                readOnly: true,
                                textAlign: TextAlign.center,
                                style: const TextStyle(color: Colors.black),
                                decoration: const InputDecoration(
                                    contentPadding: EdgeInsets.all(5),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide:
                                            BorderSide(color: Colors.black)),
                                    filled: true,
                                    fillColor: Colors.white),
                                keyboardType: TextInputType.number,
                                textInputAction: TextInputAction.done,
                                controller: _sideKneeLeftResult,
                              ),
                            ),
                            FittedBox(
                              fit:BoxFit.scaleDown,
                              child: Text(
                                getTranslated(context, 'Cm'),
                                textScaleFactor: 1,
                                style: TextStyle(
                                    fontSize: 20,
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold),
                              ),
                            )
                          ],
                        ),
                        //左側彎XX
                        Container(
                          margin: const EdgeInsets.only(top: 5, left: 30),
                          child: Row(
                            children: [
                              _patientVm!.patient!.side_left != 0 || _patientVm!.patient!.side_left == 2
                                  ? Image.asset(
                                      'assets/images/pain_level.png',
                                      width: 20,
                                    )
                                  : Container(),
                              _patientVm!.patient!.side_left != 0
                                  ? Image.asset(
                                      'assets/images/pain_level.png',
                                      width: 20,
                                    )
                                  : Container(),
                            ],
                          ),
                        )
                      ],
                    ),
                    //身體前屈XX
                    Row(
                      children: [
                        int.parse(_patientVm!.patient!.floor) <= 5 &&
                                int.parse(_patientVm!.patient!.floor) != 100 &&
                                int.parse(_patientVm!.patient!.floor) != 0
                            ? Image.asset(
                                'assets/images/pain_level.png',
                                width: 20,
                              )
                            : Container(),
                        int.parse(_patientVm!.patient!.floor) > 5 &&
                                int.parse(_patientVm!.patient!.floor) != 0 &&
                                int.parse(_patientVm!.patient!.floor) != 100
                            ? Image.asset(
                                'assets/images/pain_level.png',
                                width: 20,
                              )
                            : Container(),
                        int.parse(_patientVm!.patient!.floor) == 100
                            ? FittedBox(
                              fit:BoxFit.scaleDown,
                              child: Text(
                                  getTranslated(context, 'default_value'),
                                  textScaleFactor: 1,
                                  style: const TextStyle(color: Colors.red),
                                ),
                            )
                            : Container(),
                      ],
                    ),
                    //右側彎
                    Column(
                      children: [
                        FittedBox(
                          fit:BoxFit.scaleDown,
                          child: Text(
                            getTranslated(context, 'Right_side_bend'),
                            textScaleFactor: 1,
                            style: const TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              margin: const EdgeInsets.all(8),
                              width: 80,
                              child: TextField(
                                readOnly: true,
                                textAlign: TextAlign.center,
                                style: const TextStyle(color: Colors.black),
                                decoration: const InputDecoration(
                                    contentPadding: EdgeInsets.all(5),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide:
                                            BorderSide(color: Colors.black)),
                                    filled: true,
                                    fillColor: Colors.white),
                                keyboardType: TextInputType.number,
                                textInputAction: TextInputAction.done,
                                controller: _sideKneeRightResult,
                              ),
                            ),
                            FittedBox(
                              fit:BoxFit.scaleDown,
                              child: Text(
                                getTranslated(context, 'Cm'),
                                textScaleFactor: 1,
                                style: TextStyle(
                                    fontSize: 20,
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold),
                              ),
                            )
                          ],
                        ),
                        //右側彎XX
                        Container(
                          margin: const EdgeInsets.only(top: 5, right: 30),
                          child: Row(
                            children: [
                              _patientVm!.patient!.side_right != 0 && _patientVm!.patient!.side_right == 2
                                  ? Image.asset(
                                      'assets/images/pain_level.png',
                                      width: 20,
                                    )
                                  : Container(),
                              _patientVm!.patient!.side_right != 0
                                  ? Image.asset(
                                      'assets/images/pain_level.png',
                                      width: 20,
                                    )
                                  : Container(),
                            ],
                          ),
                        ),
                      ],
                    )
                  ],
                ),
                SizedBox(
                  height: 35,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Column(
                      children: [
                        //左旋轉XX
                        Row(
                          children: [
                            _patientVm!.patient!.rotation_left != 0 &&
                                    _patientVm!.patient!.rotation_left == 2
                                ? Image.asset(
                                    'assets/images/pain_level.png',
                                    width: 20,
                                  )
                                : Container(),
                            _patientVm!.patient!.rotation_left != 0
                                ? Image.asset(
                                    'assets/images/pain_level.png',
                                    width: 20,
                                  )
                                : Container(),
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Container(
                          alignment: Alignment.center,
                          child: FittedBox(
                            fit:BoxFit.scaleDown,
                            child: Text(
                              getTranslated(context, 'Rotate_left'),
                              textScaleFactor: 1,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 20),
                            ),
                          ),
                        )
                      ],
                    ),
                    Column(
                      children: [
                        //右旋轉XX
                        Row(
                          children: [
                            _patientVm!.patient!.rotation_right != 0 &&
                                    _patientVm!.patient!.rotation_right == 2
                                ? Image.asset(
                                    'assets/images/pain_level.png',
                                    width: 20,
                                  )
                                : Container(),
                            _patientVm!.patient!.rotation_right != 0
                                ? Image.asset(
                                    'assets/images/pain_level.png',
                                    width: 20,
                                  )
                                : Container(),
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Container(
                          alignment: Alignment.center,
                          child: FittedBox(
                            fit:BoxFit.scaleDown,
                            child: Text(
                              getTranslated(context, 'Rotate_right'),
                              textScaleFactor: 1,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 20),
                            ),
                          ),
                        )
                      ],
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    _patientVm!.patient!.Extension != 0 && _patientVm!.patient!.Extension == 2
                        ? Image.asset(
                            'assets/images/pain_level.png',
                            width: 20,
                          )
                        : Container(),
                    _patientVm!.patient!.Extension != 0
                        ? Image.asset(
                            'assets/images/pain_level.png',
                            width: 20,
                          )
                        : Container(),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  alignment: Alignment.center,
                  child: FittedBox(
                    fit:BoxFit.scaleDown,
                    child: Text(
                      getTranslated(context, 'Straighten_the_body'),
                      textScaleFactor: 1,
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                    ),
                  ),
                )
              ],
            ),
          ),
          //圖
          Container(
            alignment: Alignment.bottomCenter,
            child: Image.asset(
              'assets/images/result_blue.png',
              width: 850,
              height: 550,
            ),
          )
        ],
      ),
    );
  }

  //介入調整後
  Widget _afterInterventionAndAdjustmentWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 8),
      child: Column(
        children: [
          //標題
          Container(
            height: 50,
            alignment: Alignment.center,
            decoration: _dataTitleBg,
            child: FittedBox(
              fit:BoxFit.scaleDown,
              child: Text(
                getTranslated(context, 'After_intervention_and_adjustment'),
                textScaleFactor: 1,
                style:
                    const TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
              ),
            ),
          ),
          //圖片
          Container(
            alignment: Alignment.centerRight,
            child: Stack(
              children: [
                Container(
                  alignment: Alignment.center,
                  child: Image.asset(
                    'assets/images/body.png',
                    fit: BoxFit.fill,
                  ),
                ),
                Positioned(
                    bottom: 1,
                    right: 1,
                    child: Container(
                      alignment: Alignment.centerRight,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Row(
                            children: [
                              Image.asset(
                                'assets/images/pain_level.png',
                                width: 20,
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              FittedBox(
                                fit:BoxFit.scaleDown,
                                child: const Text(
                                  '輕微受損',
                                  textScaleFactor: 1,
                                  style: TextStyle(
                                      color: Colors.red,
                                      fontWeight: FontWeight.bold),
                                ),
                              )
                            ],
                          ),
                          Row(
                            children: [
                              Image.asset(
                                'assets/images/pain_level.png',
                                width: 20,
                              ),
                              Image.asset(
                                'assets/images/pain_level.png',
                                width: 20,
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              FittedBox(
                                fit:BoxFit.scaleDown,
                                child: const Text(
                                  '嚴重受損',
                                  textScaleFactor: 1,
                                  style: TextStyle(
                                      color: Colors.red,
                                      fontWeight: FontWeight.bold),
                                ),
                              )
                            ],
                          )
                        ],
                      ),
                    )),
              ],
            ),
          ),
          //介入後身體圖
          _afterBodyImageWidget(),
          //備註
          Column(
            children: [
              Container(
                alignment: Alignment.centerLeft,
                child: FittedBox(
                  fit:BoxFit.scaleDown,
                  child: const Text(
                    '備註',
                    textScaleFactor: 1,
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(left: 25, right: 25),
                child: TextField(
                  maxLines: 5,
                  maxLength: 500,
                  decoration: InputDecoration(
                      errorText: null,
                      contentPadding: const EdgeInsets.all(5.0),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey[400]!),
                          borderRadius: BorderRadius.circular(10)),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10)),
                      filled: true,
                      fillColor: Colors.white,
                      hintText: '',
                      hintStyle: const TextStyle(color: Colors.grey)),
                  keyboardType: TextInputType.text,
                  textInputAction: TextInputAction.done,
                  controller: _summary2,
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  //介入後身體圖
  Widget _afterBodyImageWidget() {
    return SizedBox(
      width: 950,
      height: 390,
      child: Stack(
        children: [
          //文字
          Container(
            margin: const EdgeInsets.only(top: 10, left: 15, right: 15),
            child: Column(
              children: [
                //身體前屈
                Column(
                  children: [
                    FittedBox(
                      fit:BoxFit.scaleDown,
                      child: Text(
                        getTranslated(context, 'Bend_forward'),
                        textScaleFactor: 1,
                        style: const TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          margin: const EdgeInsets.all(8),
                          width: 80,
                          child: TextField(
                            readOnly: true,
                            textAlign: TextAlign.center,
                            style: const TextStyle(color: Colors.black),
                            decoration: const InputDecoration(
                                contentPadding: EdgeInsets.all(5),
                                enabledBorder: OutlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.black)),
                                filled: true,
                                fillColor: Colors.white),
                            keyboardType: TextInputType.number,
                            textInputAction: TextInputAction.done,
                            controller: _floorRecheckResult,
                          ),
                        ),
                        FittedBox(
                          fit:BoxFit.scaleDown,
                          child: Text(
                            getTranslated(context, 'Cm'),
                            textScaleFactor: 1,
                            style: TextStyle(
                                fontSize: 20,
                                color: Colors.black,
                                fontWeight: FontWeight.bold),
                          ),
                        )
                      ],
                    )
                  ],
                ),
                //左側
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    //左側彎
                    Column(
                      children: [
                        FittedBox(
                          fit:BoxFit.scaleDown,
                          child: Text(
                            getTranslated(context, 'Left_side_bend'),
                            textScaleFactor: 1,
                            style: const TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              margin: const EdgeInsets.all(8),
                              width: 80,
                              child: TextField(
                                readOnly: true,
                                textAlign: TextAlign.center,
                                style: const TextStyle(color: Colors.black),
                                decoration: const InputDecoration(
                                    contentPadding: EdgeInsets.all(5),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide:
                                            BorderSide(color: Colors.black)),
                                    filled: true,
                                    fillColor: Colors.white),
                                keyboardType: TextInputType.number,
                                textInputAction: TextInputAction.done,
                                controller: _sideKneeLeftRecheckResult,
                              ),
                            ),
                            FittedBox(
                              fit:BoxFit.scaleDown,
                              child: Text(
                                getTranslated(context, 'Cm'),
                                textScaleFactor: 1,
                                style: TextStyle(
                                    fontSize: 20,
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold),
                              ),
                            )
                          ],
                        ),
                        //左側彎XX
                        Container(
                          margin: const EdgeInsets.only(top: 5, left: 30),
                          child: Row(
                            children: [
                              _patientVm!.patient!.side_left_recheck != 0 ||
                                      _patientVm!.patient!.side_left_recheck == 2
                                  ? Image.asset(
                                      'assets/images/pain_level.png',
                                      width: 20,
                                    )
                                  : Container(),
                              _patientVm!.patient!.side_left_recheck != 0
                                  ? Image.asset(
                                      'assets/images/pain_level.png',
                                      width: 20,
                                    )
                                  : Container(),
                            ],
                          ),
                        )
                      ],
                    ),
                    //身體前屈XX
                    Row(
                      children: [
                        int.parse(_patientVm!.patient!.floor_recheck) <= 5 &&
                                int.parse(_patientVm!.patient!.floor_recheck) != 100 &&
                                int.parse(_patientVm!.patient!.floor_recheck) != 0
                            ? Image.asset(
                                'assets/images/pain_level.png',
                                width: 20,
                              )
                            : Container(),
                        int.parse(_patientVm!.patient!.floor_recheck) > 5 &&
                                int.parse(_patientVm!.patient!.floor_recheck) != 0 &&
                                int.parse(_patientVm!.patient!.floor_recheck) != 100
                            ? Image.asset(
                                'assets/images/pain_level.png',
                                width: 20,
                              )
                            : Container(),
                        int.parse(_patientVm!.patient!.floor_recheck) == 100
                            ? FittedBox(
                              fit:BoxFit.scaleDown,
                              child: Text(
                                  getTranslated(context, 'default_value'),
                                  textScaleFactor: 1,
                                  style: const TextStyle(color: Colors.red),
                                ),
                            )
                            : Container(),
                      ],
                    ),
                    //右側彎
                    Column(
                      children: [
                        FittedBox(
                          fit:BoxFit.scaleDown,
                          child: Text(
                            getTranslated(context, 'Right_side_bend'),
                            textScaleFactor: 1,
                            style: const TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              margin: const EdgeInsets.all(8),
                              width: 80,
                              child: TextField(
                                readOnly: true,
                                textAlign: TextAlign.center,
                                style: const TextStyle(color: Colors.black),
                                decoration: const InputDecoration(
                                  contentPadding: EdgeInsets.all(5),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Colors.black)),
                                  filled: true,
                                  fillColor: Colors.white,
                                ),
                                keyboardType: TextInputType.number,
                                textInputAction: TextInputAction.done,
                                controller: _sideKneeRightRecheckResult,
                              ),
                            ),
                            FittedBox(
                              fit:BoxFit.scaleDown,
                              child: Text(
                                getTranslated(context, 'Cm'),
                                textScaleFactor: 1,
                                style: TextStyle(
                                    fontSize: 20,
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold),
                              ),
                            )
                          ],
                        ),
                        //右側彎XX
                        Container(
                          margin: const EdgeInsets.only(top: 5, right: 30),
                          child: Row(
                            children: [
                              _patientVm!.patient!.side_right_recheck != 0 &&
                                      _patientVm!.patient!.side_right_recheck == 2
                                  ? Image.asset(
                                      'assets/images/pain_level.png',
                                      width: 20,
                                    )
                                  : Container(),
                              _patientVm!.patient!.side_right_recheck != 0
                                  ? Image.asset(
                                      'assets/images/pain_level.png',
                                      width: 20,
                                    )
                                  : Container(),
                            ],
                          ),
                        )
                      ],
                    )
                  ],
                ),
                SizedBox(
                  height: 35,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Column(
                      children: [
                        //左旋轉XX
                        Row(
                          children: [
                            _patientVm!.patient!.rotation_left_recheck != 0 &&
                                    _patientVm!.patient!.rotation_left_recheck == 2
                                ? Image.asset(
                                    'assets/images/pain_level.png',
                                    width: 20,
                                  )
                                : Container(),
                            _patientVm!.patient!.rotation_left_recheck != 0
                                ? Image.asset(
                                    'assets/images/pain_level.png',
                                    width: 20,
                                  )
                                : Container(),
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Container(
                          alignment: Alignment.center,
                          child: FittedBox(
                            fit:BoxFit.scaleDown,
                            child: Text(
                              getTranslated(context, 'Rotate_left'),
                              textScaleFactor: 1,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 20),
                            ),
                          ),
                        )
                      ],
                    ),
                    Column(
                      children: [
                        //右旋轉
                        Row(
                          children: [
                            _patientVm!.patient!.rotation_right_recheck != 0 &&
                                    _patientVm!.patient!.rotation_right_recheck == 2
                                ? Image.asset(
                                    'assets/images/pain_level.png',
                                    width: 20,
                                  )
                                : Container(),
                            _patientVm!.patient!.rotation_right_recheck != 0
                                ? Image.asset(
                                    'assets/images/pain_level.png',
                                    width: 20,
                                  )
                                : Container(),
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Container(
                          alignment: Alignment.center,
                          child: FittedBox(
                            fit:BoxFit.scaleDown,
                            child: Text(
                              getTranslated(context, 'Rotate_right'),
                              textScaleFactor: 1,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 20),
                            ),
                          ),
                        )
                      ],
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    _patientVm!.patient!.extension_recheck != 0 &&
                            _patientVm!.patient!.extension_recheck == 2
                        ? Image.asset(
                            'assets/images/pain_level.png',
                            width: 20,
                          )
                        : Container(),
                    _patientVm!.patient!.extension_recheck != 0
                        ? Image.asset(
                            'assets/images/pain_level.png',
                            width: 20,
                          )
                        : Container(),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  alignment: Alignment.center,
                  child: FittedBox(
                    fit:BoxFit.scaleDown,
                    child: Text(
                      getTranslated(context, 'Straighten_the_body'),
                      textScaleFactor: 1,
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                    ),
                  ),
                )
              ],
            ),
          ),
          //圖
          Container(
            alignment: Alignment.bottomCenter,
            child: Image.asset(
              'assets/images/result_blue.png',
              width: 850,
              height: 550,
            ),
          )
        ],
      ),
    );
  }
}
