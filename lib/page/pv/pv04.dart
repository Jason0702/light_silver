import 'dart:async';
import 'dart:developer';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:light_silver/language/language_constants.dart';
import 'package:light_silver/models/patient.dart';
import 'package:light_silver/utils/utils.dart';
import 'package:light_silver/widgets/noShadowScrollbehavior.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:provider/provider.dart';
import 'package:share_plus/share_plus.dart';

import '../../app.dart';
import '../../routes/route_name.dart';
import '../../view_model/member_vm.dart';
import '../../view_model/patient_vm.dart';
import '../../view_model/user_company_information_vm.dart';
import '../../widgets/left_button.dart';
import '../../widgets/title_widget.dart';

class PV04Routes extends StatefulWidget {
  @override
  State createState() => _PV04RoutesState();
}

class _PV04RoutesState extends State<PV04Routes> {
  PatientVm? _patientVm;
  MemberVm? _memberVm;
  UserCompanyInformationVm? _userCompanyInformationVm;

  String _companyName = '';
  String _companyPhone = '';
  String _companyAddress = '';
  String _companyLOGO = '';

  String _patientName = '';
  String _patientGender = '';
  String _patientAge = '';
  String _patientPhone = '';
  String _patientProblem = '';

  String _problemText = '';
  String _problemSideText = '';
  bool _pad = false;
  String _padHigh = '';

  //減緩不適症狀
  bool _relieveSymptomsOfDiscomfort = false;

  //姿勢矯正
  bool _postureCorrection = false;

  //資訊衛教
  bool _informationHealthEducation = false;

  //關節復位
  bool _jointReduction = false;

  //核心肌群訓練
  bool _coreMuscleTraining = false;

  //健康促進
  bool _healthPromotion = false;

//資料title
  final _dataTitleBg = BoxDecoration(
      borderRadius: const BorderRadius.only(
          topLeft: Radius.circular(10), topRight: Radius.circular(10)),
      color: HexColor.fromHex('#096BC7'));

  //_buttonBG

  final TextEditingController _floorResult = TextEditingController();
  final TextEditingController _sideKneeLeftResult = TextEditingController();
  final TextEditingController _sideKneeRightResult = TextEditingController();

  final TextEditingController _summary1 = TextEditingController();

  final TextEditingController _floorRecheckResult = TextEditingController();
  final TextEditingController _sideKneeLeftRecheckResult = TextEditingController();
  final TextEditingController _sideKneeRightRecheckResult = TextEditingController();

  final TextEditingController _summary2 = TextEditingController();

  _onLayoutDone(_) {
    _dataInit(_patientVm!.patient);
  }

  void _dataInit(Patient? patient) {
    if (patient == null) {
      return;
    }
    log('取得 預覽(4): $patient');
    setState(() {
      _floorResult.text = patient.floor;
      _sideKneeLeftResult.text = patient.side_knee_left;
      _sideKneeRightResult.text = patient.side_knee_right;

      _floorRecheckResult.text = patient.floor_recheck;
      _sideKneeLeftRecheckResult.text = patient.side_knee_left_recheck;
      _sideKneeRightRecheckResult.text = patient.side_knee_right_recheck;
    });
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  Widget build(BuildContext context) {
    _patientVm = Provider.of<PatientVm>(context);
    _memberVm = Provider.of<MemberVm>(context);
    _userCompanyInformationVm = Provider.of<UserCompanyInformationVm>(context);
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: true,
      body: SafeArea(
        child: Column(
          children: [
            Expanded(flex: 1, child: _titleBar()),
            Expanded(flex: 12, child: _mainWidget())
          ],
        ),
      ),
    );
  }

  //Title
  Widget _titleBar() {
    return PvTitleWidget(
      titleText: getTranslated(context, 'Preview'),
      leftButton: const LeftButton(),
      rightButton: Container(),
    );
  }

  //主體
  Widget _mainWidget() {
    return Container(
      decoration: Style.homeBg,
      alignment: Alignment.center,
      child: Container(
        width: MediaQuery.of(context).size.width / 1.1,
        height: MediaQuery.of(context).size.height / 1.2,
        decoration: Style.centerBG,
        padding: const EdgeInsets.only(top: 15, left: 10, right: 10, bottom: 15),
        child: ScrollConfiguration(
          behavior: NoShadowScrollBehavior(),
          child: buildIndexFrom(),
        ),
      ),
    );
  }

  //表單
  Widget buildIndexFrom() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Expanded(flex: 1, child: fromHeader()),
        const SizedBox(
          height: 10,
        ),
        Expanded(flex: 9, child: fromBody()),
        const SizedBox(
          height: 20,
        )
      ],
    );
  }

  //表單頭
  Widget fromHeader() {
    return Container(
      width: MediaQuery.of(context).size.width,
      decoration: const BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/images/record_sheet_title.png'),
              fit: BoxFit.fill)),
      alignment: Alignment.center,
      child: FittedBox(
        fit:BoxFit.scaleDown,
        child: Text(
          getTranslated(
              context, 'Guangyin_Method_Evaluation_and_Adjustment_Record_Sheet'),
          textScaleFactor: 1,
          style: const TextStyle(
              color: Colors.white, fontSize: 23, fontWeight: FontWeight.bold),
        ),
      ),
    );
  }

  //表單身
  Widget fromBody() {
    return ScrollConfiguration(
        behavior: NoShadowScrollBehavior(),
        child: SingleChildScrollView(
          child: Column(
            children: [
              //介入調整前
              _beforeInterventionAdjustmentWidget(),
              //介入調整後
              _afterInterventionAndAdjustmentWidget(),
              const SizedBox(
                height: 15,
              ),
              //評估者
              Container(
                alignment: Alignment.centerRight,
                child: FittedBox(
                  fit:BoxFit.scaleDown,
                  child: Consumer<MemberVm>(builder: (context, vm, _){
                    return Text(
                      '評估者 ${vm.memberModel!.name}',
                      textScaleFactor: 1,
                      style:
                      const TextStyle(fontSize: 23, fontWeight: FontWeight.bold),
                    );
                  },),
                ),
              ),
              //標語
              Container(
                height: 50,
                alignment: Alignment.centerLeft,
                decoration: BoxDecoration(color: HexColor.fromHex('#609AD1')),
                padding: const EdgeInsets.only(left: 8),
                child: FittedBox(
                  fit:BoxFit.scaleDown,
                  child: Text(
                    getTranslated(context,
                        'All_rights_reserved_by_the_light_silver_technique_piracy_must_be_punished'),
                    textScaleFactor: 1,
                    style: const TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              //選項
              Container(
                margin: const EdgeInsets.only(top: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    _saveButtonWidget(),
                    _outPDFFileWidget(),
                    _shardWidget()
                  ],
                ),
              )
            ],
          ),
        ));
  }

  //介入調整前
  Widget _beforeInterventionAdjustmentWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 8),
      child: Column(
        children: [
          //標題
          Container(
            height: 50,
            alignment: Alignment.center,
            decoration: _dataTitleBg,
            child: FittedBox(
              fit:BoxFit.scaleDown,
              child: Text(
                getTranslated(context, 'Before_intervention_adjustment'),
                textScaleFactor: 1,
                style:
                    const TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
              ),
            ),
          ),
          //圖片
          Container(
            alignment: Alignment.centerRight,
            child: Stack(
              children: [
                Container(
                  alignment: Alignment.center,
                  child: Image.asset(
                    'assets/images/body.png',
                    fit: BoxFit.fill,
                  ),
                ),
                Positioned(
                    bottom: 1,
                    right: 1,
                    child: Container(
                      alignment: Alignment.centerRight,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Row(
                            children: [
                              Image.asset(
                                'assets/images/pain_level.png',
                                width: 20,
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              FittedBox(
                                fit:BoxFit.scaleDown,
                                child: const Text(
                                  '輕微受損',
                                  textScaleFactor: 1,
                                  style: TextStyle(
                                      color: Colors.red,
                                      fontWeight: FontWeight.bold),
                                ),
                              )
                            ],
                          ),
                          Row(
                            children: [
                              Image.asset(
                                'assets/images/pain_level.png',
                                width: 20,
                              ),
                              Image.asset(
                                'assets/images/pain_level.png',
                                width: 20,
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              FittedBox(
                                fit:BoxFit.scaleDown,
                                child: const Text(
                                  '嚴重受損',
                                  textScaleFactor: 1,
                                  style: TextStyle(
                                      color: Colors.red,
                                      fontWeight: FontWeight.bold),
                                ),
                              )
                            ],
                          )
                        ],
                      ),
                    )),
              ],
            ),
          ),
          //底部身體問題圖
          _beforeBodyImageWidget(),
          //備註
          Column(
            children: [
              Container(
                alignment: Alignment.centerLeft,
                child: FittedBox(
                  fit:BoxFit.scaleDown,
                  child: const Text(
                    '備註',
                    textScaleFactor: 1,
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(left: 25, right: 25),
                child: TextField(
                  maxLines: 5,
                  maxLength: 500,
                  decoration: InputDecoration(
                      errorText: null,
                      contentPadding: const EdgeInsets.all(5.0),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey[400]!),
                          borderRadius: BorderRadius.circular(10)),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10)),
                      filled: true,
                      fillColor: Colors.white,
                      hintText: '',
                      hintStyle: const TextStyle(color: Colors.grey)),
                  keyboardType: TextInputType.text,
                  textInputAction: TextInputAction.done,
                  controller: _summary1,
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  //介入前身體圖
  Widget _beforeBodyImageWidget() {
    return SizedBox(
      width: 950,
      height: 390,
      child: Stack(
        children: [
          //文字
          Container(
            margin: const EdgeInsets.only(top: 10, left: 15, right: 15),
            child: Column(
              children: [
                //身體前屈
                Column(
                  children: [
                    FittedBox(
                      fit:BoxFit.scaleDown,
                      child: Text(
                        getTranslated(context, 'Bend_forward'),
                        textScaleFactor: 1,
                        style: const TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          margin: const EdgeInsets.all(8),
                          width: 80,
                          child: TextField(
                            readOnly: true,
                            textAlign: TextAlign.center,
                            style: const TextStyle(color: Colors.black),
                            decoration: const InputDecoration(
                                contentPadding: EdgeInsets.all(5),
                                enabledBorder: OutlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.black)),
                                filled: true,
                                fillColor: Colors.white),
                            keyboardType: TextInputType.number,
                            textInputAction: TextInputAction.done,
                            controller: _floorResult,
                          ),
                        ),
                        FittedBox(
                          fit:BoxFit.scaleDown,
                          child: Text(
                            getTranslated(context, 'Cm'),
                            textScaleFactor: 1,
                            style: const TextStyle(
                                fontSize: 20,
                                color: Colors.black,
                                fontWeight: FontWeight.bold),
                          ),
                        )
                      ],
                    )
                  ],
                ),
                //左側
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    //左側彎
                    Column(
                      children: [
                        FittedBox(
                          fit:BoxFit.scaleDown,
                          child: Text(
                            getTranslated(context, 'Left_side_bend'),
                            textScaleFactor: 1,
                            style: const TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              margin: const EdgeInsets.all(8),
                              width: 80,
                              child: TextField(
                                readOnly: true,
                                textAlign: TextAlign.center,
                                style: const TextStyle(color: Colors.black),
                                decoration: const InputDecoration(
                                    contentPadding: EdgeInsets.all(5),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide:
                                            BorderSide(color: Colors.black)),
                                    filled: true,
                                    fillColor: Colors.white),
                                keyboardType: TextInputType.number,
                                textInputAction: TextInputAction.done,
                                controller: _sideKneeLeftResult,
                              ),
                            ),
                            FittedBox(
                              fit:BoxFit.scaleDown,
                              child: Text(
                                getTranslated(context, 'Cm'),
                                textScaleFactor: 1,
                                style: const TextStyle(
                                    fontSize: 20,
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold),
                              ),
                            )
                          ],
                        ),
                        //左側彎XX
                        Container(
                          margin: const EdgeInsets.only(top: 5, left: 30),
                          child: Row(
                            children: [
                              _patientVm!.patient!.side_left != 0 || _patientVm!.patient!.side_left == 2
                                  ? Image.asset(
                                      'assets/images/pain_level.png',
                                      width: 20,
                                    )
                                  : Container(),
                              _patientVm!.patient!.side_left != 0
                                  ? Image.asset(
                                      'assets/images/pain_level.png',
                                      width: 20,
                                    )
                                  : Container(),
                            ],
                          ),
                        )
                      ],
                    ),
                    //身體前屈XX
                    Row(
                      children: [
                        _patientVm!.patient!.floor.isEmpty
                            ? FittedBox(
                              fit:BoxFit.scaleDown,
                              child: Text(
                                  getTranslated(context, 'default_value'),
                                  textScaleFactor: 1,
                                  style: const TextStyle(color: Colors.red),
                                ),
                            )
                            : Container(),
                        _patientVm!.patient!.floor.isNotEmpty &&
                                int.parse(_patientVm!.patient!.floor) <= 5 &&
                                int.parse(_patientVm!.patient!.floor) != 100 &&
                                int.parse(_patientVm!.patient!.floor) != 0
                            ? Image.asset(
                                'assets/images/pain_level.png',
                                width: 20,
                              )
                            : Container(),
                        _patientVm!.patient!.floor.isNotEmpty &&
                                int.parse(_patientVm!.patient!.floor) > 5 &&
                                int.parse(_patientVm!.patient!.floor) != 0 &&
                                int.parse(_patientVm!.patient!.floor) != 100
                            ? Image.asset(
                                'assets/images/pain_level.png',
                                width: 20,
                              )
                            : Container(),
                      ],
                    ),
                    //右側彎
                    Column(
                      children: [
                        FittedBox(
                          fit:BoxFit.scaleDown,
                          child: Text(
                            getTranslated(context, 'Right_side_bend'),
                            textScaleFactor: 1,
                            style: const TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              margin: const EdgeInsets.all(8),
                              width: 80,
                              child: TextField(
                                readOnly: true,
                                textAlign: TextAlign.center,
                                style: const TextStyle(color: Colors.black),
                                decoration: const InputDecoration(
                                    contentPadding: EdgeInsets.all(5),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide:
                                            BorderSide(color: Colors.black)),
                                    filled: true,
                                    fillColor: Colors.white),
                                keyboardType: TextInputType.number,
                                textInputAction: TextInputAction.done,
                                controller: _sideKneeRightResult,
                              ),
                            ),
                            FittedBox(
                              fit:BoxFit.scaleDown,
                              child: Text(
                                getTranslated(context, 'Cm'),
                                textScaleFactor: 1,
                                style: const TextStyle(
                                    fontSize: 20,
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold),
                              ),
                            )
                          ],
                        ),
                        //右側彎XX
                        Container(
                          margin: const EdgeInsets.only(top: 5, right: 30),
                          child: Row(
                            children: [
                              _patientVm!.patient!.side_right != 0 && _patientVm!.patient!.side_right == 2
                                  ? Image.asset(
                                      'assets/images/pain_level.png',
                                      width: 20,
                                    )
                                  : Container(),
                              _patientVm!.patient!.side_right != 0
                                  ? Image.asset(
                                      'assets/images/pain_level.png',
                                      width: 20,
                                    )
                                  : Container(),
                            ],
                          ),
                        ),
                      ],
                    )
                  ],
                ),
                SizedBox(
                  height: 35,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Column(
                      children: [
                        //左旋轉XX
                        Row(
                          children: [
                            _patientVm!.patient!.rotation_left != 0 &&
                                    _patientVm!.patient!.rotation_left == 2
                                ? Image.asset(
                                    'assets/images/pain_level.png',
                                    width: 20,
                                  )
                                : Container(),
                            _patientVm!.patient!.rotation_left != 0
                                ? Image.asset(
                                    'assets/images/pain_level.png',
                                    width: 20,
                                  )
                                : Container(),
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Container(
                          alignment: Alignment.center,
                          child: FittedBox(
                            fit:BoxFit.scaleDown,
                            child: Text(
                              getTranslated(context, 'Rotate_left'),
                              textScaleFactor: 1,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 20),
                            ),
                          ),
                        )
                      ],
                    ),
                    Column(
                      children: [
                        //右旋轉XX
                        Row(
                          children: [
                            _patientVm!.patient!.rotation_right != 0 &&
                                    _patientVm!.patient!.rotation_right == 2
                                ? Image.asset(
                                    'assets/images/pain_level.png',
                                    width: 20,
                                  )
                                : Container(),
                            _patientVm!.patient!.rotation_right != 0
                                ? Image.asset(
                                    'assets/images/pain_level.png',
                                    width: 20,
                                  )
                                : Container(),
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Container(
                          alignment: Alignment.center,
                          child: FittedBox(
                            fit:BoxFit.scaleDown,
                            child: Text(
                              getTranslated(context, 'Rotate_right'),
                              textScaleFactor: 1,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 20),
                            ),
                          ),
                        )
                      ],
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    _patientVm!.patient!.Extension != 0 && _patientVm!.patient!.Extension == 2
                        ? Image.asset(
                            'assets/images/pain_level.png',
                            width: 20,
                          )
                        : Container(),
                    _patientVm!.patient!.Extension != 0
                        ? Image.asset(
                            'assets/images/pain_level.png',
                            width: 20,
                          )
                        : Container(),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  alignment: Alignment.center,
                  child: FittedBox(
                    fit:BoxFit.scaleDown,
                    child: Text(
                      getTranslated(context, 'Straighten_the_body'),
                      textScaleFactor: 1,
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                    ),
                  ),
                )
              ],
            ),
          ),
          //圖
          Container(
            alignment: Alignment.bottomCenter,
            child: Image.asset(
              'assets/images/result_blue.png',
              width: 850,
              height: 550,
            ),
          )
        ],
      ),
    );
  }

  //介入調整後
  Widget _afterInterventionAndAdjustmentWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 8),
      child: Column(
        children: [
          //標題
          Container(
            height: 80,
            alignment: Alignment.center,
            decoration: _dataTitleBg,
            child: FittedBox(
              fit:BoxFit.scaleDown,
              child: Text(
                getTranslated(context, 'After_intervention_and_adjustment'),
                textScaleFactor: 1,
                style:
                    const TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
              ),
            ),
          ),
          //圖片
          Container(
            alignment: Alignment.centerRight,
            child: Stack(
              children: [
                Container(
                  alignment: Alignment.center,
                  child: Image.asset(
                    'assets/images/body.png',
                    fit: BoxFit.fill,
                  ),
                ),
                Positioned(
                    bottom: 1,
                    right: 1,
                    child: Container(
                      alignment: Alignment.centerRight,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Row(
                            children: [
                              Image.asset(
                                'assets/images/pain_level.png',
                                width: 20,
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              FittedBox(
                                fit:BoxFit.scaleDown,
                                child: const Text(
                                  '輕微受損',
                                  textScaleFactor: 1,
                                  style: TextStyle(
                                      color: Colors.red,
                                      fontWeight: FontWeight.bold),
                                ),
                              )
                            ],
                          ),
                          Row(
                            children: [
                              Image.asset(
                                'assets/images/pain_level.png',
                                width: 20,
                              ),
                              Image.asset(
                                'assets/images/pain_level.png',
                                width: 20,
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              FittedBox(
                                fit:BoxFit.scaleDown,
                                child: const Text(
                                  '嚴重受損',
                                  textScaleFactor: 1,
                                  style: TextStyle(
                                      color: Colors.red,
                                      fontWeight: FontWeight.bold),
                                ),
                              )
                            ],
                          )
                        ],
                      ),
                    )),
              ],
            ),
          ),
          //介入後身體圖
          _afterBodyImageWidget(),
          //備註
          Column(
            children: [
              Container(
                alignment: Alignment.centerLeft,
                child: FittedBox(
                  fit:BoxFit.scaleDown,
                  child: const Text(
                    '備註',
                    textScaleFactor: 1,
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(left: 25, right: 25),
                child: TextField(
                  maxLines: 5,
                  maxLength: 500,
                  decoration: InputDecoration(
                      errorText: null,
                      contentPadding: const EdgeInsets.all(5.0),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey[400]!),
                          borderRadius: BorderRadius.circular(10)),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10)),
                      filled: true,
                      fillColor: Colors.white,
                      hintText: '',
                      hintStyle: const TextStyle(color: Colors.grey)),
                  keyboardType: TextInputType.text,
                  textInputAction: TextInputAction.done,
                  controller: _summary2,
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  //介入後身體圖
  Widget _afterBodyImageWidget() {
    return SizedBox(
      width: 950,
      height: 390,
      child: Stack(
        children: [
          //文字
          Container(
            margin: const EdgeInsets.only(top: 10, left: 15, right: 15),
            child: Column(
              children: [
                //身體前屈
                Column(
                  children: [
                    FittedBox(
                      fit:BoxFit.scaleDown,
                      child: Text(
                        getTranslated(context, 'Bend_forward'),
                        textScaleFactor: 1,
                        style: const TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          margin: const EdgeInsets.all(8),
                          width: 80,
                          child: TextField(
                            readOnly: true,
                            textAlign: TextAlign.center,
                            style: const TextStyle(color: Colors.black),
                            decoration: const InputDecoration(
                                contentPadding: EdgeInsets.all(5),
                                enabledBorder: OutlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.black)),
                                filled: true,
                                fillColor: Colors.white),
                            keyboardType: TextInputType.number,
                            textInputAction: TextInputAction.done,
                            controller: _floorRecheckResult,
                          ),
                        ),
                        FittedBox(
                          fit:BoxFit.scaleDown,
                          child: Text(
                            getTranslated(context, 'Cm'),
                            textScaleFactor: 1,
                            style: TextStyle(
                                fontSize: 20,
                                color: Colors.black,
                                fontWeight: FontWeight.bold),
                          ),
                        )
                      ],
                    )
                  ],
                ),
                //左側
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    //左側彎
                    Column(
                      children: [
                        FittedBox(
                          fit:BoxFit.scaleDown,
                          child: Text(
                            getTranslated(context, 'Left_side_bend'),
                            textScaleFactor: 1,
                            style: const TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              margin: const EdgeInsets.all(8),
                              width: 80,
                              child: TextField(
                                readOnly: true,
                                textAlign: TextAlign.center,
                                style: const TextStyle(color: Colors.black),
                                decoration: const InputDecoration(
                                    contentPadding: EdgeInsets.all(5),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide:
                                            BorderSide(color: Colors.black)),
                                    filled: true,
                                    fillColor: Colors.white),
                                keyboardType: TextInputType.number,
                                textInputAction: TextInputAction.done,
                                controller: _sideKneeLeftRecheckResult,
                              ),
                            ),
                            FittedBox(
                              fit:BoxFit.scaleDown,
                              child: Text(
                                getTranslated(context, 'Cm'),
                                textScaleFactor: 1,
                                style: TextStyle(
                                    fontSize: 20,
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold),
                              ),
                            )
                          ],
                        ),
                        //左側彎XX
                        Container(
                          margin: const EdgeInsets.only(top: 5, left: 30),
                          child: Row(
                            children: [
                              _patientVm!.patient!.side_left_recheck != 0 ||
                                      _patientVm!.patient!.side_left_recheck == 2
                                  ? Image.asset(
                                      'assets/images/pain_level.png',
                                      width: 20,
                                    )
                                  : Container(),
                              _patientVm!.patient!.side_left_recheck != 0
                                  ? Image.asset(
                                      'assets/images/pain_level.png',
                                      width: 20,
                                    )
                                  : Container(),
                            ],
                          ),
                        )
                      ],
                    ),
                    //身體前屈XX
                    Row(
                      children: [
                        _patientVm!.patient!.floor_recheck.isNotEmpty &&
                                int.parse(_patientVm!.patient!.floor_recheck) <= 5 &&
                                int.parse(_patientVm!.patient!.floor_recheck) != 100 &&
                                int.parse(_patientVm!.patient!.floor_recheck) != 0
                            ? Image.asset(
                                'assets/images/pain_level.png',
                                width: 20,
                              )
                            : Container(),
                        _patientVm!.patient!.floor_recheck.isNotEmpty &&
                                int.parse(_patientVm!.patient!.floor_recheck) > 5 &&
                                int.parse(_patientVm!.patient!.floor_recheck) != 0 &&
                                int.parse(_patientVm!.patient!.floor_recheck) != 100
                            ? Image.asset(
                                'assets/images/pain_level.png',
                                width: 20,
                              )
                            : Container(),
                        _patientVm!.patient!.floor_recheck.isEmpty
                            ? FittedBox(
                              fit:BoxFit.scaleDown,
                              child: Text(
                                  getTranslated(context, 'default_value'),
                                  textScaleFactor: 1,
                                  style: const TextStyle(color: Colors.red),
                                ),
                            )
                            : Container(),
                      ],
                    ),
                    //右側彎
                    Column(
                      children: [
                        FittedBox(
                          fit:BoxFit.scaleDown,
                          child: Text(
                            getTranslated(context, 'Right_side_bend'),
                            textScaleFactor: 1,
                            style: const TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              margin: const EdgeInsets.all(8),
                              width: 80,
                              child: TextField(
                                readOnly: true,
                                textAlign: TextAlign.center,
                                style: const TextStyle(color: Colors.black),
                                decoration: const InputDecoration(
                                  contentPadding: EdgeInsets.all(5),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Colors.black)),
                                  filled: true,
                                  fillColor: Colors.white,
                                ),
                                keyboardType: TextInputType.number,
                                textInputAction: TextInputAction.done,
                                controller: _sideKneeRightRecheckResult,
                              ),
                            ),
                            FittedBox(
                              fit:BoxFit.scaleDown,
                              child: Text(
                                getTranslated(context, 'Cm'),
                                textScaleFactor: 1,
                                style: TextStyle(
                                    fontSize: 20,
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold),
                              ),
                            )
                          ],
                        ),
                        //右側彎XX
                        Container(
                          margin: const EdgeInsets.only(top: 5, right: 30),
                          child: Row(
                            children: [
                              _patientVm!.patient!.side_right_recheck != 0 &&
                                      _patientVm!.patient!.side_right_recheck == 2
                                  ? Image.asset(
                                      'assets/images/pain_level.png',
                                      width: 20,
                                    )
                                  : Container(),
                              _patientVm!.patient!.side_right_recheck != 0
                                  ? Image.asset(
                                      'assets/images/pain_level.png',
                                      width: 20,
                                    )
                                  : Container(),
                            ],
                          ),
                        )
                      ],
                    )
                  ],
                ),
                SizedBox(
                  height: 35,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Column(
                      children: [
                        //左旋轉XX
                        Row(
                          children: [
                            _patientVm!.patient!.rotation_left_recheck != 0 &&
                                    _patientVm!.patient!.rotation_left_recheck == 2
                                ? Image.asset(
                                    'assets/images/pain_level.png',
                                    width: 20,
                                  )
                                : Container(),
                            _patientVm!.patient!.rotation_left_recheck != 0
                                ? Image.asset(
                                    'assets/images/pain_level.png',
                                    width: 20,
                                  )
                                : Container(),
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Container(
                          alignment: Alignment.center,
                          child: FittedBox(
                            fit:BoxFit.scaleDown,
                            child: Text(
                              getTranslated(context, 'Rotate_left'),
                              textScaleFactor: 1,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 20),
                            ),
                          ),
                        )
                      ],
                    ),
                    Column(
                      children: [
                        //右旋轉
                        Row(
                          children: [
                            _patientVm!.patient!.rotation_right_recheck != 0 &&
                                    _patientVm!.patient!.rotation_right_recheck == 2
                                ? Image.asset(
                                    'assets/images/pain_level.png',
                                    width: 20,
                                  )
                                : Container(),
                            _patientVm!.patient!.rotation_right_recheck != 0
                                ? Image.asset(
                                    'assets/images/pain_level.png',
                                    width: 20,
                                  )
                                : Container(),
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Container(
                          alignment: Alignment.center,
                          child: FittedBox(
                            fit:BoxFit.scaleDown,
                            child: Text(
                              getTranslated(context, 'Rotate_right'),
                              textScaleFactor: 1,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 20),
                            ),
                          ),
                        )
                      ],
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    _patientVm!.patient!.extension_recheck != 0 &&
                            _patientVm!.patient!.extension_recheck == 2
                        ? Image.asset(
                            'assets/images/pain_level.png',
                            width: 20,
                          )
                        : Container(),
                    _patientVm!.patient!.extension_recheck != 0
                        ? Image.asset(
                            'assets/images/pain_level.png',
                            width: 20,
                          )
                        : Container(),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  alignment: Alignment.center,
                  child: FittedBox(
                    fit:BoxFit.scaleDown,
                    child: Text(
                      getTranslated(context, 'Straighten_the_body'),
                      textScaleFactor: 1,
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                    ),
                  ),
                )
              ],
            ),
          ),
          //圖
          Container(
            alignment: Alignment.bottomCenter,
            child: Image.asset(
              'assets/images/result_blue.png',
              width: 850,
              height: 550,
            ),
          )
        ],
      ),
    );
  }

  //儲存
  Widget _saveButtonWidget() {
    return InkWell(
      onTap: _saveButtonOnTap,
      child: Container(
        padding: const EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 5),
        alignment: Alignment.center,
        decoration: BoxDecoration(
            color: HexColor.fromHex('#0067B4'),
            borderRadius: BorderRadius.circular(10)),
        child: Row(
          children: const [
            FittedBox(
              fit:BoxFit.scaleDown,
              child: Text(
                '儲存',
                textScaleFactor: 1,
                style: TextStyle(color: Colors.white),
              ),
            )
          ],
        ),
      ),
    );
  }

  void _saveButtonOnTap() {
    delegate.popUtil();
  }

  //輸出PDF
  Widget _outPDFFileWidget() {
    return InkWell(
      onTap: () {
        delegate.push(name: RouteName.pvPDF);
      },
      child: Container(
        padding: const EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 5),
        alignment: Alignment.center,
        decoration: BoxDecoration(
            color: HexColor.fromHex('#0067B4'),
            borderRadius: BorderRadius.circular(10)),
        child: Row(
          children: const [
            FittedBox(
              fit:BoxFit.scaleDown,
              child: Text(
                '輸出PDF',
                textScaleFactor: 1,
                style: TextStyle(color: Colors.white),
              ),
            )
          ],
        ),
      ),
    );
  }

  //分享
  Widget _shardWidget() {
    return InkWell(
      onTap: () {
        _createPDF();
      },
      child: Container(
        padding: const EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 5),
        alignment: Alignment.center,
        decoration: BoxDecoration(
            color: HexColor.fromHex('#0067B4'),
            borderRadius: BorderRadius.circular(10)),
        child: Row(
          children: const [
            FittedBox(
              fit:BoxFit.scaleDown,
              child: Text(
                '分享',
                textScaleFactor: 1,
                style: TextStyle(color: Colors.white),
              ),
            )
          ],
        ),
      ),
    );
  }

  void dataInit() {
    _companyName = _userCompanyInformationVm!.userCompanyInformationModel!.name;
    _companyPhone = _userCompanyInformationVm!.userCompanyInformationModel!.phone;
    _companyAddress =
        '${_userCompanyInformationVm!.userCompanyInformationModel!.city} ${_userCompanyInformationVm!.userCompanyInformationModel!.area}${_userCompanyInformationVm!.userCompanyInformationModel!.address}';
    _companyLOGO = _userCompanyInformationVm!.userCompanyInformationModel!.logo;

    _patientName = _patientVm!.patient!.name;
    _patientGender = _patientVm!.patient!.gender;
    _patientAge = _patientVm!.patient!.age.toString();
    _patientPhone = _patientVm!.patient!.phone;
    _patientProblem = _patientVm!.patient!.problem;
    _relieveSymptomsOfDiscomfort =
    _patientVm!.patient!.relieveSymptomsOfDiscomfort == 1 ? true : false;
    _postureCorrection = _patientVm!.patient!.postureCorrection == 1 ? true : false;
    _informationHealthEducation =
    _patientVm!.patient!.informationHealthEducation == 1 ? true : false;
    _jointReduction = _patientVm!.patient!.jointReduction == 1 ? true : false;
    _coreMuscleTraining = _patientVm!.patient!.coreMuscleTraining == 1 ? true : false;
    _healthPromotion = _patientVm!.patient!.healthPromotion == 1 ? true : false;
    switch (_patientVm!.patient!.GT) {
      case 0:
        _problemText =
        '${getTranslated(context, 'Foot_length')}${getTranslated(context, 'Equal_length')}';
        break;
      case 1:
        _problemText =
        '${getTranslated(context, 'Foot_length')}${getTranslated(context, 'Unequal_length')}';
        break;
      case 2:
        _problemText =
        '${getTranslated(context, 'Foot_length')}${getTranslated(context, 'Equal_length')}';
        break;
      case 3:
        _problemText =
        '${getTranslated(context, 'Foot_length')}${getTranslated(context, 'Unequal_length')}';
        break;
    }
    switch (_patientVm!.patient!.pad) {
      case 0:
        _problemSideText = '';
        break;
      case 1:
        _problemSideText = getTranslated(context, 'left');
        break;
      case 2:
        _problemSideText = '';
        break;
      case 3:
        _problemSideText = getTranslated(context, 'right');
        break;
    }
    if (_patientVm!.patient!.pad != 2) {
      _pad = false;
    } else {
      _pad = true;
    }
    _padHigh = _patientVm!.patient!.padHigh;

    _floorResult.text = _patientVm!.patient!.floor;
    _sideKneeLeftResult.text = _patientVm!.patient!.side_knee_left;
    _sideKneeRightResult.text = _patientVm!.patient!.side_knee_right;

    _floorRecheckResult.text = _patientVm!.patient!.floor_recheck;
    _sideKneeLeftRecheckResult.text = _patientVm!.patient!.side_knee_left_recheck;
    _sideKneeRightRecheckResult.text = _patientVm!.patient!.side_knee_right_recheck;
  }

  Future<void> _createPDF() async {
    dataInit();
    print(_relieveSymptomsOfDiscomfort);
    //region 圖片
    Uint8List _titleBg =
        (await rootBundle.load('assets/images/record_sheet_title.png'))
            .buffer
            .asUint8List();
    Uint8List _bodyImage =
        (await rootBundle.load('assets/images/body.png')).buffer.asUint8List();
    Uint8List _painImage =
        (await rootBundle.load('assets/images/pain_level.png'))
            .buffer
            .asUint8List();
    Uint8List _resultImage =
        (await rootBundle.load('assets/images/result_blue.png'))
            .buffer
            .asUint8List();
    //endregion
    final doc = pw.Document();
    //region pdf檔案格式
    doc.addPage(pw.MultiPage(
        maxPages: 9,
        theme: pw.ThemeData.withFont(
          base: pw.Font.ttf(await rootBundle.load("assets/fonts/msjh.ttf")),
        ),
        crossAxisAlignment: pw.CrossAxisAlignment.start,
        //region 頁首
        header: (pw.Context context) {
          if (context.pageNumber != 1) {
            return pw.Container();
          }
          return pw.Container(
              height: 70,
              alignment: pw.Alignment.center,
              decoration: pw.BoxDecoration(
                  image: pw.DecorationImage(
                image: pw.MemoryImage(_titleBg),
              )),
              child: pw.Text('光銀式手法評估與調整紀錄',
                  style: pw.Theme.of(context)
                      .defaultTextStyle
                      .copyWith(fontSize: 30, color: PdfColors.white)));
        },
        //endregion
        //region 頁尾
        footer: (pw.Context context) {
          return pw.Column(
              crossAxisAlignment: pw.CrossAxisAlignment.center,
              mainAxisAlignment: pw.MainAxisAlignment.center,
              children: [
                pw.Center(
                    child:
                        pw.Text('${context.pageNumber}/${context.pagesCount}'))
              ]);
        },
        //endregion
        build: (pw.Context context) => [
              //region 第一列
              pw.Row(
                  mainAxisAlignment: pw.MainAxisAlignment.spaceAround,
                  children: [
                    //region 公司資訊
                    pw.Expanded(
                      child: pw.Container(
                        height: 70,
                        margin: const pw.EdgeInsets.only(top: 8),
                        decoration: pw.BoxDecoration(
                          border:
                              pw.Border.all(color: PdfColors.grey, width: 1.5),
                          borderRadius:
                              const pw.BorderRadius.all(pw.Radius.circular(10)),
                        ),
                        child: pw.Row(
                          children: [
                            pw.Container(
                              margin: const pw.EdgeInsets.only(left: 8),
                              child: pw.Column(
                                mainAxisAlignment: pw.MainAxisAlignment.center,
                                crossAxisAlignment: pw.CrossAxisAlignment.start,
                                children: [
                                  pw.Text(
                                    _companyName,
                                    textScaleFactor: 1,
                                  ),
                                  pw.Text(
                                    _companyPhone,
                                    textScaleFactor: 1,
                                  ),
                                  pw.Text(
                                    _companyAddress,
                                    textScaleFactor: 1,
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    //endregion
                    //region 診察與治療日期
                    pw.Container()
                    //endregion
                  ]),
              //endregion
              //region 第二列
              pw.Row(
                  mainAxisAlignment: pw.MainAxisAlignment.spaceAround,
                  children: [
                    //region 病人姓名
                    pw.Expanded(
                      child: pw.Container(
                          margin: const pw.EdgeInsets.only(top: 8),
                          child: pw.Column(
                            crossAxisAlignment: pw.CrossAxisAlignment.start,
                            children: [
                              pw.Container(
                                child: pw.Text(
                                  '姓名',
                                  style: pw.Theme.of(context)
                                      .defaultTextStyle
                                      .copyWith(
                                          fontSize: 20, color: PdfColors.black),
                                ),
                              ),
                              pw.Container(
                                  alignment: pw.Alignment.centerLeft,
                                  padding: const pw.EdgeInsets.only(
                                      top: 5, bottom: 5, left: 10, right: 10),
                                  decoration: pw.BoxDecoration(
                                    border: pw.Border.all(
                                        color: PdfColors.grey, width: 1.5),
                                    borderRadius: const pw.BorderRadius.all(
                                        pw.Radius.circular(10)),
                                  ),
                                  child: pw.Text(_patientName,
                                      style: pw.Theme.of(context)
                                          .defaultTextStyle
                                          .copyWith(
                                              fontSize: 15,
                                              color: PdfColors.black))),
                            ],
                          )),
                    ),

                    //endregion
                    //region 病人性別
                    pw.Expanded(
                      child: pw.Container(
                          margin: const pw.EdgeInsets.only(top: 8),
                          child: pw.Column(
                            crossAxisAlignment: pw.CrossAxisAlignment.start,
                            children: [
                              pw.Container(
                                child: pw.Text(
                                  '性別',
                                  style: pw.Theme.of(context)
                                      .defaultTextStyle
                                      .copyWith(
                                          fontSize: 20, color: PdfColors.black),
                                ),
                              ),
                              pw.Container(
                                  alignment: pw.Alignment.centerLeft,
                                  padding: const pw.EdgeInsets.only(
                                      top: 5, bottom: 5, left: 10, right: 10),
                                  decoration: pw.BoxDecoration(
                                    border: pw.Border.all(
                                        color: PdfColors.grey, width: 1.5),
                                    borderRadius: const pw.BorderRadius.all(
                                        pw.Radius.circular(10)),
                                  ),
                                  child: pw.Text(_patientGender,
                                      style: pw.Theme.of(context)
                                          .defaultTextStyle
                                          .copyWith(
                                              fontSize: 15,
                                              color: PdfColors.black))),
                            ],
                          )),
                    )

                    //endregion
                  ]),
              //endregion
              //region 第三列
              pw.Row(
                  mainAxisAlignment: pw.MainAxisAlignment.spaceAround,
                  children: [
                    //region 病人年齡
                    pw.Expanded(
                      child: pw.Container(
                          margin: const pw.EdgeInsets.only(top: 8),
                          child: pw.Column(
                            crossAxisAlignment: pw.CrossAxisAlignment.start,
                            children: [
                              pw.Container(
                                child: pw.Text(
                                  '年齡',
                                  style: pw.Theme.of(context)
                                      .defaultTextStyle
                                      .copyWith(
                                          fontSize: 20, color: PdfColors.black),
                                ),
                              ),
                              pw.Container(
                                  alignment: pw.Alignment.centerLeft,
                                  padding: const pw.EdgeInsets.only(
                                      top: 5, bottom: 5, left: 10, right: 10),
                                  decoration: pw.BoxDecoration(
                                    border: pw.Border.all(
                                        color: PdfColors.grey, width: 1.5),
                                    borderRadius: const pw.BorderRadius.all(
                                        pw.Radius.circular(10)),
                                  ),
                                  child: pw.Text(_patientAge,
                                      style: pw.Theme.of(context)
                                          .defaultTextStyle
                                          .copyWith(
                                              fontSize: 15,
                                              color: PdfColors.black))),
                            ],
                          )),
                    ),
                    //endregion
                    //region 病人電話
                    pw.Expanded(
                      child: pw.Container(
                          margin: const pw.EdgeInsets.only(top: 8),
                          child: pw.Column(
                            crossAxisAlignment: pw.CrossAxisAlignment.start,
                            children: [
                              pw.Container(
                                child: pw.Text(
                                  '電話',
                                  style: pw.Theme.of(context)
                                      .defaultTextStyle
                                      .copyWith(
                                          fontSize: 20, color: PdfColors.black),
                                ),
                              ),
                              pw.Container(
                                  alignment: pw.Alignment.centerLeft,
                                  padding: const pw.EdgeInsets.only(
                                      top: 5, bottom: 5, left: 10, right: 10),
                                  decoration: pw.BoxDecoration(
                                    border: pw.Border.all(
                                        color: PdfColors.grey, width: 1.5),
                                    borderRadius: const pw.BorderRadius.all(
                                        pw.Radius.circular(10)),
                                  ),
                                  child: pw.Text(_patientPhone,
                                      style: pw.Theme.of(context)
                                          .defaultTextStyle
                                          .copyWith(
                                              fontSize: 15,
                                              color: PdfColors.black))),
                            ],
                          )),
                    ),
                    //endregion
                  ]),
              //endregion
              //region 問題
              pw.Container(
                height: 90,
                margin: const pw.EdgeInsets.only(top: 8),
                child: pw.Column(
                  children: [
                    pw.Expanded(
                      flex: 1,
                      child: pw.Container(
                        alignment: pw.Alignment.center,
                        decoration: pw.BoxDecoration(
                            borderRadius: const pw.BorderRadius.only(
                                topLeft: pw.Radius.circular(10),
                                topRight: pw.Radius.circular(10)),
                            color: PdfColor.fromHex('#096BC7')),
                        child: pw.Text(
                          '問題',
                          textScaleFactor: 1,
                          style: const pw.TextStyle(
                              fontSize: 25, color: PdfColors.white),
                        ),
                      ),
                    ),
                    pw.Expanded(
                      flex: 2,
                      child: pw.Container(
                          alignment: pw.Alignment.topLeft,
                          decoration: pw.BoxDecoration(
                              borderRadius: const pw.BorderRadius.only(
                                  bottomRight: pw.Radius.circular(10),
                                  bottomLeft: pw.Radius.circular(10)),
                              border: pw.Border.all(color: PdfColors.grey)),
                          child: pw.Text(_patientProblem,
                              style: const pw.TextStyle(fontSize: 15))),
                    ),
                  ],
                ),
              ),
              //endregion
              //region 目標
              pw.Container(
                height:130,
                margin: const pw.EdgeInsets.only(top: 8),
                child: pw.Column(
                  mainAxisAlignment: pw.MainAxisAlignment.spaceAround,
                  children: [
                    pw.Expanded(
                      flex: 1,
                      child: pw.Container(
                        alignment: pw.Alignment.center,
                        decoration: pw.BoxDecoration(
                            borderRadius: const pw.BorderRadius.only(
                                topLeft: pw.Radius.circular(10),
                                topRight: pw.Radius.circular(10)),
                            color: PdfColor.fromHex('#096BC7')),
                        child: pw.Text(
                          '目標',
                          textScaleFactor: 1,
                          style: const pw.TextStyle(
                              fontSize: 25, color: PdfColors.white),
                        ),
                      ),
                    ),
                    pw.Expanded(
                      flex: 3,
                      child: pw.Container(
                          padding: const pw.EdgeInsets.all(8),
                          alignment: pw.Alignment.topLeft,
                          decoration: pw.BoxDecoration(
                              borderRadius: const pw.BorderRadius.only(
                                  bottomRight: pw.Radius.circular(10),
                                  bottomLeft: pw.Radius.circular(10)),
                              border: pw.Border.all(color: PdfColors.grey)),
                          child: pw.Column(
                            children: [
                              //region 減緩不適症狀 姿勢矯正
                              pw.Row(
                                  mainAxisAlignment:
                                      pw.MainAxisAlignment.spaceAround,
                                  children: [
                                    //region 減緩不適症狀
                                    pw.Expanded(
                                      child: pw.Row(
                                        children: [
                                          pw.Transform.scale(
                                            scale: 1.5,
                                            child: pw.Checkbox(
                                              name: '',
                                              value: _relieveSymptomsOfDiscomfort,
                                            ),
                                          ),
                                          pw.SizedBox(width: 5),
                                          pw.Text(
                                            '減緩不適症狀',
                                            textScaleFactor: 1,
                                            style: const pw.TextStyle(fontSize: 15),
                                          )
                                        ],
                                      ),
                                    ),
                                    //endregion
                                    //region 姿勢矯正
                                    pw.Expanded(
                                      child: pw.Row(
                                        children: [
                                          pw.Transform.scale(
                                            scale: 1.5,
                                            child: pw.Checkbox(
                                              name: '',
                                              value: _postureCorrection,
                                            ),
                                          ),
                                          pw.SizedBox(width: 5),
                                          pw.Text(
                                            '姿勢矯正',
                                            textScaleFactor: 1,
                                            style: pw.TextStyle(
                                              fontSize: 20,
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                    //endregion
                                  ]),
                              //endregion
                              //region 資訊衛教 關節復位
                              pw.Row(
                                  mainAxisAlignment:
                                      pw.MainAxisAlignment.spaceAround,
                                  children: [
                                    //資訊衛教
                                    pw.Expanded(
                                      child: pw.Row(
                                        children: [
                                          pw.Transform.scale(
                                            scale: 1.5,
                                            child: pw.Checkbox(
                                              name: '',
                                              value: _informationHealthEducation,
                                            ),
                                          ),
                                          pw.SizedBox(width: 5),
                                          pw.Text(
                                            '資訊衛教',
                                            textScaleFactor: 1,
                                            style:
                                                pw.TextStyle(fontSize: 20),
                                          )
                                        ],
                                      ),
                                    ),
                                    //關節復位
                                    pw.Expanded(
                                      child: pw.Row(
                                        children: [
                                          pw.Transform.scale(
                                            scale: 1.5,
                                            child: pw.Checkbox(
                                              name: '',
                                              value: _jointReduction,
                                            ),
                                          ),
                                          pw.SizedBox(width: 5),
                                          pw.Text(
                                            '關節復位',
                                            textScaleFactor: 1,
                                            style:
                                                pw.TextStyle(fontSize: 20),
                                          )
                                        ],
                                      ),
                                    ),
                                  ]),
                              //endregion
                              //region 核心肌群訓練 健康促進
                              pw.Row(
                                  mainAxisAlignment:
                                      pw.MainAxisAlignment.spaceAround,
                                  children: [
                                    //核心肌群訓練
                                    pw.Expanded(
                                      child: pw.Row(
                                        children: [
                                          pw.Transform.scale(
                                            scale: 1.5,
                                            child: pw.Checkbox(
                                              name: '',
                                              value: _coreMuscleTraining,
                                            ),
                                          ),
                                          pw.SizedBox(width: 5),
                                          pw.Text(
                                            '核心肌群訓練',
                                            textScaleFactor: 1,
                                            style:
                                                pw.TextStyle(fontSize: 20),
                                          )
                                        ],
                                      ),
                                    ),
                                    //健康促進
                                    pw.Expanded(
                                        child: pw.Row(
                                      children: [
                                        pw.Transform.scale(
                                          scale: 1.5,
                                          child: pw.Checkbox(
                                            name: '',
                                            value: _healthPromotion,
                                          ),
                                        ),
                                        pw.SizedBox(width: 5),
                                        pw.Text(
                                          '健康促進',
                                          textScaleFactor: 1,
                                          style: pw.TextStyle(fontSize: 20),
                                        )
                                      ],
                                    ))
                                  ])
                              //endregion
                            ],
                          )),
                    ),
                  ],
                ),
              ),
              //endregion
              //region 長短腳問題列表與調整紀錄
              pw.Container(
                height: 170,
                margin: pw.EdgeInsets.only(top: 8),
                child: pw.Column(
                  children: [
                    pw.Expanded(
                      flex: 1,
                      child: pw.Container(
                        alignment: pw.Alignment.center,
                        decoration: pw.BoxDecoration(
                            borderRadius: const pw.BorderRadius.only(
                                topLeft: pw.Radius.circular(10),
                                topRight: pw.Radius.circular(10)),
                            color: PdfColor.fromHex('#096BC7')),
                        child: pw.Text(
                          '長短腳問題列表與調整紀錄',
                          textScaleFactor: 1,
                          style: const pw.TextStyle(
                              fontSize: 25, color: PdfColors.white),
                        ),
                      ),
                    ),
                    pw.Expanded(
                      flex: 3,
                      child: pw.Container(
                        child: pw.Column(
                          children: [
                            pw.Row(
                              mainAxisAlignment:
                                  pw.MainAxisAlignment.spaceAround,
                              crossAxisAlignment: pw.CrossAxisAlignment.start,
                              children: [
                                pw.Column(
                                    mainAxisAlignment:
                                        pw.MainAxisAlignment.start,
                                    children: [
                                      pw.Container(
                                        margin: const pw.EdgeInsets.only(top: 8),
                                        alignment: pw.Alignment.centerLeft,
                                        child: pw.Text(
                                          '問題',
                                          style: const pw.TextStyle(fontSize: 20),
                                        ),
                                      ),
                                      pw.Container(
                                          padding: const pw.EdgeInsets.only(
                                              top: 5,
                                              bottom: 5,
                                              left: 10,
                                              right: 10),
                                          decoration: pw.BoxDecoration(
                                            border: pw.Border.all(
                                                color: PdfColors.grey,
                                                width: 1.5),
                                            borderRadius: const pw.BorderRadius.all(
                                                pw.Radius.circular(10)),
                                          ),
                                          child: pw.Text(_problemText,
                                              style:
                                                  const pw.TextStyle(fontSize: 15))),
                                    ]),
                                pw.Column(
                                    mainAxisAlignment:
                                        pw.MainAxisAlignment.start,
                                    children: [
                                      pw.Container(
                                        margin: const pw.EdgeInsets.only(top: 8),
                                        alignment: pw.Alignment.centerLeft,
                                        child: pw.Text(
                                          '表徵',
                                          style: const pw.TextStyle(fontSize: 20),
                                        ),
                                      ),
                                      pw.Container(
                                        padding: const pw.EdgeInsets.only(
                                            top: 5,
                                            bottom: 5,
                                            left: 10,
                                            right: 10),
                                        decoration: pw.BoxDecoration(
                                          border: pw.Border.all(
                                              color: PdfColors.grey,
                                              width: 1.5),
                                          borderRadius: const pw.BorderRadius.all(
                                              pw.Radius.circular(10)),
                                        ),
                                        child: pw.Text(
                                            '腳長$_problemSideText側腳長較短',
                                            style: const pw.TextStyle(fontSize: 15)),
                                      ),
                                    ]),
                              ],
                            ),
                            pw.Container(
                              margin: const pw.EdgeInsets.only(top: 8),
                              alignment: pw.Alignment.centerLeft,
                              child: pw.Text(
                                '結果',
                                style: const pw.TextStyle(fontSize: 15),
                              ),
                            ),
                            pw.Container(
                              margin: const pw.EdgeInsets.only(top: 8),
                              child: pw.Row(
                                children: [
                                  pw.Transform.scale(
                                    scale: 1.5,
                                    child: pw.Checkbox(name: '',value: _pad),
                                  ),
                                  pw.SizedBox(width: 10),
                                  pw.Text(
                                    '建議穿著/購置鞋墊',
                                    textScaleFactor: 1,
                                    style: const pw.TextStyle(fontSize: 15),
                                  ),
                                  pw.Container(
                                    height: 20,
                                    width: 50,
                                    padding: const pw.EdgeInsets.only(
                                        top: 5, bottom: 5, left: 10, right: 10),
                                    decoration: pw.BoxDecoration(
                                      border: pw.Border.all(
                                          color: PdfColors.grey, width: 1.5),
                                      borderRadius: const pw.BorderRadius.all(
                                          pw.Radius.circular(10)),
                                    ),
                                    child: pw.Text(_padHigh,
                                        style: const pw.TextStyle(fontSize: 15)),
                                  ),
                                  pw.Text(
                                    '公分',
                                    textScaleFactor: 1,
                                    style: const pw.TextStyle(fontSize: 15),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              //endregion
              //region 標語
              pw.Container(
                alignment: pw.Alignment.centerLeft,
                decoration:
                    pw.BoxDecoration(color: PdfColor.fromHex('#609AD1')),
                padding: const pw.EdgeInsets.only(left: 8),
                child: pw.Text(
                  '光銀式手法版權所有，翻版必究',
                  style: const pw.TextStyle(color: PdfColors.white),
                ),
              ),
              //endregion
              //region 介入調整前
              pw.Container(
                margin: const pw.EdgeInsets.only(top: 8),
                child: pw.Column(
                  children: [
                    //標題
                    pw.Container(
                      height: 40,
                      alignment: pw.Alignment.center,
                      decoration: pw.BoxDecoration(
                          borderRadius: const pw.BorderRadius.only(
                              topLeft: pw.Radius.circular(10),
                              topRight: pw.Radius.circular(10)),
                          color: PdfColor.fromHex('#096BC7')),
                      child: pw.Text(
                        '介入調整前',
                        textScaleFactor: 1,
                        style: const pw.TextStyle(color: PdfColors.white),
                      ),
                    ),
                    pw.Row(children: [
                      //region 身體圖片
                      pw.Container(
                        width: 200,
                        alignment: pw.Alignment.centerRight,
                        child: pw.Stack(
                          children: [
                            pw.Container(
                              height: 200,
                              alignment: pw.Alignment.center,
                              child: pw.Image(pw.MemoryImage(_bodyImage),
                                  fit: pw.BoxFit.contain),
                            ),
                            pw.Positioned(
                                bottom: 1,
                                right: 1,
                                child: pw.Container(
                                  alignment: pw.Alignment.centerRight,
                                  child: pw.Column(
                                    crossAxisAlignment:
                                        pw.CrossAxisAlignment.end,
                                    children: [
                                      pw.Row(
                                        children: [
                                          pw.Image(pw.MemoryImage(_painImage),
                                              width: 10),
                                          pw.SizedBox(
                                            width: 5,
                                          ),
                                          pw.Text(
                                            '輕微受損',
                                            textScaleFactor: 1,
                                            style: const pw.TextStyle(
                                              color: PdfColors.red,
                                            ),
                                          )
                                        ],
                                      ),
                                      pw.Row(
                                        children: [
                                          pw.Image(pw.MemoryImage(_painImage),
                                              width: 10),
                                          pw.Image(pw.MemoryImage(_painImage),
                                              width: 10),
                                          pw.SizedBox(
                                            width: 5,
                                          ),
                                          pw.Text(
                                            '嚴重受損',
                                            textScaleFactor: 1,
                                            style: const pw.TextStyle(
                                                color: PdfColors.red),
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                )),
                          ],
                        ),
                      ),
                      //endregion
                      //region底部身體問題圖
                      pw.Container(
                        width: 300,
                        height: 200,
                        child: pw.Stack(
                          children: [
                            //文字
                            pw.Container(
                              margin: const pw.EdgeInsets.only(
                                  top: 10, left: 15, right: 15),
                              child: pw.Column(
                                children: [
                                  //身體前屈
                                  pw.Column(
                                    children: [
                                      pw.Text(
                                        '身體前屈 (指尖到地板距離)',
                                        textScaleFactor: 1,
                                      ),
                                      pw.Row(
                                        mainAxisAlignment:
                                            pw.MainAxisAlignment.center,
                                        children: [
                                          pw.Container(
                                            margin: pw.EdgeInsets.all(8),
                                            width: 10,
                                            child: pw.Text(_floorResult.text),
                                          ),
                                          pw.Text(
                                            '公分',
                                            textScaleFactor: 1,
                                            style: const pw.TextStyle(
                                              fontSize: 15,
                                              color: PdfColors.black,
                                            ),
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                  //左側
                                  pw.Row(
                                    mainAxisAlignment:
                                        pw.MainAxisAlignment.spaceBetween,
                                    children: [
                                      //左側彎
                                      pw.Column(
                                        children: [
                                          pw.Container(
                                            child: pw.Text(
                                              '左側彎',
                                            ),
                                          ),
                                          pw.Container(
                                            child: pw.Row(
                                              mainAxisAlignment:
                                                  pw.MainAxisAlignment.center,
                                              children: [
                                                pw.Container(
                                                  margin: pw.EdgeInsets.all(8),
                                                  width: 10,
                                                  child: pw.Text(
                                                      _sideKneeLeftResult.text),
                                                ),
                                                pw.Text(
                                                  '公分',
                                                  textScaleFactor: 1,
                                                  style: const pw.TextStyle(
                                                    fontSize: 15,
                                                    color: PdfColors.black,
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                          //左側彎XX
                                          pw.Container(
                                            margin: const pw.EdgeInsets.only(
                                                top: 5, left: 10),
                                            child: pw.Row(
                                              children: [
                                                _patientVm!.patient!.side_left != 0 ||
                                                        _patientVm!.patient!.side_left == 2
                                                    ? pw.Image(
                                                        pw.MemoryImage(
                                                            _painImage),
                                                        width: 10)
                                                    : pw.Container(),
                                                _patientVm!.patient!.side_left != 0
                                                    ? pw.Image(
                                                        pw.MemoryImage(
                                                            _painImage),
                                                        width: 10)
                                                    : pw.Container(),
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                                      //身體前屈XX
                                      pw.Row(
                                        children: [
                                          int.parse(_patientVm!.patient!.floor) <= 5 &&
                                                  int.parse(_patientVm!.patient!.floor) !=
                                                      100 &&
                                                  int.parse(_patientVm!.patient!.floor) != 0
                                              ? pw.Image(
                                                  pw.MemoryImage(_painImage),
                                                  width: 10)
                                              : pw.Container(),
                                          int.parse(_patientVm!.patient!.floor) > 5 &&
                                                  int.parse(_patientVm!.patient!.floor) !=
                                                      0 &&
                                                  int.parse(_patientVm!.patient!.floor) !=
                                                      100
                                              ? pw.Image(
                                                  pw.MemoryImage(_painImage),
                                                  width: 10)
                                              : pw.Container(),
                                          int.parse(_patientVm!.patient!.floor) == 100
                                              ? pw.Text(
                                                  '預設值',
                                                  style: const pw.TextStyle(
                                                      color: PdfColors.red),
                                                )
                                              : pw.Container(),
                                        ],
                                      ),
                                      //右側彎
                                      pw.Column(
                                        children: [
                                          pw.Container(
                                            child: pw.Text(
                                              '右側彎',
                                            ),
                                          ),
                                          pw.Row(
                                            mainAxisAlignment:
                                                pw.MainAxisAlignment.center,
                                            children: [
                                              pw.Container(
                                                margin: const pw.EdgeInsets.all(8),
                                                width: 10,
                                                child: pw.Text(
                                                    _sideKneeRightResult.text),
                                              ),
                                              pw.Text(
                                                '公分',
                                                textScaleFactor: 1,
                                                style: const pw.TextStyle(
                                                  fontSize: 15,
                                                  color: PdfColors.black,
                                                ),
                                              )
                                            ],
                                          ),
                                          //右側彎XX
                                          pw.Container(
                                            margin: const pw.EdgeInsets.only(
                                                top: 5, right: 30),
                                            child: pw.Row(
                                              children: [
                                                _patientVm!.patient!.side_right != 0 &&
                                                        _patientVm!.patient!.side_right == 2
                                                    ? pw.Image(
                                                        pw.MemoryImage(
                                                            _painImage),
                                                        width: 10)
                                                    : pw.Container(),
                                                _patientVm!.patient!.side_right != 0
                                                    ? pw.Image(
                                                        pw.MemoryImage(
                                                            _painImage),
                                                        width: 10)
                                                    : pw.Container(),
                                              ],
                                            ),
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                  pw.SizedBox(
                                    height: 5,
                                  ),
                                  pw.Row(
                                    mainAxisAlignment:
                                        pw.MainAxisAlignment.spaceAround,
                                    children: [
                                      pw.Column(
                                        children: [
                                          //左旋轉XX
                                          pw.Container(
                                            child: pw.Row(
                                              children: [
                                                _patientVm!.patient!.rotation_left != 0 &&
                                                        _patientVm!.patient!.rotation_left ==
                                                            2
                                                    ? pw.Image(
                                                        pw.MemoryImage(
                                                            _painImage),
                                                        width: 10)
                                                    : pw.Container(),
                                                _patientVm!.patient!.rotation_left != 0
                                                    ? pw.Image(
                                                        pw.MemoryImage(
                                                            _painImage),
                                                        width: 10)
                                                    : pw.Container(),
                                              ],
                                            ),
                                          ),
                                          pw.SizedBox(
                                            height: 5,
                                          ),
                                          pw.Container(
                                            alignment: pw.Alignment.center,
                                            child: pw.Text(
                                              '左旋轉',
                                              style: const pw.TextStyle(fontSize: 15),
                                            ),
                                          )
                                        ],
                                      ),
                                      pw.Column(
                                        children: [
                                          //右旋轉XX
                                          pw.Container(
                                            child: pw.Row(
                                              children: [
                                                _patientVm!.patient!.rotation_right != 0 &&
                                                        _patientVm!.patient!.rotation_right ==
                                                            2
                                                    ? pw.Image(
                                                        pw.MemoryImage(
                                                            _painImage),
                                                        width: 10)
                                                    : pw.Container(),
                                                _patientVm!.patient!.rotation_right != 0
                                                    ? pw.Image(
                                                        pw.MemoryImage(
                                                            _painImage),
                                                        width: 10)
                                                    : pw.Container(),
                                              ],
                                            ),
                                          ),
                                          pw.SizedBox(
                                            height: 5,
                                          ),
                                          pw.Container(
                                            alignment: pw.Alignment.center,
                                            child: pw.Text(
                                              '右旋轉',
                                              textScaleFactor: 1,
                                              style: const pw.TextStyle(fontSize: 15),
                                            ),
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                  pw.Container(
                                    child: pw.Row(
                                      mainAxisAlignment:
                                          pw.MainAxisAlignment.center,
                                      children: [
                                        _patientVm!.patient!.Extension != 0 &&
                                                _patientVm!.patient!.Extension == 2
                                            ? pw.Image(
                                                pw.MemoryImage(_painImage),
                                                width: 10)
                                            : pw.Container(),
                                        _patientVm!.patient!.Extension != 0
                                            ? pw.Image(
                                                pw.MemoryImage(_painImage),
                                                width: 10)
                                            : pw.Container(),
                                      ],
                                    ),
                                  ),
                                  pw.SizedBox(
                                    height: 5,
                                  ),
                                  pw.Container(
                                    alignment: pw.Alignment.center,
                                    child: pw.Text(
                                      '身體伸直',
                                      textScaleFactor: 1,
                                      style: const pw.TextStyle(fontSize: 15),
                                    ),
                                  )
                                ],
                              ),
                            ),
                            //圖
                            pw.Container(
                                alignment: pw.Alignment.bottomCenter,
                                margin: const pw.EdgeInsets.only(bottom: 15),
                                child: pw.Image(pw.MemoryImage(_resultImage),
                                    width: 250, height: 250))
                          ],
                        ),
                      ),
                      //endregion
                    ]),

                    //備註
                    pw.Column(
                      children: [
                        pw.Container(
                          alignment: pw.Alignment.centerLeft,
                          child: pw.Text(
                            '備註',
                            textScaleFactor: 1,
                          ),
                        ),
                        pw.Container(
                          height: 70,
                          margin: const pw.EdgeInsets.only(left: 10, right: 10),
                          alignment: pw.Alignment.topLeft,
                          decoration: pw.BoxDecoration(
                            border: pw.Border.all(
                                color: PdfColors.grey, width: 1.5),
                            borderRadius:
                                pw.BorderRadius.all(const pw.Radius.circular(10)),
                          ),
                          child: pw.Text(
                            _summary1.text,
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
              //endregion

              //region 介入調整後
              pw.Container(
                margin: const pw.EdgeInsets.only(top: 8),
                child: pw.Column(
                  children: [
                    //標題
                    pw.Container(
                      height: 40,
                      alignment: pw.Alignment.center,
                      decoration: pw.BoxDecoration(
                          borderRadius: const pw.BorderRadius.only(
                              topLeft: pw.Radius.circular(10),
                              topRight: pw.Radius.circular(10)),
                          color: PdfColor.fromHex('#096BC7')),
                      child: pw.Text(
                        '介入調整後',
                        textScaleFactor: 1,
                        style: const pw.TextStyle(color: PdfColors.white),
                      ),
                    ),
                    pw.Row(children: [
                      //region 身體圖片
                      pw.Container(
                        width: 200,
                        alignment: pw.Alignment.centerRight,
                        child: pw.Stack(
                          children: [
                            pw.Container(
                              height: 200,
                              alignment: pw.Alignment.center,
                              child: pw.Image(pw.MemoryImage(_bodyImage),
                                  fit: pw.BoxFit.contain),
                            ),
                            pw.Positioned(
                                bottom: 1,
                                right: 1,
                                child: pw.Container(
                                  alignment: pw.Alignment.centerRight,
                                  child: pw.Column(
                                    crossAxisAlignment:
                                        pw.CrossAxisAlignment.end,
                                    children: [
                                      pw.Row(
                                        children: [
                                          pw.Image(pw.MemoryImage(_painImage),
                                              width: 10),
                                          pw.SizedBox(
                                            width: 5,
                                          ),
                                          pw.Text(
                                            '輕微受損',
                                            textScaleFactor: 1,
                                            style: const pw.TextStyle(
                                              color: PdfColors.red,
                                            ),
                                          )
                                        ],
                                      ),
                                      pw.Row(
                                        children: [
                                          pw.Image(pw.MemoryImage(_painImage),
                                              width: 10),
                                          pw.Image(pw.MemoryImage(_painImage),
                                              width: 10),
                                          pw.SizedBox(
                                            width: 5,
                                          ),
                                          pw.Text(
                                            '嚴重受損',
                                            textScaleFactor: 1,
                                            style: const pw.TextStyle(
                                                color: PdfColors.red),
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                )),
                          ],
                        ),
                      ),
                      //endregion
                      //region底部身體問題圖
                      pw.Container(
                        width: 300,
                        height: 200,
                        child: pw.Stack(
                          children: [
                            //文字
                            pw.Container(
                              margin: const pw.EdgeInsets.only(
                                  top: 10, left: 15, right: 15),
                              child: pw.Column(
                                children: [
                                  //身體前屈
                                  pw.Column(
                                    children: [
                                      pw.Text(
                                        '身體前屈 (指尖到地板距離)',
                                        textScaleFactor: 1,
                                      ),
                                      pw.Row(
                                        mainAxisAlignment:
                                            pw.MainAxisAlignment.center,
                                        children: [
                                          pw.Container(
                                            margin: const pw.EdgeInsets.all(8),
                                            width: 10,
                                            child: pw.Text(
                                                _floorRecheckResult.text),
                                          ),
                                          pw.Text(
                                            '公分',
                                            textScaleFactor: 1,
                                            style: const pw.TextStyle(
                                              fontSize: 15,
                                              color: PdfColors.black,
                                            ),
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                  //左側
                                  pw.Row(
                                    mainAxisAlignment:
                                        pw.MainAxisAlignment.spaceBetween,
                                    children: [
                                      //左側彎
                                      pw.Column(
                                        children: [
                                          pw.Container(
                                            child: pw.Text(
                                              '左側彎',
                                            ),
                                          ),
                                          pw.Container(
                                            child: pw.Row(
                                              mainAxisAlignment:
                                                  pw.MainAxisAlignment.center,
                                              children: [
                                                pw.Container(
                                                  margin: const pw.EdgeInsets.all(8),
                                                  width: 10,
                                                  child: pw.Text(
                                                      _sideKneeLeftRecheckResult
                                                          .text),
                                                ),
                                                pw.Text(
                                                  '公分',
                                                  textScaleFactor: 1,
                                                  style: const pw.TextStyle(
                                                    fontSize: 15,
                                                    color: PdfColors.black,
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                          //左側彎XX
                                          pw.Container(
                                            margin: const pw.EdgeInsets.only(
                                                top: 5, left: 10),
                                            child: pw.Row(
                                              children: [
                                                _patientVm!.patient!.side_left_recheck !=
                                                            0 ||
                                                        _patientVm!.patient!.side_left_recheck ==
                                                            2
                                                    ? pw.Image(
                                                        pw.MemoryImage(
                                                            _painImage),
                                                        width: 10)
                                                    : pw.Container(),
                                                _patientVm!.patient!.side_left_recheck != 0
                                                    ? pw.Image(
                                                        pw.MemoryImage(
                                                            _painImage),
                                                        width: 10)
                                                    : pw.Container(),
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                                      //身體前屈XX
                                      pw.Row(
                                        children: [
                                          int.parse(_patientVm!.patient!.floor_recheck) <=
                                                      5 &&
                                                  int.parse(_patientVm!.patient!.floor_recheck) !=
                                                      100 &&
                                                  int.parse(_patientVm!.patient!.floor_recheck) !=
                                                      0
                                              ? pw.Image(
                                                  pw.MemoryImage(_painImage),
                                                  width: 10)
                                              : pw.Container(),
                                          int.parse(_patientVm!.patient!.floor_recheck) >
                                                      5 &&
                                                  int.parse(_patientVm!.patient!.floor_recheck) !=
                                                      0 &&
                                                  int.parse(_patientVm!.patient!.floor_recheck) !=
                                                      100
                                              ? pw.Image(
                                                  pw.MemoryImage(_painImage),
                                                  width: 10)
                                              : pw.Container(),
                                          int.parse(_patientVm!.patient!.floor_recheck) ==
                                                  100
                                              ? pw.Text(
                                                  '預設值',
                                                  style: const pw.TextStyle(
                                                      color: PdfColors.red),
                                                )
                                              : pw.Container(),
                                        ],
                                      ),
                                      //右側彎
                                      pw.Column(
                                        children: [
                                          pw.Container(
                                            child: pw.Text(
                                              '右側彎',
                                            ),
                                          ),
                                          pw.Row(
                                            mainAxisAlignment:
                                                pw.MainAxisAlignment.center,
                                            children: [
                                              pw.Container(
                                                margin: const pw.EdgeInsets.all(8),
                                                width: 10,
                                                child: pw.Text(
                                                    _sideKneeRightRecheckResult
                                                        .text),
                                              ),
                                              pw.Text(
                                                '公分',
                                                textScaleFactor: 1,
                                                style: const pw.TextStyle(
                                                  fontSize: 15,
                                                  color: PdfColors.black,
                                                ),
                                              )
                                            ],
                                          ),
                                          //右側彎XX
                                          pw.Container(
                                            margin: const pw.EdgeInsets.only(
                                                top: 5, right: 30),
                                            child: pw.Row(
                                              children: [
                                                _patientVm!.patient!.side_right_recheck !=
                                                            0 &&
                                                        _patientVm!.patient!.side_right_recheck ==
                                                            2
                                                    ? pw.Image(
                                                        pw.MemoryImage(
                                                            _painImage),
                                                        width: 10)
                                                    : pw.Container(),
                                                _patientVm!.patient!.side_right_recheck != 0
                                                    ? pw.Image(
                                                        pw.MemoryImage(
                                                            _painImage),
                                                        width: 10)
                                                    : pw.Container(),
                                              ],
                                            ),
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                  pw.SizedBox(
                                    height: 5,
                                  ),
                                  pw.Row(
                                    mainAxisAlignment:
                                        pw.MainAxisAlignment.spaceAround,
                                    children: [
                                      pw.Column(
                                        children: [
                                          //左旋轉XX
                                          pw.Container(
                                            child: pw.Row(
                                              children: [
                                                _patientVm!.patient!.rotation_left_recheck !=
                                                            0 &&
                                                        _patientVm!.patient!.rotation_left_recheck ==
                                                            2
                                                    ? pw.Image(
                                                        pw.MemoryImage(
                                                            _painImage),
                                                        width: 10)
                                                    : pw.Container(),
                                                _patientVm!.patient!.rotation_left_recheck !=
                                                        0
                                                    ? pw.Image(
                                                        pw.MemoryImage(
                                                            _painImage),
                                                        width: 10)
                                                    : pw.Container(),
                                              ],
                                            ),
                                          ),
                                          pw.SizedBox(
                                            height: 5,
                                          ),
                                          pw.Container(
                                            alignment: pw.Alignment.center,
                                            child: pw.Text(
                                              '左旋轉',
                                              style: const pw.TextStyle(fontSize: 15),
                                            ),
                                          )
                                        ],
                                      ),
                                      pw.Column(
                                        children: [
                                          //右旋轉XX
                                          pw.Container(
                                            child: pw.Row(
                                              children: [
                                                _patientVm!.patient!.rotation_right_recheck !=
                                                            0 &&
                                                        _patientVm!.patient!.rotation_right_recheck ==
                                                            2
                                                    ? pw.Image(
                                                        pw.MemoryImage(
                                                            _painImage),
                                                        width: 10)
                                                    : pw.Container(),
                                                _patientVm!.patient!.rotation_right_recheck !=
                                                        0
                                                    ? pw.Image(
                                                        pw.MemoryImage(
                                                            _painImage),
                                                        width: 10)
                                                    : pw.Container(),
                                              ],
                                            ),
                                          ),
                                          pw.SizedBox(
                                            height: 5,
                                          ),
                                          pw.Container(
                                            alignment: pw.Alignment.center,
                                            child: pw.Text(
                                              '右旋轉',
                                              textScaleFactor: 1,
                                              style: const pw.TextStyle(fontSize: 15),
                                            ),
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                  pw.Container(
                                    child: pw.Row(
                                      mainAxisAlignment:
                                          pw.MainAxisAlignment.center,
                                      children: [
                                        _patientVm!.patient!.extension_recheck != 0 &&
                                                _patientVm!.patient!.extension_recheck == 2
                                            ? pw.Image(
                                                pw.MemoryImage(_painImage),
                                                width: 10)
                                            : pw.Container(),
                                        _patientVm!.patient!.extension_recheck != 0
                                            ? pw.Image(
                                                pw.MemoryImage(_painImage),
                                                width: 10)
                                            : pw.Container(),
                                      ],
                                    ),
                                  ),
                                  pw.SizedBox(
                                    height: 5,
                                  ),
                                  pw.Container(
                                    alignment: pw.Alignment.center,
                                    child: pw.Text(
                                      '身體伸直',
                                      textScaleFactor: 1,
                                      style: const pw.TextStyle(fontSize: 15),
                                    ),
                                  )
                                ],
                              ),
                            ),
                            //圖
                            pw.Container(
                                alignment: pw.Alignment.bottomCenter,
                                margin: const pw.EdgeInsets.only(bottom: 15),
                                child: pw.Image(pw.MemoryImage(_resultImage),
                                    width: 250, height: 250))
                          ],
                        ),
                      ),
                      //endregion
                    ]),

                    //備註
                    pw.Column(
                      children: [
                        pw.Container(
                          alignment: pw.Alignment.centerLeft,
                          child: pw.Text(
                            '備註',
                            textScaleFactor: 1,
                          ),
                        ),
                        pw.Container(
                          height: 70,
                          margin: const pw.EdgeInsets.only(left: 10, right: 10),
                          alignment: pw.Alignment.topLeft,
                          decoration: pw.BoxDecoration(
                            border: pw.Border.all(
                                color: PdfColors.grey, width: 1.5),
                            borderRadius:
                                pw.BorderRadius.all(const pw.Radius.circular(10)),
                          ),
                          child: pw.Text(
                            _summary1.text,
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
              //endregion
              //region
              pw.SizedBox(height: 8),
              //endregion
              //region 評估者
              pw.Container(
                alignment: pw.Alignment.centerRight,
                child: pw.Container(
                    padding: const pw.EdgeInsets.all(10),
                    decoration: pw.BoxDecoration(
                        borderRadius: pw.BorderRadius.circular(10),
                        border: pw.Border.all(color: PdfColors.red, width: 1)),
                    child: pw.Text(
                      _memberVm!.memberModel!.name,
                      textScaleFactor: 1,
                      style: pw.TextStyle(
                        color: PdfColors.red,
                        fontSize: 30,
                      ),
                    )),
              )
              //endregion
            ]));
    //endregion

    //儲存並分享

    final path = Platform.isAndroid?
     (await getExternalStorageDirectory())!.path:
     (await getApplicationSupportDirectory()).path;
    debugPrint(path);
    final file = File('$path/${'Output.pdf'}');
    await file.writeAsBytes(await doc.save());
    Share.shareFiles(['$path/Output.pdf'], text: '我的報告');
  }
}
