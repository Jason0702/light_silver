import 'dart:developer';

import 'package:dotted_line/dotted_line.dart';
import 'package:flutter/material.dart';
import 'package:light_silver/database/database_helper.dart';
import 'package:light_silver/language/language_constants.dart';
import 'package:light_silver/models/operation_item_model.dart';
import 'package:light_silver/models/patient.dart';
import 'package:light_silver/utils/api.dart';
import 'package:light_silver/utils/shared_preference_util.dart';
import 'package:light_silver/utils/utils.dart';
import 'package:light_silver/widgets/noShadowScrollbehavior.dart';
import 'package:provider/provider.dart';

import '../../app.dart';
import '../../routes/route_name.dart';
import '../../view_model/operation_item_vm.dart';
import '../../view_model/patient_vm.dart';
import '../../widgets/left_button.dart';
import '../../widgets/title_widget.dart';

class PV02Routes extends StatefulWidget{

  @override
  State createState() => _PV02RoutesState();
}

class _PV02RoutesState extends State<PV02Routes>{
  PatientVm? _patientVm;
  OperationItemVm? _operationItemVm;

  final TextEditingController _pubisSide = TextEditingController();
  final TextEditingController _pubisProblem = TextEditingController();
  final TextEditingController _supineLegLengthSide = TextEditingController();
  final TextEditingController _supineLegLengthSideBack = TextEditingController();
  final TextEditingController _adductorSide = TextEditingController();
  final TextEditingController _adductorSideBack = TextEditingController();

  final TextEditingController _sacrumProblem = TextEditingController();
  final TextEditingController _proneLegLengthSide = TextEditingController();
  final TextEditingController _addROMSide = TextEditingController();
  final TextEditingController _extRotationSide = TextEditingController();
  final TextEditingController _proneLegLengthSideBack = TextEditingController();
  final TextEditingController _addROMSideBack = TextEditingController();
  final TextEditingController _extRotationSideBack = TextEditingController();

  final TextEditingController _lumbarProblemList = TextEditingController();
  final TextEditingController _lumbarSide = TextEditingController();
  final TextEditingController _rotationSide = TextEditingController();
  final TextEditingController _rotationLimit = TextEditingController();
  final TextEditingController _sideSide = TextEditingController();
  final TextEditingController _sideLimit = TextEditingController();
  final TextEditingController _flexionLimit = TextEditingController();
  final TextEditingController _extensionLimit = TextEditingController();

  OperationItemsModel? imageListProblem;
  String netUpImageUrlProblem = '';
  OperationItemsModel? imageListSacrum;
  String netUpImageUrlSacrum = '';
  OperationItemsModel? imageListLumbar;
  String netUpImageUrlLumbar = '';

  bool _pubis = false;
  bool _sacrum = false;
  bool _lumbar = false;

  int _lumbarLeft = 0;
  int _lumbarRight = 0;

  //資料title
  final _dataTitleBg = BoxDecoration(
      borderRadius: const BorderRadius.only(
          topLeft: Radius.circular(10),
          topRight: Radius.circular(10)),
      color: HexColor.fromHex('#096BC7'));


  _onLayoutDone(_) {
    List list =
    _operationItemVm!.operationItemsModelList!.where((element) => element.id == 3).toList();
    for (var element in list) {
      imageListProblem = element;
    }
    List _list = _operationItemVm!.operationItemsModelList!.where((element) => element.id == 5).toList();
    for (var element in _list) {
      imageListSacrum = element;
      setState(() {});
    }
    _dataInit(_patientVm!.patient);
  }
  void _dataInit(Patient? patient) {
    if (patient == null) {
      return;
    }
    log('取得 預覽(2): $patient');
    //region pubisSide
    switch(patient.inguinal){
      case 1:
        //左
        _pubisSide.text = getTranslated(context, 'left');
        break;
      case 2:
        //右
        _pubisSide.text = getTranslated(context, 'right');
        break;
    }
    //endregion
    //region pubisProblem 圖片
    if (patient.inguinal == 1 && patient.supine_leg_length == 1) {
      //左上
      debugPrint(imageListProblem!.completePics
          .where((element) => element.containsKey('左_向上'))
          .toString());
      debugPrint(imageListProblem!.completeVideos.where((element) => element.containsKey('左_向上')).toString());
      Map<String, dynamic> _map = imageListProblem!.completePics
          .where((element) => element.containsKey('左_向上'))
          .first;
      netUpImageUrlProblem = _map.values.first;
    }
    if (patient.inguinal == 1 && patient.supine_leg_length == 3) {
      //左下
      debugPrint(imageListProblem!.completePics
          .where((element) => element.containsKey('左_向下'))
          .toString());
      Map<String, dynamic> _map = imageListProblem!.completePics
          .where((element) => element.containsKey('左_向下'))
          .first;
      netUpImageUrlProblem = _map.values.first;
    }
    if (patient.inguinal == 2 && patient.supine_leg_length == 3) {
      //右上
      debugPrint(imageListProblem!.completePics
          .where((element) => element.containsKey('右_向上'))
          .toString());
      Map<String, dynamic> _map = imageListProblem!.completePics
          .where((element) => element.containsKey('右_向上'))
          .first;
      netUpImageUrlProblem = _map.values.first;
    }
    if (patient.inguinal == 2 && patient.supine_leg_length == 1) {
      //右下
      debugPrint(imageListProblem!.completePics
          .where((element) => element.containsKey('右_向下'))
          .toString());
      Map<String, dynamic> _map = imageListProblem!.completePics
          .where((element) => element.containsKey('右_向下'))
          .first;
      netUpImageUrlProblem = _map.values.first;
    }

    if(patient.inguinal == 1 && patient.supine_leg_length == 1){
      //向上
      _pubisProblem.text = getTranslated(context, 'up');
    }else if(patient.inguinal == 1 && patient.supine_leg_length == 3){
      //向下
      _pubisProblem.text = getTranslated(context, 'down');
    } else if(patient.inguinal == 2 && patient.supine_leg_length == 1){
      //向下
      _pubisProblem.text = getTranslated(context, 'down');
    }else if(patient.inguinal == 2 && patient.supine_leg_length == 3){
      //向上
      _pubisProblem.text = getTranslated(context, 'up');
    }
    //endregion
    //region supineLegLength
    switch(patient.supine_leg_length){
      case 1:
        //右 較長
        _supineLegLengthSide.text = getTranslated(context, 'right');
        _supineLegLengthSideBack.text = getTranslated(context, 'Longer');
        break;
      case 2:
        //雙 相等
        _supineLegLengthSide.text = getTranslated(context, 'twin');
        _supineLegLengthSideBack.text = getTranslated(context, 'equality');
        break;
      case 3:
        //左 較長
        _supineLegLengthSide.text = getTranslated(context, 'left');
        _supineLegLengthSideBack.text = getTranslated(context, 'Longer');
        break;
    }
    //endregion
    //region addcutor
    switch(patient.adductor){
      case 1:
        //左 無力
        _adductorSide.text = getTranslated(context, 'left');
        _adductorSideBack.text = getTranslated(context, 'powerless');
        break;
      case 2:
        //雙 相等
        _adductorSide.text = getTranslated(context, 'twin');
        _adductorSideBack.text = getTranslated(context, 'equality');
        break;
      case 3:
        //右 無力
        _adductorSide.text = getTranslated(context, 'right');
        _adductorSideBack.text = getTranslated(context, 'powerless');
        debugPrint('add ${getTranslated(context, 'powerless')}');
        break;
    }
    //endregion
    //region sacrumProblem
    if(patient.PSIS_left == 1 && patient.PSIS_right == 1 && patient.ILA_left == 1 && patient.ILA_right == 1){
      //雙側薦椎屈曲(點頭)
      _sacrumProblem.text = '${getTranslated(context, 'Bilateral_sacral_flexion')}(${getTranslated(context, 'nod')})';
    }else if(patient.PSIS_left == 2 && patient.PSIS_right == 2 && patient.ILA_left == 2 && patient.ILA_right == 2){
      //雙側薦椎伸直(抬頭)
      _sacrumProblem.text = '${getTranslated(context, 'Bilateral_sacral_extension')}(${getTranslated(context, 'look_up')})';
    }else if(patient.PSIS_left == 1 && patient.PSIS_right == 2 && patient.ILA_left == 1 && patient.ILA_right == 2 && patient.Prop_left == 1){
      //左側薦椎屈曲(點頭)
      _sacrumProblem.text = '${getTranslated(context, 'Left_sacral_flexion')}(${getTranslated(context, 'nod')})';
    }else if(patient.PSIS_left == 1 && patient.PSIS_right == 2 && patient.ILA_left == 1 && patient.ILA_right == 2 && patient.Prop_left == 2){
      //右側薦椎伸直(抬頭)
      _sacrumProblem.text = '${getTranslated(context, 'Right_sacral_extension')}(${getTranslated(context, 'look_up')})';
    }else if(patient.PSIS_left == 2 && patient.PSIS_right == 1 && patient.ILA_left == 2 && patient.ILA_right == 1 && patient.Prop_left == 1){
      //左側薦椎伸直(抬頭)
      _sacrumProblem.text = '${getTranslated(context, 'Left_sacral_extension')}(${getTranslated(context, 'look_up')})';
    }else if(patient.PSIS_left == 2 && patient.PSIS_right == 1 && patient.ILA_left == 2 && patient.ILA_right == 1 && patient.Prop_left == 2){
      //右側薦椎屈曲(點頭)
      _sacrumProblem.text = '${getTranslated(context, 'Right_sacral_flexion')}(${getTranslated(context, 'nod')})';
    }else if(patient.PSIS_left == 1 && patient.PSIS_right == 2 && patient.ILA_left == 2 && patient.ILA_right == 1 && patient.Prop_left == 1){
      //左側向前扭轉(R/R forward torsion)
      _sacrumProblem.text = '${getTranslated(context, 'Twisted_left_side_forward')}(R/R forward torsion)';
    }else if(patient.PSIS_left == 1 && patient.PSIS_right == 2 && patient.ILA_left == 2 && patient.ILA_right == 1 && patient.Prop_left == 2){
      //右側向後扭轉(R/L backward torsion)
      _sacrumProblem.text = '${getTranslated(context, 'Twisted_right_back')}(R/L backward torsion)';
    }else if(patient.PSIS_left == 2 && patient.PSIS_right == 1 && patient.ILA_left == 1 && patient.ILA_right == 2 && patient.Prop_left == 1){
      //左側向後扭轉(L/R backward torsion)
      _sacrumProblem.text = '${getTranslated(context, 'Twisted_left_side_backward')}(L/R backward torsion)';
    }else if(patient.PSIS_left == 2 && patient.PSIS_right == 1 && patient.ILA_left == 1 && patient.ILA_right == 2 && patient.Prop_left == 2){
      //右側向前扭轉(L/L forward torsion)
      _sacrumProblem.text = '${getTranslated(context, 'Twisted_right_side_forward')}(L/L forward torsion)';
    }
    //endregion
    //region proneLegLength
    switch(patient.prone_leg_length){
      case 1:
        //左 較長
        _proneLegLengthSide.text = getTranslated(context, 'left');
        _proneLegLengthSideBack.text = getTranslated(context, 'Longer');
        break;
      case 2:
        //雙 相等
        _proneLegLengthSide.text = getTranslated(context, 'twin');
        _proneLegLengthSideBack.text = getTranslated(context, 'equality');
        break;
      case 3:
        //右 較長
        _proneLegLengthSide.text = getTranslated(context, 'right');
        _proneLegLengthSideBack.text = getTranslated(context, 'Longer');
        break;
    }
    //endregion
    //region addRomSide
    switch(patient.add_ROM){
      case 1:
        //左 較大
        _addROMSide.text = getTranslated(context, 'left');
        _addROMSideBack.text = getTranslated(context, 'Larger');
        break;
      case 2:
        //雙 相等
        _addROMSide.text = getTranslated(context, 'twin');
        _addROMSideBack.text = getTranslated(context, 'equality');
        break;
      case 3:
        //右 較大
        _addROMSide.text = getTranslated(context, 'right');
        _addROMSideBack.text = getTranslated(context, 'Larger');
        break;
    }
    //endregion
    //region extRotatorSide
    switch(patient.ext_rotator){
      case 1:
        //右 無力
        _extRotationSide.text = getTranslated(context, 'right');
        _extRotationSideBack.text = getTranslated(context, 'powerless');
        break;
      case 2:
        //雙 相等
        _extRotationSide.text = getTranslated(context, 'twin');
        _extRotationSideBack.text = getTranslated(context, 'equality');
        break;
      case 3:
        //左 無力
        _extRotationSide.text = getTranslated(context, 'left');
        _extRotationSideBack.text = getTranslated(context, 'powerless');
        break;
    }
    //endregion
    //region lumbarSideList left
    StringBuffer _lumbarLeftProblem = StringBuffer();
    _lumbarLeftProblem.write(getTranslated(context, 'Left_side'));
    switch(patient.lumbar_left){
      case 0:
        //無
        _lumbarLeftProblem.clear();
        _lumbarLeft = 0;
        break;
      case 1:
        //左側 L5/S1
        _lumbarLeftProblem.write('L5/S1');
        _lumbarLeft = 1;
        break;
      case 2:
        //左側 L4/L5
        _lumbarLeftProblem.write('L4/L5');
        _lumbarLeft = 2;
        break;
      case 3:
        //左側 L3/L4
        _lumbarLeftProblem.write('L3/L4');
        _lumbarLeft = 3;
        break;
      case 4:
        //左側 L2/L3
        _lumbarLeftProblem.write('L2/L3');
        _lumbarLeft = 4;
        break;
      case 5:
        //左側 L1/L2
        _lumbarLeftProblem.write('L1/L2');
        _lumbarLeft = 5;
        break;
    }
    //endregion
    //region lumbarSideList right
    StringBuffer _lumbarRightProblem = StringBuffer();
    _lumbarRightProblem.write(getTranslated(context, 'Right_side'));
    switch(patient.lumbar_right){
      case 0:
        //無
        _lumbarRightProblem.clear();
        _lumbarRight = 0;
        break;
      case 1:
        //右側 L5/S1
        _lumbarRightProblem.write('L5/S1');
        _lumbarRight = 1;
        break;
      case 2:
        //右側 L4/L5
        _lumbarRightProblem.write('L4/L5');
        _lumbarRight = 2;
        break;
      case 3:
        //右側 L3/L4
        _lumbarRightProblem.write('L3/L4');
        _lumbarRight = 3;
        break;
      case 4:
        //右側 L2/L3
        _lumbarRightProblem.write('L2/L3');
        _lumbarRight = 4;
        break;
      case 5:
        //右側 L1/L2
        _lumbarRightProblem.write('L1/L2');
        _lumbarRight = 5;
        break;
    }
    //endregion
    _lumbarProblemList.text = '${_lumbarLeftProblem.toString()} , ${_lumbarRightProblem.toString()}';
    //region lumbarSide
    if(_lumbarLeftProblem.isNotEmpty){
      //左
      _lumbarSide.text = getTranslated(context, 'left');
      if(_lumbarRightProblem.isNotEmpty){
        //, 右
        _lumbarSide.text = '${getTranslated(context, 'left')} , ${getTranslated(context, 'right')}';
      }
    }else{
      if(_lumbarRightProblem.isNotEmpty){
        //右
        _lumbarSide.text = getTranslated(context, 'right');
      }
    }
    //endregion
    //region rotationSide
    if(patient.rotation_left > patient.rotation_right){
      //左
      _rotationSide.text = getTranslated(context, 'left');
    }else if(patient.rotation_left < patient.rotation_right){
      //右
      _rotationSide.text = getTranslated(context, 'right');
    }
    //endregion
    //region rotationLimit
    if(patient.rotation_left > patient.rotation_right){
      if(patient.rotation_left == 1){
        //輕微
        _rotationLimit.text = getTranslated(context, 'Slightly');
      }else if(patient.rotation_left == 2){
        //嚴重
        _rotationLimit.text = getTranslated(context, 'Severely');
      }
    }else if(patient.rotation_left < patient.rotation_right){
      if(patient.rotation_right == 1){
        //輕微
        _rotationLimit.text = getTranslated(context, 'Slightly');
      }else if(patient.rotation_right == 2){
        //嚴重
        _rotationLimit.text = getTranslated(context, 'Severely');
      }
    }
    //endregion
    //region sideSide
    if(patient.side_left > patient.side_right){
      //左
      _sideSide.text = getTranslated(context, 'left');
    }else if(patient.side_right > patient.side_left){
      //右
      _sideSide.text = getTranslated(context, 'right');
    }
    //endregion
    //region sideLimit
    if(patient.side_left > patient.side_right){
      if(patient.side_left == 1){
        //輕微
        _sideLimit.text = getTranslated(context, 'Slightly');
      }else if(patient.side_left == 2){
        //嚴重
        _sideLimit.text = getTranslated(context, 'Severely');
      }
    }else{
      if(patient.side_right == 1){
        //輕微
        _sideLimit.text = getTranslated(context, 'Slightly');
      }else if(patient.side_right == 2){
        //嚴重
        _sideLimit.text = getTranslated(context, 'Severely');
      }
    }
    //endregion
    //region flexionLimit
    if(patient.floor.isEmpty){
      //預設值
      _flexionLimit.text = getTranslated(context, 'default_value');
    } else if(int.parse(patient.floor) <= 0 && int.parse(patient.floor) != 100){
      //無
      _flexionLimit.text = getTranslated(context, 'no');
    }else if(int.parse(patient.floor) != 0 && int.parse(patient.floor) <= 5 && int.parse(patient.floor) != 100){
      //輕微
      _flexionLimit.text = getTranslated(context, 'Slightly');
    }else if(int.parse(patient.floor) > 5 && int.parse(patient.floor) != 100){
      //嚴重
      _flexionLimit.text = getTranslated(context, 'Severely');
    }
    //endregion
    //region extensionLimit
    switch(patient.Extension){
      case 0:
        //無
        _extensionLimit.text = getTranslated(context, 'no');
        break;
      case 1:
        //輕微
        _flexionLimit.text = getTranslated(context, 'Slightly');
        _extensionLimit.text = getTranslated(context, 'Slightly');
        break;
      case 2:
        //嚴重
        _flexionLimit.text = getTranslated(context, 'Severely');
        break;
    }
    //endregion

    //region Sacrum圖片部分
    //雙側薦椎屈曲(點頭)
    if(patient.PSIS_left == 1 && patient.PSIS_right == 1 && patient.ILA_left == 1 && patient.ILA_right == 1){
      Map<String, dynamic> _map = imageListSacrum!.completePics.where((element) => element.containsKey('雙側薦椎屈曲')).first;
      netUpImageUrlSacrum = _map.values.first;
    }
    //雙側薦椎伸直(抬頭)
    if(patient.PSIS_left == 2 && patient.PSIS_right == 2 && patient.ILA_left == 2 && patient.ILA_right == 2){
      Map<String, dynamic> _map = imageListSacrum!.completePics.where((element) => element.containsKey('雙側薦椎伸直')).first;
      netUpImageUrlSacrum = _map.values.first;
    }
    //左側薦椎屈曲(點頭)
    if(patient.PSIS_left == 1 && patient.PSIS_right == 2 && patient.ILA_left == 1 && patient.ILA_right == 2 && patient.Prop_left == 1){
      Map<String, dynamic> _map = imageListSacrum!.completePics.where((element) => element.containsKey('左側薦椎屈曲')).first;
      netUpImageUrlSacrum = _map.values.first;
    }
    //右側薦椎伸直(抬頭)
    if(patient.PSIS_left == 1 && patient.PSIS_right == 2 && patient.ILA_left == 1 && patient.ILA_right == 2 && patient.Prop_left == 2){
      Map<String, dynamic> _map = imageListSacrum!.completePics.where((element) => element.containsKey('右側薦椎伸直')).first;
      netUpImageUrlSacrum = _map.values.first;
    }
    //左側薦椎伸直(抬頭)
    if(patient.PSIS_left == 2 && patient.PSIS_right == 1 && patient.ILA_left == 2 && patient.ILA_right == 1 && patient.Prop_left == 1){
      Map<String, dynamic> _map = imageListSacrum!.completePics.where((element) => element.containsKey('左側薦椎伸直')).first;
      netUpImageUrlSacrum = _map.values.first;
    }
    //右側薦椎屈曲(點頭)
    if(patient.PSIS_left == 2 && patient.PSIS_right == 1 && patient.ILA_left == 2 && patient.ILA_right == 1 && patient.Prop_left == 2){
      Map<String, dynamic> _map = imageListSacrum!.completePics.where((element) => element.containsKey('右側薦椎屈曲')).first;
      netUpImageUrlSacrum = _map.values.first;
    }
    //左側向前扭轉(R/R forward torsion)
    if(patient.PSIS_left == 1 && patient.PSIS_right == 2 && patient.ILA_left == 2 && patient.ILA_right == 1 && patient.Prop_left == 1){
      Map<String, dynamic> _map = imageListSacrum!.completePics.where((element) => element.containsKey('左側向前扭轉')).first;
      netUpImageUrlSacrum = _map.values.first;
    }
    //右側向後扭轉(R/Ｌ backward torsion)
    if(patient.PSIS_left == 1 && patient.PSIS_right == 2 && patient.ILA_left == 2 && patient.ILA_right == 1 && patient.Prop_left == 2){
      Map<String, dynamic> _map = imageListSacrum!.completePics.where((element) => element.containsKey('右側向後扭轉')).first;
      netUpImageUrlSacrum = _map.values.first;
    }
    //左側向後扭轉(L/R backward torsion)
    if(patient.PSIS_left == 2 && patient.PSIS_right == 1 && patient.ILA_left == 1 && patient.ILA_right == 2 && patient.Prop_left == 1){
      Map<String, dynamic> _map = imageListSacrum!.completePics.where((element) => element.containsKey('左側向後扭轉')).first;
      netUpImageUrlSacrum = _map.values.first;
    }
    //右側向前扭轉(L/L forward torsion)
    if(patient.PSIS_left == 2 && patient.PSIS_right == 1 && patient.ILA_left == 1 && patient.ILA_right == 2 && patient.Prop_left == 2){
      Map<String, dynamic> _map = imageListSacrum!.completePics.where((element) => element.containsKey('右側向前扭轉')).first;
      netUpImageUrlSacrum = _map.values.first;
    }
    //endregion



    _pubis = true;
    _sacrum = true;
    _lumbar = true;
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  Widget build(BuildContext context) {
    _patientVm = Provider.of<PatientVm>(context);
    _operationItemVm = Provider.of<OperationItemVm>(context);
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: true,
      body: SafeArea(
        child: Column(
          children: [
            Expanded(flex: 1, child: _titleBar()),
            Expanded(flex: 12, child: _mainWidget())
          ],
        ),
      ),
    );
  }

  //Title
  Widget _titleBar() {
    return PvTitleWidget(
      titleText: getTranslated(context, 'Preview'),
      leftButton: const LeftButton(),
      rightButton: Container(),
    );
  }

  //主體
  Widget _mainWidget() {
    return Container(
      decoration: Style.homeBg,
      alignment: Alignment.center,
      child: Container(
        width: MediaQuery.of(context).size.width / 1.1,
        height: MediaQuery.of(context).size.height / 1.2,
        decoration: Style.centerBG,
        padding: const EdgeInsets.only(top: 15, left: 10, right: 10, bottom: 15),
        child: ScrollConfiguration(
          behavior: NoShadowScrollBehavior(),
          child: buildIndexFrom(),
        ),
      ),
    );
  }

  //表單
  Widget buildIndexFrom() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Expanded(flex: 1, child: fromHeader()),
        const SizedBox(
          height: 10,
        ),
        Expanded(flex: 9, child: fromBody()),
        const SizedBox(
          height: 20,
        ),
        Expanded(
            flex: 1,
            child: InkWell(
              onTap: _nextPage,
              child: Container(
                alignment: Alignment.center,
                width: 500,
                decoration: Style.imageButtonBG,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Text(
                        getTranslated(context, 'Next_page'),
                        textScaleFactor: 1,
                        style: const TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 20),
                      ),
                    ),
                    Image.asset(
                      'assets/images/forward.png',
                      width: 30,
                    )
                  ],
                ),
              ),
            ))
      ],
    );
  }
  //下一頁
  void _nextPage(){
    delegate.push(name: RouteName.pv03);
  }

  //表單頭
  Widget fromHeader() {
    return Container(
      width: MediaQuery.of(context).size.width,
      decoration: const BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/images/record_sheet_title.png'),
              fit: BoxFit.fill)),
      alignment: Alignment.center,
      child: FittedBox(
        fit: BoxFit.scaleDown,
        child: Text(
          getTranslated(
              context, 'Guangyin_Method_Evaluation_and_Adjustment_Record_Sheet'),
          textScaleFactor: 1,
          style: const TextStyle(
              color: Colors.white, fontSize: 23, fontWeight: FontWeight.bold),
        ),
      ),
    );
  }

  //表單身
  Widget fromBody() {
    return ScrollConfiguration(
        behavior: NoShadowScrollBehavior(),
        child: SingleChildScrollView(
          child: Column(
            children: [
              //恥骨問題列表與調整紀錄
              _pubicWidget(),
              //薦椎問題列表與調整紀錄
              _sacrumWidget(),
              //腰椎問題列表與調整紀錄
              _lumbarSpine(),

              SizedBox(
                height: 15,
              ),
              //標語
              Container(
                height: 50,
                alignment: Alignment.centerLeft,
                decoration: BoxDecoration(color: HexColor.fromHex('#609AD1')),
                padding: const EdgeInsets.only(left: 8),
                child: FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    getTranslated(context,
                        'All_rights_reserved_by_the_light_silver_technique_piracy_must_be_punished'),
                    textScaleFactor: 1,
                    style: const TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  ),
                ),
              )
            ],
          ),
        ));
  }

  //恥骨問題列表與調整紀錄
  Widget _pubicWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 8),
      child: Column(
        children: [
          Container(
            height: 50,
            alignment: Alignment.center,
            decoration: _dataTitleBg,
            child: FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                getTranslated(context, 'List_of_pubic_problems_and_adjustment_records'),
                textScaleFactor: 1,
                style: const TextStyle(
                  fontWeight: FontWeight.bold, color: Colors.white
                ),
              ),
            ),
          ),
          Column(
            children: [
              Container(
                alignment: Alignment.centerLeft,
                child: FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    getTranslated(context, 'Problem'),
                    textScaleFactor: 1,
                    style: const TextStyle(
                      fontWeight: FontWeight.bold
                    ),
                  ),
                ),
              ),
              //問題
              Row(
                children: [
                  SizedBox(
                    width: 100,
                    child: TextField(
                      readOnly: true,
                      textAlign: TextAlign.center,
                      style: const TextStyle(color: Colors.black),
                      decoration: InputDecoration(
                        errorText: null,
                        contentPadding: const EdgeInsets.all(5.0),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey[400]!),
                          borderRadius: BorderRadius.circular(10.0)),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0)),
                        filled: true,
                        fillColor: Colors.white,
                        hintText: '',
                        hintStyle: const TextStyle(color: Colors.grey)),
                      keyboardType: TextInputType.text,
                      textInputAction: TextInputAction.next,
                      controller: _pubisSide,
                    ),
                  ),
                  FittedBox(
                    fit: BoxFit.scaleDown,
                    child: Text(
                      getTranslated(context, 'Lateral_pubic_bone'),
                      textScaleFactor: 1,
                      style: const TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 100,
                    child: TextField(
                      readOnly: true,
                      textAlign: TextAlign.center,
                      style: const TextStyle(color: Colors.black),
                      decoration: InputDecoration(
                          errorText: null,
                          contentPadding: const EdgeInsets.all(5.0),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.grey[400]!),
                              borderRadius: BorderRadius.circular(10.0)),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0)),
                          filled: true,
                          fillColor: Colors.white,
                          hintText: '',
                          hintStyle: const TextStyle(color: Colors.grey)),
                      keyboardType: TextInputType.text,
                      textInputAction: TextInputAction.next,
                      controller: _pubisProblem,
                    ),
                  ),
                  FittedBox(
                    fit: BoxFit.scaleDown,
                    child: Text(
                      getTranslated(context, 'shift'),
                      textScaleFactor: 1,
                      style: const TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 10,),
              //表徵
              Column(
                children: [
                  Container(
                    alignment: Alignment.centerLeft,
                    child: FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Text(
                        getTranslated(context, 'Characterization'),
                        textScaleFactor: 1,
                        style: const TextStyle(
                            fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: 250,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            //鼠蹊韌帶
                            Container(
                              height: 80,
                              alignment: Alignment.center,
                              child: FittedBox(
                                fit: BoxFit.scaleDown,
                                child: Text(
                                  getTranslated(context, 'Groin_ligament'),
                                  textScaleFactor: 1,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: HexColor.fromHex('#096BC7')
                                  ),
                                ),
                              ),
                            ),
                            //腳長
                            Container(
                              height: 80,
                              alignment: Alignment.center,
                              child: FittedBox(
                                fit: BoxFit.scaleDown,
                                child: Text(
                                  getTranslated(context, 'Foot_length'),
                                  textScaleFactor: 1,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: HexColor.fromHex('#096BC7')
                                  ),
                                ),
                              ),
                            ),
                            //肌力檢測
                            Container(
                              height: 80,
                              alignment: Alignment.center,
                              child: FittedBox(
                                fit: BoxFit.scaleDown,
                                child: Text(
                                  getTranslated(context, 'Muscle_strength_test'),
                                  textScaleFactor: 1,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: HexColor.fromHex('#096BC7')
                                  ),
                                ),
                              )
                            )
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 250,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            //鼠蹊韌帶
                            SizedBox(
                              width: 80,
                              height: 80,
                              child: TextField(
                                readOnly: true,
                                textAlign: TextAlign.center,
                                style: const TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                    errorText: null,
                                    contentPadding: const EdgeInsets.all(5.0),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.grey[400]!),
                                        borderRadius: BorderRadius.circular(10.0)),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10.0)),
                                    filled: true,
                                    fillColor: Colors.white,
                                    hintText: '',
                                    hintStyle: const TextStyle(color: Colors.grey)),
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.next,
                                controller: _pubisSide,
                              ),
                            ),
                            //腳長
                            SizedBox(
                              width: 80,
                              height: 80,
                              child: TextField(
                                readOnly: true,
                                textAlign: TextAlign.center,
                                style: const TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                    errorText: null,
                                    contentPadding: const EdgeInsets.all(5.0),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.grey[400]!),
                                        borderRadius: BorderRadius.circular(10.0)),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10.0)),
                                    filled: true,
                                    fillColor: Colors.white,
                                    hintText: '',
                                    hintStyle: const TextStyle(color: Colors.grey)),
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.next,
                                controller: _supineLegLengthSide,
                              ),
                            ),
                            //肌力檢測
                            SizedBox(
                              width: 80,
                              height: 80,
                              child: TextField(
                                readOnly: true,
                                textAlign: TextAlign.center,
                                style: const TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                    errorText: null,
                                    contentPadding: const EdgeInsets.all(5.0),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.grey[400]!),
                                        borderRadius: BorderRadius.circular(10.0)),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10.0)),
                                    filled: true,
                                    fillColor: Colors.white,
                                    hintText: '',
                                    hintStyle: const TextStyle(color: Colors.grey)),
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.next,
                                controller: _adductorSide,
                              ),
                            )
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 250,
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              //鼠蹊韌帶
                              Container(
                                height: 80,
                                alignment: Alignment.center,
                                child: FittedBox(
                                  fit: BoxFit.scaleDown,
                                  child: Text(
                                    getTranslated(context, 'Side_tightness_pain'),
                                    textScaleFactor: 1,
                                    style: const TextStyle(
                                        fontWeight: FontWeight.bold
                                    ),
                                  ),
                                ),
                              ),
                              //腳長
                              Container(
                                height: 80,
                                alignment: Alignment.center,
                                child: FittedBox(
                                  fit:BoxFit.scaleDown,
                                  child: Text(
                                    getTranslated(context, 'Side_foot_length'),
                                    textScaleFactor: 1,
                                    style: const TextStyle(
                                        fontWeight: FontWeight.bold
                                    ),
                                  ),
                                ),
                              ),
                              //肌力檢測
                              Container(
                                height: 80,
                                alignment: Alignment.center,
                                child: FittedBox(
                                  fit:BoxFit.scaleDown,
                                  child: Text(
                                    getTranslated(context, 'Lateral_adductor'),
                                    textScaleFactor: 1,
                                    style: const TextStyle(
                                        fontWeight: FontWeight.bold
                                    ),
                                  ),
                                )
                              ),
                            ],
                          ),
                      ),
                      SizedBox(
                        height: 240,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            //鼠蹊韌帶
                            Container(
                              height: 80,
                            ),
                            //腳長
                            SizedBox(
                              width: 100,
                              height: 80,
                              child: TextField(
                                readOnly: true,
                                textAlign: TextAlign.center,
                                style: const TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                    errorText: null,
                                    contentPadding: const EdgeInsets.all(5.0),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.grey[400]!),
                                        borderRadius: BorderRadius.circular(10.0)),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10.0)),
                                    filled: true,
                                    fillColor: Colors.white,
                                    hintText: '',
                                    hintStyle: const TextStyle(color: Colors.grey)),
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.next,
                                controller: _supineLegLengthSideBack,
                              ),
                            ),
                            //肌力檢測
                            SizedBox(
                              width: 100,
                              height: 80,
                              child: TextField(
                                readOnly: true,
                                textAlign: TextAlign.center,
                                style: const TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                    errorText: null,
                                    contentPadding: const EdgeInsets.all(5.0),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.grey[400]!),
                                        borderRadius: BorderRadius.circular(10.0)),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10.0)),
                                    filled: true,
                                    fillColor: Colors.white,
                                    hintText: '',
                                    hintStyle: const TextStyle(color: Colors.grey)),
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.next,
                                controller: _adductorSideBack,
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  )
                ],
              ),
              SizedBox(height: 10,),
              //圖片
              Container(
                width: 800,
                height: 350,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  borderRadius: const BorderRadius.all(Radius.circular(10)),
                  border: Border.all(color: Colors.grey, width: 1)
                ),
                child: Image.network('$imageUrlOffline$imageUrlPublic$netUpImageUrlProblem'),

              ),
              //結果
              Row(
                children: [
                  FittedBox(
                    fit:BoxFit.scaleDown,
                    child: Text(
                      getTranslated(context, 'result'),
                      textScaleFactor: 1,
                      style: const TextStyle(
                        fontWeight: FontWeight.bold
                      ),
                    ),
                  ),
                  Transform.scale(
                    scale: 1.5,
                    child: Checkbox(value: _pubis, onChanged: null),
                  ),
                  FittedBox(
                    fit:BoxFit.scaleDown,
                    child: Text(
                      getTranslated(context, 'Normal_position_restored'),
                      textScaleFactor: 1,
                      style: const TextStyle(
                          fontWeight: FontWeight.bold
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),

        ],
      ),
    );
  }

  //薦椎問題列表與調整紀錄
  Widget _sacrumWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 8),
      child: Column(
        children: [
          Container(
            height: 50,
            alignment: Alignment.center,
            decoration: _dataTitleBg,
            child: FittedBox(
              fit:BoxFit.scaleDown,
              child: Text(
                getTranslated(context, 'List_of_sacral_problems_and_adjustment_records'),
                textScaleFactor: 1,
                style: const TextStyle(
                    fontWeight: FontWeight.bold, color: Colors.white
                ),
              ),
            ),
          ),
          Column(
            children: [
              Container(
                alignment: Alignment.centerLeft,
                child: FittedBox(
                  fit:BoxFit.scaleDown,
                  child: Text(
                    getTranslated(context, 'Problem'),
                    textScaleFactor: 1,
                    style: const TextStyle(
                        fontWeight: FontWeight.bold
                    ),
                  ),
                ),
              ),
              //問題
              TextField(
                readOnly: true,
                style: const TextStyle(color: Colors.black),
                decoration: InputDecoration(
                    errorText: null,
                    contentPadding: const EdgeInsets.all(5.0),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey[400]!),
                        borderRadius: BorderRadius.circular(10.0)),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0)),
                    filled: true,
                    fillColor: Colors.white,
                    hintText: '',
                    hintStyle: const TextStyle(color: Colors.grey)),
                keyboardType: TextInputType.text,
                textInputAction: TextInputAction.next,
                controller: _sacrumProblem,
              ),
              SizedBox(height: 10,),
              //表徵
              Column(
                children: [
                  Container(
                    alignment: Alignment.centerLeft,
                    child: FittedBox(
                      fit:BoxFit.scaleDown,
                      child: Text(
                        getTranslated(context, 'Characterization'),
                        textScaleFactor: 1,
                        style: const TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: 180,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            //腳長
                            Container(
                              height: 60,
                              child: FittedBox(
                                fit:BoxFit.scaleDown,
                                child: Text(
                                  getTranslated(context, 'Foot_length'),
                                  textScaleFactor: 1,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: HexColor.fromHex('#096BC7')
                                  ),
                                ),
                              )
                            ),
                            //內轉角度
                            Container(
                                height: 60,
                                child: FittedBox(
                                  fit:BoxFit.scaleDown,
                                  child: Text(
                                    getTranslated(context, 'Inward_rotation_angle'),
                                    textScaleFactor: 1,
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: HexColor.fromHex('#096BC7')
                                    ),
                                  ),
                                ),
                            ),
                            //肌力檢測
                            Container(
                              height: 60,
                              child: FittedBox(
                                fit:BoxFit.scaleDown,
                                child: Text(
                                  getTranslated(context, 'Muscle_strength_test'),
                                  textScaleFactor: 1,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: HexColor.fromHex('#096BC7')
                                  ),
                                ),
                              )
                            ),
                          ],
                        ),
                      ),
                      SizedBox(width: 10,),
                      SizedBox(
                        height: 180,
                        child: Column(
                          children: [
                            //腳長
                            SizedBox(
                              width: 80,
                              height: 60,
                              child: TextField(
                                readOnly: true,
                                textAlign: TextAlign.center,
                                style: const TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                    errorText: null,
                                    contentPadding: const EdgeInsets.all(5.0),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.grey[400]!),
                                        borderRadius: BorderRadius.circular(10.0)),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10.0)),
                                    filled: true,
                                    fillColor: Colors.white,
                                    hintText: '',
                                    hintStyle: const TextStyle(color: Colors.grey)),
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.next,
                                controller: _proneLegLengthSide,
                              ),
                            ),
                            //內轉角度
                            SizedBox(
                              width: 80,
                              height: 60,
                              child: TextField(
                                readOnly: true,
                                textAlign: TextAlign.center,
                                style: const TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                    errorText: null,
                                    contentPadding: const EdgeInsets.all(5.0),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.grey[400]!),
                                        borderRadius: BorderRadius.circular(10.0)),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10.0)),
                                    filled: true,
                                    fillColor: Colors.white,
                                    hintText: '',
                                    hintStyle: const TextStyle(color: Colors.grey)),
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.next,
                                controller: _addROMSide,
                              ),
                            ),
                            //肌力檢測
                            SizedBox(
                              width: 80,
                              height: 60,
                              child: TextField(
                                readOnly: true,
                                textAlign: TextAlign.center,
                                style: const TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                    errorText: null,
                                    contentPadding: const EdgeInsets.all(5.0),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.grey[400]!),
                                        borderRadius: BorderRadius.circular(10.0)),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10.0)),
                                    filled: true,
                                    fillColor: Colors.white,
                                    hintText: '',
                                    hintStyle: const TextStyle(color: Colors.grey)),
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.next,
                                controller: _extRotationSide,
                              ),
                            )
                          ],
                        ),
                      ),
                      SizedBox(width: 10,),
                      SizedBox(
                        height: 180,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            //腳長
                            SizedBox(
                              height: 60,
                              child: FittedBox(
                                fit:BoxFit.scaleDown,
                                child: Text(
                                  getTranslated(context, 'Side_foot_length'),
                                  textScaleFactor: 1,
                                  style: const TextStyle(
                                      fontWeight: FontWeight.bold
                                  ),
                                ),
                              ),
                            ),
                            //內轉角度
                            SizedBox(
                              height: 60,
                              child: FittedBox(
                                fit:BoxFit.scaleDown,
                                child: Text(
                                  getTranslated(context, 'Side_angle'),
                                  textScaleFactor: 1,
                                  style: const TextStyle(
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                            //肌力檢測
                            SizedBox(
                              height: 60,
                              child: FittedBox(
                                fit:BoxFit.scaleDown,
                                child: Text(
                                  getTranslated(context, 'Lateral_abduction_muscle'),
                                  textScaleFactor: 1,
                                  style: const TextStyle(
                                      fontWeight: FontWeight.bold
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 180,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            //腳長
                            SizedBox(
                              width: 80,
                              height: 60,
                              child: TextField(
                                readOnly: true,
                                textAlign: TextAlign.center,
                                style: const TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                    errorText: null,
                                    contentPadding: const EdgeInsets.all(5.0),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.grey[400]!),
                                        borderRadius: BorderRadius.circular(10.0)),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10.0)),
                                    filled: true,
                                    fillColor: Colors.white,
                                    hintText: '',
                                    hintStyle: const TextStyle(color: Colors.grey)),
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.next,
                                controller: _proneLegLengthSideBack,
                              ),
                            ),
                            //內轉角度
                            SizedBox(
                              width: 80,
                              height: 60,
                              child: TextField(
                                readOnly: true,
                                textAlign: TextAlign.center,
                                style: const TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                    errorText: null,
                                    contentPadding: const EdgeInsets.all(5.0),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.grey[400]!),
                                        borderRadius: BorderRadius.circular(10.0)),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10.0)),
                                    filled: true,
                                    fillColor: Colors.white,
                                    hintText: '',
                                    hintStyle: const TextStyle(color: Colors.grey)),
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.next,
                                controller: _addROMSideBack,
                              ),
                            ),
                            //肌力檢測
                            SizedBox(
                              width: 80,
                              height: 60,
                              child: TextField(
                                readOnly: true,
                                textAlign: TextAlign.center,
                                style: const TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                    errorText: null,
                                    contentPadding: const EdgeInsets.all(5.0),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.grey[400]!),
                                        borderRadius: BorderRadius.circular(10.0)),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10.0)),
                                    filled: true,
                                    fillColor: Colors.white,
                                    hintText: '',
                                    hintStyle: const TextStyle(color: Colors.grey)),
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.next,
                                controller: _extRotationSideBack,
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  )
                ],
              ),
              const SizedBox(height: 10,),
              //圖片
              Container(
                width: 800,
                height: 450,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    borderRadius: const BorderRadius.all(Radius.circular(10)),
                    border: Border.all(color: Colors.grey, width: 1)
                ),
                child: Image.network('$imageUrlOffline$imageUrlPublic$netUpImageUrlProblem'),

              ),
              //結果
              Row(
                children: [
                  FittedBox(
                    fit:BoxFit.scaleDown,
                    child: Text(
                      getTranslated(context, 'result'),
                      textScaleFactor: 1,
                      style: const TextStyle(
                          fontWeight: FontWeight.bold
                      ),
                    ),
                  ),
                  Transform.scale(
                    scale: 1.5,
                    child: Checkbox(value: _sacrum, onChanged: null),
                  ),
                  FittedBox(
                    fit:BoxFit.scaleDown,
                    child: Text(
                      getTranslated(context, 'Normal_position_restored'),
                      textScaleFactor: 1,
                      style: const TextStyle(
                          fontWeight: FontWeight.bold
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
        ],
      ),
    );
  }
  //腰椎問題列表與調整紀錄
  Widget _lumbarSpine() {
    return Container(
      margin: const EdgeInsets.only(top: 8),
      child: Column(
        children: [
          Container(
            height: 50,
            alignment: Alignment.center,
            decoration: _dataTitleBg,
            child: FittedBox(
              fit:BoxFit.scaleDown,
              child: Text(
                getTranslated(context, 'List_of_lumbar_spine_problems_and_adjustment_records'),
                textScaleFactor: 1,
                style: const TextStyle(
                    fontWeight: FontWeight.bold, color: Colors.white
                ),
              ),
            ),
          ),
          Column(
            children: [
              Container(
                alignment: Alignment.centerLeft,
                child: FittedBox(
                  fit:BoxFit.scaleDown,
                  child: Text(
                    getTranslated(context, 'Problem'),
                    textScaleFactor: 1,
                    style: const TextStyle(
                        fontWeight: FontWeight.bold
                    ),
                  ),
                ),
              ),
              //問題
              TextField(
                readOnly: true,
                style: const TextStyle(color: Colors.black),
                decoration: InputDecoration(
                    errorText: null,
                    contentPadding: const EdgeInsets.all(5.0),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey[400]!),
                        borderRadius: BorderRadius.circular(10.0)),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0)),
                    filled: true,
                    fillColor: Colors.white,
                    hintText: '',
                    hintStyle: const TextStyle(color: Colors.grey)),
                keyboardType: TextInputType.text,
                textInputAction: TextInputAction.next,
                controller: _lumbarProblemList,
              ),
              SizedBox(height: 10,),
              //表徵
              Column(
                children: [
                  Container(
                    alignment: Alignment.centerLeft,
                    child: FittedBox(
                      fit:BoxFit.scaleDown,
                      child: Text(
                        getTranslated(context, 'Characterization'),
                        textScaleFactor: 1,
                        style: const TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: 300,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            //小面關節
                            Container(
                              height: 60,
                              child: FittedBox(
                                fit:BoxFit.scaleDown,
                                child: Text(
                                  getTranslated(context, 'Facet_joint'),
                                  textScaleFactor: 1,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: HexColor.fromHex('#096BC7')
                                  ),
                                ),
                              ),
                            ),
                            //旋轉
                            Container(
                              height: 60,
                              child: FittedBox(
                                fit:BoxFit.scaleDown,
                                child: Text(
                                  getTranslated(context, 'whirl'),
                                  textScaleFactor: 1,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: HexColor.fromHex('#096BC7')
                                  ),
                                ),
                              ),
                            ),
                            //側彎
                            Container(
                                height: 60,
                                child: FittedBox(
                                  fit:BoxFit.scaleDown,
                                  child: Text(
                                    getTranslated(context, 'Side_bend'),
                                    textScaleFactor: 1,
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: HexColor.fromHex('#096BC7')
                                    ),
                                  ),
                                ),
                            ),
                            //屈曲
                            Container(
                              height: 60,
                                child: FittedBox(
                                  fit:BoxFit.scaleDown,
                                  child: Text(
                                    getTranslated(context, 'Buckling'),
                                    textScaleFactor: 1,
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: HexColor.fromHex('#096BC7')
                                    ),
                                  ),
                                ),
                            ),
                            //伸展
                            Container(
                              height: 60,
                              child: FittedBox(
                                fit:BoxFit.scaleDown,
                                child: Text(
                                  getTranslated(context, 'stretch'),
                                  textScaleFactor: 1,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: HexColor.fromHex('#096BC7')
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(width: 10,),
                      SizedBox(
                        height: 300,
                        child: Column(
                          children: [
                            //小面關節
                            SizedBox(
                              width: 80,
                              height: 60,
                              child: TextField(
                                readOnly: true,
                                textAlign: TextAlign.center,
                                style: const TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                    errorText: null,
                                    contentPadding: const EdgeInsets.all(5.0),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.grey[400]!),
                                        borderRadius: BorderRadius.circular(10.0)),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10.0)),
                                    filled: true,
                                    fillColor: Colors.white,
                                    hintText: '',
                                    hintStyle: const TextStyle(color: Colors.grey)),
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.next,
                                controller: _lumbarSide,
                              ),
                            ),
                            //旋轉
                            SizedBox(
                              width: 80,
                              height: 60,
                              child: TextField(
                                readOnly: true,
                                textAlign: TextAlign.center,
                                style: const TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                    errorText: null,
                                    contentPadding: const EdgeInsets.all(5.0),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.grey[400]!),
                                        borderRadius: BorderRadius.circular(10.0)),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10.0)),
                                    filled: true,
                                    fillColor: Colors.white,
                                    hintText: '',
                                    hintStyle: const TextStyle(color: Colors.grey)),
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.next,
                                controller: _rotationSide,
                              ),
                            ),
                            //側彎
                            SizedBox(
                              width: 80,
                              height: 60,
                              child: TextField(
                                readOnly: true,
                                textAlign: TextAlign.center,
                                style: const TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                    errorText: null,
                                    contentPadding: const EdgeInsets.all(5.0),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.grey[400]!),
                                        borderRadius: BorderRadius.circular(10.0)),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10.0)),
                                    filled: true,
                                    fillColor: Colors.white,
                                    hintText: '',
                                    hintStyle: const TextStyle(color: Colors.grey)),
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.next,
                                controller: _sideSide,
                              ),
                            ),
                            //屈曲
                            SizedBox(
                              width: 80,
                              height: 60,
                              child: TextField(
                                readOnly: true,
                                textAlign: TextAlign.center,
                                style: const TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                    errorText: null,
                                    contentPadding: const EdgeInsets.all(5.0),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.grey[400]!),
                                        borderRadius: BorderRadius.circular(10.0)),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10.0)),
                                    filled: true,
                                    fillColor: Colors.white,
                                    hintText: '',
                                    hintStyle: const TextStyle(color: Colors.grey)),
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.next,
                                controller: _flexionLimit,
                              ),
                            ),
                            //伸展
                            Container(
                              width: 80,
                              height: 60,
                              alignment: Alignment.center,
                              child: TextField(
                                readOnly: true,
                                textAlign: TextAlign.center,
                                style: const TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                    errorText: null,
                                    contentPadding: const EdgeInsets.all(5.0),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.grey[400]!),
                                        borderRadius: BorderRadius.circular(10.0)),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10.0)),
                                    filled: true,
                                    fillColor: Colors.white,
                                    hintText: '',
                                    hintStyle: const TextStyle(color: Colors.grey)),
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.next,
                                controller: _extensionLimit,
                              ),
                            )
                          ],
                        ),
                      ),
                      SizedBox(width: 10,),
                      SizedBox(
                        height: 300,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            //小面關節
                            Expanded(child: Container(
                              alignment: Alignment.center,
                              child: FittedBox(
                                fit:BoxFit.scaleDown,
                                child: Text(
                                  getTranslated(context, 'Side_tightness'),
                                  textScaleFactor: 1,
                                  style: const TextStyle(
                                      fontWeight: FontWeight.bold
                                  ),
                                ),
                              ),
                            )),
                            //旋轉
                            Expanded(child: Row(
                              children: [
                                FittedBox(
                                  fit:BoxFit.scaleDown,
                                  child: Text(
                                    getTranslated(context, 'side'),
                                    textScaleFactor: 1,
                                    style: const TextStyle(
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                                //欄位
                                Container(
                                  width: 80,
                                  height: 60,
                                  alignment: Alignment.center,
                                  child: TextField(
                                    readOnly: true,
                                    textAlign: TextAlign.center,
                                    style: const TextStyle(color: Colors.black),
                                    decoration: InputDecoration(
                                        errorText: null,
                                        contentPadding: const EdgeInsets.all(5.0),
                                        enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: Colors.grey[400]!),
                                            borderRadius: BorderRadius.circular(10.0)),
                                        border: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(10.0)),
                                        filled: true,
                                        fillColor: Colors.white,
                                        hintText: '',
                                        hintStyle: const TextStyle(color: Colors.grey)),
                                    keyboardType: TextInputType.text,
                                    textInputAction: TextInputAction.next,
                                    controller: _rotationLimit,
                                  ),
                                ),
                                FittedBox(
                                  fit:BoxFit.scaleDown,
                                  child: Text(
                                    getTranslated(context, 'Restricted'),
                                    textScaleFactor: 1,
                                    style: const TextStyle(
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                              ],
                            ),),
                            //側彎
                            Expanded(child: Row(
                              children: [
                                FittedBox(
                                  fit:BoxFit.scaleDown,
                                  child: Text(
                                    getTranslated(context, 'side'),
                                    textScaleFactor: 1,
                                    style: const TextStyle(
                                        fontWeight: FontWeight.bold
                                    ),
                                  ),
                                ),
                                //欄位
                                SizedBox(
                                  width: 80,
                                  height: 60,
                                  child: TextField(
                                    readOnly: true,
                                    textAlign: TextAlign.center,
                                    style: const TextStyle(color: Colors.black),
                                    decoration: InputDecoration(
                                        errorText: null,
                                        contentPadding: const EdgeInsets.all(5.0),
                                        enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: Colors.grey[400]!),
                                            borderRadius: BorderRadius.circular(10.0)),
                                        border: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(10.0)),
                                        filled: true,
                                        fillColor: Colors.white,
                                        hintText: '',
                                        hintStyle: const TextStyle(color: Colors.grey)),
                                    keyboardType: TextInputType.text,
                                    textInputAction: TextInputAction.next,
                                    controller: _sideLimit,
                                  ),
                                ),
                                FittedBox(
                                  fit:BoxFit.scaleDown,
                                  child: Text(
                                    getTranslated(context, 'Restricted'),
                                    textScaleFactor: 1,
                                    style: const TextStyle(
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                              ],
                            )),
                            //屈曲
                            Expanded(child: Container(
                              alignment: Alignment.center,
                              child: FittedBox(
                                fit:BoxFit.scaleDown,
                                child: Text(
                                  getTranslated(context, 'Restricted'),
                                  textScaleFactor: 1,
                                  style: const TextStyle(
                                      fontWeight: FontWeight.bold
                                  ),
                                ),
                              ),
                            )),
                            //伸展
                            Expanded(child: Container(
                              alignment: Alignment.center,
                              child: FittedBox(
                                fit:BoxFit.scaleDown,
                                child: Text(
                                  getTranslated(context, 'Restricted'),
                                  textScaleFactor: 1,
                                  style: const TextStyle(
                                      fontWeight: FontWeight.bold
                                  ),
                                ),
                              ),
                            ))
                          ],
                        ),
                      ),
                    ],
                  )
                ],
              ),
              SizedBox(height: 10,),
              //圖片
              _showImageWidget(),
              //結果
              Row(
                children: [
                  FittedBox(
                    fit:BoxFit.scaleDown,
                    child: Text(
                      getTranslated(context, 'result'),
                      textScaleFactor: 1,
                      style: const TextStyle(
                          fontWeight: FontWeight.bold
                      ),
                    ),
                  ),
                  Transform.scale(
                    scale: 1.5,
                    child: Checkbox(value: _lumbar, onChanged: null),
                  ),
                  FittedBox(
                    fit:BoxFit.scaleDown,
                    child: Text(
                      getTranslated(context, 'Normal_position_restored'),
                      textScaleFactor: 1,
                      style: const TextStyle(
                          fontWeight: FontWeight.bold
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),

        ],
      ),
    );
  }
//顯示圖片
  Widget _showImageWidget(){
    return Container(
      width: 800,
      height: 310,
      decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(Radius.circular(10)),
          border: Border.all(color: Colors.black, width: 2.0)
      ),
      margin: const EdgeInsets.only(left: 25, right: 25),
      child: Stack(
        alignment: Alignment.center,
        children: [
          Image.asset('assets/images/6back.png'),
          Container(
            margin: const EdgeInsets.only(bottom: 15),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                //L1/L2
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    //L1/L2 Text
                    FittedBox(
                      fit:BoxFit.scaleDown,
                      child: const Text(
                        'L1/L2',
                        textScaleFactor: 1,
                      ),
                    ),
                    //L1/L2 圓
                    Container(
                      width: 30,
                      height: 30,
                      decoration: BoxDecoration(
                        borderRadius: const BorderRadius.all(Radius.circular(50)),
                        border: Border.all(
                            width: 5.0,
                            color: Colors.black),
                        color: _lumbarLeft == 5 ? Colors.red : Colors.white,
                      ),
                    ),
                    //L1/L2 線
                    SizedBox(
                      width: 120,
                      child: DottedLine(
                        lineThickness: 3.0,
                        dashColor: HexColor.fromHex('#24AF88'),
                      ),
                    ),
                    //L1/L2 圓
                    Container(
                      width: 30,
                      height: 30,
                      decoration: BoxDecoration(
                          borderRadius: const BorderRadius.all(Radius.circular(50)),
                          border: Border.all(
                              width: 5.0,
                              color: Colors.black),
                          color: _lumbarRight == 5 ? Colors.red : Colors.white
                      ),
                    ),
                    //L1/L2 Text
                    FittedBox(
                      fit:BoxFit.scaleDown,
                      child: const Text(
                        'L1/L2',
                        textScaleFactor: 1,
                      ),
                    ),
                  ],
                ),
                //L2/L3
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    //L2/L3 Text
                    FittedBox(
                      fit:BoxFit.scaleDown,
                      child: const Text(
                        'L2/L3',
                        textScaleFactor: 1,
                      ),
                    ),
                    //L2/L3 圓
                    Container(
                      width: 30,
                      height: 30,
                      decoration: BoxDecoration(
                          borderRadius: const BorderRadius.all(Radius.circular(50)),
                          border: Border.all(
                              width: 5.0,
                              color: Colors.black),
                          color: _lumbarLeft == 4 ? Colors.red : Colors.white
                      ),
                    ),
                    //L2/L3 線
                    SizedBox(
                      width: 120,
                      child: DottedLine(
                        lineThickness: 3.0,
                        dashColor: HexColor.fromHex('#24AF88'),
                      ),
                    ),
                    //L2/L3 圓
                    Container(
                      width: 30,
                      height: 30,
                      decoration: BoxDecoration(
                        borderRadius: const BorderRadius.all(Radius.circular(50)),
                        border: Border.all(
                            width: 5.0,
                            color: Colors.black),
                        color: _lumbarRight == 4 ? Colors.red : Colors.white,
                      ),
                    ),
                    //L2/L3 Text
                    FittedBox(
                      fit:BoxFit.scaleDown,
                      child: const Text(
                        'L2/L3',
                        textScaleFactor: 1,
                      ),
                    ),
                  ],
                ),
                //L3/L4
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    //L3/L4 Text
                    FittedBox(
                      fit:BoxFit.scaleDown,
                      child: const Text(
                        'L3/L4',
                        textScaleFactor: 1,
                      ),
                    ),
                    //L3/L4 圓
                    Container(
                      width: 30,
                      height: 30,
                      decoration: BoxDecoration(
                          borderRadius: const BorderRadius.all(Radius.circular(50)),
                          border: Border.all(
                              width: 5.0,
                              color: Colors.black),
                          color: _lumbarLeft == 3 ? Colors.red : Colors.white
                      ),
                    ),
                    //L3/L4 線
                    SizedBox(
                      width: 120,
                      child: DottedLine(
                        lineThickness: 3.0,
                        dashColor: HexColor.fromHex('#24AF88'),
                      ),
                    ),
                    //L3/L4 圓
                    Container(
                      width: 30,
                      height: 30,
                      decoration: BoxDecoration(
                          borderRadius: const BorderRadius.all(Radius.circular(50)),
                          border: Border.all(
                              width: 5.0,
                              color: Colors.black),
                          color: _lumbarRight == 3 ? Colors.red : Colors.white
                      ),
                    ),
                    //L3/L4 Text
                    FittedBox(
                      fit:BoxFit.scaleDown,
                      child: const Text(
                        'L3/L4',
                        textScaleFactor: 1,
                      ),
                    ),
                  ],
                ),
                //L4/L5
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    //L4/L5 Text
                    FittedBox(
                      fit:BoxFit.scaleDown,
                      child: const Text(
                        'L4/L5',
                        textScaleFactor: 1,
                      ),
                    ),
                    //L4/L5 圓
                    Container(
                      width: 30,
                      height: 30,
                      decoration: BoxDecoration(
                          borderRadius: const BorderRadius.all(Radius.circular(50)),
                          border: Border.all(
                              width: 5.0,
                              color: Colors.black),
                          color: _lumbarLeft == 2 ? Colors.red : Colors.white
                      ),
                    ),
                    //L4/L5 線
                    SizedBox(
                      width: 120,
                      child: DottedLine(
                        lineThickness: 3.0,
                        dashColor: HexColor.fromHex('#24AF88'),
                      ),
                    ),
                    //L4/L5 圓
                    Container(
                      width: 30,
                      height: 30,
                      decoration: BoxDecoration(
                          borderRadius: const BorderRadius.all(Radius.circular(50)),
                          border: Border.all(
                              width: 5.0,
                              color: Colors.black),
                          color: _lumbarRight == 2 ? Colors.red : Colors.white
                      ),
                    ),
                    //L4/L5 Text
                    FittedBox(
                      fit:BoxFit.scaleDown,
                      child: const Text(
                        'L4/L5',
                        textScaleFactor: 1,
                      ),
                    ),
                  ],
                ),
                //L5/S1
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    //L5/S1 Text
                    FittedBox(
                      fit:BoxFit.scaleDown,
                      child: const Text(
                        'L5/S1',
                        textScaleFactor: 1,
                      ),
                    ),
                    //L5/S1 圓
                    Container(
                      width: 30,
                      height: 30,
                      decoration: BoxDecoration(
                          borderRadius: const BorderRadius.all(Radius.circular(50)),
                          border: Border.all(
                              width: 5.0,
                              color: Colors.black),
                          color: _lumbarLeft == 1 ? Colors.red : Colors.white
                      ),
                    ),
                    //L5/S1 線
                    SizedBox(
                      width: 120,
                      child: DottedLine(
                        lineThickness: 3.0,
                        dashColor: HexColor.fromHex('#24AF88'),
                      ),
                    ),
                    //L5/S1 圓
                    Container(
                      width: 30,
                      height: 30,
                      decoration: BoxDecoration(
                          borderRadius: const BorderRadius.all(Radius.circular(50)),
                          border: Border.all(
                              width: 5.0,
                              color: Colors.black),
                          color: _lumbarRight == 1 ? Colors.red : Colors.white
                      ),
                    ),
                    //L5/S1 Text
                    FittedBox(
                      fit:BoxFit.scaleDown,
                      child: const Text(
                        'L5/S1',
                        textScaleFactor: 1,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}