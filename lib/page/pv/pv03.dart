import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:light_silver/database/database_helper.dart';
import 'package:light_silver/language/language_constants.dart';
import 'package:light_silver/models/operation_item_model.dart';
import 'package:light_silver/models/patient.dart';
import 'package:light_silver/utils/api.dart';
import 'package:light_silver/utils/shared_preference_util.dart';
import 'package:light_silver/utils/utils.dart';
import 'package:light_silver/widgets/noShadowScrollbehavior.dart';
import 'package:provider/provider.dart';

import '../../app.dart';
import '../../routes/route_name.dart';
import '../../view_model/operation_item_vm.dart';
import '../../view_model/patient_vm.dart';
import '../../widgets/left_button.dart';
import '../../widgets/title_widget.dart';

class PV03Routes extends StatefulWidget{
  @override
  State createState() => _PV03RoutesState();
}
class _PV03RoutesState extends State<PV03Routes>{
  PatientVm? _patientVm;
  OperationItemVm? _operationItemVm;

  bool _iliumProblem = false;
  bool _iliumSide1 = false;
  bool _iliumSide2 = false;

  final TextEditingController _iliumProblemSide = TextEditingController();
  final TextEditingController _iliumProblemSideBack = TextEditingController();
  final TextEditingController _iliumSide = TextEditingController();
  final TextEditingController _iliumSideBack = TextEditingController();

  final TextEditingController _iliumRotation = TextEditingController();
  final TextEditingController _supineIliumLegLength = TextEditingController();
  final TextEditingController _longSitResult = TextEditingController();
  final TextEditingController _sLRSide = TextEditingController();
  final TextEditingController _patrickSide = TextEditingController();
  final TextEditingController _kneeFlexorSide = TextEditingController();
  final TextEditingController _supineIliumLegLengthBack = TextEditingController();
  final TextEditingController _longSitResultBack = TextEditingController();
  final TextEditingController _sLRSideBack = TextEditingController();
  final TextEditingController _patrickSideBack = TextEditingController();
  final TextEditingController _kneeFlexorSideBack = TextEditingController();

  final TextEditingController _iliumFlare = TextEditingController();
  final TextEditingController _distance = TextEditingController();

  late OperationItemsModel imageListUpSlip;
  String netUpImageUrlUpSlip = '';
  late OperationItemsModel imageListRotation;
  String netUpImageUrlRotation = '';
  late OperationItemsModel imageListFlare;
  String netUpImageUrlFlare = '';

  //資料title
  final _dataTitleBg = BoxDecoration(
      borderRadius: const BorderRadius.only(
          topLeft: Radius.circular(10),
          topRight: Radius.circular(10)),
      color: HexColor.fromHex('#096BC7'));

  _onLayoutDone(_) {
    List _list =
    _operationItemVm!.operationItemsModelList!.where((element) => element.id == 3).toList();
    for (var element in _list) {
      imageListUpSlip = element;
    }
    List list = _operationItemVm!.operationItemsModelList!.where((element) => element.id == 8).toList();
    for (var element in list) {
      imageListRotation = element;
      imageListFlare = element;
    }
    _dataInit(_patientVm!.patient);
  }
  void _dataInit(Patient? patient) {
    if (patient == null) {
      return;
    }
    log('取得 預覽(3): $patient');
    //region iliumProblemSide
    if(patient.gillet == 1 && patient.IC == 3 && patient.ASIS == 3 && patient.PSIS == 3){
      //左
      _iliumProblemSide.text = getTranslated(context, 'left');
    }else if(patient.gillet == 2 && patient.IC == 1 && patient.ASIS == 1 && patient.PSIS == 1){
      //右
      _iliumProblemSide.text = getTranslated(context, 'right');
    }else{
      //無 問題
      _iliumProblemSide.text = getTranslated(context, 'no');
      _iliumProblemSideBack.text = getTranslated(context, 'Problem');
    }
    //endregion
    //region iliumSide
    switch(patient.gillet){
      case 1:
        _iliumSide.text = getTranslated(context, 'left');
        _iliumSideBack.text = getTranslated(context, 'Higher');
        _longSitResult.text = getTranslated(context, 'left');
        break;
      case 2:
        _iliumSide.text = getTranslated(context, 'right');
        _iliumSideBack.text = getTranslated(context, 'Higher');
        _longSitResult.text = getTranslated(context, 'right');
        break;
    }
    //endregion
    //region iliumRotation
    if(patient.gillet == 1 && patient.supine_ilium_leg_length == 1){
      //後
      _iliumRotation.text = getTranslated(context, 'after');
      debugPrint(getTranslated(context, 'after'));
    }else if(patient.gillet == 1 && patient.supine_ilium_leg_length == 3){
      //前
      _iliumRotation.text = getTranslated(context, 'before');
      debugPrint(getTranslated(context, 'before'));
    }else if(patient.gillet == 2 && patient.supine_ilium_leg_length == 1){
      //前
      _iliumRotation.text = getTranslated(context, 'after');
      debugPrint(getTranslated(context, 'after'));
    }else if(patient.gillet == 2 && patient.supine_ilium_leg_length == 3){
      //後
      _iliumRotation.text = getTranslated(context, 'before');
    }
    //endregion
    //region supineIliumLegSide
    switch(patient.supine_ilium_leg_length){
      case 1:
        //右 較長
        _supineIliumLegLength.text = getTranslated(context, 'right');
        _supineIliumLegLengthBack.text = getTranslated(context, 'Longer');
        break;
      case 2:
        //雙 相等
        _supineIliumLegLength.text = getTranslated(context, 'twin');
        _supineIliumLegLengthBack.text = getTranslated(context, 'equality');
        break;
      case 3:
        //左 較長
        _supineIliumLegLength.text = getTranslated(context, 'left');
        _supineIliumLegLengthBack.text = getTranslated(context, 'Longer');
        break;
    }
    //endregion
    //region longSitResult
    switch(patient.Long_sit){
      case 0:
        _longSitResultBack.text = '';
        break;
      case 1:
        //由長變短
        _longSitResultBack.text = getTranslated(context, 'From_long_to_short');
        break;
      case 2:
        //由短變長
        _longSitResultBack.text = getTranslated(context, 'From_short_to_long');
        break;
    }
    //endregion
    //region SLRSide
    switch(patient.SLR){
      case 1:
        //右 較大
        _sLRSide.text = getTranslated(context, 'right');
        _sLRSideBack.text = getTranslated(context, 'Larger');
        break;
      case 2:
        //雙 相等
        _sLRSide.text = getTranslated(context, 'twin');
        _sLRSideBack.text = getTranslated(context, 'equality');
        break;
      case 3:
        //左 較大
        _sLRSide.text = getTranslated(context, 'left');
        _sLRSideBack.text = getTranslated(context, 'Larger');
        break;
    }
    //endregion
    //region patrick
    switch(patient.Patrick){
      case 1:
        //右 較大
        _patrickSide.text = getTranslated(context, 'right');
        _patrickSideBack.text = getTranslated(context, 'Larger');
        break;
      case 2:
        //雙 相等
        _patrickSide.text = getTranslated(context, 'twin');
        _patrickSideBack.text = getTranslated(context, 'equality');
        break;
      case 3:
        //左 較大
        _patrickSide.text = getTranslated(context, 'left');
        _patrickSideBack.text = getTranslated(context, 'Larger');
        break;
    }
    //endregion
    //region kneeflexor
    switch(patient.flexion){
      case 1:
        //左 無力
        _kneeFlexorSide.text = getTranslated(context, 'left');
        _kneeFlexorSideBack.text = getTranslated(context, 'powerless');
        break;
      case 2:
        //雙 相等
        _kneeFlexorSide.text = getTranslated(context, 'twin');
        _kneeFlexorSideBack.text = getTranslated(context, 'equality');
        break;
      case 3:
        //右 無力
        _kneeFlexorSide.text = getTranslated(context, 'right');
        _kneeFlexorSideBack.text = getTranslated(context, 'powerless');
        break;
    }
    //endregion
    //region iliumFlare
    if(patient.gillet == 1 && patient.ASIS_umbilical == 1){
      //內
      _iliumFlare.text = getTranslated(context, 'Inside');
    }else if(patient.gillet == 1 && patient.ASIS_umbilical == 2){
      //外
      _iliumFlare.text = getTranslated(context, 'outer');
    }else if(patient.gillet == 2 && patient.ASIS_umbilical == 1){
      //內
      _iliumFlare.text = getTranslated(context, 'Inside');
    }else if(patient.gillet == 2 && patient.ASIS_umbilical == 2){
      //外
      _iliumFlare.text = getTranslated(context, 'outer');
    }
    //endregion
    //region distance
    switch(patient.ASIS_umbilical){
      case 1:
        //短
        _distance.text = getTranslated(context, 'Short');
        break;
      case 2:
        _distance.text = getTranslated(context, 'Short');
        break;
    }
    //endregion
    _iliumProblem = true;
    _iliumSide1 = true;
    _iliumSide2 = true;
    //region 髂骨上移
    if (patient.inguinal == 1 && patient.supine_leg_length == 1) {
      //左上
      Map<String, dynamic> _map = imageListUpSlip.completePics
          .where((element) => element.containsKey('左_向上'))
          .first;
      netUpImageUrlUpSlip = _map.values.first;
    }
    if (patient.inguinal == 1 && patient.supine_leg_length == 3) {
      //左下
      Map<String, dynamic> _map = imageListUpSlip.completePics
          .where((element) => element.containsKey('左_向下'))
          .first;
      netUpImageUrlUpSlip = _map.values.first;
    }
    if (patient.inguinal == 2 && patient.supine_leg_length == 3) {
      //右上
      Map<String, dynamic> _map = imageListUpSlip.completePics
          .where((element) => element.containsKey('右_向上'))
          .first;
      netUpImageUrlUpSlip = _map.values.first;
    }
    if (patient.inguinal == 2 && patient.supine_leg_length == 1) {
      //右下
      Map<String, dynamic> _map = imageListUpSlip.completePics
          .where((element) => element.containsKey('右_向下'))
          .first;
      netUpImageUrlUpSlip = _map.values.first;
    }
    //endregion
    //region 髂骨左右
    if(patient.gillet == 1 && patient.supine_ilium_leg_length == 1 ){
      //左後
      Map<String, dynamic> _map = imageListRotation.completePics
          .where((element) => element.containsKey('左_後'))
          .first;
      netUpImageUrlRotation = _map.values.first;
    }
    if(patient.gillet == 1 && patient.supine_ilium_leg_length == 3){
      //左前
      Map<String, dynamic> _map = imageListRotation.completePics
          .where((element) => element.containsKey('左_前'))
          .first;
      netUpImageUrlRotation = _map.values.first;
    }
    if(patient.gillet == 2 && patient.supine_ilium_leg_length == 1){
      //右前
      Map<String, dynamic> _map = imageListRotation.completePics
          .where((element) => element.containsKey('右_前'))
          .first;
      netUpImageUrlRotation = _map.values.first;
    }
    if(patient.gillet == 2 && patient.supine_ilium_leg_length == 3){
      //右後
      Map<String, dynamic> _map = imageListRotation.completePics
          .where((element) => element.containsKey('右_後'))
          .first;
      netUpImageUrlRotation = _map.values.first;
    }
    //endregion
    //region 髂骨前後
    if(patient.gillet == 1 && patient.supine_ilium_leg_length == 1 ){
      //左內
      Map<String, dynamic> _map = imageListFlare.completePics
          .where((element) => element.containsKey('左_內'))
          .first;
      netUpImageUrlFlare = _map.values.first;
    }
    if(patient.gillet == 1 && patient.supine_ilium_leg_length == 3){
      //左外
      Map<String, dynamic> _map = imageListFlare.completePics
          .where((element) => element.containsKey('左_外'))
          .first;
      netUpImageUrlFlare = _map.values.first;
    }
    if(patient.gillet == 2 && patient.supine_ilium_leg_length == 1){
      //右內
      Map<String, dynamic> _map = imageListFlare.completePics
          .where((element) => element.containsKey('右_內'))
          .first;
      netUpImageUrlFlare = _map.values.first;
    }
    if(patient.gillet == 2 && patient.supine_ilium_leg_length == 3){
      //右外
      Map<String, dynamic> _map = imageListFlare.completePics
          .where((element) => element.containsKey('右_外'))
          .first;
      netUpImageUrlFlare = _map.values.first;
    }
    //endregion

    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  Widget build(BuildContext context) {
    _patientVm = Provider.of<PatientVm>(context);
    _operationItemVm = Provider.of<OperationItemVm>(context);
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: true,
      body: SafeArea(
        child: Column(
          children: [
            Expanded(flex: 1, child: _titleBar()),
            Expanded(flex: 12, child: _mainWidget())
          ],
        ),
      ),
    );
  }

  //Title
  Widget _titleBar() {
    return PvTitleWidget(
      titleText: getTranslated(context, 'Preview'),
      leftButton: const LeftButton(),
      rightButton: Container(),
    );
  }

  //主體
  Widget _mainWidget() {
    return Container(
      decoration: Style.homeBg,
      alignment: Alignment.center,
      child: Container(
        width: MediaQuery.of(context).size.width / 1.1,
        height: MediaQuery.of(context).size.height / 1.2,
        decoration: Style.centerBG,
        padding: const EdgeInsets.only(top: 15, left: 10, right: 10, bottom: 15),
        child: ScrollConfiguration(
          behavior: NoShadowScrollBehavior(),
          child: buildIndexFrom(),
        ),
      ),
    );
  }

  //表單
  Widget buildIndexFrom() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Expanded(flex: 1, child: fromHeader()),
        const SizedBox(
          height: 10,
        ),
        Expanded(flex: 9, child: fromBody()),
        const SizedBox(
          height: 20,
        ),
        Expanded(
            flex: 1,
            child: InkWell(
              onTap: _nextPage,
              child: Container(
                alignment: Alignment.center,
                width: 500,
                decoration: Style.imageButtonBG,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    FittedBox(
                      fit:BoxFit.scaleDown,
                      child: Text(
                        getTranslated(context, 'Next_page'),
                        textScaleFactor: 1,
                        style: const TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 20),
                      ),
                    ),
                    Image.asset(
                      'assets/images/forward.png',
                      width: 30,
                    )
                  ],
                ),
              ),
            ))
      ],
    );
  }
  //下一頁
  void _nextPage(){
    delegate.push(name: RouteName.pv04);
  }

  //表單頭
  Widget fromHeader() {
    return Container(
      width: MediaQuery.of(context).size.width,
      decoration: const BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/images/record_sheet_title.png'),
              fit: BoxFit.fill)),
      alignment: Alignment.center,
      child: FittedBox(
        fit:BoxFit.scaleDown,
        child: Text(
          getTranslated(
              context, 'Guangyin_Method_Evaluation_and_Adjustment_Record_Sheet'),
          textScaleFactor: 1,
          style: const TextStyle(
              color: Colors.white, fontSize: 23, fontWeight: FontWeight.bold),
        ),
      ),
    );
  }

  //表單身
  Widget fromBody() {
    return ScrollConfiguration(
        behavior: NoShadowScrollBehavior(),
        child: SingleChildScrollView(
          child: Column(
            children: [
              //髂骨上下移問題列表與調整紀錄
              _moveUpAndDownWidget(),
              //髂骨前後轉問題列表與調整紀錄
              _forwardAndBackwardWidget(),
              //髂骨內外旋問題列表與調整紀錄
              _internalAndExternalRotationWidget(),

              SizedBox(
                height: 15,
              ),
              //標語
              Container(
                height: 50,
                alignment: Alignment.centerLeft,
                decoration: BoxDecoration(color: HexColor.fromHex('#609AD1')),
                padding: const EdgeInsets.only(left: 8),
                child: FittedBox(
                  fit:BoxFit.scaleDown,
                  child: Text(
                    getTranslated(context,
                        'All_rights_reserved_by_the_light_silver_technique_piracy_must_be_punished'),
                    textScaleFactor: 1,
                    style: const TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  ),
                ),
              )
            ],
          ),
        ));
  }

  //髂骨上下移問題列表與調整紀錄
  Widget _moveUpAndDownWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 8),
      child: Column(
        children: [
          Container(
            height: 50,
            alignment: Alignment.center,
            decoration: _dataTitleBg,
            child: FittedBox(
              fit:BoxFit.scaleDown,
              child: Text(
                getTranslated(context, 'List_of_problems_with_iliac_bone_movement_up_and_down_and_adjustment_records'),
                textScaleFactor: 1,
                style: const TextStyle(
                    fontWeight: FontWeight.bold, color: Colors.white
                ),
              ),
            ),
          ),
          Column(
            children: [
              Container(
                alignment: Alignment.centerLeft,
                child: FittedBox(
                  fit:BoxFit.scaleDown,
                  child: Text(
                    getTranslated(context, 'Problem'),
                    textScaleFactor: 1,
                    style: const TextStyle(
                        fontWeight: FontWeight.bold
                    ),
                  ),
                ),
              ),
              //問題
              Row(
                children: [
                  SizedBox(
                    width: 80,
                    child: TextField(
                      readOnly: true,
                      textAlign: TextAlign.center,
                      style: const TextStyle(color: Colors.black),
                      decoration: InputDecoration(
                          errorText: null,
                          contentPadding: const EdgeInsets.all(5.0),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.grey[400]!),
                              borderRadius: BorderRadius.circular(10.0)),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0)),
                          filled: true,
                          fillColor: Colors.white,
                          hintText: '',
                          hintStyle: const TextStyle(color: Colors.grey)),
                      keyboardType: TextInputType.text,
                      textInputAction: TextInputAction.next,
                      controller: _iliumProblemSide,
                    ),
                  ),
                  FittedBox(
                    fit:BoxFit.scaleDown,
                    child: Text(
                      getTranslated(context, 'Upward_iliac_bone'),
                      textScaleFactor: 1,
                      style: const TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  _iliumProblemSideBack.text.isNotEmpty ? SizedBox(
                    width: 80,
                    child: TextField(
                      readOnly: true,
                      textAlign: TextAlign.center,
                      style: const TextStyle(color: Colors.black),
                      decoration: InputDecoration(
                          errorText: null,
                          contentPadding: const EdgeInsets.all(5.0),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.grey[400]!),
                              borderRadius: BorderRadius.circular(10.0)),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0)),
                          filled: true,
                          fillColor: Colors.white,
                          hintText: '',
                          hintStyle: const TextStyle(color: Colors.grey)),
                      keyboardType: TextInputType.text,
                      textInputAction: TextInputAction.next,
                      controller: _iliumProblemSideBack,
                    ),
                  ) : Container(),
                  _iliumProblemSideBack.text.isNotEmpty ? FittedBox(
                    fit:BoxFit.scaleDown,
                    child: Text(
                      getTranslated(context, 'shift'),
                      textScaleFactor: 1,
                      style: const TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ) : Container(),
                ],
              ),
              SizedBox(height: 10,),
              //表徵
              Column(
                children: [
                  Container(
                    alignment: Alignment.centerLeft,
                    child: FittedBox(
                      fit:BoxFit.scaleDown,
                      child: Text(
                        getTranslated(context, 'Characterization'),
                        textScaleFactor: 1,
                        style: const TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: 180,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            //髂嵴
                            Container(
                              height: 60,
                              child: FittedBox(
                                fit:BoxFit.scaleDown,
                                child: Text(
                                  getTranslated(context, 'Iliac_crest'),
                                  textScaleFactor: 1,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: HexColor.fromHex('#096BC7')
                                  ),
                                ),
                              ),
                            ),
                            //髂骨前上棘ASIS
                            Container(
                              height: 60,
                              child: FittedBox(
                                fit:BoxFit.scaleDown,
                                child: Text(
                                  getTranslated(context, 'Anterior_superior_iliac_spine_ASIS'),
                                  textScaleFactor: 1,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: HexColor.fromHex('#096BC7')
                                  ),
                                ),
                              ),
                            ),
                            //髂骨後上棘PSIS
                            Container(
                                height: 60,
                              child: FittedBox(
                                fit:BoxFit.scaleDown,
                                child: Text(
                                  getTranslated(context, 'Posterior_superior_iliac_spine_PSIS'),
                                  textScaleFactor: 1,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: HexColor.fromHex('#096BC7')
                                  ),
                                ),
                              )
                            )
                          ],
                        ),
                      ),
                      SizedBox(width: 10,),
                      SizedBox(
                        height: 200,
                        child: Column(
                          children: [
                            //髂嵴
                            SizedBox(
                              width: 80,
                              height: 60,
                              child: TextField(
                                readOnly: true,
                                textAlign: TextAlign.center,
                                style: const TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                    errorText: null,
                                    contentPadding: const EdgeInsets.all(5.0),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.grey[400]!),
                                        borderRadius: BorderRadius.circular(10.0)),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10.0)),
                                    filled: true,
                                    fillColor: Colors.white,
                                    hintText: '',
                                    hintStyle: const TextStyle(color: Colors.grey)),
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.next,
                                controller: _iliumSide,
                              ),
                            ),
                            //髂骨前上棘ASIS
                            SizedBox(
                              width: 80,
                              height: 60,
                              child: TextField(
                                readOnly: true,
                                textAlign: TextAlign.center,
                                style: const TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                    errorText: null,
                                    contentPadding: const EdgeInsets.all(5.0),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.grey[400]!),
                                        borderRadius: BorderRadius.circular(10.0)),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10.0)),
                                    filled: true,
                                    fillColor: Colors.white,
                                    hintText: '',
                                    hintStyle: const TextStyle(color: Colors.grey)),
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.next,
                                controller: _iliumSide,
                              ),
                            ),
                            //髂骨後上棘PSIS
                            SizedBox(
                              width: 80,
                              height: 60,
                              child: TextField(
                                readOnly: true,
                                textAlign: TextAlign.center,
                                style: const TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                    errorText: null,
                                    contentPadding: const EdgeInsets.all(5.0),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.grey[400]!),
                                        borderRadius: BorderRadius.circular(10.0)),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10.0)),
                                    filled: true,
                                    fillColor: Colors.white,
                                    hintText: '',
                                    hintStyle: const TextStyle(color: Colors.grey)),
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.next,
                                controller: _iliumSide,
                              ),
                            )
                          ],
                        ),
                      ),
                      SizedBox(width: 10,),
                      SizedBox(
                        height: 180,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            //髂嵴
                            Container(
                              height: 60,
                              child: FittedBox(
                                fit:BoxFit.scaleDown,
                                child: Text(
                                  getTranslated(context, 'side'),
                                  textScaleFactor: 1,
                                  style: const TextStyle(
                                      fontWeight: FontWeight.bold
                                  ),
                                ),
                              )
                            ),
                            //髂骨前上棘ASIS
                            Container(
                              height: 60,
                              child: FittedBox(
                                fit:BoxFit.scaleDown,
                                child: Text(
                                  getTranslated(context, 'side'),
                                  textScaleFactor: 1,
                                  style: const TextStyle(
                                      fontWeight: FontWeight.bold
                                  ),
                                ),
                              ),
                            ),
                            //髂骨後上棘PSIS
                            Container(
                              height: 60,
                              child: FittedBox(
                                fit:BoxFit.scaleDown,
                                child: Text(
                                  getTranslated(context, 'side'),
                                  textScaleFactor: 1,
                                  style: const TextStyle(
                                      fontWeight: FontWeight.bold
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 200,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            //鼠蹊韌帶
                            SizedBox(
                              width: 80,
                              height: 60,
                              child: TextField(
                                readOnly: true,
                                textAlign: TextAlign.center,
                                style: const TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                    errorText: null,
                                    contentPadding: const EdgeInsets.all(5.0),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.grey[400]!),
                                        borderRadius: BorderRadius.circular(10.0)),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10.0)),
                                    filled: true,
                                    fillColor: Colors.white,
                                    hintText: '',
                                    hintStyle: const TextStyle(color: Colors.grey)),
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.next,
                                controller: _iliumSideBack,
                              ),
                            ),
                            //腳長
                            SizedBox(
                              width: 80,
                              height: 60,
                              child: TextField(
                                readOnly: true,
                                textAlign: TextAlign.center,
                                style: const TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                    errorText: null,
                                    contentPadding: const EdgeInsets.all(5.0),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.grey[400]!),
                                        borderRadius: BorderRadius.circular(10.0)),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10.0)),
                                    filled: true,
                                    fillColor: Colors.white,
                                    hintText: '',
                                    hintStyle: const TextStyle(color: Colors.grey)),
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.next,
                                controller: _iliumSideBack,
                              ),
                            ),
                            //肌力檢測
                            SizedBox(
                              width: 80,
                              height: 60,
                              child: TextField(
                                readOnly: true,
                                textAlign: TextAlign.center,
                                style: const TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                    errorText: null,
                                    contentPadding: const EdgeInsets.all(5.0),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.grey[400]!),
                                        borderRadius: BorderRadius.circular(10.0)),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10.0)),
                                    filled: true,
                                    fillColor: Colors.white,
                                    hintText: '',
                                    hintStyle: const TextStyle(color: Colors.grey)),
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.next,
                                controller: _iliumSideBack,
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  )
                ],
              ),
              SizedBox(height: 10,),
              //圖片
              Container(
                width: 800,
                height: 350,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    borderRadius: const BorderRadius.all(Radius.circular(10)),
                    border: Border.all(color: Colors.grey, width: 1)
                ),
                child: Image.network('$imageUrlOffline$imageUrlPublic$netUpImageUrlUpSlip'),
              ),
              //結果
              Row(
                children: [
                  FittedBox(
                    fit:BoxFit.scaleDown,
                    child: Text(
                      getTranslated(context, 'result'),
                      textScaleFactor: 1,
                      style: const TextStyle(
                          fontWeight: FontWeight.bold
                      ),
                    ),
                  ),
                  Transform.scale(
                    scale: 1.5,
                    child: Checkbox(value: _iliumProblem, onChanged: null),
                  ),
                  FittedBox(
                    fit:BoxFit.scaleDown,
                    child: Text(
                      getTranslated(context, 'Normal_position_restored'),
                      textScaleFactor: 1,
                      style: const TextStyle(
                          fontWeight: FontWeight.bold
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),

        ],
      ),
    );
  }

  //髂骨前後轉問題列表與調整紀錄
  Widget _forwardAndBackwardWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 8),
      child: Column(
        children: [
          Container(
            height: 50,
            alignment: Alignment.center,
            decoration: _dataTitleBg,
            child: FittedBox(
              fit:BoxFit.scaleDown,
              child: Text(
                getTranslated(context, 'List_of_iliac_bone_anteroposterior_rotation_problems_and_adjustment_records'),
                textScaleFactor: 1,
                style: const TextStyle(
                    fontWeight: FontWeight.bold, color: Colors.white
                ),
              ),
            ),
          ),
          Column(
            children: [
              Container(
                alignment: Alignment.centerLeft,
                child: FittedBox(
                  fit:BoxFit.scaleDown,
                  child: Text(
                    getTranslated(context, 'Problem'),
                    textScaleFactor: 1,
                    style: const TextStyle(
                        fontWeight: FontWeight.bold
                    ),
                  ),
                ),
              ),
              //問題
              Row(
                children: [
                  SizedBox(
                    width: 80,
                    child: TextField(
                      readOnly: true,
                      textAlign: TextAlign.center,
                      style: const TextStyle(color: Colors.black),
                      decoration: InputDecoration(
                          errorText: null,
                          contentPadding: const EdgeInsets.all(5.0),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.grey[400]!),
                              borderRadius: BorderRadius.circular(10.0)),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0)),
                          filled: true,
                          fillColor: Colors.white,
                          hintText: '',
                          hintStyle: const TextStyle(color: Colors.grey)),
                      keyboardType: TextInputType.text,
                      textInputAction: TextInputAction.next,
                      controller: _iliumSide,
                    ),
                  ),
                  FittedBox(
                    fit:BoxFit.scaleDown,
                    child: Text(
                      getTranslated(context, 'Lateral_ilium'),
                      textScaleFactor: 1,
                      style: const TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 80,
                    child: TextField(
                      readOnly: true,
                      textAlign: TextAlign.center,
                      style: const TextStyle(color: Colors.black),
                      decoration: InputDecoration(
                          errorText: null,
                          contentPadding: const EdgeInsets.all(5.0),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.grey[400]!),
                              borderRadius: BorderRadius.circular(10.0)),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0)),
                          filled: true,
                          fillColor: Colors.white,
                          hintText: '',
                          hintStyle: const TextStyle(color: Colors.grey)),
                      keyboardType: TextInputType.text,
                      textInputAction: TextInputAction.next,
                      controller: _iliumRotation,
                    ),
                  ),
                  FittedBox(
                    fit:BoxFit.scaleDown,
                    child: Text(
                      getTranslated(context, 'turn'),
                      textScaleFactor: 1,
                      style: const TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 10,),
              //表徵
              Column(
                children: [
                  Container(
                    alignment: Alignment.centerLeft,
                    child: FittedBox(
                      fit:BoxFit.scaleDown,
                      child: Text(
                        getTranslated(context, 'Characterization'),
                        textScaleFactor: 1,
                        style: const TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: 400,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            //腳長
                            Expanded(child: Container(
                              alignment: Alignment.center,
                              child: FittedBox(
                                fit:BoxFit.scaleDown,
                                child: Text(
                                  getTranslated(context, 'Foot_length'),
                                  textScaleFactor: 1,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: HexColor.fromHex('#096BC7')
                                  ),
                                ),
                              ),
                            )),
                            //長坐姿腳長
                            Expanded(child: Container(
                              alignment: Alignment.center,
                              child: FittedBox(
                                fit:BoxFit.scaleDown,
                                child: Text(
                                  getTranslated(context, 'Long_sitting_and_long_feet'),
                                  textScaleFactor: 1,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: HexColor.fromHex('#096BC7')
                                  ),
                                ),
                              ),
                            )),
                            //被動直腿上舉
                            Expanded(child: Container(
                              alignment: Alignment.center,
                              child: FittedBox(
                                fit:BoxFit.scaleDown,
                                child: Text(
                                  getTranslated(context, 'Passive_straight_leg_lift'),
                                  textScaleFactor: 1,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: HexColor.fromHex('#096BC7')
                                  ),
                                ),
                              ),
                            )),
                            //Patricks_Test
                            Expanded(child: Container(
                              alignment: Alignment.center,
                              child: FittedBox(
                                fit:BoxFit.scaleDown,
                                child: Text(
                                  getTranslated(context, 'Patricks_Test'),
                                  textScaleFactor: 1,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: HexColor.fromHex('#096BC7')
                                  ),
                                ),
                              ),
                            )),
                            //肌力
                            Expanded(child: Container(
                              alignment: Alignment.center,
                              child: FittedBox(
                                fit:BoxFit.scaleDown,
                                child: Text(
                                  getTranslated(context, 'Muscle_strength'),
                                  textScaleFactor: 1,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: HexColor.fromHex('#096BC7')
                                  ),
                                ),
                              ),
                            ))
                          ],
                        ),
                      ),
                      SizedBox(width: 10,),
                      SizedBox(
                        height: 400,
                        child: Column(
                          children: [
                            //腳長
                            Expanded(child: SizedBox(
                              width: 80,
                              child: TextField(
                                readOnly: true,
                                textAlign: TextAlign.center,
                                style: const TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                    errorText: null,
                                    contentPadding: const EdgeInsets.all(5.0),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.grey[400]!),
                                        borderRadius: BorderRadius.circular(10.0)),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10.0)),
                                    filled: true,
                                    fillColor: Colors.white,
                                    hintText: '',
                                    hintStyle: const TextStyle(color: Colors.grey)),
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.next,
                                controller: _supineIliumLegLength,
                              ),
                            )),
                            //長坐姿腳長
                            Expanded(child: SizedBox(
                              width: 80,
                              child: TextField(
                                readOnly: true,
                                textAlign: TextAlign.center,
                                style: const TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                    errorText: null,
                                    contentPadding: const EdgeInsets.all(5.0),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.grey[400]!),
                                        borderRadius: BorderRadius.circular(10.0)),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10.0)),
                                    filled: true,
                                    fillColor: Colors.white,
                                    hintText: '',
                                    hintStyle: const TextStyle(color: Colors.grey)),
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.next,
                                controller: _longSitResult,
                              ),
                            )),
                            //被動直腿上舉
                            Expanded(child: SizedBox(
                              width: 80,
                              child: TextField(
                                readOnly: true,
                                textAlign: TextAlign.center,
                                style: const TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                    errorText: null,
                                    contentPadding: const EdgeInsets.all(5.0),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.grey[400]!),
                                        borderRadius: BorderRadius.circular(10.0)),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10.0)),
                                    filled: true,
                                    fillColor: Colors.white,
                                    hintText: '',
                                    hintStyle: const TextStyle(color: Colors.grey)),
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.next,
                                controller: _sLRSide,
                              ),
                            )),
                            //被動直腿上舉
                            Expanded(child: SizedBox(
                              width: 80,
                              child: TextField(
                                readOnly: true,
                                textAlign: TextAlign.center,
                                style: const TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                    errorText: null,
                                    contentPadding: const EdgeInsets.all(5.0),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.grey[400]!),
                                        borderRadius: BorderRadius.circular(10.0)),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10.0)),
                                    filled: true,
                                    fillColor: Colors.white,
                                    hintText: '',
                                    hintStyle: const TextStyle(color: Colors.grey)),
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.next,
                                controller: _patrickSide,
                              ),
                            )),
                            //被動直腿上舉
                            Expanded(child: SizedBox(
                              width: 80,
                              child: TextField(
                                readOnly: true,
                                textAlign: TextAlign.center,
                                style: const TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                    errorText: null,
                                    contentPadding: const EdgeInsets.all(5.0),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.grey[400]!),
                                        borderRadius: BorderRadius.circular(10.0)),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10.0)),
                                    filled: true,
                                    fillColor: Colors.white,
                                    hintText: '',
                                    hintStyle: const TextStyle(color: Colors.grey)),
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.next,
                                controller: _kneeFlexorSide,
                              ),
                            ))
                          ],
                        ),
                      ),
                      SizedBox(width: 10,),
                      SizedBox(
                        height: 400,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            //腳長
                            Expanded(child: Container(
                              alignment: Alignment.center,
                              child: FittedBox(
                                fit:BoxFit.scaleDown,
                                child: Text(
                                  getTranslated(context, 'Side_foot_length'),
                                  textScaleFactor: 1,
                                  style: const TextStyle(
                                      fontWeight: FontWeight.bold
                                  ),
                                ),
                              ),
                            )),
                            //長坐姿腳長
                            Expanded(child: Container(
                              alignment: Alignment.center,
                              child: FittedBox(
                                fit:BoxFit.scaleDown,
                                child: Text(
                                  getTranslated(context, 'Side_foot_length'),
                                  textScaleFactor: 1,
                                  style: const TextStyle(
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            )),
                            //被動直腿上舉
                            Expanded(child: Container(
                              alignment: Alignment.center,
                              child: FittedBox(
                                fit:BoxFit.scaleDown,
                                child: Text(
                                  getTranslated(context, 'Side_angle'),
                                  textScaleFactor: 1,
                                  style: const TextStyle(
                                      fontWeight: FontWeight.bold
                                  ),
                                ),
                              ),
                            )),
                            //Patrick's Test
                            Expanded(child: Container(
                              alignment: Alignment.center,
                              child: FittedBox(
                                fit:BoxFit.scaleDown,
                                child: Text(
                                  getTranslated(context, 'Side_angle'),
                                  textScaleFactor: 1,
                                  style: const TextStyle(
                                      fontWeight: FontWeight.bold
                                  ),
                                ),
                              ),
                            )),
                            //肌力
                            Expanded(child: Container(
                              alignment: Alignment.center,
                              child: FittedBox(
                                fit:BoxFit.scaleDown,
                                child: Text(
                                  getTranslated(context, 'Side_knee_flexion'),
                                  textScaleFactor: 1,
                                  style: const TextStyle(
                                      fontWeight: FontWeight.bold
                                  ),
                                ),
                              ),
                            )),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 400,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            //腳長
                            Expanded(child: SizedBox(
                              width: 80,
                              child: TextField(
                                readOnly: true,
                                textAlign: TextAlign.center,
                                style: const TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                    errorText: null,
                                    contentPadding: const EdgeInsets.all(5.0),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.grey[400]!),
                                        borderRadius: BorderRadius.circular(10.0)),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10.0)),
                                    filled: true,
                                    fillColor: Colors.white,
                                    hintText: '',
                                    hintStyle: const TextStyle(color: Colors.grey)),
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.next,
                                controller: _supineIliumLegLengthBack,
                              ),
                            )),
                            //長坐姿腳長
                            Expanded(child: SizedBox(
                              width: 80,
                              child: TextField(
                                readOnly: true,
                                textAlign: TextAlign.center,
                                style: const TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                    errorText: null,
                                    contentPadding: const EdgeInsets.all(5.0),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.grey[400]!),
                                        borderRadius: BorderRadius.circular(10.0)),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10.0)),
                                    filled: true,
                                    fillColor: Colors.white,
                                    hintText: '',
                                    hintStyle: const TextStyle(color: Colors.grey)),
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.next,
                                controller: _longSitResultBack,
                              ),
                            )),
                            //被動直腿上舉
                            Expanded(child: SizedBox(
                              width: 80,
                              child: TextField(
                                readOnly: true,
                                textAlign: TextAlign.center,
                                style: const TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                    errorText: null,
                                    contentPadding: const EdgeInsets.all(5.0),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.grey[400]!),
                                        borderRadius: BorderRadius.circular(10.0)),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10.0)),
                                    filled: true,
                                    fillColor: Colors.white,
                                    hintText: '',
                                    hintStyle: const TextStyle(color: Colors.grey)),
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.next,
                                controller: _sLRSideBack,
                              ),
                            )),
                            //Patrick's Test
                            Expanded(child: SizedBox(
                              width: 80,
                              child: TextField(
                                readOnly: true,
                                textAlign: TextAlign.center,
                                style: const TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                    errorText: null,
                                    contentPadding: const EdgeInsets.all(5.0),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.grey[400]!),
                                        borderRadius: BorderRadius.circular(10.0)),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10.0)),
                                    filled: true,
                                    fillColor: Colors.white,
                                    hintText: '',
                                    hintStyle: const TextStyle(color: Colors.grey)),
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.next,
                                controller: _patrickSideBack,
                              ),
                            )),
                            //肌力
                            Expanded(child: SizedBox(
                              width: 80,
                              child: TextField(
                                readOnly: true,
                                textAlign: TextAlign.center,
                                style: const TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                    errorText: null,
                                    contentPadding: const EdgeInsets.all(5.0),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.grey[400]!),
                                        borderRadius: BorderRadius.circular(10.0)),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10.0)),
                                    filled: true,
                                    fillColor: Colors.white,
                                    hintText: '',
                                    hintStyle: const TextStyle(color: Colors.grey)),
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.next,
                                controller: _kneeFlexorSideBack,
                              ),
                            ))
                          ],
                        ),
                      )
                    ],
                  )
                ],
              ),
              SizedBox(height: 10,),
              //圖片
              Container(
                width: 800,
                height: 350,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    borderRadius: const BorderRadius.all(Radius.circular(10)),
                    border: Border.all(color: Colors.grey, width: 1)
                ),
                child: Image.network('$imageUrlOffline$imageUrlPublic$netUpImageUrlRotation'),
              ),
              //結果
              Row(
                children: [
                  FittedBox(
                    fit:BoxFit.scaleDown,
                    child: Text(
                      getTranslated(context, 'result'),
                      textScaleFactor: 1,
                      style: const TextStyle(
                          fontWeight: FontWeight.bold
                      ),
                    ),
                  ),
                  Transform.scale(
                    scale: 1.5,
                    child: Checkbox(value: _iliumSide1, onChanged: null),
                  ),
                  FittedBox(
                    fit:BoxFit.scaleDown,
                    child: Text(
                      getTranslated(context, 'Normal_position_restored'),
                      textScaleFactor: 1,
                      style: const TextStyle(
                          fontWeight: FontWeight.bold
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
        ],
      ),
    );
  }

  //髂骨內外旋問題列表與調整紀錄
  Widget _internalAndExternalRotationWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 8),
      child: Column(
        children: [
          Container(
            height: 50,
            alignment: Alignment.center,
            decoration: _dataTitleBg,
            child: FittedBox(
              fit:BoxFit.scaleDown,
              child: Text(
                getTranslated(context, 'List_of_lumbar_spine_problems_and_adjustment_records'),
                textScaleFactor: 1,
                style: const TextStyle(
                    fontWeight: FontWeight.bold, color: Colors.white
                ),
              ),
            ),
          ),
          Column(
            children: [
              Container(
                alignment: Alignment.centerLeft,
                child: FittedBox(
                  fit:BoxFit.scaleDown,
                  child: Text(
                    getTranslated(context, 'Problem'),
                    textScaleFactor: 1,
                    style: const TextStyle(
                        fontWeight: FontWeight.bold
                    ),
                  ),
                ),
              ),
              //問題
              Row(
                children: [
                  SizedBox(
                    width: 80,
                    child: TextField(
                      readOnly: true,
                      textAlign: TextAlign.center,
                      style: const TextStyle(color: Colors.black),
                      decoration: InputDecoration(
                          errorText: null,
                          contentPadding: const EdgeInsets.all(5.0),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.grey[400]!),
                              borderRadius: BorderRadius.circular(10.0)),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0)),
                          filled: true,
                          fillColor: Colors.white,
                          hintText: '',
                          hintStyle: const TextStyle(color: Colors.grey)),
                      keyboardType: TextInputType.text,
                      textInputAction: TextInputAction.next,
                      controller: _iliumSide,
                    ),
                  ),
                  FittedBox(
                    fit:BoxFit.scaleDown,
                    child: Text(
                      getTranslated(context, 'Lateral_ilium'),
                      textScaleFactor: 1,
                      style: const TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 80,
                    child: TextField(
                      readOnly: true,
                      textAlign: TextAlign.center,
                      style: const TextStyle(color: Colors.black),
                      decoration: InputDecoration(
                          errorText: null,
                          contentPadding: const EdgeInsets.all(5.0),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.grey[400]!),
                              borderRadius: BorderRadius.circular(10.0)),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0)),
                          filled: true,
                          fillColor: Colors.white,
                          hintText: '',
                          hintStyle: const TextStyle(color: Colors.grey)),
                      keyboardType: TextInputType.text,
                      textInputAction: TextInputAction.next,
                      controller: _iliumFlare,
                    ),
                  ),
                  FittedBox(
                    fit:BoxFit.scaleDown,
                    child: Text(
                      getTranslated(context, 'Spin'),
                      textScaleFactor: 1,
                      style: const TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 10,),
              //表徵
              Column(
                children: [
                  Container(
                    alignment: Alignment.centerLeft,
                    child: FittedBox(
                      fit:BoxFit.scaleDown,
                      child: Text(
                        getTranslated(context, 'Characterization'),
                        textScaleFactor: 1,
                        style: const TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                  //髂骨前上棘ASIS與肚臍距離
                  Container(
                    alignment: Alignment.centerLeft,
                    child: FittedBox(
                      fit:BoxFit.scaleDown,
                      child: Text(
                        getTranslated(context, 'The_distance_between_ASIS_of_the_anterior_superior_iliac_spine_and_the_navel'),
                        textScaleFactor: 1,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: HexColor.fromHex('#096BC7')
                        ),
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(width: 10,),
                      Column(
                        children: [
                          //髂骨前上棘ASIS與肚臍距離
                          SizedBox(
                            width: 80,
                            child: TextField(
                              readOnly: true,
                              textAlign: TextAlign.center,
                              style: const TextStyle(color: Colors.black),
                              decoration: InputDecoration(
                                  errorText: null,
                                  contentPadding: const EdgeInsets.all(5.0),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.grey[400]!),
                                      borderRadius: BorderRadius.circular(10.0)),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10.0)),
                                  filled: true,
                                  fillColor: Colors.white,
                                  hintText: '',
                                  hintStyle: const TextStyle(color: Colors.grey)),
                              keyboardType: TextInputType.text,
                              textInputAction: TextInputAction.next,
                              controller: _iliumSide,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(width: 10,),
                      Row(
                        children: [
                          FittedBox(
                            fit:BoxFit.scaleDown,
                            child: Text(
                              getTranslated(context, 'Side_comparison'),
                              textScaleFactor: 1,
                              style: const TextStyle(
                                  fontWeight: FontWeight.bold
                              ),
                            ),
                          ),
                          //欄位
                          SizedBox(
                            width: 80,
                            child: TextField(
                              readOnly: true,
                              textAlign: TextAlign.center,
                              style: const TextStyle(color: Colors.black),
                              decoration: InputDecoration(
                                  errorText: null,
                                  contentPadding: const EdgeInsets.all(5.0),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.grey[400]!),
                                      borderRadius: BorderRadius.circular(10.0)),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10.0)),
                                  filled: true,
                                  fillColor: Colors.white,
                                  hintText: '',
                                  hintStyle: const TextStyle(color: Colors.grey)),
                              keyboardType: TextInputType.text,
                              textInputAction: TextInputAction.next,
                              controller: _distance,
                            ),
                          ),
                        ],
                      ),
                    ],
                  )
                ],
              ),
              SizedBox(height: 10,),
              //圖片
              Container(
                width: 800,
                height: 350,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    borderRadius: const BorderRadius.all(Radius.circular(10)),
                    border: Border.all(color: Colors.grey, width: 1)
                ),
                child: Image.network('$imageUrlOffline$imageUrlPublic$netUpImageUrlFlare'),
              ),
              //結果
              Row(
                children: [
                  FittedBox(
                    fit:BoxFit.scaleDown,
                    child: Text(
                      getTranslated(context, 'result'),
                      textScaleFactor: 1,
                      style: const TextStyle(
                          fontWeight: FontWeight.bold
                      ),
                    ),
                  ),
                  Transform.scale(
                    scale: 1.5,
                    child: Checkbox(value: _iliumSide2, onChanged: null),
                  ),
                  FittedBox(
                    fit:BoxFit.scaleDown,
                    child: Text(
                      getTranslated(context, 'Normal_position_restored'),
                      textScaleFactor: 1,
                      style: const TextStyle(
                          fontWeight: FontWeight.bold
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),

        ],
      ),
    );
  }

}