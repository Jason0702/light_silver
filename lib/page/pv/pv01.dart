import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:light_silver/language/language_constants.dart';
import 'package:light_silver/models/patient.dart';
import 'package:light_silver/utils/utils.dart';
import 'package:light_silver/view_model/user_company_information_vm.dart';
import 'package:light_silver/widgets/left_button.dart';
import 'package:light_silver/widgets/noShadowScrollbehavior.dart';
import 'package:provider/provider.dart';

import '../../app.dart';
import '../../routes/route_name.dart';
import '../../view_model/patient_vm.dart';
import '../../widgets/title_widget.dart';

class PV01Routes extends StatefulWidget {
  @override
  State createState() => _PV01RoutesState();
}

class _PV01RoutesState extends State<PV01Routes> {
  PatientVm? _patientVm;
  UserCompanyInformationVm? _userCompanyInformationVm;

  //資料框
  final _inputBg = BoxDecoration(
      border: Border.all(color: Colors.grey, width: 1.5),
      borderRadius: const BorderRadius.all(Radius.circular(10)));

  String _companyName = '載入中';
  String _companyPhone = '載入中';
  String _companyAddress = '載入中';
  String _companyLOGO = '載入中';

  final TextEditingController _patientName = TextEditingController();
  final TextEditingController _patientGender = TextEditingController();
  final TextEditingController _patientAge = TextEditingController();
  final TextEditingController _patientPhone = TextEditingController();
  final TextEditingController _patientProblem = TextEditingController();

  final TextEditingController _problemText = TextEditingController();
  final TextEditingController _problemSideText = TextEditingController();
  bool _pad = false;
  final TextEditingController _padHigh = TextEditingController();

  //減緩不適症狀
  bool _relieveSymptomsOfDiscomfort = false;
  //姿勢矯正
  bool _postureCorrection = false;
  //資訊衛教
  bool _informationHealthEducation = false;
  //關節復位
  bool _jointReduction = false;
  //核心肌群訓練
  bool _coreMuscleTraining = false;
  //健康促進
  bool _healthPromotion = false;

  List<String> items = ["F", "M"];

  _onLayoutDone(_) {
    _dataInit(_patientVm!.patient);
  }

  void _dataInit(Patient? patient) {
    if (patient == null) {
      return;
    }
    log('取得 預覽(1): $patient');
    _companyName = _userCompanyInformationVm!.userCompanyInformationModel!.name;
    _companyPhone = _userCompanyInformationVm!.userCompanyInformationModel!.phone;
    _companyAddress = '${_userCompanyInformationVm!.userCompanyInformationModel!.city} ${_userCompanyInformationVm!.userCompanyInformationModel!.area}${_userCompanyInformationVm!.userCompanyInformationModel!.address}';
    _companyLOGO = _userCompanyInformationVm!.userCompanyInformationModel!.logo;

    _patientName.text = patient.name;
    _patientGender.text = patient.gender;
    _patientAge.text = patient.age.toString();
    _patientPhone.text = patient.phone;
    _patientProblem.text = patient.problem;
    _relieveSymptomsOfDiscomfort =
    patient.relieveSymptomsOfDiscomfort == 1 ? true : false;
    _postureCorrection = patient.postureCorrection == 1 ? true : false;
    _informationHealthEducation =
    patient.informationHealthEducation == 1 ? true : false;
    _jointReduction = patient.jointReduction == 1 ? true : false;
    _coreMuscleTraining = patient.coreMuscleTraining == 1 ? true : false;
    _healthPromotion = patient.healthPromotion == 1 ? true : false;
    switch (patient.GT) {
      case 0:
        _problemText.text =
        '${getTranslated(context, 'Foot_length')}${getTranslated(context, 'Equal_length')}';
        break;
      case 1:
        _problemText.text =
        '${getTranslated(context, 'Foot_length')}${getTranslated(context, 'Unequal_length')}';
        break;
      case 2:
        _problemText.text =
        '${getTranslated(context, 'Foot_length')}${getTranslated(context, 'Equal_length')}';
        break;
      case 3:
        _problemText.text =
        '${getTranslated(context, 'Foot_length')}${getTranslated(context, 'Unequal_length')}';
        break;
    }
    switch (patient.pad) {
      case 0:
        _problemSideText.text = '';
        break;
      case 1:
        _problemSideText.text = getTranslated(context, 'left');
        break;
      case 2:
        _problemSideText.text = '';
        break;
      case 3:
        _problemSideText.text = getTranslated(context, 'right');
        break;
    }
    if (patient.pad != 2) {
      _pad = false;
    } else {
      _pad = true;
    }
    _padHigh.text = patient.padHigh;
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  Widget build(BuildContext context) {
    _patientVm = Provider.of<PatientVm>(context);
    _userCompanyInformationVm = Provider.of<UserCompanyInformationVm>(context);
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: true,
      body: SafeArea(
        child: Column(
          children: [
            Expanded(flex: 1, child: _titleBar()),
            Expanded(flex: 12, child: _mainWidget())
          ],
        ),
      ),
    );
  }

  //Title
  Widget _titleBar() {
    return PvTitleWidget(
      titleText: getTranslated(context, 'Preview'),
      leftButton: const LeftButton(),
      rightButton: Container(),
    );
  }

  //主體
  Widget _mainWidget() {
    return Container(
      decoration: Style.homeBg,
      alignment: Alignment.center,
      child: Container(
        width: MediaQuery.of(context).size.width / 1.1,
        height: MediaQuery.of(context).size.height / 1.2,
        decoration: Style.centerBG,
        padding: const EdgeInsets.only(top: 15, left: 10, right: 10, bottom: 15),
        child: buildIndexFrom(),
      ),
    );
  }

  //表單
  Widget buildIndexFrom() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Expanded(flex: 1, child: fromHeader()),
        const SizedBox(
          height: 10,
        ),
        Expanded(flex: 9, child: fromBody()),
        const SizedBox(
          height: 20,
        ),
        Expanded(
            flex: 1,
            child: InkWell(
              onTap: _nextPage,
              child: Container(
                alignment: Alignment.center,
                width: 500,
                decoration: Style.imageButtonBG,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Text(
                        getTranslated(context, 'Next_page'),
                        textScaleFactor: 1,
                        style: const TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 20),
                      ),
                    ),
                    Image.asset(
                      'assets/images/forward.png',
                      width: 30,
                    )
                  ],
                ),
              ),
            ))
      ],
    );
  }
  //下一頁
  void _nextPage(){
    editPatient();
    delegate.push(name: RouteName.pv02);
  }

  //儲存
  void editPatient() {
    Patient patient = Patient(
        name: _patientVm!.patient!.name != _patientName.text ? _patientName.text : _patientVm!.patient!.name,
        gender: _patientVm!.patient!.gender != _patientGender.text ? _patientGender.text : _patientVm!.patient!.gender,
        age: _patientVm!.patient!.age != int.parse(_patientAge.text) ? int.parse(_patientAge.text) : _patientVm!.patient!.age,
        phone: _patientVm!.patient!.phone != _patientPhone.text ? _patientPhone.text : _patientVm!.patient!.phone,
        problem: _patientVm!.patient!.problem != _patientProblem.text ? _patientProblem.text : _patientVm!.patient!.problem,
        relieveSymptomsOfDiscomfort: _relieveSymptomsOfDiscomfort ? 1 : 0,
        postureCorrection: _postureCorrection ? 1 : 0,
        informationHealthEducation: _informationHealthEducation ? 1 : 0,
        jointReduction: _jointReduction ? 1 : 0,
        coreMuscleTraining: _coreMuscleTraining ? 1 : 0,
        healthPromotion: _healthPromotion ? 1 : 0,
        GT: _patientVm!.patient!.GT,
        pad: _patientVm!.patient!.pad,
        padHigh: _patientVm!.patient!.padHigh.isEmpty ? '' : _patientVm!.patient!.padHigh,
        IC: _patientVm!.patient!.IC,
        ASIS: _patientVm!.patient!.ASIS,
        PSIS: _patientVm!.patient!.PSIS,
        flexion: _patientVm!.patient!.flexion,
        floor: _patientVm!.patient!.floor,
        gillet: _patientVm!.patient!.gillet,
        Extension: _patientVm!.patient!.Extension,
        rotation_right: _patientVm!.patient!.rotation_right,
        rotation_left: _patientVm!.patient!.rotation_left,
        side_left: _patientVm!.patient!.side_left,
        side_right: _patientVm!.patient!.side_right,
        side_knee_left: _patientVm!.patient!.side_knee_left,
        side_knee_right: _patientVm!.patient!.side_knee_right,
        supine_leg_length: _patientVm!.patient!.supine_leg_length,
        adductor: _patientVm!.patient!.adductor,
        inguinal: _patientVm!.patient!.inguinal,
        prone_leg_length: _patientVm!.patient!.prone_leg_length,
        add_ROM: _patientVm!.patient!.add_ROM,
        ext_rotator: _patientVm!.patient!.ext_rotator,
        PSIS_left: _patientVm!.patient!.PSIS_left,
        PSIS_right: _patientVm!.patient!.PSIS_right,
        ILA_left: _patientVm!.patient!.ILA_left,
        ILA_right: _patientVm!.patient!.ILA_right,
        Prop_left: _patientVm!.patient!.Prop_left,
        Prop_right: _patientVm!.patient!.Prop_right,
        lumbar_left: _patientVm!.patient!.lumbar_left,
        lumbar_right: _patientVm!.patient!.lumbar_right,
        supine_ilium_leg_length: _patientVm!.patient!.supine_ilium_leg_length,
        Long_sit: _patientVm!.patient!.Long_sit,
        SLR: _patientVm!.patient!.SLR,
        Patrick: _patientVm!.patient!.Patrick,
        Knee_flexor: _patientVm!.patient!.Knee_flexor,
        ASIS_umbilical: _patientVm!.patient!.ASIS_umbilical,
        IC_recheck: _patientVm!.patient!.IC_recheck,
        flexion_recheck: _patientVm!.patient!.flexion_recheck,
        floor_recheck: _patientVm!.patient!.floor_recheck,
        gillet_recheck: _patientVm!.patient!.gillet_recheck,
        extension_recheck: _patientVm!.patient!.extension_recheck,
        rotation_left_recheck: _patientVm!.patient!.rotation_left_recheck,
        rotation_right_recheck: _patientVm!.patient!.rotation_right_recheck,
        side_left_recheck: _patientVm!.patient!.side_left_recheck,
        side_right_recheck: _patientVm!.patient!.side_right_recheck,
        side_knee_left_recheck: _patientVm!.patient!.side_knee_left_recheck,
        side_knee_right_recheck: _patientVm!.patient!.side_knee_right_recheck,
    );
    _patientVm!.editPatient(patient);
  }

  //表單頭
  Widget fromHeader() {
    return Container(
      width: MediaQuery.of(context).size.width,
      decoration: const BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/images/record_sheet_title.png'),
              fit: BoxFit.fill)),
      alignment: Alignment.center,
      child: FittedBox(
        fit: BoxFit.scaleDown,
        child: Text(
          getTranslated(
              context, 'Guangyin_Method_Evaluation_and_Adjustment_Record_Sheet'),
          textScaleFactor: 1,
          style: const TextStyle(
              color: Colors.white, fontSize: 23, fontWeight: FontWeight.bold),
        ),
      ),
    );
  }

  //表單身
  Widget fromBody() {
    return ScrollConfiguration(
        behavior: NoShadowScrollBehavior(),
        child: SingleChildScrollView(
          child: Column(
            children: [
              //公司資訊
              _companyInfoWidget(),
              //診察與治療日期
              _dateWidget(),
              //病人姓名
              _patientNameWidget(),
              //病人性別/年齡
              _patientGenderAndAgeWidget(),
              //病人電話
              _patientPhoneWidget(),
              //病人問題
              _patientProblemWidget(),
              //病人目標
              _patientTheGoalWidget(),
              //長短腳問題
              _longAndShortFeetWidget(),
              const SizedBox(
                height: 15,
              ),
              //標語
              Container(
                height: 50,
                alignment: Alignment.centerLeft,
                decoration: BoxDecoration(color: HexColor.fromHex('#609AD1')),
                padding: const EdgeInsets.only(left: 8),
                child: FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    getTranslated(context,
                        'All_rights_reserved_by_the_light_silver_technique_piracy_must_be_punished'),
                    textScaleFactor: 1,
                    style: const TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  ),
                ),
              )
            ],
          ),
        ));
  }

  //公司資訊
  Widget _companyInfoWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 8),
      decoration: _inputBg,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            margin: const EdgeInsets.only(left: 8),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    _companyName,
                    textScaleFactor: 1,
                  ),
                ),
                FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    _companyPhone,
                    textScaleFactor: 1,
                  ),
                ),
                FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    _companyAddress,
                    textScaleFactor: 1,
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 150,
            width: 150,
            child: Image.network(_companyLOGO),
          )
        ],
      ),
    );
  }

  //診察與治療日期
  Widget _dateWidget() {
    return Container();
  }

  //病人姓名
  Widget _patientNameWidget() {
    return Container(
        margin: const EdgeInsets.only(top: 8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                getTranslated(context, 'Name'),
                textScaleFactor: 1,
                style: const TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            TextField(
              style: const TextStyle(color: Colors.black),
              decoration: InputDecoration(
                  errorText: null,
                  contentPadding: const EdgeInsets.all(5.0),
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey[400]!),
                      borderRadius: BorderRadius.circular(10.0)),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                  filled: true,
                  fillColor: Colors.white,
                  hintText: getTranslated(context, 'Please_type_in_your_name'),
                  hintStyle: const TextStyle(color: Colors.grey)),
              keyboardType: TextInputType.text,
              textInputAction: TextInputAction.next,
              controller: _patientName,
            ),
          ],
        ));
  }

  //病人性別/年齡
  Widget _patientGenderAndAgeWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 8),
      child: Row(
        children: [
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              FittedBox(
                fit: BoxFit.scaleDown,
                child: Text(
                  getTranslated(context, 'Gender'),
                  textScaleFactor: 1,
                  style: const TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
              _buildGender(),
            ],
          )),
          const SizedBox(
            width: 10,
          ),
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              FittedBox(
                fit: BoxFit.scaleDown,
                child: Text(
                  getTranslated(context, 'Age'),
                  textScaleFactor: 1,
                  style: const TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
              _buildAge(),
            ],
          )),
        ],
      ),
    );
  }

  //性別
  Widget _buildGender() {
    return TextField(
      readOnly: true,
      style: const TextStyle(color: Colors.black),
      decoration: InputDecoration(
        suffixIcon: PopupMenuButton<String>(
          offset: const Offset(-100, 70),
          icon: const Icon(
            Icons.arrow_drop_down,
            size: 35,
          ),
          onSelected: (String value) {
            _patientGender.text = value;
          },
          itemBuilder: (BuildContext context) {
            return items.map<PopupMenuItem<String>>((String value) {
              return PopupMenuItem(
                value: value,
                child: FittedBox(fit: BoxFit.scaleDown,child: Text(value)),
              );
            }).toList();
          },
        ),
        contentPadding: const EdgeInsets.all(5),
        enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey[400]!),
            borderRadius: BorderRadius.circular(10)),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
        filled: true,
        fillColor: Colors.white,
        hintText: getTranslated(context, 'Please_enter_gender'),
        hintStyle: TextStyle(color: HexColor.fromHex('#919191')),
      ),
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _patientGender,
    );
  }

  //年齡
  Widget _buildAge() {
    return TextField(
      style: const TextStyle(color: Colors.black),
      decoration: InputDecoration(
          errorText: null,
          contentPadding: const EdgeInsets.all(5.0),
          enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.grey[400]!),
              borderRadius: BorderRadius.circular(10)),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
          filled: true,
          fillColor: Colors.white,
          hintText: getTranslated(context, 'Please_enter_age'),
          hintStyle: const TextStyle(color: Colors.grey)),
      keyboardType: TextInputType.number,
      textInputAction: TextInputAction.done,
      controller: _patientAge,
    );
  }

  //病人電話
  Widget _patientPhoneWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          FittedBox(
            fit: BoxFit.scaleDown,
            child: Text(
              getTranslated(context, 'Phone'),
              textScaleFactor: 1,
              style: const TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          TextField(
            style: const TextStyle(color: Colors.black),
            decoration: InputDecoration(
                errorText: null,
                contentPadding: const EdgeInsets.all(5.0),
                enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey[400]!),
                    borderRadius: BorderRadius.circular(10.0)),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0)),
                filled: true,
                fillColor: Colors.white,
                hintText:
                    getTranslated(context, 'Please_enter_your_phone_number'),
                hintStyle: const TextStyle(color: Colors.grey)),
            keyboardType: TextInputType.phone,
            textInputAction: TextInputAction.next,
            controller: _patientPhone,
          ),
        ],
      ),
    );
  }

  //病人問題
  Widget _patientProblemWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 8),
      child: Column(
        children: [
          Container(
            height: 50,
            alignment: Alignment.center,
            decoration: BoxDecoration(
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(10),
                    topRight: Radius.circular(10)),
                color: HexColor.fromHex('#096BC7')),
            child: FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                getTranslated(context, 'Problem'),
                textScaleFactor: 1,
                style:
                    const TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
              ),
            ),
          ),
          TextField(
            maxLines: 5,
            maxLength: 500,
            decoration: InputDecoration(
                counterText: '',
                errorText: null,
                contentPadding: const EdgeInsets.all(5.0),
                enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey[400]!),
                    borderRadius: const BorderRadius.only(
                        bottomRight: Radius.circular(10),
                        bottomLeft: Radius.circular(10))),
                border: const OutlineInputBorder(
                    borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(10),
                        bottomLeft: Radius.circular(10))),
                filled: true,
                fillColor: Colors.white,
                hintText: '',
                hintStyle: const TextStyle(color: Colors.grey)),
            keyboardType: TextInputType.text,
            textInputAction: TextInputAction.done,
            controller: _patientProblem,
          ),
        ],
      ),
    );
  }

  //病人目標
  Widget _patientTheGoalWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 8),
      child: Column(
        children: [
          Container(
            height: 50,
            alignment: Alignment.center,
            decoration: BoxDecoration(
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(10),
                    topRight: Radius.circular(10)),
                color: HexColor.fromHex('#096BC7')),
            child: FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                getTranslated(context, 'The_goal'),
                textScaleFactor: 1,
                style:
                    const TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
              ),
            ),
          ),
          Container(
            decoration: BoxDecoration(
                borderRadius: const BorderRadius.only(
                    bottomRight: Radius.circular(10),
                    bottomLeft: Radius.circular(10)),
                border: Border.all(color: Colors.grey, width: 1.5)),
            child: Column(
              children: [
                //減緩不適症狀
                Row(
                  children: [
                    Transform.scale(
                      scale: 1.5,
                      child: Checkbox(
                        value: _relieveSymptomsOfDiscomfort,
                        onChanged: (value) {
                          setState(() {
                            _relieveSymptomsOfDiscomfort = value!;
                          });
                        },
                      ),
                    ),
                    FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Text(
                        getTranslated(context, 'Relieve_symptoms_of_discomfort'),
                        textScaleFactor: 1,
                        style: const TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                    )
                  ],
                ),
                //姿勢矯正
                Row(
                  children: [
                    Transform.scale(
                      scale: 1.5,
                      child: Checkbox(
                        value: _postureCorrection,
                        onChanged: (value) {
                          setState(() {
                            _postureCorrection = value!;
                          });
                        },
                      ),
                    ),
                    FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Text(
                        getTranslated(context, 'Posture_correction'),
                        textScaleFactor: 1,
                        style: const TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                    )
                  ],
                ),
                //資訊衛教
                Row(
                  children: [
                    Transform.scale(
                      scale: 1.5,
                      child: Checkbox(
                        value: _informationHealthEducation,
                        onChanged: (value) {
                          setState(() {
                            _informationHealthEducation = value!;
                          });
                        },
                      ),
                    ),
                    FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Text(
                        getTranslated(context, 'Information_Health_Education'),
                        textScaleFactor: 1,
                        style: const TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                    )
                  ],
                ),
                //關節復位
                Row(
                  children: [
                    Transform.scale(
                      scale: 1.5,
                      child: Checkbox(
                        value: _jointReduction,
                        onChanged: (value) {
                          setState(() {
                            _jointReduction = value!;
                          });
                        },
                      ),
                    ),
                    FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Text(
                        getTranslated(context, 'Joint_reduction'),
                        textScaleFactor: 1,
                        style: const TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                    )
                  ],
                ),
                //核心肌群訓練
                Row(
                  children: [
                    Transform.scale(
                      scale: 1.5,
                      child: Checkbox(
                        value: _coreMuscleTraining,
                        onChanged: (value) {
                          setState(() {
                            _coreMuscleTraining = value!;
                          });
                        },
                      ),
                    ),
                    FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Text(
                        getTranslated(context, 'Core_muscle_training'),
                        textScaleFactor: 1,
                        style: const TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                    )
                  ],
                ),
                //健康促進
                Row(
                  children: [
                    Transform.scale(
                      scale: 1.5,
                      child: Checkbox(
                        value: _healthPromotion,
                        onChanged: (value) {
                          setState(() {
                            _healthPromotion = value!;
                          });
                        },
                      ),
                    ),
                    FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Text(
                        getTranslated(context, 'Health_promotion'),
                        textScaleFactor: 1,
                        style: const TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                    )
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  //長短腳問題
  Widget _longAndShortFeetWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 8),
      child: Column(
        children: [
          Container(
            height: 50,
            alignment: Alignment.center,
            decoration: BoxDecoration(
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(10),
                    topRight: Radius.circular(10)),
                color: HexColor.fromHex('#096BC7')),
            child: FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                getTranslated(context,
                    'List_of_long_and_short_feet_problems_and_adjustment_records'),
                textScaleFactor: 1,
                style:
                    const TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
              ),
            ),
          ),
          Column(
            children: [
              Container(
                alignment: Alignment.centerLeft,
                child: FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    getTranslated(context, 'Problem'),
                    textScaleFactor: 1,
                    style: const TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              TextField(
                readOnly: true,
                style: const TextStyle(color: Colors.black),
                decoration: InputDecoration(
                    errorText: null,
                    contentPadding: const EdgeInsets.all(5.0),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey[400]!),
                        borderRadius: BorderRadius.circular(10.0)),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0)),
                    filled: true,
                    fillColor: Colors.white,
                    hintText: '',
                    hintStyle: const TextStyle(color: Colors.grey)),
                keyboardType: TextInputType.text,
                textInputAction: TextInputAction.next,
                controller: _problemText,
              ),
              Container(
                alignment: Alignment.centerLeft,
                child: FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    getTranslated(context, 'Characterization'),
                    textScaleFactor: 1,
                    style: const TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              TextField(
                readOnly: true,
                style: const TextStyle(color: Colors.black),
                decoration: InputDecoration(
                    errorText: null,
                    contentPadding: const EdgeInsets.all(5.0),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey[400]!),
                        borderRadius: BorderRadius.circular(10.0)),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0)),
                    filled: true,
                    fillColor: Colors.white,
                    hintText: '',
                    hintStyle: const TextStyle(color: Colors.grey)),
                keyboardType: TextInputType.text,
                textInputAction: TextInputAction.next,
                controller: _problemSideText,
              ),
              Container(
                alignment: Alignment.centerLeft,
                child: FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    getTranslated(context, 'result'),
                    textScaleFactor: 1,
                    style: const TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              Row(
                children: [
                  Transform.scale(
                    scale: 1.5,
                    child: Checkbox(value: _pad, onChanged: null),
                  ),
                  FittedBox(
                    fit: BoxFit.scaleDown,
                    child: Text(
                      getTranslated(context,
                          'It_is_recommended_to_wear_purchase_insoles'),
                      textScaleFactor: 1,
                      style: const TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                  SizedBox(
                    width: 100,
                    child: TextField(
                      readOnly: true,
                      style: const TextStyle(color: Colors.black),
                      decoration: InputDecoration(
                        suffixIcon: PopupMenuButton<String>(
                          offset: const Offset(-100, 70),
                          icon: const Icon(
                            Icons.arrow_drop_down,
                            size: 25,
                          ),
                          onSelected: null,
                          itemBuilder: (BuildContext context) {
                            return [];
                          },
                        ),
                        contentPadding: const EdgeInsets.all(5),
                        enabledBorder: const OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.black)),
                        filled: true,
                        fillColor: Colors.white,
                      ),
                      keyboardType: TextInputType.text,
                      textInputAction: TextInputAction.done,
                      controller: _padHigh,
                    ),
                  ),
                  FittedBox(
                    fit: BoxFit.scaleDown,
                    child: Text(
                      getTranslated(context, 'Cm'),
                      textScaleFactor: 1,
                      style: const TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
        ],
      ),
    );
  }
}
