import 'dart:async';

import 'package:flutter/material.dart';
import 'package:light_silver/language/language_constants.dart';
import 'package:light_silver/utils/utils.dart';

import '../widgets/left_button.dart';

class ForgetPasswordRoutes extends StatefulWidget {
  @override
  State createState() => _ForgetPasswordRoutesState();
}

class _ForgetPasswordRoutesState extends State<ForgetPasswordRoutes> {
  final TextEditingController _account = TextEditingController();
  final TextEditingController _validate = TextEditingController();
  final bool _isClickSms = false;
  Timer? _timer;
  int _countdownTime = 0;
  bool _isReciprocal = false;

  //漸層背景
  final _homeBg = const BoxDecoration(
      image: DecorationImage(
          image: AssetImage('assets/images/home_bg.png'), fit: BoxFit.fill));

  final _titleBarBG = BoxDecoration(
      gradient: LinearGradient(
          colors: [HexColor.fromHex('#0067B4'), HexColor.fromHex('#003457')],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter));

  //中間背景
  final _centerBG = const BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.all(Radius.circular(10)),
      boxShadow: [
        BoxShadow(
            color: Colors.black54,
            offset: Offset(5.0, 5.0),
            blurRadius: 10.0,
            spreadRadius: 2.0)
      ]);
  final _sendBG = BoxDecoration(
      borderRadius: BorderRadius.circular(10),
      gradient: LinearGradient(colors: [
        HexColor.fromHex('#0067B4'),
        HexColor.fromHex('#003457'),
      ], begin: Alignment.topCenter, end: Alignment.bottomCenter));

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: false,
      body: GestureDetector(
        onTap: _clickOnTap,
        child: SafeArea(
          child: SizedBox(
            height: MediaQuery.of(context).size.height,
            child: Column(
              children: [
                Expanded(flex: 1, child: _titleBar()),
                Expanded(flex: 10, child: _bodyWidget()),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _clickOnTap() {
    debugPrint('點擊其他區域');
    FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus && currentFocus.focusedChild != null) {
      FocusManager.instance.primaryFocus!.unfocus();
    }
  }

  //最上層標題與按鈕
  Widget _titleBar() {
    return Container(
      decoration: _titleBarBG,
      child: Row(
        children: [
          const Expanded(flex: 1, child: LeftButton()),
          Expanded(
              flex: 5,
              child: Container(
                alignment: Alignment.center,
                child: FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    getTranslated(context, 'Forgot_password'),
                    textScaleFactor: 1,
                    style: const TextStyle(fontSize: 25, color: Colors.white),
                  ),
                ),
              )),
          const Expanded(flex: 1, child: SizedBox())
        ],
      ),
    );
  }

  //主體
  Widget _bodyWidget() {
    return Container(
      decoration: _homeBg,
      alignment: Alignment.center,
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        width: MediaQuery.of(context).size.width / 1.1,
        height: MediaQuery.of(context).size.height / 1.3,
        decoration: _centerBG,
        child: Column(
          children: [
            const SizedBox(
              height: 30,
            ),
            Container(
              alignment: Alignment.center,
              child: Image(
                image: const AssetImage('assets/images/logo_name_blue.png'),
                width: MediaQuery.of(context).size.width / 2,
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            Container(
              alignment: Alignment.centerLeft,
              child: FittedBox(
                fit: BoxFit.scaleDown,
                child: Text(
                  getTranslated(context, 'Account'),
                  textScaleFactor: 1,
                  style: TextStyle(
                      fontSize: 20,
                      color: HexColor.fromHex('#1E2C39'),
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
            _buildAccount(),
            const SizedBox(
              height: 20,
            ),
            Container(
              alignment: Alignment.centerLeft,
              child: FittedBox(
                fit: BoxFit.scaleDown,
                child: Text(
                  getTranslated(context, 'Verification_code'),
                  textScaleFactor: 1,
                  style: TextStyle(
                      fontSize: 20,
                      color: HexColor.fromHex('#1E2C39'),
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
            _buildValidate(),
            const SizedBox(
              height: 50,
            ),
            Container(
              alignment: Alignment.center,
              child: ButtonTheme(
                height: 110,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      foregroundColor: Colors.white,
                      backgroundColor: Colors.transparent,
                      shape: const RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.all(Radius.circular(10.0))),
                      padding: const EdgeInsets.all(0.0)),
                  onPressed: _sendOnTap,
                  child: Container(
                      height: 50,
                      width: 750,
                      alignment: Alignment.center,
                      decoration: _sendBG,
                      child: FittedBox(
                        fit: BoxFit.scaleDown,
                        child: Text(
                          getTranslated(context, 'Send'),
                          textScaleFactor: 1,
                          style: const TextStyle(
                              color: Colors.white, fontSize: 23),
                        ),
                      )),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  void _sendOnTap() {
    debugPrint('送出');
  }

  //帳號輸入
  Widget _buildAccount() {
    return TextField(
      style: const TextStyle(color: Colors.black),
      decoration: InputDecoration(
          prefixIcon: Container(
            padding: const EdgeInsets.only(left: 8.0),
            child: Icon(
              Icons.person,
              color: HexColor.fromHex('#1E2C39'),
              size: 25,
            ),
          ),
          errorText: null,
          contentPadding: const EdgeInsets.all(5.0),
          enabledBorder: OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.black),
              borderRadius: BorderRadius.circular(10.0)),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10.0)),
          filled: true,
          fillColor: Colors.white,
          hintText: getTranslated(context,
              'Please_enter_your_mobile_phone_number_or_company_system'),
          hintStyle: const TextStyle(color: Colors.grey)),
      keyboardType: TextInputType.number,
      textInputAction: TextInputAction.next,
      controller: _account,
    );
  }

  //驗證碼輸入
  Widget _buildValidate() {
    return SizedBox(
      height: 50,
      child: Row(
        children: [
          Expanded(
            child:TextField(
              style: const TextStyle(color: Colors.black),
              decoration: InputDecoration(
                  prefixIcon: Container(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Image.asset(
                      'assets/images/gpp_good.png',
                      color: HexColor.fromHex('#1E2C39'),
                      width: 25,
                    ),
                  ),
                  errorText: null,
                  contentPadding: const EdgeInsets.all(5.0),
                  enabledBorder: OutlineInputBorder(
                      borderSide: const BorderSide(color: Colors.black),
                      borderRadius: BorderRadius.circular(10.0)),
                  border: OutlineInputBorder(borderRadius: BorderRadius.circular(10.0)),
                  filled: true,
                  fillColor: Colors.white,
                  hintText: getTranslated(context, 'Please_enter_verification_code'),
                  hintStyle: const TextStyle(color: Colors.grey)),
              keyboardType: TextInputType.number,
              textInputAction: TextInputAction.done,
              controller: _validate,
            ),
          ),
          const SizedBox(
            width: 10,
          ),
          !_isClickSms
              ? InkWell(
                  onTap: _sendSms,
                  child: ButtonTheme(
                    height: 150,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.red,
                          shape: const RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5.0))),
                          padding: const EdgeInsets.all(10.0)),
                      onPressed: !_isReciprocal
                          ? () {
                              _reciprocal();
                              _sendSms();
                            }
                          : null,
                      child: FittedBox(
                        fit: BoxFit.scaleDown,
                        child: Text(
                            _countdownTime > 0
                                ? '$_countdownTime後重新獲取'
                                : '發送驗證碼',
                            textScaleFactor: 1,
                            style: const TextStyle(
                                fontSize: 17, color: Colors.white)),
                      ),
                    ),
                  ),
                )
              : Container()
        ],
      ),
    );
  }

  void _sendSms() {}

  //倒數
  void _reciprocal() {
    if (_countdownTime == 0 && !_isReciprocal) {
      setState(() {
        _countdownTime = 60;
        _isReciprocal = true;
      });
      startCountdownTimer();
    }
  }

  void startCountdownTimer() {
    const oneSec = Duration(seconds: 1);

    callback(timer) => {
          if (mounted)
            {
              setState(() {
                if (_countdownTime < 1) {
                  _timer!.cancel();
                  _isReciprocal = false;
                } else {
                  _countdownTime = _countdownTime - 1;
                }
              })
            }
        };
    _timer = Timer.periodic(oneSec, callback);
  }
}
