import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:light_silver/database/database_helper.dart';
import 'package:light_silver/language/language_constants.dart';
import 'package:light_silver/models/company_info_model.dart';
import 'package:light_silver/models/member_model.dart';
import 'package:light_silver/models/operation_item_model.dart';
import 'package:light_silver/models/patient.dart';
import 'package:light_silver/models/user_company_information_model.dart';
import 'package:light_silver/routes/route_name.dart';
import 'package:light_silver/utils/api.dart';
import 'package:light_silver/utils/shared_preference_util.dart';
import 'package:light_silver/utils/utils.dart';
import 'package:light_silver/view_model/member_vm.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:provider/provider.dart';

import '../app.dart';
import '../view_model/auth_vm.dart';
import '../view_model/company_info_vm.dart';
import '../view_model/operation_item_vm.dart';
import '../view_model/patient_vm.dart';
import '../view_model/user_company_information_vm.dart';

class HomeRoutes extends StatefulWidget {
  @override
  State createState() => _HomeRoutesState();
}

class _HomeRoutesState extends State<HomeRoutes> with WidgetsBindingObserver {
  AuthVm? _authVm;
  PatientVm? _patientVm;
  MemberVm? _memberVm;
  CompanyInfoVm? _companyInfoVm;
  UserCompanyInformationVm? _userCompanyInformationVm;
  OperationItemVm? _operationItemVm;

  //左側抽屜Key
  final GlobalKey<ScaffoldState> _globalKey = GlobalKey();

  late DatabaseHelper dbHelper;
  late SharedPreferenceUtil _sharedPreferenceUtil;
  /*Patient? patient;*/

  //網路狀態
  final String _connectionStatus = 'Unknown';
  final Connectivity _connectivity = Connectivity();
  late StreamSubscription<ConnectivityResult> _connectivitySubscription;
  bool _isNetWorkDialog = false;

  //大頭照取得
  bool isAvatar = false;

  //每個療程的鎖
  bool _isMedicalHistory = true;
  bool _isStandingPosture1 = true;
  bool _isLieOnYourBack1 = true;
  bool _isProne1 = false;
  bool _isProne2 = false;
  bool _isProne3 = false;
  bool _isLieDown = false;
  bool _isLieOnYourBack2 = false;
  bool _isStandingPosture = false;

  //每個選項的狀態
  /*bool _isMedicalHistoryStatus = false;
  bool _isStandingPosture1Status = false;
  bool _isLieOnYourBack1Status = false;
  bool _isProne1Status = false;
  bool _isProne2Status = false;
  bool _isProne3Status = false;
  bool _isLieDownStatus = false;
  bool _isLieOnYourBack2Status = false;
  bool _isStandingPosture2Status = false;*/
  int isApprove = 0;

  final _homeBG = const BoxDecoration(
      image: DecorationImage(
          image: AssetImage('assets/images/home_bg.png'), fit: BoxFit.fill));

  final _titleBarBG = BoxDecoration(
      gradient: LinearGradient(
          colors: [HexColor.fromHex('#0067B4'), HexColor.fromHex('#003457')],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter));

  //病史背景
  final _medicalHistoryBG = BoxDecoration(
      gradient: LinearGradient(colors: [
    HexColor.fromHex('#CB2020'),
    HexColor.fromHex('#DC6363'),
    HexColor.fromHex('#CB2020'),
  ], begin: Alignment.topCenter, end: Alignment.bottomCenter));

  //站姿背景
  final _standingPostureBG = BoxDecoration(
      gradient: LinearGradient(
    colors: [
      HexColor.fromHex('#3D9208'),
      HexColor.fromHex('#6DAC55'),
      HexColor.fromHex('#1C7B00'),
    ],
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
  ));

  //仰躺背景
  final _lieOnYourBackBG1 = BoxDecoration(
      gradient: LinearGradient(colors: [
    HexColor.fromHex('#085CAF'),
    HexColor.fromHex('#618FBC'),
    HexColor.fromHex('#0057AE'),
  ], begin: Alignment.topCenter, end: Alignment.bottomCenter));

  //俯臥背景
  final _proneBG = BoxDecoration(
      gradient: LinearGradient(colors: [
    HexColor.fromHex('#1DAE85'),
    HexColor.fromHex('#71C2AB'),
    HexColor.fromHex('#1DAE85'),
  ], begin: Alignment.topCenter, end: Alignment.bottomCenter));

  //側躺背景
  final _lieDownBG = BoxDecoration(
      gradient: LinearGradient(colors: [
    HexColor.fromHex('#4809AB'),
    HexColor.fromHex('#7E66AA'),
    HexColor.fromHex('#4B18A3'),
  ], begin: Alignment.topCenter, end: Alignment.bottomCenter));

  //仰躺背景橘
  final _lieOnYourBackBG2 = BoxDecoration(
      gradient: LinearGradient(colors: [
    HexColor.fromHex('#D45B11'),
    HexColor.fromHex('#DC9F79'),
    HexColor.fromHex('#DE530B'),
  ], begin: Alignment.topCenter, end: Alignment.bottomCenter));

  //重新整理背景
  final _retestBG = BoxDecoration(
      borderRadius: const BorderRadius.all(Radius.circular(10.0)),
      gradient: LinearGradient(colors: [
        HexColor.fromHex('#0067B4'),
        HexColor.fromHex('#003457'),
      ], begin: Alignment.topCenter, end: Alignment.bottomCenter));

  PackageInfo _packageInfo = PackageInfo(
      version: 'Unknown', appName: '', packageName: '', buildNumber: '');

  //取得版本資訊
  Future<void> _initPackageInfo() async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    setState(() {
      _packageInfo = info;
    });
  }

  _onLayoutDone(_) {
    _getCompanyInfo();
    _getOperationItems();
    _getUser();
    if (!_authVm!.isLogin) {
      _autoLogin();
    }
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);

    _authVm!.addListener(_listener);

    delegate.addListener(_routeListener);

    _patientVm!.getPatient();
  }

  _listener() {
    if (_authVm!.isLogin) {
      _memberLock();
    }
  }

  _routeListener() {
    //取得病人
    _getUser();
  }

  //取得資料
  void _getUser() {
    /*_sharedPreferenceUtil.getPatientName().then((sp) => {
          if (sp.isNotEmpty)
            {
              dbHelper.getPatient(sp).then((value) => {_dataInit(value!)})
            }
        });*/
  }

  //自動登入
  void _autoLogin() {
    _sharedPreferenceUtil.getAccount().then((account) => {
          _sharedPreferenceUtil.getPassword().then((password) => {
                if (account.isNotEmpty && password.isNotEmpty)
                  {_login(account, password)}
              })
        });
  }

  void _login(String account, String password) async {
    try {
      dynamic response = await httpUtils.post(
        postAuth,
        data: {
          'account': account,
          'password': password,
        },
      );
      log('登入Data: $response');
      accessToken = response['token']['access_token'];
      _getMember(response['id']);
    } on DioError catch (e) {
      EasyLoading.dismiss();
      EasyLoading.showError(e.message);
      debugPrint('登入Error: ${e.message}');
    }
  }

  /*void _dataInit(Patient _patient) {
    patient = _patient;
    log('取得 首頁: $patient');
    //病例打勾判斷
    if (patient!.name.isNotEmpty &&
        patient!.gender.isNotEmpty &&
        !patient!.age.isNaN &&
        patient!.phone.isNotEmpty &&
        patient!.problem.isNotEmpty) {
      debugPrint('病例打勾');
      setState(() {
        _isMedicalHistoryStatus = true;
      });
    }
    //站姿打勾判斷
    if (patient!.GT != 0 &&
        patient!.IC != 0 &&
        patient!.ASIS != 0 &&
        patient!.PSIS != 0 &&
        !patient!.flexion.isNaN &&
        patient!.gillet != 0 &&
        !patient!.Extension.isNaN &&
        !patient!.rotation_left.isNaN &&
        !patient!.rotation_right.isNaN &&
        !patient!.side_left.isNaN &&
        !patient!.side_right.isNaN) {
      setState(() {
        _isStandingPosture1Status = true;
      });
    }
    //仰躺打勾判斷
    if (patient!.supine_leg_length != 0 &&
        patient!.adductor != 0 &&
        patient!.inguinal != 0) {
      setState(() {
        _isLieOnYourBack1Status = true;
      });
    }
    //俯臥髂骨打勾判斷
    if (patient!.gillet != 0 &&
        patient!.IC != 0 &&
        patient!.ASIS != 0 &&
        patient!.PSIS != 0) {
      _isProne1Status = true;
    }
    //俯臥_薦椎 打勾判斷
    if (patient!.prone_leg_length != 0 &&
        patient!.add_ROM != 0 &&
        patient!.ext_rotator != 0 &&
        patient!.PSIS_left != 0 &&
        patient!.PSIS_right != 0 &&
        patient!.ILA_left != 0 &&
        patient!.ILA_right != 0) {
      _isProne2Status = true;
    }
    //俯臥_腰椎 打勾判斷
    if (patient!.lumbar_left != 0 && patient!.lumbar_right != 0) {
      _isProne3Status = true;
    }
    //側躺
    if (patient!.lumbar_left != 0 && patient!.lumbar_right != 0) {
      _isLieDownStatus = true;
    }
    //仰躺
    if (patient!.supine_ilium_leg_length != 0 &&
        patient!.SLR != 0 &&
        patient!.Patrick != 0 &&
        patient!.Knee_flexor != 0 &&
        patient!.ASIS_umbilical != 0) {
      _isLieOnYourBack2Status = true;
    }

    //站姿
    if (patient!.extension_recheck != 3 &&
        patient!.rotation_left_recheck != 3 &&
        patient!.rotation_right_recheck != 3 &&
        patient!.side_left_recheck != 3 &&
        patient!.side_right_recheck != 3 &&
        patient!.side_knee_left_recheck != '100' &&
        patient!.side_knee_right_recheck != '100') {
      _isStandingPosture2Status = true;
    }
    if (_isStandingPosture1Status &&
        _isLieOnYourBack1Status &&
        _isProne1Status &&
        _isProne2Status &&
        _isProne3Status &&
        _isLieDownStatus &&
        _isLieOnYourBack2Status &&
        _isStandingPosture2Status) {
      setState(() {
        isPreview = true;
      });
    }
  }*/

  @override
  void initState() {
    super.initState();
    _initPackageInfo();
    _sharedPreferenceUtil = SharedPreferenceUtil();
    dbHelper = DatabaseHelper();
    if (Platform.isIOS) {
      SystemChrome.setEnabledSystemUIMode(SystemUiMode.edgeToEdge,
          overlays: []);
      SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
        statusBarColor: Colors.black,
        systemNavigationBarColor: Colors.black,
        systemNavigationBarIconBrightness: Brightness.light,
        statusBarIconBrightness: Brightness.light,
        statusBarBrightness: Brightness.light,
      ));
    }
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  Future<void> initConnectivity() async {
    ConnectivityResult result = ConnectivityResult.none;
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      debugPrint(e.toString());
    }
    if (!mounted) {
      return Future.value(null);
    }
    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    debugPrint(result.toString());
    switch (result) {
      case ConnectivityResult.wifi:
      case ConnectivityResult.mobile:
        if (_isNetWorkDialog) {
          _isNetWorkDialog = false;
          delegate.popRoute();
        }
        break;
      case ConnectivityResult.none:
        showDialogNetWork();
        break;
      default:
        showDialogNetWork();
        break;
    }
  }

  @override
  void dispose() {
    super.dispose();
    delegate.removeListener(_routeListener);
    _authVm!.removeListener(_listener);
    _connectivitySubscription.cancel();
  }

  @override
  Widget build(BuildContext context) {
    _authVm = Provider.of<AuthVm>(context);
    _patientVm = Provider.of<PatientVm>(context);
    _memberVm = Provider.of<MemberVm>(context);
    _companyInfoVm = Provider.of<CompanyInfoVm>(context);
    _userCompanyInformationVm = Provider.of<UserCompanyInformationVm>(context);
    _operationItemVm = Provider.of<OperationItemVm>(context);
    return Scaffold(
      key: _globalKey,
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Column(
          children: [
            Expanded(flex: 1, child: _titleBar()),
            Expanded(flex: 10, child: _bodyWidget()),
          ],
        ),
      ),
      drawer: _drawer(),
    );
  }

  //最上層標題與左側選單按鈕
  Widget _titleBar() {
    return Container(
      decoration: _titleBarBG,
      child: Row(
        children: [
          Expanded(flex: 1, child: _leftTopButton()),
          const Expanded(
              flex: 5,
              child: Image(
                image: AssetImage('assets/images/logo_name_white.png'),
                height: 35,
              )),
          const Expanded(flex: 1, child: SizedBox())
        ],
      ),
    );
  }

  //左側選單按鈕
  Widget _leftTopButton() {
    return InkWell(
      onTap: () {
        debugPrint('開啟抽屜');
        _globalKey.currentState!.openDrawer();
      },
      child: const Image(
        image: AssetImage('assets/images/menu.png'),
        width: 25,
        height: 25,
        color: Colors.white,
      ),
    );
  }

  //主體
  Widget _bodyWidget() {
    return Container(
      decoration: _homeBG,
      child: Column(
        children: [
          //登入/註冊
          Expanded(flex: 1, child: _singInAndRegister()),
          //病史
          Expanded(flex: 1, child: _medicalHistoryWidget()),
          //站姿-髂骨與腰椎問題評估
          Expanded(flex: 1, child: _standingPosture1Widget()),
          //仰躺-恥骨問題評估與處理
          Expanded(flex: 1, child: _lieOnYourBack1Widget()),
          //俯臥-髂骨上移問題處理
          Expanded(flex: 1, child: _prone1Widget()),
          //俯臥-薦椎問題評估與處理
          Expanded(flex: 1, child: _prone2Widget()),
          //俯臥-腰椎小面關節問題評估
          Expanded(flex: 1, child: _prone3Widget()),
          //側躺-腰椎小面關節問題處理
          Expanded(flex: 1, child: _lieDownWidget()),
          //仰躺-髂骨問題評估與處理
          Expanded(flex: 1, child: _lieOnYourBack2Widget()),
          //站姿-髂骨與腰椎問題再評估
          Expanded(flex: 1, child: _standingPosture2Widget()),
          //重新測試
          Expanded(flex: 1, child: _retestWidget()),
        ],
      ),
    );
  }

  //進入會員中心
  void _toMemberCentre() {
    delegate.push(name: RouteName.memberCentre);
  }

  //登入與註冊
  Widget _singInAndRegister() {
    return Container(
      padding: const EdgeInsets.all(8),
      child: Row(
        children: [
          Expanded(
            flex: 1,
            child: _avatarImage(),
          ),
          Expanded(
              flex: 3,
              child: Consumer<AuthVm>(
                builder: (context, auth, _) {
                  return InkWell(
                    onTap:
                        !auth.isLogin ? _loginOrRegisterOnTap : _toMemberCentre,
                    child: FittedBox(
                      fit: BoxFit.scaleDown,
                      alignment: Alignment.centerLeft,
                      child: Consumer<MemberVm>(
                        builder: (context, vm, _) {
                          return Text(
                            auth.isLogin
                                ? vm.memberModel!.name
                                : '${getTranslated(context, 'Sing_in')}/${getTranslated(context, 'Register')}',
                            textScaleFactor: 1,
                            style: const TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold),
                          );
                        },
                      ),
                    ),
                  );
                },
              )),
          Expanded(
              flex: 2,
              child: Consumer2<AuthVm, PatientVm>(
                builder: (context, auth, vm, _) {
                  return ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        foregroundColor: Colors.white,
                        backgroundColor: Utils.getIsPreview(vm.patient)
                            ? Colors.red
                            : HexColor.fromHex('#979797'),
                        shape: const RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(5.0))),
                        padding: const EdgeInsets.all(0.0)),
                    onPressed: Utils.getIsPreview(vm.patient) && auth.isLogin
                        ? _preViewOnTap
                        : _noLoginOnTap,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const SizedBox(
                          width: 5,
                          height: 70,
                        ),
                        const Expanded(
                          flex: 1,
                          child: Image(
                            image: AssetImage('assets/images/preview.png'),
                            height: 30,
                          ),
                        ),
                        Expanded(
                            child: FittedBox(
                          alignment: Alignment.centerLeft,
                          fit: BoxFit.scaleDown,
                          child: Text(
                            getTranslated(context, 'Preview'),
                            textScaleFactor: 1,
                            style: const TextStyle(
                                fontSize: 20, color: Colors.white),
                          ),
                        )),
                        const SizedBox(
                          width: 5,
                        ),
                      ],
                    ),
                  );
                },
              ))
        ],
      ),
    );
  }

  void _noLoginOnTap() {
    EasyLoading.showToast('未登入無法使用預覽');
  }

  //大頭照
  Widget _avatarImage() {
    return Consumer<AuthVm>(
      builder: (context, auth, _) {
        if (auth.isLogin) {
          return CircleAvatar(
            backgroundColor: Colors.grey,
            backgroundImage: const NetworkImage(
                '' //'https://${photoModel.host}/${photoModel.picPath}/${carModel != null ? carModel.carPhoto : ''}'
                ),
            onBackgroundImageError: (exception, stackTrace) {
              debugPrint('NetWorkImage: ${exception.toString()}');
              setState(() {
                isAvatar = true;
              });
            },
            radius: 30.0,
            child: isAvatar
                ? const Icon(
                    Icons.account_circle,
                    color: Colors.white,
                    size: 50,
                  )
                : Container(),
          );
        } else {
          return const CircleAvatar(
            backgroundColor: Colors.white,
            radius: 30.0,
            child: Icon(
              Icons.account_circle,
              color: Colors.red,
              size: 50,
            ),
          );
        }
      },
    );
  }

  //病史
  Widget _medicalHistoryWidget() {
    return GestureDetector(
      onTap: _medicalHistoryOnTap,
      child: Container(
        margin: const EdgeInsets.only(bottom: 2, top: 2),
        color: Colors.white,
        child: Row(
          children: [
            Expanded(
                flex: 2,
                child: Container(
                  alignment: Alignment.center,
                  decoration: _medicalHistoryBG,
                  child: FittedBox(
                    fit: BoxFit.scaleDown,
                    child: Text(
                      getTranslated(context, 'Medical_history'),
                      textScaleFactor: 1,
                      style: const TextStyle(color: Colors.white, fontSize: 22),
                    ),
                  ),
                )),
            const SizedBox(
              width: 15,
            ),
            Expanded(
                flex: 5,
                child: FittedBox(
                  fit: BoxFit.scaleDown,
                  alignment: Alignment.centerLeft,
                  child: Text(
                    getTranslated(context, 'Medical_history_text'),
                    textScaleFactor: 1,
                    style: const TextStyle(
                        fontSize: 22, fontWeight: FontWeight.bold),
                  ),
                )),
            Expanded(
                flex: 1, child: _isMedicalHistory ? Container() : lockWidget()),
            Expanded(
                flex: 1,
                child: Consumer<PatientVm>(
                  builder: (context, vm, _) {
                    return Container(
                      margin: const EdgeInsets.only(right: 8),
                      child: Utils.getIsMedicalHistoryStatus(
                              vm.patient) /*_isMedicalHistoryStatus*/
                          ? Icon(
                              Icons.check_circle_rounded,
                              size: 35,
                              color: HexColor.fromHex('#00D99C'),
                            )
                          : Icon(
                              Icons.check_circle_rounded,
                              size: 35,
                              color: HexColor.fromHex('#E1E1E1'),
                            ),
                    );
                  },
                ))
          ],
        ),
      ),
    );
  }

  //站姿-髂骨與腰椎問題評估
  Widget _standingPosture1Widget() {
    return Consumer<PatientVm>(builder: (context, vm, _) {
      return GestureDetector(
        onTap: () => _standingPosture1OnTap(vm.patient),
        child: Container(
          margin: const EdgeInsets.only(bottom: 2, top: 2),
          color: Colors.white,
          child: Row(
            children: [
              Expanded(
                  flex: 2,
                  child: Container(
                    alignment: Alignment.center,
                    decoration: _standingPostureBG,
                    child: FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Text(
                        getTranslated(context, 'Standing_posture'),
                        textScaleFactor: 1,
                        style:
                            const TextStyle(color: Colors.white, fontSize: 22),
                      ),
                    ),
                  )),
              const SizedBox(
                width: 15,
              ),
              Expanded(
                  flex: 5,
                  child: FittedBox(
                    fit: BoxFit.scaleDown,
                    alignment: Alignment.centerLeft,
                    child: Text(
                      getTranslated(context,
                          'Evaluation_of_iliac_and_lumbar_spine_problems'),
                      textScaleFactor: 1,
                      style: const TextStyle(
                          fontSize: 22, fontWeight: FontWeight.bold),
                    ),
                  )),
              Expanded(
                  flex: 1,
                  child: _isStandingPosture1 ? Container() : lockWidget()),
              Expanded(
                  flex: 1,
                  child: Container(
                    margin: const EdgeInsets.only(right: 8),
                    child: Utils.getIsStandingPosture1Status(
                            vm.patient) /*_isStandingPosture1Status*/
                        ? Icon(
                            Icons.check_circle_rounded,
                            size: 35,
                            color: HexColor.fromHex('#00D99C'),
                          )
                        : Icon(
                            Icons.check_circle_rounded,
                            size: 35,
                            color: HexColor.fromHex('#E1E1E1'),
                          ),
                  ))
            ],
          ),
        ),
      );
    });
  }

  //仰躺-恥骨問題評估與處理
  Widget _lieOnYourBack1Widget() {
    return Consumer<PatientVm>(builder: (context, vm, _) {
      return GestureDetector(
        onTap: () => _lieOnYourBack1OnTap(vm.patient),
        child: Container(
          margin: const EdgeInsets.only(bottom: 2, top: 2),
          color: Colors.white,
          child: Row(
            children: [
              Expanded(
                  flex: 2,
                  child: Container(
                    alignment: Alignment.center,
                    decoration: _lieOnYourBackBG1,
                    child: FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Text(
                        getTranslated(context, 'Lie_on_your_back'),
                        textScaleFactor: 1,
                        style:
                            const TextStyle(color: Colors.white, fontSize: 22),
                      ),
                    ),
                  )),
              const SizedBox(
                width: 15,
              ),
              Expanded(
                  flex: 5,
                  child: FittedBox(
                    fit: BoxFit.scaleDown,
                    alignment: Alignment.centerLeft,
                    child: Text(
                      getTranslated(context,
                          'Assessment_and_management_of_pubic_problems'),
                      textScaleFactor: 1,
                      style: const TextStyle(
                          fontSize: 22, fontWeight: FontWeight.bold),
                    ),
                  )),
              Expanded(
                  flex: 1,
                  child: _isLieOnYourBack1 ? Container() : lockWidget()),
              Expanded(
                  flex: 1,
                  child: Consumer<PatientVm>(
                    builder: (context, vm, _) {
                      return Container(
                        margin: const EdgeInsets.only(right: 8),
                        child: Utils.getIsLieOnYourBack1Status(
                                vm.patient) /*_isLieOnYourBack1Status*/
                            ? Icon(
                                Icons.check_circle_rounded,
                                size: 35,
                                color: HexColor.fromHex('#00D99C'),
                              )
                            : Icon(
                                Icons.check_circle_rounded,
                                size: 35,
                                color: HexColor.fromHex('#E1E1E1'),
                              ),
                      );
                    },
                  ))
            ],
          ),
        ),
      );
    });
  }

  //俯臥-髂骨上移問題處理
  Widget _prone1Widget() {
    return Consumer<PatientVm>(builder: (context, vm, _) {
      return GestureDetector(
        onTap: () => _prone1OnTap(vm.patient),
        child: Container(
          margin: const EdgeInsets.only(bottom: 2, top: 2),
          color: Colors.white,
          child: Row(
            children: [
              Expanded(
                  flex: 2,
                  child: Container(
                    alignment: Alignment.center,
                    decoration: _proneBG,
                    child: FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Text(
                        getTranslated(context, 'Prone'),
                        textScaleFactor: 1,
                        style:
                            const TextStyle(color: Colors.white, fontSize: 22),
                      ),
                    ),
                  )),
              const SizedBox(
                width: 15,
              ),
              Expanded(
                  flex: 5,
                  child: FittedBox(
                    fit: BoxFit.scaleDown,
                    alignment: Alignment.centerLeft,
                    child: Text(
                      getTranslated(context,
                          'Treatment_of_the_problem_of_upward_movement_of_the_iliac_bone'),
                      textScaleFactor: 1,
                      style: const TextStyle(
                          fontSize: 22, fontWeight: FontWeight.bold),
                    ),
                  )),
              Expanded(flex: 1, child: _isProne1 ? Container() : lockWidget()),
              Expanded(
                  flex: 1,
                  child: Container(
                    margin: const EdgeInsets.only(right: 8),
                    child:
                        Utils.getIsProne1Status(vm.patient) /*_isProne1Status*/
                            ? Icon(
                                Icons.check_circle_rounded,
                                size: 35,
                                color: HexColor.fromHex('#00D99C'),
                              )
                            : Icon(
                                Icons.check_circle_rounded,
                                size: 35,
                                color: HexColor.fromHex('#E1E1E1'),
                              ),
                  ))
            ],
          ),
        ),
      );
    });
  }

  //俯臥-薦椎問題評估與處理
  Widget _prone2Widget() {
    return Consumer<PatientVm>(builder: (context, vm, _) {
      return GestureDetector(
        onTap: () => _prone2OnTap(vm.patient),
        child: Container(
          margin: const EdgeInsets.only(bottom: 2, top: 2),
          color: Colors.white,
          child: Row(
            children: [
              Expanded(
                  flex: 2,
                  child: Container(
                    alignment: Alignment.center,
                    decoration: _proneBG,
                    child: FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Text(
                        getTranslated(context, 'Prone'),
                        textScaleFactor: 1,
                        style:
                            const TextStyle(color: Colors.white, fontSize: 22),
                      ),
                    ),
                  )),
              const SizedBox(
                width: 15,
              ),
              Expanded(
                  flex: 5,
                  child: FittedBox(
                    fit: BoxFit.scaleDown,
                    alignment: Alignment.centerLeft,
                    child: Text(
                      getTranslated(context,
                          'Evaluation_and_treatment_of_sacral_problems'),
                      textScaleFactor: 1,
                      style: const TextStyle(
                          fontSize: 22, fontWeight: FontWeight.bold),
                    ),
                  )),
              Expanded(flex: 1, child: _isProne2 ? Container() : lockWidget()),
              Expanded(
                  flex: 1,
                  child: Container(
                    margin: const EdgeInsets.only(right: 8),
                    child:
                        Utils.getIsProne2Status(vm.patient) /*_isProne2Status*/
                            ? Icon(
                                Icons.check_circle_rounded,
                                size: 35,
                                color: HexColor.fromHex('#00D99C'),
                              )
                            : Icon(
                                Icons.check_circle_rounded,
                                size: 35,
                                color: HexColor.fromHex('#E1E1E1'),
                              ),
                  ))
            ],
          ),
        ),
      );
    });
  }

  //俯臥-腰椎小面關節問題評估
  Widget _prone3Widget() {
    return Consumer<PatientVm>(builder: (context, vm, _) {
      return GestureDetector(
        onTap: () => _prone3OnTap(vm.patient),
        child: Container(
          margin: const EdgeInsets.only(bottom: 2, top: 2),
          color: Colors.white,
          child: Row(
            children: [
              Expanded(
                  flex: 2,
                  child: Container(
                    alignment: Alignment.center,
                    decoration: _proneBG,
                    child: FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Text(
                        getTranslated(context, 'Prone'),
                        textScaleFactor: 1,
                        style:
                            const TextStyle(color: Colors.white, fontSize: 22),
                      ),
                    ),
                  )),
              const SizedBox(
                width: 15,
              ),
              Expanded(
                  flex: 5,
                  child: FittedBox(
                    fit: BoxFit.scaleDown,
                    child: Text(
                      getTranslated(
                          context, 'Assessment_of_lumbar_facet_joint_problems'),
                      textScaleFactor: 1,
                      style: const TextStyle(
                          fontSize: 22, fontWeight: FontWeight.bold),
                    ),
                  )),
              Expanded(flex: 1, child: _isProne3 ? Container() : lockWidget()),
              Expanded(
                  flex: 1,
                  child: Container(
                    margin: const EdgeInsets.only(right: 8),
                    child:
                        Utils.getIsProne3Status(vm.patient) /*_isProne3Status*/
                            ? Icon(
                                Icons.check_circle_rounded,
                                size: 35,
                                color: HexColor.fromHex('#00D99C'),
                              )
                            : Icon(
                                Icons.check_circle_rounded,
                                size: 35,
                                color: HexColor.fromHex('#E1E1E1'),
                              ),
                  ))
            ],
          ),
        ),
      );
    });
  }

  //側躺-腰椎小面關節問題處理
  Widget _lieDownWidget() {
    return Consumer<PatientVm>(builder: (context, vm, _) {
      return GestureDetector(
        onTap: () => _lieDownOnTap(vm.patient),
        child: Container(
          margin: const EdgeInsets.only(bottom: 2, top: 2),
          color: Colors.white,
          child: Row(
            children: [
              Expanded(
                  flex: 2,
                  child: Container(
                    alignment: Alignment.center,
                    decoration: _lieDownBG,
                    child: FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Text(
                        getTranslated(context, 'Lie_down'),
                        textScaleFactor: 1,
                        style:
                            const TextStyle(color: Colors.white, fontSize: 22),
                      ),
                    ),
                  )),
              const SizedBox(
                width: 15,
              ),
              Expanded(
                  flex: 5,
                  child: FittedBox(
                    fit: BoxFit.scaleDown,
                    alignment: Alignment.centerLeft,
                    child: Text(
                      getTranslated(
                          context, 'Treatment_of_lumbar_facet_joint_problems'),
                      textScaleFactor: 1,
                      style: const TextStyle(
                          fontSize: 22, fontWeight: FontWeight.bold),
                    ),
                  )),
              Expanded(flex: 1, child: _isLieDown ? Container() : lockWidget()),
              Expanded(
                  flex: 1,
                  child: Container(
                    margin: const EdgeInsets.only(right: 8),
                    child: Utils.getIsLieDownStatus(
                            vm.patient) /*_isLieDownStatus*/
                        ? Icon(
                            Icons.check_circle_rounded,
                            size: 35,
                            color: HexColor.fromHex('#00D99C'),
                          )
                        : Icon(
                            Icons.check_circle_rounded,
                            size: 35,
                            color: HexColor.fromHex('#E1E1E1'),
                          ),
                  ))
            ],
          ),
        ),
      );
    });
  }

  //仰躺-髂骨問題評估與處理
  Widget _lieOnYourBack2Widget() {
    return Consumer<PatientVm>(builder: (context, vm, _) {
      return GestureDetector(
        onTap: () => _lieOnYourBack2OnTap(vm.patient),
        child: Container(
          margin: const EdgeInsets.only(bottom: 2, top: 2),
          color: Colors.white,
          child: Row(
            children: [
              Expanded(
                  flex: 2,
                  child: Container(
                    alignment: Alignment.center,
                    decoration: _lieOnYourBackBG2,
                    child: FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Text(
                        getTranslated(context, 'Lie_on_your_back'),
                        textScaleFactor: 1,
                        style:
                            const TextStyle(color: Colors.white, fontSize: 22),
                      ),
                    ),
                  )),
              const SizedBox(
                width: 15,
              ),
              Expanded(
                  flex: 5,
                  child: FittedBox(
                    fit: BoxFit.scaleDown,
                    alignment: Alignment.centerLeft,
                    child: Text(
                      getTranslated(context,
                          'Evaluation_and_treatment_of_iliac_problems'),
                      textScaleFactor: 1,
                      style: const TextStyle(
                          fontSize: 22, fontWeight: FontWeight.bold),
                    ),
                  )),
              Expanded(
                  flex: 1,
                  child: _isLieOnYourBack2 ? Container() : lockWidget()),
              Expanded(
                  flex: 1,
                  child: Container(
                    margin: const EdgeInsets.only(right: 8),
                    child: Utils.getIsLieOnYourBack2Status(
                            vm.patient) /*_isLieOnYourBack2Status*/
                        ? Icon(
                            Icons.check_circle_rounded,
                            size: 35,
                            color: HexColor.fromHex('#00D99C'),
                          )
                        : Icon(
                            Icons.check_circle_rounded,
                            size: 35,
                            color: HexColor.fromHex('#E1E1E1'),
                          ),
                  ))
            ],
          ),
        ),
      );
    });
  }

  //站姿-髂骨與腰椎問題再評估
  Widget _standingPosture2Widget() {
    return Consumer<PatientVm>(builder: (context, vm, _) {
      return GestureDetector(
        onTap: () => _standingPosture2OnTap(vm.patient),
        child: Container(
          margin: const EdgeInsets.only(bottom: 2, top: 2),
          color: Colors.white,
          child: Row(
            children: [
              Expanded(
                  flex: 2,
                  child: Container(
                    alignment: Alignment.center,
                    decoration: _standingPostureBG,
                    child: FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Text(
                        getTranslated(context, 'Standing_posture'),
                        textScaleFactor: 1,
                        style:
                            const TextStyle(color: Colors.white, fontSize: 22),
                      ),
                    ),
                  )),
              const SizedBox(
                width: 15,
              ),
              Expanded(
                  flex: 5,
                  child: FittedBox(
                    fit: BoxFit.scaleDown,
                    alignment: Alignment.centerLeft,
                    child: Text(
                      getTranslated(context,
                          'Reassessment_of_iliac_and_lumbar_spine_problems'),
                      textScaleFactor: 1,
                      style: const TextStyle(
                          fontSize: 22, fontWeight: FontWeight.bold),
                    ),
                  )),
              Expanded(
                  flex: 1,
                  child: _isStandingPosture ? Container() : lockWidget()),
              Expanded(
                  flex: 1,
                  child: Container(
                    margin: const EdgeInsets.only(right: 8),
                    child: Utils.getIsStandingPosture2Status(
                            vm.patient) /*_isStandingPosture2Status*/
                        ? Icon(
                            Icons.check_circle_rounded,
                            size: 35,
                            color: HexColor.fromHex('#00D99C'),
                          )
                        : Icon(
                            Icons.check_circle_rounded,
                            size: 35,
                            color: HexColor.fromHex('#E1E1E1'),
                          ),
                  ))
            ],
          ),
        ),
      );
    });
  }

  //重新測試
  Widget _retestWidget() {
    return Container(
      margin: const EdgeInsets.only(right: 5),
      child: Row(
        children: [
          Expanded(flex: 4, child: Container()),
          Expanded(
              flex: 3,
              child: ButtonTheme(
                height: 110,
                child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        foregroundColor: Colors.white,
                        backgroundColor: Colors.transparent,
                        shape: const RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.0))),
                        padding: const EdgeInsets.all(0.0)),
                    onPressed: _retestOnTap,
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 10, vertical: 10),
                      decoration: _retestBG,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          const Expanded(
                              flex: 1,
                              child: Icon(
                                Icons.refresh,
                                size: 30,
                              )),
                          Expanded(
                              flex: 2,
                              child: FittedBox(
                                fit: BoxFit.scaleDown,
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  getTranslated(context, 'Retest'),
                                  textScaleFactor: 1,
                                  style: const TextStyle(fontSize: 27),
                                ),
                              )),
                        ],
                      ),
                    )),
              ))
        ],
      ),
    );
  }

  //側選單
  Widget _drawer() {
    return Drawer(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          //頭
          _drawerHeader(),
          //身
          _drawerBodyIcon(
              0, getTranslated(context, 'Member_Centre'), Icons.person),
          const Divider(
            height: 3,
          ),
          _drawerBodyIcon(
              1, getTranslated(context, 'Basic_settings'), Icons.settings),
          const Divider(
            height: 3,
          ),
          _drawerBodyImg(
              2, getTranslated(context, 'Record'), 'assets/images/record.png'),
          const Divider(
            height: 3,
          ),
          _drawerBodyImg(3, getTranslated(context, 'Contact_us'),
              'assets/images/contact_us.png'),
          Divider(
            height: 3,
          ),
          //付費升級
          /*_drawerBodyIcon(
              4, getTranslated(context, 'Paid_upgrade'), Icons.shopping_cart),
          Divider(
            height: 3,
          ),*/
          _drawerBodyImg(5, getTranslated(context, 'Sign_out'),
              'assets/images/logout.png'),
          Divider(
            height: 3,
          ),
          Expanded(
              child: Align(
            alignment: Alignment.bottomCenter,
            child: ListTile(
              title: FittedBox(
                fit: BoxFit.scaleDown,
                child: Text(
                  '${getTranslated(context, 'Version')} v${_packageInfo.version}',
                  textScaleFactor: 1,
                  textAlign: TextAlign.end,
                ),
              ),
            ),
          ))
        ],
      ),
    );
  }

  //側選單頭
  Widget _drawerHeader() {
    return DrawerHeader(
      padding: EdgeInsets.zero,
      decoration: const BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/images/drawer_bg.png'),
          fit: BoxFit.fill,
        ),
      ),
      child: Container(
        padding: const EdgeInsets.only(left: 8.0, right: 8.0),
        child: Row(
          children: [
            Expanded(
                child: SizedBox(
              width: 100,
              height: 100,
              child: Image.asset('assets/images/genuine_avatar.png'),
            )),
            Expanded(
              child: Consumer<AuthVm>(
                builder: (context, auth, _) {
                  return InkWell(
                      onTap: !auth.isLogin ? _loginOrRegisterOnTap : null,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          //使用者名稱
                          FittedBox(
                            fit: BoxFit.scaleDown,
                            child: Consumer<MemberVm>(
                              builder: (context, vm, _) {
                                return Text(
                                  auth.isLogin ? vm.memberModel!.name : '登入/註冊',
                                  textScaleFactor: 1,
                                  style: const TextStyle(
                                      fontSize: 20,
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                );
                              },
                            ),
                          ),
                          FittedBox(
                            fit: BoxFit.scaleDown,
                            child: Text(
                              isApprove == 0 ? '試用版' : '正式版(已授權)',
                              textScaleFactor: 1,
                              style: const TextStyle(
                                  fontSize: 20, color: Colors.white),
                            ),
                          )
                        ],
                      ));
                },
              ),
            )
          ],
        ),
      ),
    );
  }

  //側選單身體Icon選項
  Widget _drawerBodyIcon(int id, String text, IconData icon) {
    return Consumer<AuthVm>(
      builder: (context, auth, _) {
        return ListTile(
          title: FittedBox(
            fit: BoxFit.scaleDown,
            alignment: Alignment.centerLeft,
            child: Text(
              text,
              textScaleFactor: 1,
              style: TextStyle(
                  color: HexColor.fromHex('#032137'),
                  fontSize: 22,
                  fontWeight: FontWeight.bold),
            ),
          ),
          leading: Icon(
            icon,
            color: HexColor.fromHex('#032137'),
            size: 35,
          ),
          contentPadding: const EdgeInsets.symmetric(horizontal: 40),
          onTap: () {
            if (id == 0) {
              debugPrint('會員中心');
              if (auth.isLogin) {
                delegate.push(name: RouteName.memberCentre);
              } else {
                EasyLoading.showToast(
                    getTranslated(context, 'Please_login_first'));
              }
            }
            if (id == 1) {
              debugPrint('基本設定');
              if (auth.isLogin) {
                delegate.push(name: RouteName.setUp);
              } else {
                EasyLoading.showToast(
                    getTranslated(context, 'Please_login_first'));
              }
            }
          },
        );
      },
    );
  }

  //側選單身體Image選項
  Widget _drawerBodyImg(int id, String text, String icon) {
    return Consumer<AuthVm>(builder: (context, auth, _) {
      return ListTile(
        title: FittedBox(
          fit: BoxFit.scaleDown,
          alignment: Alignment.centerLeft,
          child: Text(
            text,
            textScaleFactor: 1,
            style: TextStyle(
                color: HexColor.fromHex('#032137'),
                fontSize: 22,
                fontWeight: FontWeight.bold),
          ),
        ),
        leading: Container(
          padding: const EdgeInsets.only(left: 5),
          child: Image.asset(
            icon,
            width: 25,
            color: HexColor.fromHex('#032137'),
          ),
        ),
        contentPadding: const EdgeInsets.symmetric(horizontal: 40),
        onTap: () {
          if (id == 2) {
            debugPrint('紀錄');
            if (auth.isLogin) {
              delegate.push(name: RouteName.recordRoutes);
            } else {
              EasyLoading.showToast(
                  getTranslated(context, 'Please_login_first'));
            }
          }
          if (id == 3) {
            debugPrint('聯絡我們');
            delegate.push(name: RouteName.contactUs);
          }
          if (id == 5) {
            debugPrint('登出');
            if (auth.isLogin) {
              showLogOutDialogFunction();
            } else {
              EasyLoading.showToast('未登入');
            }
          }
        },
      );
    });
  }

  //region 點擊後行為
  //登入/註冊
  void _loginOrRegisterOnTap() async {
    delegate.push(name: RouteName.singIn);
  }

  //帳號啟用鎖判斷
  void _memberLock() {
    setState(() {
      if (_memberVm!.memberModel!.operationItems.isNotEmpty) {
        return;
      }
      for (var element in _memberVm!.memberModel!.operationItems) {
        if (element.name == '恥骨問題評估與處理') {
          _isLieOnYourBack1 = element.isLock == 1 ? false : true;
        }
        if (element.name == '恥骨上移問題處理') {
          _isProne1 = element.isLock == 1 ? false : true;
        }
        if (element.name == '薦椎問題評估與處理') {
          _isProne2 = element.isLock == 1 ? false : true;
        }
        if (element.name == '腰椎小面關節問題評估') {
          _isProne3 = element.isLock == 1 ? false : true;
        }
        if (element.name == '腰椎小面關節問題處理') {
          _isLieDown = element.isLock == 1 ? false : true;
        }
        if (element.name == '髂骨問題評估與處理') {
          _isLieOnYourBack2 = element.isLock == 1 ? false : true;
        }
        if (element.name == '髂骨與腰椎問題再評估') {
          _isStandingPosture = element.isLock == 1 ? false : true;
        }
      }
    });
  }

  //預覽
  void _preViewOnTap() {
    delegate.push(name: RouteName.pv01);
  }

  //病史
  void _medicalHistoryOnTap() async {
    if (_operationItemVm!.operationItemsModelList == null) {
      return;
    }
    delegate.push(name: RouteName.medicalHistory);
  }

  //站姿-髂骨與腰椎問題評估
  void _standingPosture1OnTap(Patient? patient) async {
    if (patient == null) {
      EasyLoading.showToast('請填個案姓名');
      return;
    }
    if (patient.name.isEmpty) {
      EasyLoading.showToast('請填個案姓名');
      return;
    }
    if (_operationItemVm!.operationItemsModelList == null) {
      return;
    }
    delegate.push(name: RouteName.spe01);
  }

  //仰躺-恥骨問題評估與處理
  void _lieOnYourBack1OnTap(Patient? patient) async {
    if (patient == null) {
      EasyLoading.showToast('請填個案姓名');
      return;
    }
    if (patient.name.isEmpty) {
      EasyLoading.showToast('請填個案姓名');
      return;
    }
    if (_operationItemVm!.operationItemsModelList == null) {
      return;
    }
    if (_isLieOnYourBack1) {
      delegate.push(name: RouteName.lbk01);
    } else {
      //showDialogFunction();
    }
  }

  //俯臥-髂骨上移問題處理
  void _prone1OnTap(Patient? patient) {
    if (patient == null) {
      EasyLoading.showToast('請填個案姓名');
      return;
    }
    if (patient.name.isEmpty) {
      EasyLoading.showToast('請填個案姓名');
      return;
    }
    if (_operationItemVm!.operationItemsModelList == null) {
      return;
    }
    if (_isProne1) {
      if (!Utils.getIsProne1Status(patient) /*_isProne1Status*/) {
        //有問題 要進去
        delegate.push(name: RouteName.sps41);
      }
      delegate.push(name: RouteName.sps41);
    } else {
      //showDialogFunction();
    }
  }

  //俯臥-薦椎問題評估與處理
  void _prone2OnTap(Patient? patient) {
    if (patient == null) {
      EasyLoading.showToast('請填個案姓名');
      return;
    }
    if (patient.name.isEmpty) {
      EasyLoading.showToast('請填個案姓名');
      return;
    }
    if (_operationItemVm!.operationItemsModelList == null) {
      return;
    }
    if (_isProne2) {
      delegate.push(name: RouteName.sps01);
    } else {
      //showDialogFunction();
    }
  }

  //俯臥-腰椎小面關節問題評估
  void _prone3OnTap(Patient? patient) {
    if (patient == null) {
      EasyLoading.showToast('請填個案姓名');
      return;
    }
    if (patient.name.isEmpty) {
      EasyLoading.showToast('請填個案姓名');
      return;
    }
    if (_operationItemVm!.operationItemsModelList == null) {
      return;
    }
    if (_isProne3) {
      delegate.push(name: RouteName.sps06);
    } else {
      //showDialogFunction();
    }
  }

  //側躺-腰椎小面關節問題處理
  void _lieDownOnTap(Patient? patient) {
    if (patient == null) {
      EasyLoading.showToast('請填個案姓名');
      return;
    }
    if (patient.name.isEmpty) {
      EasyLoading.showToast('請填個案姓名');
      return;
    }
    if (_operationItemVm!.operationItemsModelList == null) {
      return;
    }
    if (_isLieDown) {
      delegate.push(name: RouteName.ld01);
    } else {
      //showDialogFunction();
    }
  }

  //仰躺-髂骨問題評估與處理
  void _lieOnYourBack2OnTap(Patient? patient) {
    if (patient == null) {
      EasyLoading.showToast('請填個案姓名');
      return;
    }
    if (patient.name.isEmpty) {
      EasyLoading.showToast('請填個案姓名');
      return;
    }
    if (_operationItemVm!.operationItemsModelList == null) {
      return;
    }
    if (_isLieOnYourBack2) {
      delegate.push(name: RouteName.lbk11);
    } else {
      //showDialogFunction();
    }
  }

  //站姿-髂骨與腰椎問題再評估
  void _standingPosture2OnTap(Patient? patient) {
    if (patient == null) {
      EasyLoading.showToast('請填個案姓名');
      return;
    }
    if (patient.name.isEmpty) {
      EasyLoading.showToast('請填個案姓名');
      return;
    }
    if (_operationItemVm!.operationItemsModelList == null) {
      return;
    }
    if (_isStandingPosture) {
      delegate.push(name: RouteName.spe91);
    } else {
      //showDialogFunction();
    }
  }

  //重新測試
  void _retestOnTap() {
    _patientVm!.delPatient();
    /*_isMedicalHistoryStatus = false;
    _isStandingPosture1Status = false;
    _isLieOnYourBack1Status = false;
    _isProne1Status = false;
    _isProne2Status = false;
    _isProne3Status = false;
    _isLieDownStatus = false;
    _isLieOnYourBack2Status = false;
    _isStandingPosture2Status = false;
    setState(() {});*/
  }

  //endregion

  //提示網路連線
  void showDialogNetWork() {
    _isNetWorkDialog = true;
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: const [
                FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    '請連接網路',
                    textAlign: TextAlign.center,
                    textScaleFactor: 1,
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 23,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ],
            ),
          );
        });
  }

  //登出
  void showLogOutDialogFunction() async {
    bool isSelect = await showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                const FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    '確定要登出嗎?',
                    textAlign: TextAlign.center,
                    textScaleFactor: 1,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    //確定
                    ButtonTheme(
                      height: 80,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            foregroundColor: Colors.white,
                            backgroundColor: Colors.transparent,
                            shape: const RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0))),
                            padding: const EdgeInsets.all(0.0)),
                        onPressed: () {
                          debugPrint('確認');
                          Navigator.of(context).pop(true);
                        },
                        child: Container(
                          height: 50,
                          width: 100,
                          alignment: Alignment.center,
                          decoration: _retestBG,
                          child: const FittedBox(
                            fit: BoxFit.scaleDown,
                            child: Text(
                              '確定',
                              textScaleFactor: 1,
                              style:
                                  TextStyle(color: Colors.white, fontSize: 20),
                            ),
                          ),
                        ),
                      ),
                    ),
                    //取消
                    ButtonTheme(
                      height: 80,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            foregroundColor: Colors.white,
                            backgroundColor: Colors.transparent,
                            shape: const RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0))),
                            padding: const EdgeInsets.all(0.0)),
                        onPressed: () {
                          debugPrint('取消');
                          Navigator.of(context).pop(false);
                        },
                        child: Container(
                          height: 50,
                          width: 100,
                          alignment: Alignment.center,
                          decoration: _retestBG,
                          child: const FittedBox(
                            fit: BoxFit.scaleDown,
                            child: Text(
                              '取消',
                              textScaleFactor: 1,
                              style:
                                  TextStyle(color: Colors.white, fontSize: 20),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          );
        });
    if (isSelect) {
      _authVm!.setLogin(false);
      //每個療程的鎖
      _isLieOnYourBack1 = true;
      _isProne1 = false;
      _isProne2 = false;
      _isProne3 = false;
      _isLieDown = false;
      _isLieOnYourBack2 = false;
      _isStandingPosture = false;
      //每個選項的狀態
      /*_isMedicalHistoryStatus = false;
      _isStandingPosture1Status = false;
      _isLieOnYourBack1Status = false;
      _isProne1Status = false;
      _isProne2Status = false;
      _isProne3Status = false;
      _isLieDownStatus = false;
      _isLieOnYourBack2Status = false;
      _isStandingPosture2Status = false;*/
      setState(() {});
      _memberVm!.initMemberModel();
    }
  }

  //聯絡我們資料
  void _getCompanyInfo() async {
    try {
      dynamic response = await httpUtils.get(
        getCompanyInfo,
        options: Options(
            contentType: 'application/json',
            responseType: ResponseType.json,
            headers: {"authorization": "Basic $auth"}),
      );
      log('聯絡我們: $response');
      _companyInfoVm!.setCompanyInfoModel(
          CompanyInfoModel.fromJson(json.decode(json.encode(response))));
    } on DioError catch (e) {
      debugPrint('聯絡我們error ${e.message}');
    }
  }

  //操作項目列表
  void _getOperationItems() async {
    debugPrint('操作項目列表');
    try {
      dynamic response = await httpUtils.get(
        getOperationItems,
        options: Options(
            contentType: 'application/json',
            responseType: ResponseType.json,
            headers: {"authorization": "Basic $auth"}),
      );
      log('操作項目列表: $response');
      List list = json.decode(json.encode(response));
      _operationItemVm!
          .setList(list.map((e) => OperationItemsModel.fromJson(e)).toList());

      for (var element in _operationItemVm!.operationItemsModelList!) {
        log(element.toJson().toString());
        if (element.name == '病史諮詢') {
          _isMedicalHistory = element.isFree == 0 ? false : true;
        }
        if (element.name == '髂骨與腰椎問題評估') {
          _isStandingPosture1 = element.isFree == 0 ? false : true;
        }
        if (element.name == '恥骨問題評估與處理') {
          _isLieOnYourBack1 = element.isFree == 0 ? false : true;
        }
        if (element.name == '恥骨上移問題處理') {
          _isProne1 = element.isFree == 0 ? false : true;
        }
        if (element.name == '薦椎問題評估與處理') {
          _isProne2 = element.isFree == 0 ? false : true;
        }
        if (element.name == '腰椎小面關節問題評估') {
          _isProne3 = element.isFree == 0 ? false : true;
        }
        if (element.name == '腰椎小面關節問題處理') {
          _isLieDown = element.isFree == 0 ? false : true;
        }
        if (element.name == '髂骨問題評估與處理') {
          _isLieOnYourBack2 = element.isFree == 0 ? false : true;
        }
        if (element.name == '髂骨與腰椎問題再評估') {
          _isStandingPosture = element.isFree == 0 ? false : true;
        }
        setState(() {});
      }
    } on DioError catch (e) {
      debugPrint('操作項目列表error: ${e.message}');
    }
  }

  //取得用戶資料
  void _getMember(int id) async {
    try {
      dynamic response = await httpUtils.get(
        '$getMember$id',
        options: Options(
            contentType: 'application/json',
            responseType: ResponseType.json,
            headers: {"authorization": "Bearer $accessToken"}),
      );
      log('用戶資訊 $response');
      _memberVm!.setMemberModel(
          MemberModel.fromJson(json.decode(json.encode(response))));
      isApprove = _memberVm!.memberModel!.isApprove;
      _getUserData(_memberVm!.memberModel!.usersId);
      EasyLoading.dismiss();
      EasyLoading.showToast(
          '${getTranslated(context, 'Sing_in')}${getTranslated(context, 'success')}!');
      _authVm!.setLogin(true);
      _memberLock();
    } on DioError catch (e) {
      EasyLoading.dismiss();
      EasyLoading.showError(e.message);
      debugPrint('用戶資料Error: ${e.message}');
    }
  }

  //取得用戶所屬公司資料
  void _getUserData(int id) async {
    if (id == 1) {
      _userCompanyInformationVm!.initModel();
      _userCompanyInformationVm!
          .setName(_companyInfoVm!.companyInfoModel!.name);
      _userCompanyInformationVm!
          .setPhone(_companyInfoVm!.companyInfoModel!.phone);
      _userCompanyInformationVm!
          .setAddress(_companyInfoVm!.companyInfoModel!.address);
      _userCompanyInformationVm!
          .setLogo("https://jg-hp.com/apps/adminn/assets/img/brand/logo.png");
      return;
    }
    try {
      dynamic response = await httpUtils.get(
        '$getUserData$id',
        options: Options(
            contentType: 'application/json',
            responseType: ResponseType.json,
            headers: {"authorization": "Bearer $accessToken"}),
      );
      debugPrint('所屬公司資料: $response');
      _userCompanyInformationVm!.setModel(UserCompanyInformationModel.fromJson(
          json.decode(json.encode(response))));
    } on DioError catch (e) {
      debugPrint('所屬公司資料Error: ${e.toString()}');
    }
  }

  //鎖頭
  Widget lockWidget() {
    return const Image(
      image: AssetImage('assets/images/lock.png'),
      width: 30,
      height: 30,
    );
  }
}
