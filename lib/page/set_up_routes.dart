import 'package:flutter/material.dart';
import 'package:light_silver/language/language_constants.dart';
import 'package:light_silver/utils/utils.dart';
import 'package:provider/provider.dart';

import '../page/basic_settings_page.dart';
import '../utils/app_image.dart';
import '../view_model/set_up_vm.dart';
import '../widgets/left_button.dart';
import '../widgets/set_up/title.dart';
import 'preview_settings_page.dart';

class SetUpRoutes extends StatefulWidget{

  @override
  State createState() => _SetUpRoutesState();
}

class _SetUpRoutesState extends State<SetUpRoutes>{

  SetUpVm? _setUpVm;

  late PageController _pageController;
  late PageView _pageView;
  int _currentPage = 0;

  //漸層背景
  final _homeBg = const BoxDecoration(
    image: DecorationImage(
      image: AssetImage(AppImage.homeBg), fit: BoxFit.fill));
  final _titleBarBG = BoxDecoration(
      gradient: LinearGradient(
          colors: [HexColor.fromHex('#0067B4'), HexColor.fromHex('#003457')],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter));
  //中間背景
  final _centerBG = const BoxDecoration(
    color: Colors.white,
    borderRadius: BorderRadius.all(Radius.circular(10)),
    boxShadow: [
      BoxShadow(
        color: Colors.black54,
        offset: Offset(5.0, 5.0),
        blurRadius: 10.0,
        spreadRadius: 2.0)
    ]);

  _onLayoutDone(_){
    _setUpVm!.setTitle(getTranslated(context, 'Preview_settings'));
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
    _pageController = PageController();
    _pageView = PageView(
      controller: _pageController,
      children: [
        PreviewSettingsPage(),
        BasicSettingsPage(),
      ],
      onPageChanged: (index){
        if(index == 0){
          _setUpVm!.setTitle(getTranslated(context, 'Preview_settings'));
        }else{
          _setUpVm!.setTitle(getTranslated(context, 'Basic_settings'));
        }
        setState(() {
          _currentPage = index;
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    _setUpVm = Provider.of<SetUpVm>(context);
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: false,
      body: GestureDetector(
        onTap: _clickOnTap,
        child: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Expanded(flex: 1, child: _titleBar()),
              Expanded(flex: 10, child: _bodyWidget()),
            ],
          ),
        ),
      ),
    );
  }

  void _clickOnTap(){
    FocusScopeNode currentFocus = FocusScope.of(context);
    if(!currentFocus.hasPrimaryFocus && currentFocus.focusedChild != null){
      FocusManager.instance.primaryFocus!.unfocus();
    }
  }

  //最上層標題與按鈕
  Widget _titleBar(){
    return Container(
      decoration: _titleBarBG,
      child: Row(
        children: [
          const Expanded(flex: 1, child: LeftButton()),
          Expanded(flex: 5, child: Container(
            alignment: Alignment.center,
            child: const TitleTextWidget(),
          )),
          const Expanded(flex: 1,child: SizedBox(width: 50,))
        ],
      ),
    );
  }

  //主體
  Widget _bodyWidget(){
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      decoration: _homeBg,
      alignment: Alignment.center,
      child: Container(
        width: MediaQuery.of(context).size.width / 1.05,
        height: MediaQuery.of(context).size.height / 1.25 ,
        decoration: _centerBG,
        child: Column(
          children: [
            Container(
              height: 60,
              margin: const EdgeInsets.symmetric(horizontal: 10,vertical: 10),
              padding: const EdgeInsets.all(5),
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(Radius.circular(10)),
                color: HexColor.fromHex('#F5F5F5'),
                boxShadow: const [
                  BoxShadow(
                    color: Colors.black38,
                    offset: Offset(5.0,5.0),
                    blurRadius: 10.0,
                    spreadRadius: 2.0
                  )
                ]),
              child: Row(
                children: [
                  Expanded(child: Container(
                    decoration: _currentPage == 0
                        ? BoxDecoration(
                      borderRadius: const BorderRadius.all(Radius.circular(10)),
                      gradient: LinearGradient(
                        colors: [
                          HexColor.fromHex('#0067B4'),
                          HexColor.fromHex('#003457'),
                        ],
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter))
                        : null,
                    child: Center(
                      child: TextButton(
                        onPressed: (){
                          _pageController.animateToPage(0, duration: const Duration(milliseconds: 300), curve: Curves.decelerate);
                        },
                        child: FittedBox(
                          fit: BoxFit.scaleDown,
                          child: Text(
                            getTranslated(context, 'Preview_settings'),
                            textScaleFactor: 1,
                            style: TextStyle(
                              color: _currentPage == 0
                                  ? Colors.white
                                  : HexColor.fromHex('#032137'))),
                        ),
                      ),
                    ),
                  )),
                  Expanded(child: Container(
                    decoration: _currentPage == 1
                    ? BoxDecoration(
                        borderRadius: const BorderRadius.all(Radius.circular(10)),
                        gradient: LinearGradient(
                            colors: [
                              HexColor.fromHex('#0067B4'),
                              HexColor.fromHex('#003457'),
                            ],
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter))
                        : null,
                    child: TextButton(
                      onPressed: (){
                        _pageController.animateToPage(1, duration: const Duration(milliseconds: 300), curve: Curves.decelerate);
                      },
                      child: FittedBox(
                        fit: BoxFit.scaleDown,
                        child: Text(
                          getTranslated(context, 'Basic_settings'),
                          textScaleFactor: 1,
                          style: TextStyle(
                            color: _currentPage == 1
                                ? Colors.white
                                : HexColor.fromHex('#032137')
                          ),
                        ),
                      ),
                    ),
                  ))
                ],
              ),
            ),
            Expanded(flex: 6,child: _pageView)
          ],
        ),
      ),
    );
  }

}