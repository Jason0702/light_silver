import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:light_silver/app.dart';
import 'package:light_silver/language/language_constants.dart';
import 'package:light_silver/models/member_model.dart';
import 'package:light_silver/models/user_company_information_model.dart';
import 'package:light_silver/utils/api.dart';
import 'package:light_silver/utils/shared_preference_util.dart';
import 'package:light_silver/utils/utils.dart';
import 'package:light_silver/widgets/noShadowScrollbehavior.dart';
import 'package:provider/provider.dart';

import '../view_model/company_info_vm.dart';
import '../view_model/member_vm.dart';
import '../view_model/user_company_information_vm.dart';

class RegisterPage extends StatefulWidget {
  @override
  State createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  MemberVm? _memberVm;
  CompanyInfoVm? _companyInfoVm;
  UserCompanyInformationVm? _userCompanyInformationVm;

  late SharedPreferenceUtil _sharedPreferenceUtil;

  final TextEditingController _name = TextEditingController();
  final TextEditingController _account = TextEditingController();
  final TextEditingController _password = TextEditingController();
  final TextEditingController _phone = TextEditingController();
  final TextEditingController _validate = TextEditingController();

  bool isShowPassword = false;
  bool _isReciprocal = false;
  bool _isClickSms = false;

  Timer? _timer;
  int _countdownTime = 0;

  final _registerBG = BoxDecoration(
      borderRadius: const BorderRadius.all(Radius.circular(5.0)),
      gradient: LinearGradient(colors: [
        HexColor.fromHex('#0067B4'),
        HexColor.fromHex('#003457'),
      ], begin: Alignment.topCenter, end: Alignment.bottomCenter));

  @override
  void initState() {
    super.initState();
    _sharedPreferenceUtil = SharedPreferenceUtil();
  }

  @override
  Widget build(BuildContext context) {
    _memberVm = Provider.of<MemberVm>(context);
    _companyInfoVm = Provider.of<CompanyInfoVm>(context);
    _userCompanyInformationVm = Provider.of<UserCompanyInformationVm>(context);
    return Scaffold(
      backgroundColor: Colors.transparent,
      resizeToAvoidBottomInset: true,
      body: SafeArea(
        child: ScrollConfiguration(
          behavior: NoShadowScrollBehavior(),
          child: SingleChildScrollView(
            child: Container(
              padding: const EdgeInsets.only(top: 23, left: 23, right: 23),
              child: buildSingInTextForm(),
            ),
          ),
        ),
      ),
    );
  }

  // 點擊控制密碼是否顯示
  void showPassWord() {
    setState(() {
      isShowPassword = !isShowPassword;
    });
  }

  Widget buildSingInTextForm() {
    return Container(
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(8)),
        color: Colors.white,
      ),
      height: 500,
      child: Column(
        children: [
          Container(
            alignment: Alignment.centerLeft,
            child: FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                getTranslated(context, 'Name'),
                textScaleFactor: 1,
                style: TextStyle(
                    fontSize: 20,
                    color: HexColor.fromHex('#1E2C39'),
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
          _buildName(),
          //帳號
          Container(
            alignment: Alignment.centerLeft,
            child: FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                getTranslated(context, 'Account'),
                textScaleFactor: 1,
                style: TextStyle(
                    fontSize: 20,
                    color: HexColor.fromHex('#1E2C39'),
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
          _buildAccount(),
          //密碼
          Container(
            alignment: Alignment.centerLeft,
            child: FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                getTranslated(context, 'Password'),
                textScaleFactor: 1,
                style: TextStyle(
                    fontSize: 20,
                    color: HexColor.fromHex('#1E2C39'),
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
          _buildPassword(),
          //行動電話
          Container(
            alignment: Alignment.centerLeft,
            child: FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                getTranslated(context, 'Phone'),
                textScaleFactor: 1,
                style: TextStyle(
                    fontSize: 20,
                    color: HexColor.fromHex('#1E2C39'),
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
          _buildPhone(),
          //驗證碼
          Container(
            alignment: Alignment.centerLeft,
            child: FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                getTranslated(context, 'Verification_code'),
                textScaleFactor: 1,
                style: TextStyle(
                    fontSize: 20,
                    color: HexColor.fromHex('#1E2C39'),
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
          _buildReferralCode(),
          const SizedBox(
            height: 10,
          ),
          Container(
            alignment: Alignment.center,
            child: ButtonTheme(
              height: 40,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                    foregroundColor: Colors.white,
                    backgroundColor: Colors.transparent,
                    shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0))),
                    padding: const EdgeInsets.all(0.0)),
                onPressed: _registerOnTap,
                child: Container(
                    height: 50,
                    width: 750,
                    alignment: Alignment.center,
                    decoration: _registerBG,
                    child: FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Text(
                        '${getTranslated(context, 'Confirm')}${getTranslated(context, 'Register')}',
                        textScaleFactor: 1,
                        style:
                            const TextStyle(color: Colors.white, fontSize: 21),
                      ),
                    )),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildName() {
    return TextField(
      style: const TextStyle(color: Colors.black),
      decoration: InputDecoration(
          errorText: null,
          contentPadding: const EdgeInsets.all(5.0),
          enabledBorder: OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.black),
              borderRadius: BorderRadius.circular(10.0)),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10.0)),
          filled: true,
          fillColor: Colors.white,
          hintText: getTranslated(context, 'Please_type_in_your_name'),
          hintStyle: const TextStyle(color: Colors.grey)),
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _name,
    );
  }

  Widget _buildAccount() {
    return TextField(
      style: const TextStyle(color: Colors.black),
      decoration: InputDecoration(
          prefixIcon: Container(
            padding: const EdgeInsets.only(left: 8.0),
            child: Icon(
              Icons.person,
              color: HexColor.fromHex('#1E2C39'),
              size: 25,
            ),
          ),
          errorText: null,
          contentPadding: const EdgeInsets.all(5.0),
          enabledBorder: OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.black),
              borderRadius: BorderRadius.circular(10.0)),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10.0)),
          filled: true,
          fillColor: Colors.white,
          hintText: getTranslated(context,
              'Please_enter_your_mobile_phone_number_or_company_system'),
          hintStyle: const TextStyle(color: Colors.grey)),
      keyboardType: TextInputType.number,
      textInputAction: TextInputAction.next,
      controller: _account,
    );
  }

  Widget _buildPassword() {
    return TextField(
      style: const TextStyle(color: Colors.black),
      decoration: InputDecoration(
          prefixIcon: Container(
            padding: const EdgeInsets.only(left: 8.0),
            child: Icon(
              Icons.lock,
              color: HexColor.fromHex('#1E2C39'),
              size: 25,
            ),
          ),
          suffixIcon: Container(
            padding: const EdgeInsets.only(right: 8.0),
            child: IconButton(
              icon: Icon(
                isShowPassword ? Icons.visibility : Icons.visibility_off,
                color: Colors.grey[600],
                size: 25,
              ),
              onPressed: () {
                setState(() {
                  isShowPassword = !isShowPassword;
                });
              },
            ),
          ),
          errorText: null,
          contentPadding: const EdgeInsets.all(10.0),
          enabledBorder: OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.black),
              borderRadius: BorderRadius.circular(10.0)),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
          filled: true,
          fillColor: Colors.white,
          hintText: getTranslated(context, 'Please_enter_alphanumeric'),
          hintStyle: const TextStyle(color: Colors.grey)),
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _password,
      obscureText: !isShowPassword,
    );
  }

  Widget _buildPhone() {
    return TextField(
      style: const TextStyle(color: Colors.black),
      decoration: InputDecoration(
          prefixIcon: Container(
            padding: const EdgeInsets.only(left: 8.0),
            child: Icon(
              Icons.phone,
              color: HexColor.fromHex('#1E2C39'),
              size: 25,
            ),
          ),
          errorText: null,
          contentPadding: const EdgeInsets.all(5.0),
          enabledBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.black),
            borderRadius: BorderRadius.circular(10.0),
          ),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10.0)),
          filled: true,
          fillColor: Colors.white,
          hintText: getTranslated(context, 'Please_enter_mobile_phone'),
          hintStyle: const TextStyle(color: Colors.grey)),
      keyboardType: TextInputType.phone,
      textInputAction: TextInputAction.next,
      controller: _phone,
    );
  }

  Widget _buildReferralCode() {
    return SizedBox(
      height: 50,
      child: Row(
        children: [
          Expanded(
            child: TextField(
              style: const TextStyle(color: Colors.black),
              decoration: InputDecoration(
                  prefixIcon: Container(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Image.asset(
                        'assets/images/gpp_good.png',
                        color: HexColor.fromHex('#1E2C39'),
                        width: 25,
                      )),
                  errorText: null,
                  contentPadding: const EdgeInsets.all(5.0),
                  enabledBorder: OutlineInputBorder(
                    borderSide: const BorderSide(color: Colors.black),
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                  filled: true,
                  fillColor: Colors.white,
                  hintText:
                      getTranslated(context, 'Please_enter_verification_code'),
                  hintStyle: const TextStyle(color: Colors.grey)),
              keyboardType: TextInputType.number,
              textInputAction: TextInputAction.next,
              controller: _validate,
            ),
          ),
          const SizedBox(
            width: 10,
          ),
          !_isClickSms
              ? InkWell(
                  onTap: _sendSms,
                  child: ButtonTheme(
                    height: 140,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.red,
                          shape: const RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5.0))),
                          padding: const EdgeInsets.all(10.0)),
                      onPressed: !_isReciprocal
                          ? () {
                              _reciprocal();
                              _sendSms();
                            }
                          : null,
                      child: FittedBox(
                        fit: BoxFit.scaleDown,
                        child: Text(
                            _countdownTime > 0
                                ? '$_countdownTime後重新獲取'
                                : '發送驗證碼',
                            textScaleFactor: 1,
                            style: const TextStyle(
                                fontSize: 17, color: Colors.white)),
                      ),
                    ),
                  ),
                )
              : Container()
        ],
      ),
    );
  }

  void _registerOnTap() async {
    debugPrint('註冊');
    if (_name.text.isEmpty) {
      EasyLoading.showToast(getTranslated(context, 'Please_type_in_your_name'));
      return;
    }
    if (_account.text.isEmpty) {
      EasyLoading.showToast(getTranslated(
          context, 'Please_enter_your_mobile_phone_number_or_company_system'));
    }
    if (_password.text.isEmpty) {
      EasyLoading.showToast(
          getTranslated(context, 'Please_enter_the_password'));
    }
    if (_validate.text.isEmpty) {
      EasyLoading.showToast(
          getTranslated(context, 'Please_fill_in_the_verification_code_first'));
      return;
    }
    EasyLoading.show(status: '${getTranslated(context, 'Verifying')}..');
    try {
      dynamic response = await httpUtils.put(
        '$putVreify${_account.text}',
        data: {'account': _account.text, 'verification_code': _validate.text},
      );
      EasyLoading.dismiss();
      EasyLoading.showToast(
          '${getTranslated(context, 'Register')}${getTranslated(context, 'success')}!');
      _login();
    } on DioError catch (e) {
      EasyLoading.dismiss();
      EasyLoading.showError(e.message);
      debugPrint('註冊驗證Error: ${e.message}');
    }
  }

  void _sendSms() async {
    if (_account.text.isEmpty ||
        _name.text.isEmpty ||
        _phone.text.isEmpty ||
        _password.text.isEmpty) {
      EasyLoading.showToast('${getTranslated(context, 'Incomplete')}!');
      return;
    }
    EasyLoading.show(
        status:
            '${getTranslated(context, 'send')}${getTranslated(context, 'Verification_code')}');
    setState(() {
      _isClickSms = true;
    });
    try {
      dynamic response = await httpUtils.post(
        postSingUp,
        data: {
          'account': _account.text,
          'name': _name.text,
          'cellphone': _phone.text,
          'password': _password.text,
          'gender': 'MALE',
          'Platform': Platform.isIOS ? 0 : 1
        },
      );
      _validate.text = response;
      setState(() {});
      EasyLoading.dismiss();
      EasyLoading.showToast(
          '${getTranslated(context, 'send')}${getTranslated(context, 'success')}!');
    } on DioError catch (e) {
      EasyLoading.dismiss();
      EasyLoading.showError(e.message);
      debugPrint('註冊Error: ${e.message}');
    }
  }

  void _login() async {
    try {
      dynamic response = await httpUtils.post(
        postAuth,
        data: {
          'account': _account.text,
          'password': _password.text,
        },
      );
      log('登入Data: $response');
      _sharedPreferenceUtil.saveAccount(_account.text);
      _sharedPreferenceUtil.savePassword(_password.text);
      accessToken = response['token']['access_token'];
      id = response['id'];

      _getMember(id);
    } on DioError catch (e) {
      EasyLoading.dismiss();
      EasyLoading.showError(e.message!);
      debugPrint('登入Error: ${e.message}');
    }
  }

  //取得用戶資料
  void _getMember(int id) async {
    try{
      dynamic response = await httpUtils.get(
        '$getMember$id',
        options: Options(
            contentType: 'application/json',
            responseType: ResponseType.json,
            headers: {"authorization": "Bearer $accessToken"}),
      );
      log('用戶資訊 $response');
      _memberVm!.setMemberModel(
          MemberModel.fromJson(json.decode(json.encode(response))));
      _getUserData(_memberVm!.memberModel!.usersId);
      EasyLoading.dismiss();
      EasyLoading.showToast(
          '${getTranslated(context, 'Sing_in')}${getTranslated(context, 'success')}!');
      delegate.popUtil();
    } on DioError catch(e){
      EasyLoading.dismiss();
      EasyLoading.showError(e.message);
      debugPrint('用戶資料Error: ${e.message}');
    }
  }

  //取得用戶所屬公司資料
  void _getUserData(int id) async{
    if (id == 1) {
      _userCompanyInformationVm!
          .setName(_companyInfoVm!.companyInfoModel!.name);
      _userCompanyInformationVm!
          .setPhone(_companyInfoVm!.companyInfoModel!.phone);
      _userCompanyInformationVm!
          .setAddress(_companyInfoVm!.companyInfoModel!.address);
      _userCompanyInformationVm!
          .setLogo("https://jg-hp.com/apps/adminn/assets/img/brand/logo.png");
      return;
    }
    try{
      dynamic response = await httpUtils.get(
        '$getUserData$id',
        options: Options(
            contentType: 'application/json',
            responseType: ResponseType.json,
            headers: {"authorization": "Bearer $accessToken"}),
      );
      debugPrint('所屬公司資料: $response');
      _userCompanyInformationVm!.setModel(
          UserCompanyInformationModel.fromJson(
              json.decode(json.encode(response))));
    } on DioError catch (e){
      debugPrint('所屬公司資料Error: ${e.toString()}');
    }
  }

  //倒數
  void _reciprocal() {
    if (_countdownTime == 0 && !_isReciprocal) {
      setState(() {
        _countdownTime = 60;
        _isReciprocal = true;
      });
      startCountdownTimer();
    }
  }

  void startCountdownTimer() {
    const oneSec = Duration(seconds: 1);

    callback(timer) => {
          if (mounted)
            {
              setState(() {
                if (_countdownTime < 1) {
                  _timer!.cancel();
                  _isReciprocal = false;
                } else {
                  _countdownTime = _countdownTime - 1;
                }
              })
            }
        };
    _timer = Timer.periodic(oneSec, callback);
  }
}
