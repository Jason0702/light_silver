import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';

import '../app.dart';

class NetWorkImagePage extends StatelessWidget {
  final String imageUrl;

  const NetWorkImagePage({Key? key, required this.imageUrl}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    debugPrint(imageUrl);
    return SafeArea(
        child: Material(
      color: Colors.white,
      child: Container(
        color: Colors.white,
        constraints: BoxConstraints.expand(
          height: MediaQuery.of(context).size.height,
        ),
        child: Stack(
          children: [
            PhotoView(
              imageProvider: NetworkImage(imageUrl),
              backgroundDecoration: const BoxDecoration(
                color: Colors.white,
              ),
            ),
            Positioned(
                top: 5,
                left: 5,
                child: IconButton(
                  icon: const Icon(
                    Icons.arrow_back_ios,
                    color: Colors.black,
                  ),
                  onPressed: () => delegate.popRoute(),
                )),
          ],
        ),
      ),
    ));
  }
}
