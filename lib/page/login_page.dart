import 'dart:convert';
import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:light_silver/language/language_constants.dart';
import 'package:light_silver/models/member_model.dart';
import 'package:light_silver/models/user_company_information_model.dart';
import 'package:light_silver/utils/api.dart';
import 'package:light_silver/utils/shared_preference_util.dart';
import 'package:light_silver/utils/utils.dart';
import 'package:light_silver/widgets/noShadowScrollbehavior.dart';
import 'package:provider/provider.dart';

import '../app.dart';
import '../routes/route_name.dart';
import '../view_model/company_info_vm.dart';
import '../view_model/member_vm.dart';
import '../view_model/user_company_information_vm.dart';

class LoginPage extends StatefulWidget {
  @override
  State createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  MemberVm? _memberVm;
  CompanyInfoVm? _companyInfoVm;
  UserCompanyInformationVm? _userCompanyInformationVm;
  final TextEditingController _account = TextEditingController();
  final TextEditingController _password = TextEditingController();

  bool isShowPassword = false;
  bool _checkboxSelected = false;

  late SharedPreferenceUtil _sharedPreferenceUtil;

  final _loginBG = BoxDecoration(
      borderRadius: const BorderRadius.all(Radius.circular(5.0)),
      gradient: LinearGradient(colors: [
        HexColor.fromHex('#0067B4'),
        HexColor.fromHex('#003457'),
      ], begin: Alignment.topCenter, end: Alignment.bottomCenter));

  _onLayoutDone(_) {
    _sharedPreferenceUtil.getAccount().then((value) => {
          if (value.isNotEmpty) {_initData(value)}
        });
  }

  void _initData(String value) {
    setState(() {
      _account.text = value;
      _checkboxSelected = true;
    });
  }

  @override
  void initState() {
    super.initState();
    _sharedPreferenceUtil = SharedPreferenceUtil();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  Widget build(BuildContext context) {
    _memberVm = Provider.of<MemberVm>(context);
    _companyInfoVm = Provider.of<CompanyInfoVm>(context);
    _userCompanyInformationVm = Provider.of<UserCompanyInformationVm>(context);
    return Scaffold(
        backgroundColor: Colors.transparent,
        resizeToAvoidBottomInset: true,
        body: SafeArea(
          child: ScrollConfiguration(
            behavior: NoShadowScrollBehavior(),
            child: SingleChildScrollView(
              child: Container(
                padding: const EdgeInsets.only(
                    top: 15, left: 20, right: 20, bottom: 0),
                child: buildSingInTextForm(),
              ),
            ),
          ),
        ));
  }

  // 點擊控制密碼是否顯示
  void showPassWord() {
    setState(() {
      isShowPassword = !isShowPassword;
    });
  }

  Widget buildSingInTextForm() {
    return Container(
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(8)),
        color: Colors.white,
      ),
      width: 800,
      height: MediaQuery.of(context).size.height / 2,
      child: Column(children: [
        const SizedBox(
          height: 20,
        ),
        Container(
          alignment: Alignment.centerLeft,
          child: FittedBox(
            fit: BoxFit.scaleDown,
            child: Text(
              getTranslated(context, 'Account'),
              textScaleFactor: 1,
              style: TextStyle(
                  fontSize: 20,
                  color: HexColor.fromHex('#1E2C39'),
                  fontWeight: FontWeight.bold),
            ),
          ),
        ),
        _buildAccount(),
        const SizedBox(
          height: 20,
        ),
        Container(
          alignment: Alignment.centerLeft,
          child: FittedBox(
            fit: BoxFit.scaleDown,
            child: Text(
              getTranslated(context, 'Password'),
              textScaleFactor: 1,
              style: TextStyle(
                  fontSize: 20,
                  color: HexColor.fromHex('#1E2C39'),
                  fontWeight: FontWeight.bold),
            ),
          ),
        ),
        _buildPassword(),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                Checkbox(
                    value: _checkboxSelected,
                    activeColor: HexColor.fromHex('#1E2C39'),
                    checkColor: Colors.white,
                    onChanged: (value) {
                      setState(() {
                        _checkboxSelected = value!;
                      });
                    }),
                FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    getTranslated(context, 'Remember_my_account'),
                    textScaleFactor: 1,
                    style: TextStyle(
                        color: HexColor.fromHex('#1E2C39'), fontSize: 17),
                  ),
                ),
              ],
            ),
            InkWell(
              onTap: _forgotPasswordOnTap,
              child: Row(
                children: [
                  const Icon(
                    Icons.help,
                    size: 25,
                  ),
                  FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Text(
                        '${getTranslated(context, 'Forgot_password')}?',
                        textScaleFactor: 1,
                      ))
                ],
              ),
            )
          ],
        ),
        Expanded(
            child: Container(
          alignment: Alignment.center,
          child: ButtonTheme(
            height: 110,
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  foregroundColor: Colors.white,
                  backgroundColor: Colors.transparent,
                  shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(5.0))),
                  padding: const EdgeInsets.all(0.0)),
              onPressed: _loginOnTap,
              child: Container(
                  height: 50,
                  width: 750,
                  alignment: Alignment.center,
                  decoration: _loginBG,
                  child: FittedBox(
                    child: Text(
                      getTranslated(context, 'Sing_in'),
                      textScaleFactor: 1,
                      style: const TextStyle(color: Colors.white, fontSize: 21),
                    ),
                  )),
            ),
          ),
        ))
      ]),
    );
  }

  void _loginOnTap() async{
    debugPrint('登入');
    if (_account.text.isEmpty || _password.text.isEmpty) {
      if (_account.text.isEmpty) {
        EasyLoading.showToast(getTranslated(context,
            'Please_enter_your_mobile_phone_number_or_company_system'));
      }
      if (_password.text.isEmpty) {
        EasyLoading.showToast(
            getTranslated(context, 'Please_enter_the_password'));
      }
      return;
    }
    EasyLoading.show(status: '${getTranslated(context, 'Signing_in')}..');
    try{
      dynamic response = await httpUtils.post(
        postAuth,
        data: {
          'account': _account.text,
          'password': _password.text,
        },
      );
      log('登入Data: $response');
      _sharedPreferenceUtil.saveAccount(_account.text);
      _sharedPreferenceUtil.savePassword(_password.text);
      accessToken = response['token']['access_token'];
      _getMember(response['id']);
    } on DioError catch (e){
      EasyLoading.dismiss();
      EasyLoading.showError(e.message);
      debugPrint('登入Error: ${e.message}');
    }
  }

  //取得用戶資料
  void _getMember(int id) async{
    try{
      dynamic response = await httpUtils.get(
        '$getMember$id',
        options: Options(
            contentType: 'application/json',
            responseType: ResponseType.json,
            headers: {"authorization": "Bearer $accessToken"}),
      );
      log('用戶資訊 $response');
      _memberVm!.setMemberModel(MemberModel.fromJson(json.decode(json.encode(response))));
      _getUserData(_memberVm!.memberModel!.usersId);
      EasyLoading.dismiss();
      EasyLoading.showToast(
          '${getTranslated(context, 'Sing_in')}${getTranslated(context, 'success')}!');
      delegate.popRoute();
    } on DioError catch (e) {
      EasyLoading.dismiss();
      EasyLoading.showError(e.message);
      debugPrint('用戶資料Error: ${e.message}');
    }
  }

  //取得用戶所屬公司資料
  void _getUserData(int id) async{
    if (id == 1) {
      _userCompanyInformationVm!.initModel();
      _userCompanyInformationVm!.setName(_companyInfoVm!.companyInfoModel!.name);
      _userCompanyInformationVm!.setPhone(_companyInfoVm!.companyInfoModel!.phone);
      _userCompanyInformationVm!.setAddress(_companyInfoVm!.companyInfoModel!.address);
      _userCompanyInformationVm!.setLogo("https://jg-hp.com/apps/adminn/assets/img/brand/logo.png");
      return;
    }
    try{
      dynamic response = await httpUtils.get(
        '$getUserData$id',
        options: Options(
            contentType: 'application/json',
            responseType: ResponseType.json,
            headers: {"authorization": "Bearer $accessToken"}),
      );
      debugPrint('所屬公司資料: $response');
      _userCompanyInformationVm!.setModel(UserCompanyInformationModel.fromJson(
          json.decode(json.encode(response))));
    } on DioError catch (e){
      debugPrint('所屬公司資料Error: ${e.toString()}');
    }
  }

  void _forgotPasswordOnTap() {
    debugPrint('忘記密碼');
    delegate.push(name: RouteName.forgotPassword);
  }

  Widget _buildAccount() {
    return TextField(
      style: const TextStyle(color: Colors.black),
      decoration: InputDecoration(
          prefixIcon: Container(
            padding: const EdgeInsets.only(left: 8.0),
            child: Icon(
              Icons.person,
              color: HexColor.fromHex('#1E2C39'),
              size: 25,
            ),
          ),
          errorText: null,
          contentPadding: const EdgeInsets.all(5.0),
          enabledBorder: OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.black),
              borderRadius: BorderRadius.circular(10.0)),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10.0)),
          filled: true,
          fillColor: Colors.white,
          hintText: getTranslated(context, 'Please_enter_your_account'),
          hintStyle: const TextStyle(color: Colors.grey)),
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _account,
    );
  }

  Widget _buildPassword() {
    return TextField(
      style: const TextStyle(color: Colors.black),
      decoration: InputDecoration(
          prefixIcon: Container(
            padding: const EdgeInsets.only(left: 8.0),
            child: Icon(
              Icons.lock,
              color: HexColor.fromHex('#1E2C39'),
              size: 25,
            ),
          ),
          suffixIcon: Container(
            padding: const EdgeInsets.only(right: 8.0),
            child: IconButton(
              icon: Icon(
                isShowPassword ? Icons.visibility : Icons.visibility_off,
                color: Colors.grey[600],
                size: 25,
              ),
              onPressed: () {
                setState(() {
                  isShowPassword = !isShowPassword;
                });
              },
            ),
          ),
          errorText: null,
          contentPadding: const EdgeInsets.all(10.0),
          enabledBorder: OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.black),
              borderRadius: BorderRadius.circular(10.0)),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
          filled: true,
          fillColor: Colors.white,
          hintText: getTranslated(context, 'Please_enter_the_password'),
          hintStyle: const TextStyle(color: Colors.grey)),
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _password,
      obscureText: !isShowPassword,
    );
  }
}
