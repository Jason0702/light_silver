import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:light_silver/app.dart';
import 'package:light_silver/database/database_helper.dart';
import 'package:light_silver/language/language_constants.dart';
import 'package:light_silver/models/operation_item_model.dart';
import 'package:light_silver/models/patient.dart';
import 'package:light_silver/routes/route_name.dart';
import 'package:light_silver/utils/api.dart';
import 'package:light_silver/utils/shared_preference_util.dart';
import 'package:light_silver/utils/utils.dart';
import 'package:light_silver/widgets/button.dart';
import 'package:light_silver/widgets/left_button.dart';
import 'package:light_silver/widgets/noShadowScrollbehavior.dart';
import 'package:provider/provider.dart';

import '../../../view_model/operation_item_vm.dart';
import '../../../view_model/patient_vm.dart';
import '../../../widgets/title_widget.dart';
import '../../network_image_page.dart';
import '../video_palyer_screen.dart';

class LBk160Routes extends StatefulWidget {
  @override
  State createState() => _LBk160RoutesState();
}

class _LBk160RoutesState extends State<LBk160Routes> {
  PatientVm? _patientVm;
  OperationItemVm? _operationItemVm;

  final TextEditingController _iliumSide = TextEditingController();
  final TextEditingController _iliumRotation = TextEditingController();

  OperationItemsModel? imageList;
  String netUpImageUrl = '';
  String netDownImageUrl = '';
  String netVideoUrl = '';

  _onLayoutDone(_) {
    List list =
    _operationItemVm!.operationItemsModelList!.where((element) => element.id == 8).toList();
    for (var element in list) {
      imageList = element;
    }
    _dataInit(_patientVm!.patient);
  }

  void _dataInit(Patient? patient) {
    if (patient == null) {
      return;
    }
    log('取得 仰躺(6): $patient');
    //before 前  after 後
    if (patient.gillet == 1) {
      _iliumSide.text = getTranslated(context, 'left');
    }
    if (patient.gillet == 2) {
      _iliumSide.text = getTranslated(context, 'right');
    }
    if (patient.gillet == 1 && patient.supine_ilium_leg_length == 1) {
      //後
      _iliumRotation.text = getTranslated(context, 'after');
    }
    if (patient.gillet == 1 && patient.supine_ilium_leg_length == 3) {
      //前
      _iliumRotation.text = getTranslated(context, 'before');
    }
    if (patient.gillet == 2 && patient.supine_ilium_leg_length == 1) {
      //前
      _iliumRotation.text = getTranslated(context, 'before');
    }
    if (patient.gillet == 2 && patient.supine_ilium_leg_length == 3) {
      //後
      _iliumRotation.text = getTranslated(context, 'after');
    }

    //圖片
    if (patient.gillet == 1 && patient.supine_ilium_leg_length == 1) {
      //左後
      debugPrint(imageList!.completePics
          .where((element) => element.containsKey('左_後'))
          .toString());
      debugPrint(imageList!.completeVideos
          .where((element) => element.containsKey('左_後'))
          .toString());
      Map<String, dynamic> map = imageList!.completePics
          .where((element) => element.containsKey('左_後'))
          .first;
      Map<String, CompleteVideo> videoMap = imageList!.completeVideos
          .where((element) => element.containsKey('左_後'))
          .first;
      netUpImageUrl = map.values.first;
      netDownImageUrl = videoMap.values.first.thumbnail;
      netVideoUrl = videoMap.values.first.src;
    }
    if (patient.gillet == 1 && patient.supine_ilium_leg_length == 3) {
      //左前
      debugPrint(imageList!.completePics
          .where((element) => element.containsKey('左_前'))
          .toString());
      debugPrint(imageList!.completeVideos
          .where((element) => element.containsKey('左_前'))
          .toString());
      Map<String, dynamic> map = imageList!.completePics
          .where((element) => element.containsKey('左_前'))
          .first;
      Map<String, CompleteVideo> videoMap = imageList!.completeVideos
          .where((element) => element.containsKey('左_前'))
          .first;
      netUpImageUrl = map.values.first;
      netDownImageUrl = videoMap.values.first.thumbnail;
      netVideoUrl = videoMap.values.first.src;
    }
    if (patient.gillet == 2 && patient.supine_ilium_leg_length == 1) {
      //右前
      debugPrint(imageList!.completePics
          .where((element) => element.containsKey('右_前'))
          .toString());
      debugPrint(imageList!.completeVideos
          .where((element) => element.containsKey('右_前'))
          .toString());
      Map<String, dynamic> map = imageList!.completePics
          .where((element) => element.containsKey('右_前'))
          .first;
      Map<String, CompleteVideo> videoMap = imageList!.completeVideos
          .where((element) => element.containsKey('右_前'))
          .first;
      netUpImageUrl = map.values.first;
      netDownImageUrl = videoMap.values.first.thumbnail;
      netVideoUrl = videoMap.values.first.src;
    }
    if (patient.gillet == 2 && patient.supine_ilium_leg_length == 3) {
      //右後
      debugPrint(imageList!.completePics
          .where((element) => element.containsKey('右_後'))
          .toString());
      debugPrint(imageList!.completeVideos
          .where((element) => element.containsKey('右_後'))
          .toString());
      Map<String, dynamic> map = imageList!.completePics
          .where((element) => element.containsKey('右_後'))
          .first;
      Map<String, CompleteVideo> videoMap = imageList!.completeVideos
          .where((element) => element.containsKey('右_後'))
          .first;
      netUpImageUrl = map.values.first;
      netDownImageUrl = videoMap.values.first.thumbnail;
      netVideoUrl = videoMap.values.first.src;
    }
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  Widget build(BuildContext context) {
    _patientVm = Provider.of<PatientVm>(context);
    _operationItemVm = Provider.of<OperationItemVm>(context);
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Column(
          children: [
            Expanded(flex: 1, child: _titleBar()),
            Expanded(flex: 10, child: _bodyWidget())
          ],
        ),
      ),
    );
  }

  //最上層標題與按鈕
  Widget _titleBar() {
    return LbkTenTitleWidget(
      titleText:  '${getTranslated(context, 'Lie_on_your_back')}(6)',
      leftButton: const LeftButton(),
      rightButton: _rightTopButton(),
    );
  }

  //右側按鈕
  Widget _rightTopButton() {
    return InkWell(
      onTap: () => delegate.popUtil(),
      child: const Icon(
        Icons.home,
        color: Colors.white,
        size: 45,
      ),
    );
  }

//主體
  Widget _bodyWidget() {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 10),
      decoration: Style.homeBg,
      alignment: Alignment.center,
      child: Container(
        width: MediaQuery.of(context).size.width / 1.1,
        decoration: Style.centerBG,
        child: ScrollConfiguration(
          behavior: NoShadowScrollBehavior(),
          child: SingleChildScrollView(
            child: Column(
              children: [
                //主體內部
                _bodyTopWidget(),
                //題目
                _topicWidget(),
                //顯示數值
                _showTextWidget(),
                //圖片
                _imageWidget(),
                const SizedBox(
                  height: 10,
                ),
                //影片與圖片
                _videoWidget(),
                //按鈕
                _bottomButtonWidget(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  //主體內部
  Widget _bodyTopWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 20, left: 50, right: 50),
      child: Column(
        children: [
          FittedBox(
            fit: BoxFit.scaleDown,
            child: Text(
              getTranslated(
                  context, 'Evaluation_and_adjustment_of_iliac_problems'),
              textScaleFactor: 1,
              style: const TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Container(
            height: 3,
            color: HexColor.fromHex('#D66C2C'),
          )
        ],
      ),
    );
  }

  //題目
  Widget _topicWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 10, left: 25),
      child: Row(
        children: [
          Container(
            width: 40,
            height: 40,
            alignment: Alignment.center,
            decoration: BoxDecoration(
                color: HexColor.fromHex('#1E2C39'),
                borderRadius: const BorderRadius.all(Radius.circular(25))),
            child: const FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                '6',
                textScaleFactor: 1,
                style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    color: Colors.white),
              ),
            ),
          ),
          const SizedBox(
            width: 20,
          ),
          Expanded(
            child: FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                getTranslated(
                    context, 'Tips_for_handling_the_problem_of_iliac_rotation'),
                textScaleFactor: 1,
                style:
                    const TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
              ),
            ),
          )
        ],
      ),
    );
  }

  //顯示數值
  Widget _showTextWidget() {
    return Container(
      padding: const EdgeInsets.only(left: 25),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              Container(
                margin: const EdgeInsets.all(8),
                width: 70,
                child: TextField(
                  readOnly: true,
                  textAlign: TextAlign.center,
                  style: TextStyle(color: HexColor.fromHex('#096BC7')),
                  decoration: const InputDecoration(
                    contentPadding: EdgeInsets.all(5),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black)),
                    filled: true,
                    fillColor: Colors.white,
                  ),
                  keyboardType: TextInputType.text,
                  textInputAction: TextInputAction.done,
                  controller: _iliumSide,
                ),
              ),
              //側
              FittedBox(
                fit: BoxFit.scaleDown,
                child: Text(
                  getTranslated(context, 'side'),
                  textScaleFactor: 1,
                  style: const TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 21),
                ),
              ),
            ],
          ),
          Row(
            children: [
              //髂骨向
              FittedBox(
                fit: BoxFit.scaleDown,
                child: Text(
                  getTranslated(context, 'Iliac_direction'),
                  textScaleFactor: 1,
                  style: const TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 21),
                ),
              ),
              //
              Container(
                margin: const EdgeInsets.all(8),
                width: 70,
                child: TextField(
                  readOnly: true,
                  textAlign: TextAlign.center,
                  style: TextStyle(color: HexColor.fromHex('#096BC7')),
                  decoration: const InputDecoration(
                    contentPadding: EdgeInsets.all(5),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black)),
                    filled: true,
                    fillColor: Colors.white,
                  ),
                  keyboardType: TextInputType.text,
                  textInputAction: TextInputAction.done,
                  controller: _iliumRotation,
                ),
              ),
              //旋
              FittedBox(
                fit: BoxFit.scaleDown,
                child: Text(
                  '${getTranslated(context, 'turn')}(${getTranslated(context, 'pour')})',
                  textScaleFactor: 1,
                  style: const TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 21),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  //圖片
  Widget _imageWidget() {
    debugPrint('圖片網址: $imageUrlOffline$imageUrlPublic$netUpImageUrl');
    return Container(
        width: 300,
        height: 200,
        decoration: BoxDecoration(
            borderRadius: const BorderRadius.all(Radius.circular(15)),
            border: Border.all(color: Colors.black, width: 1)),
        child: Stack(
          alignment: Alignment.center,
          children: [
            Image(
              image:
                  NetworkImage('$imageUrlOffline$imageUrlPublic$netUpImageUrl'),
            ),
            Positioned(
              bottom: 10,
              right: 10,
              child: InkWell(
                  onTap: _upImageOnTap,
                  child: const Icon(
                    Icons.zoom_in,
                    size: 45,
                  )),
            )
          ],
        ));
  }

  //影片與圖片
  Widget _videoWidget() {
    return Container(
      width: 300,
      height: 180,
      decoration: BoxDecoration(
          image: DecorationImage(
              image: NetworkImage(
                  '$imageUrlOffline$imageUrlPublic$netDownImageUrl'),
              fit: BoxFit.fill),
          borderRadius: const BorderRadius.all(Radius.circular(15)),
          border: Border.all(color: Colors.black, width: 1)),
      child: Stack(
        alignment: Alignment.center,
        children: [
          Positioned(
              bottom: 0,
              right: 0,
              child: Row(
                children: [
                  InkWell(
                    onTap: _videoOnTap,
                    child: Container(
                      decoration: const BoxDecoration(
                          color: Colors.black,
                          borderRadius: BorderRadius.all(Radius.circular(25))),
                      child: const Icon(
                        Icons.arrow_right,
                        size: 30,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 5,
                  ),
                  InkWell(
                    onTap: _downImageOnTap,
                    child: const Icon(Icons.zoom_in, size: 45),
                  )
                ],
              ))
        ],
      ),
    );
  }

  //下面影片
  void _videoOnTap() {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) {
      return VideoPlayerScreen(
        videoUrl: '$imageUrlOffline$imageUrlPublic$netVideoUrl',
      );
    }));
  }

  //下面圖片放大
  void _downImageOnTap() {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) {
      return NetWorkImagePage(
          imageUrl: '$imageUrlOffline$imageUrlPublic$netDownImageUrl');
    }));
  }

  //上面圖片放大
  void _upImageOnTap() {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) {
      return NetWorkImagePage(
          imageUrl: '$imageUrlOffline$imageUrlPublic$netUpImageUrl');
    }));
  }

  //按鈕
  Widget _bottomButtonWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 10, right: 10, bottom: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          //下一步
          NextButton(
              click: _nextOnTap,
              buttonText: getTranslated(context, 'Next_step'),
              decoration: Style.lbkTenNextBG),
        ],
      ),
    );
  }

  //下一步
  void _nextOnTap() {
    delegate.push(name: RouteName.lbk16);
  }
}
