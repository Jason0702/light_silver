import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:light_silver/app.dart';
import 'package:light_silver/language/language_constants.dart';
import 'package:light_silver/models/patient.dart';
import 'package:light_silver/routes/route_name.dart';
import 'package:light_silver/utils/utils.dart';
import 'package:light_silver/widgets/title_widget.dart';
import 'package:provider/provider.dart';

import '../../../view_model/patient_vm.dart';
import '../../../widgets/button.dart';

class LBk04Routes extends StatefulWidget{

  @override
  State createState() => _LBk04RoutesState();
}
class _LBk04RoutesState extends State<LBk04Routes>{
  PatientVm? _patientVm;
  int _supineLegLength = 0;

  _onLayoutDone(_){
    _dataInit(_patientVm!.patient);
  }

  void _dataInit(Patient? patient){
    if (patient == null) {
      return;
    }
    log('取得 仰躺: $patient');
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  Widget build(BuildContext context) {
    _patientVm = Provider.of<PatientVm>(context);
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Column(
          children: [
            Expanded(flex: 1, child: _titleBar()),
            Expanded(flex: 10, child: _bodyWidget())
          ],
        ),
      ),
    );
  }

  //最上層標題
  Widget _titleBar(){
    return LbkTitleWidget(
      titleText: getTranslated(context, 'Lie_on_your_back'),
    );
  }

  //主體
  Widget _bodyWidget(){
    return Stack(
      children: [
        Container(
          padding: const EdgeInsets.symmetric(vertical: 10),
          decoration: Style.homeBg,
          alignment: Alignment.center,
          child: Container(
            width: MediaQuery.of(context).size.width / 1.1,
            decoration: Style.centerBG,
            child: Column(
              children: [
                //主體內部
                _bodyTopWidget(),
                //題目
                //_topicWidget(),
                //提示
                //_promptWidget(),
                //重新測試
                _retestWidget(),
                //左或右
                _leftOrRightWidget(),
                const Spacer(),
                //按鈕
                _bottomButtonWidget(),
              ],
            ),
          ),
        )
      ],
    );
  }

  //主體內部
  Widget _bodyTopWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 20, left: 50, right: 50),
      child: Column(
        children: [
          FittedBox(
            fit:BoxFit.scaleDown,
            child: Text(
              getTranslated(
                  context, 'Please_reconfirm_whether_the_length_of_your_feet_is_the_same'),
              textScaleFactor: 1,
              style: const TextStyle(fontSize: 24, fontWeight: FontWeight.bold, color: Colors.red),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          /*Container(
            height: 3,
            color: HexColor.fromHex('#4A991E'),
          )*/
        ],
      ),
    );
  }
  //重新測試
  Widget _retestWidget(){
    return Container(
      margin: const EdgeInsets.only(top: 10,left: 30, right: 30),
      child: ElevatedButton(
          style: ElevatedButton.styleFrom(
              foregroundColor: Colors.white,
              backgroundColor: Colors.transparent,
              shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10.0))),
              padding: const EdgeInsets.all(0.0)
          ),
          onPressed: _retestOnTap,
          child: Container(
            height: 60,
            decoration: Style.lbkNextBG,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                FittedBox(
                  fit:BoxFit.scaleDown,
                  child: Text(
                    getTranslated(context, 'Retest'),
                    textScaleFactor: 1,
                    style: const TextStyle(fontSize: 23),
                  ),
                ),
                const SizedBox(
                  width: 15,
                ),
                const Icon(
                  Icons.refresh,
                  size: 25,
                ),
              ],
            ),
          )),
    );
  }
  //左或右
  Widget _leftOrRightWidget() {
    return Column(
      children: [
        //灰色分隔線
        Container(
          margin: const EdgeInsets.only(top: 20),
          height: 5,
          color: Colors.grey[200],
        ),
        //或
        Container(
          margin: const EdgeInsets.only(top: 20),
          child: FittedBox(
            fit:BoxFit.scaleDown,
            child: Text(
              getTranslated(context, 'or'),
              textScaleFactor: 1,
              style: const TextStyle(
                  fontSize: 23,
                  fontWeight: FontWeight.bold,
                  color: Colors.black
              ),
            ),
          ),
        ),
        //髂骨問題側
        Container(
          margin: const EdgeInsets.only(left: 25, right: 25),
          alignment: Alignment.centerLeft,
          child: FittedBox(
            fit:BoxFit.scaleDown,
            child: Text(
              getTranslated(context, 'The_longer_feet_are'),
              textScaleFactor: 1,
              style: const TextStyle(
                  fontSize: 23,
                  fontWeight: FontWeight.bold
              ),),
          ),
        ),
        //左或右
        Container(
          height: 50,
          margin: const EdgeInsets.only(top: 20, left: 25, right: 25),
          child: Row(
            children: [
              Expanded(
                  flex: 1,
                  child: InkWell(
                    onTap: _leftSideOnTap,
                    child: Container(
                      alignment: Alignment.center,
                      decoration: Style.imageButtonBG,
                      child: FittedBox(
                        fit:BoxFit.scaleDown,
                        child: Text(
                          getTranslated(context, 'Left_side'),
                          textScaleFactor: 1,
                          style: const TextStyle(
                            fontSize: 23,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  )),
              const SizedBox(
                width: 10,
              ),
              Expanded(
                  flex: 1,
                  child: InkWell(
                    onTap: _rightSideOnTap,
                    child: Container(
                      alignment: Alignment.center,
                      decoration: Style.imageButtonBG,
                      child: FittedBox(
                        fit:BoxFit.scaleDown,
                        child: Text(
                          getTranslated(context, 'Right_side'),
                          textScaleFactor: 1,
                          style: const TextStyle(
                            fontSize: 23,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ))
            ],
          ),
        )
      ],
    );
  }

  //按鈕
  Widget _bottomButtonWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 10,right: 10, bottom: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          //下一步
          NextButton(
              click: _nextOnTap,
              buttonText: getTranslated(context, 'Next_step'),
              decoration: Style.lbkNextBG),
        ],
      ),
    );
  }

  //左側
  void _leftSideOnTap(){
    setState(() {
      _supineLegLength = 1;
    });
  }
  //右側
  void _rightSideOnTap(){
    setState(() {
      _supineLegLength = 2;
    });
  }

  //重新測試
  void _retestOnTap(){
    delegate.push(name: RouteName.lbk01);
  }
  //下一步
  void _nextOnTap(){
    editPatient();
    if (_patientVm!.patient!.gillet == 1 &&
        _patientVm!.patient!.IC == 3 &&
        _patientVm!.patient!.ASIS == 3 &&
        _patientVm!.patient!.PSIS == 3) {
      delegate.push(name: RouteName.sps41);
    } else if (_patientVm!.patient!.gillet == 2 &&
        _patientVm!.patient!.IC == 1 &&
        _patientVm!.patient!.ASIS == 1 &&
        _patientVm!.patient!.PSIS == 1) {
      delegate.push(name: RouteName.sps41);
    } else {
      delegate.push(name: RouteName.sps01);
    }
  }

  void editPatient(){
    Patient patient = Patient(
        name: _patientVm!.patient!.name,
        gender: _patientVm!.patient!.gender,
        age: _patientVm!.patient!.age,
        phone: _patientVm!.patient!.phone,
        problem: _patientVm!.patient!.problem,
        relieveSymptomsOfDiscomfort: _patientVm!.patient!.relieveSymptomsOfDiscomfort,
        postureCorrection: _patientVm!.patient!.postureCorrection,
        informationHealthEducation: _patientVm!.patient!.informationHealthEducation,
        jointReduction: _patientVm!.patient!.jointReduction,
        coreMuscleTraining: _patientVm!.patient!.coreMuscleTraining,
        healthPromotion: _patientVm!.patient!.healthPromotion,
        GT: _patientVm!.patient!.GT,
        pad: _patientVm!.patient!.pad,
        padHigh: _patientVm!.patient!.padHigh.isEmpty ? '' : _patientVm!.patient!.padHigh,
        IC: _patientVm!.patient!.IC,
        ASIS: _patientVm!.patient!.ASIS,
        PSIS: _patientVm!.patient!.PSIS,
        flexion: _patientVm!.patient!.flexion,
        floor: _patientVm!.patient!.floor,
        gillet: _patientVm!.patient!.gillet,
        Extension: _patientVm!.patient!.Extension,
        rotation_right: _patientVm!.patient!.rotation_right,
        rotation_left: _patientVm!.patient!.rotation_left,
        side_left: _patientVm!.patient!.side_left,
        side_right: _patientVm!.patient!.side_right,
        side_knee_left: _patientVm!.patient!.side_knee_left,
        side_knee_right: _patientVm!.patient!.side_knee_right,
        supine_leg_length: _supineLegLength,
        adductor: _patientVm!.patient!.adductor,
        inguinal: _patientVm!.patient!.inguinal,
        prone_leg_length: _patientVm!.patient!.prone_leg_length,
        add_ROM: _patientVm!.patient!.add_ROM,
        ext_rotator: _patientVm!.patient!.ext_rotator,
        PSIS_left: _patientVm!.patient!.PSIS_left,
        PSIS_right: _patientVm!.patient!.PSIS_right,
        ILA_left: _patientVm!.patient!.ILA_left,
        ILA_right: _patientVm!.patient!.ILA_right,
        Prop_left: _patientVm!.patient!.Prop_left,
        Prop_right: _patientVm!.patient!.Prop_right,
        lumbar_left: _patientVm!.patient!.lumbar_left,
        lumbar_right: _patientVm!.patient!.lumbar_right,
        supine_ilium_leg_length: _patientVm!.patient!.supine_ilium_leg_length,
        Long_sit: _patientVm!.patient!.Long_sit,
        SLR: _patientVm!.patient!.SLR,
        Patrick: _patientVm!.patient!.Patrick,
        Knee_flexor: _patientVm!.patient!.Knee_flexor,
        ASIS_umbilical: _patientVm!.patient!.ASIS_umbilical,
        IC_recheck: _patientVm!.patient!.IC_recheck,
        flexion_recheck: _patientVm!.patient!.flexion_recheck,
        floor_recheck: _patientVm!.patient!.floor_recheck,
        gillet_recheck: _patientVm!.patient!.gillet_recheck,
        extension_recheck: _patientVm!.patient!.extension_recheck,
        rotation_left_recheck: _patientVm!.patient!.rotation_left_recheck,
        rotation_right_recheck: _patientVm!.patient!.rotation_right_recheck,
        side_left_recheck: _patientVm!.patient!.side_left_recheck,
        side_right_recheck: _patientVm!.patient!.side_right_recheck,
        side_knee_left_recheck: _patientVm!.patient!.side_knee_left_recheck,
        side_knee_right_recheck: _patientVm!.patient!.side_knee_right_recheck
    );
    _patientVm!.editPatient(patient);
  }
}