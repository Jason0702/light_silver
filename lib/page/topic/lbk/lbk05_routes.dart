import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:light_silver/database/database_helper.dart';
import 'package:light_silver/language/language_constants.dart';
import 'package:light_silver/models/operation_item_model.dart';
import 'package:light_silver/models/patient.dart';
import 'package:light_silver/utils/api.dart';
import 'package:light_silver/utils/shared_preference_util.dart';
import 'package:light_silver/utils/utils.dart';
import 'package:light_silver/widgets/noShadowScrollbehavior.dart';
import 'package:provider/provider.dart';

import '../../../app.dart';
import '../../../routes/route_name.dart';
import '../../../view_model/auth_vm.dart';
import '../../../view_model/operation_item_vm.dart';
import '../../../view_model/patient_vm.dart';
import '../../../widgets/button.dart';
import '../../../widgets/title_widget.dart';
import '../../network_image_page.dart';
import '../video_palyer_screen.dart';

class LBk05Routes extends StatefulWidget {
  @override
  State createState() => _LBk05RoutesState();
}

class _LBk05RoutesState extends State<LBk05Routes>
    with WidgetsBindingObserver {
  PatientVm? _patientVm;
  AuthVm? _authVm;
  OperationItemVm? _operationItemVm;

  OperationItemsModel? imageList;
  String netUpImageUrl = '';
  String netDownImageUrl = '';
  String netVideoUrl = '';

  final TextEditingController _pubisSideKey = TextEditingController();
  final TextEditingController _pubisProblem = TextEditingController();

  _onLayoutDone(_) {
    List list =
    _operationItemVm!.operationItemsModelList!.where((element) => element.id == 3).toList();
    for (var element in list) {
      imageList = element;
    }
    _dataInit(_patientVm!.patient);
  }

  void _dataInit(Patient? patient) {
    if (patient == null) {
      return;
    }
    log('取得 仰躺(4): $patient');
    if (patient.inguinal == 1) {
      _pubisSideKey.text = getTranslated(context, 'left');
    }
    if (patient.inguinal == 2) {
      _pubisSideKey.text = getTranslated(context, 'right');
    }

    if (patient.inguinal == 1 && patient.supine_leg_length == 1) {
      //上
      _pubisProblem.text = getTranslated(context, 'up');
    }
    if (patient.inguinal == 1 && patient.supine_leg_length == 3) {
      //下
      _pubisProblem.text = getTranslated(context, 'down');
    }
    if (patient.inguinal == 2 && patient.supine_leg_length == 1) {
      //下
      _pubisProblem.text = getTranslated(context, 'down');
    }
    if (patient.inguinal == 2 && patient.supine_leg_length == 3) {
      //上
      _pubisProblem.text = getTranslated(context, 'up');
    }

    if (patient.inguinal == 1 && patient.supine_leg_length == 1) {
      //左上
      debugPrint(imageList!.completePics
          .where((element) => element.containsKey('左_向上'))
          .toString());
      debugPrint(imageList!.completeVideos
          .where((element) => element.containsKey('左_向上'))
          .toString());
      Map<String, dynamic> map = imageList!.completePics
          .where((element) => element.containsKey('左_向上'))
          .first;
      Map<String, CompleteVideo> videoMap = imageList!.completeVideos
          .where((element) => element.containsKey('左_向上'))
          .first;
      netUpImageUrl = map.values.first;
      netDownImageUrl = videoMap.values.first.thumbnail;
      netVideoUrl = videoMap.values.first.src;
    }
    if (patient.inguinal == 1 && patient.supine_leg_length == 3) {
      //左下
      debugPrint(imageList!.completePics
          .where((element) => element.containsKey('左_向下'))
          .toString());
      Map<String, dynamic> map = imageList!.completePics
          .where((element) => element.containsKey('左_向下'))
          .first;
      Map<String, CompleteVideo> videoMap = imageList!.completeVideos
          .where((element) => element.containsKey('左_向下'))
          .first;
      netUpImageUrl = map.values.first;
      netDownImageUrl = videoMap.values.first.thumbnail;
      netVideoUrl = videoMap.values.first.src;
    }
    if (patient.inguinal == 2 && patient.supine_leg_length == 3) {
      //右上
      debugPrint(imageList!.completePics
          .where((element) => element.containsKey('右_向上'))
          .toString());
      Map<String, dynamic> map = imageList!.completePics
          .where((element) => element.containsKey('右_向上'))
          .first;
      Map<String, CompleteVideo> videoMap = imageList!.completeVideos
          .where((element) => element.containsKey('右_向上'))
          .first;
      netUpImageUrl = map.values.first;
      netDownImageUrl = videoMap.values.first.thumbnail;
      netVideoUrl = videoMap.values.first.src;
    }
    if (patient.inguinal == 2 && patient.supine_leg_length == 1) {
      //右下
      debugPrint(imageList!.completePics
          .where((element) => element.containsKey('右_向下'))
          .toString());
      Map<String, dynamic> map = imageList!.completePics
          .where((element) => element.containsKey('右_向下'))
          .first;
      Map<String, CompleteVideo> videoMap = imageList!.completeVideos
          .where((element) => element.containsKey('右_向下'))
          .first;
      netUpImageUrl = map.values.first;
      netDownImageUrl = videoMap.values.first.thumbnail;
      netVideoUrl = videoMap.values.first.src;
    }
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  Widget build(BuildContext context) {
    _patientVm = Provider.of<PatientVm>(context);
    _authVm = Provider.of<AuthVm>(context);
    _operationItemVm = Provider.of<OperationItemVm>(context);
    return WillPopScope(
        child: Scaffold(
          backgroundColor: Colors.white,
          resizeToAvoidBottomInset: false,
          body: SafeArea(
            child: Column(
              children: [
                Expanded(flex: 1, child: _titleBar()),
                Expanded(flex: 10, child: _bodyWidget())
              ],
            ),
          ),
        ),
        onWillPop: () async {
          showBackDialog(false);
          return false;
        });
  }

  //最上層標題與按鈕
  Widget _titleBar() {
    return LbkTitleWidget(
      titleText: '${getTranslated(context, 'Lie_on_your_back')}(4)',
      leftButton: _leftTopButton(),
      rightButton: _rightTopButton(),
    );
  }

  //左側按鈕
  Widget _leftTopButton() {
    return InkWell(
      onTap: () {
        showBackDialog(false);
      },
      child: const Icon(
        Icons.arrow_back_rounded,
        color: Colors.white,
        size: 45,
      ),
    );
  }

  //右側按鈕
  Widget _rightTopButton() {
    return InkWell(
      onTap: () {
        showBackDialog(true);
      },
      child: const Icon(
        Icons.home,
        color: Colors.white,
        size: 45,
      ),
    );
  }

  void showBackDialog(bool isBackHome) async {
    bool isSelect = await showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                const FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    '可能會變更之前的評估結果，是否確定\n更改先前評估記錄?',
                    textAlign: TextAlign.left,
                    textScaleFactor: 1,
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 24,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    //是
                    ButtonTheme(
                      height: 80,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            foregroundColor: Colors.white,
                            backgroundColor: Colors.transparent,
                            shape: const RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0))),
                            padding: const EdgeInsets.all(0.0)),
                        onPressed: () {
                          debugPrint('是');
                          Navigator.of(context).pop(true);
                        },
                        child: Container(
                          height: 60,
                          width: 120,
                          alignment: Alignment.center,
                          decoration: Style.imageButtonBG,
                          child: const FittedBox(
                            fit: BoxFit.scaleDown,
                            child: Text(
                              '是',
                              textScaleFactor: 1,
                              style:
                                  TextStyle(color: Colors.white, fontSize: 20),
                            ),
                          ),
                        ),
                      ),
                    ),
                    //否
                    ButtonTheme(
                      height: 80,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            foregroundColor: Colors.white,
                            backgroundColor: Colors.transparent,
                            shape: const RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0))),
                            padding: const EdgeInsets.all(0.0)),
                        onPressed: () => delegate.popRoute(),
                        child: Container(
                          height: 60,
                          width: 120,
                          alignment: Alignment.center,
                          decoration: Style.lbkNextBG,
                          child: const FittedBox(
                            fit: BoxFit.scaleDown,
                            child: Text(
                              '否',
                              textScaleFactor: 1,
                              style:
                                  TextStyle(color: Colors.white, fontSize: 20),
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                )
              ],
            ),
          );
        });
    if (isSelect) {
      if (isBackHome) {
        delegate.popUtil();
      } else {
        delegate.popRoute();
      }
    }
  }

//主體
  Widget _bodyWidget() {
    return Stack(
      children: [
        Container(
          padding: const EdgeInsets.only(top: 10, bottom: 10),
          decoration: Style.homeBg,
          alignment: Alignment.center,
          child: Container(
            width: MediaQuery.of(context).size.width / 1.1,
            decoration: Style.centerBG,
            child: ScrollConfiguration(
              behavior: NoShadowScrollBehavior(),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    //主體內部
                    _bodyTopWidget(),
                    //題目
                    _topicWidget(),
                    //問題
                    _problemWidget(),
                    //圖片
                    _imageWidget(),
                    const SizedBox(
                      height: 10,
                    ),
                    //影片與圖片
                    _videoWidget(),
                    //按鈕
                    _bottomButtonWidget(),
                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  //主體內部
  Widget _bodyTopWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 20, left: 50, right: 50),
      child: Column(
        children: [
          FittedBox(
            fit: BoxFit.scaleDown,
            child: Text(
              getTranslated(
                  context, 'Assessment_and_management_of_pubic_problems'),
              textScaleFactor: 1,
              style: const TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Container(
            height: 3,
            color: HexColor.fromHex('#216AB2'),
          )
        ],
      ),
    );
  }

  //題目
  Widget _topicWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 20, left: 25),
      child: Row(
        children: [
          Container(
            width: 40,
            height: 40,
            alignment: Alignment.center,
            decoration: BoxDecoration(
                color: HexColor.fromHex('#1E2C39'),
                borderRadius: const BorderRadius.all(Radius.circular(25))),
            child: const FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                '4',
                textScaleFactor: 1,
                style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    color: Colors.white),
              ),
            ),
          ),
          const SizedBox(
            width: 20,
          ),
          FittedBox(
            fit: BoxFit.scaleDown,
            child: Text(
              getTranslated(context, 'Pubic_problem_management_skills'),
              textScaleFactor: 1,
              style: const TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            ),
          ),
        ],
      ),
    );
  }

  //問題
  Widget _problemWidget() {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              margin: const EdgeInsets.all(8),
              width: 100,
              child: TextField(
                readOnly: true,
                textAlign: TextAlign.center,
                style: const TextStyle(color: Colors.black),
                decoration: const InputDecoration(
                  contentPadding: EdgeInsets.all(5),
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.black)),
                  filled: true,
                  fillColor: Colors.white,
                ),
                keyboardType: TextInputType.number,
                textInputAction: TextInputAction.done,
                controller: _pubisSideKey,
              ),
            ),
            FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                getTranslated(context, 'Lateral_pubic_bone'),
                textScaleFactor: 1,
                style: const TextStyle(
                    fontSize: 20,
                    color: Colors.black,
                    fontWeight: FontWeight.bold),
              ),
            ),
            Container(
              margin: const EdgeInsets.all(8),
              width: 100,
              child: TextField(
                readOnly: true,
                textAlign: TextAlign.center,
                style: const TextStyle(color: Colors.black),
                decoration: const InputDecoration(
                  contentPadding: EdgeInsets.all(5),
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.black)),
                  filled: true,
                  fillColor: Colors.white,
                ),
                keyboardType: TextInputType.number,
                textInputAction: TextInputAction.done,
                controller: _pubisProblem,
              ),
            ),
            FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                getTranslated(context, 'Displacement'),
                textScaleFactor: 1,
                style: const TextStyle(
                    fontSize: 20,
                    color: Colors.black,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
      ],
    );
  }

  //圖片
  Widget _imageWidget() {
    debugPrint('圖片網址: $imageUrlOffline$imageUrlPublic$netUpImageUrl');
    return Container(
        width: 300,
        height: 200,
        decoration: BoxDecoration(
            borderRadius: const BorderRadius.all(Radius.circular(15)),
            border: Border.all(color: Colors.black, width: 1)),
        child: Stack(
          alignment: Alignment.center,
          children: [
            Image(
              image:
                  NetworkImage('$imageUrlOffline$imageUrlPublic$netUpImageUrl'),
              fit: BoxFit.fill,
            ),
            Positioned(
              bottom: 10,
              right: 10,
              child: InkWell(
                  onTap: _upImageOnTap,
                  child: const Icon(
                    Icons.zoom_in,
                    size: 45,
                  )),
            )
          ],
        ));
  }

  //影片與圖片
  Widget _videoWidget() {
    return Container(
      width: 300,
      height: 180,
      decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(Radius.circular(15)),
          image: DecorationImage(
            image:
                NetworkImage('$imageUrlOffline$imageUrlPublic$netDownImageUrl'),
            fit: BoxFit.cover,
          ),
          border: Border.all(color: Colors.black, width: 1)),
      child: Stack(
        alignment: Alignment.center,
        children: [
          Positioned(
              bottom: 0,
              right: 0,
              child: Row(
                children: [
                  InkWell(
                    onTap: _videoOnTap,
                    child: Container(
                      decoration: const BoxDecoration(
                          color: Colors.black,
                          borderRadius: BorderRadius.all(Radius.circular(25))),
                      child: const Icon(
                        Icons.arrow_right,
                        size: 30,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 5,
                  ),
                  InkWell(
                    onTap: _downImageOnTap,
                    child: const Icon(Icons.zoom_in, size: 45),
                  )
                ],
              ))
        ],
      ),
    );
  }

  //下面影片
  void _videoOnTap() {
    debugPrint(netVideoUrl);
    Navigator.of(context).push(MaterialPageRoute(builder: (context) {
      return VideoPlayerScreen(
          videoUrl: '$imageUrlOffline$imageUrlPublic$netVideoUrl');
    }));
  }

  //下面圖片放大
  void _downImageOnTap() {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) {
      return NetWorkImagePage(
          imageUrl: '$imageUrlOffline$imageUrlPublic$netDownImageUrl');
    }));
  }

  //上面圖片放大
  void _upImageOnTap() {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) {
      return NetWorkImagePage(
          imageUrl: '$imageUrlOffline$imageUrlPublic$netUpImageUrl');
    }));
  }

  //按鈕
  Widget _bottomButtonWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 30, right: 25, bottom: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          //下一步
          NextButton(
              click: _nextOnTap,
              buttonText: getTranslated(context, 'Next_step'),
              decoration: Style.lbkNextBG),
        ],
      ),
    );
  }

  //下一步
  void _nextOnTap() {
    if (!_authVm!.isLogin &&
        _operationItemVm!.operationItemsModelList!
                .where((element) => element.name == '恥骨問題評估與處理')
                .first
                .isFree ==
            0) {
      EasyLoading.showToast('未購買正式版');
      return;
    } else {
      if (_patientVm!.patient!.gillet == 1 &&
          _patientVm!.patient!.IC == 3 &&
          _patientVm!.patient!.ASIS == 3 &&
          _patientVm!.patient!.PSIS == 3) {
        delegate.push(name: RouteName.sps41);
      } else if (_patientVm!.patient!.gillet == 2 &&
          _patientVm!.patient!.IC == 1 &&
          _patientVm!.patient!.ASIS == 1 &&
          _patientVm!.patient!.PSIS == 1) {
        delegate.push(name: RouteName.sps41);
      } else {
        delegate.push(name: RouteName.sps01);
      }
    }
  }
}
