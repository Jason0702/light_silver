import 'package:flick_video_player/flick_video_player.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:video_player/video_player.dart';

import '../../app.dart';

class VideoPlayerScreen extends StatefulWidget {
  final String videoUrl;
  const VideoPlayerScreen({Key? key, required this.videoUrl}) : super(key: key);

  @override
  State createState() => _VideoPlayerScreenState();
}

class _VideoPlayerScreenState extends State<VideoPlayerScreen> {
  bool _isFullscreen = true;
 late FlickManager flickManager;

  late InAppWebViewController webViewController;

  InAppWebViewGroupOptions options = InAppWebViewGroupOptions(
      crossPlatform: InAppWebViewOptions(
        useShouldOverrideUrlLoading: true,
        mediaPlaybackRequiresUserGesture: false,
      ),
      android: AndroidInAppWebViewOptions(
        //useHybridComposition: true,
      ),
      ios: IOSInAppWebViewOptions(
        allowsInlineMediaPlayback: true,
      ));


  @override
  void initState() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.landscapeRight,
    ]);
    //隱藏底部與頂部狀態欄
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: []);

    super.initState();

    flickManager = FlickManager(
      videoPlayerController: VideoPlayerController.network(
          widget.videoUrl,
        //closedCaptionFile: _loadCaptions(),
      ),
    );
  }

  Future<ClosedCaptionFile> _loadCaptions() async {
    final String fileContents = await DefaultAssetBundle.of(context)
        .loadString('images/bumble_bee_captions.srt');
    flickManager.flickControlManager!.togglePlay();
    return SubRipCaptionFile(fileContents);
  }

  @override
  void dispose() {
    flickManager.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _willPopOnTap,
      child: Scaffold(
       body: Stack(
         children: [
           Center(
            child: FlickVideoPlayer(
              preferredDeviceOrientation: const [ DeviceOrientation.landscapeLeft,
                DeviceOrientation.landscapeRight],
              flickManager: flickManager,
              flickVideoWithControls: const FlickVideoWithControls(
                controls: FlickPortraitControls(),
              ),
              flickVideoWithControlsFullscreen: const FlickVideoWithControls(
                controls: FlickLandscapeControls(),
              ),
            ),
      ),
           Padding(
             padding: const EdgeInsets.only(top :10,left: 10),
             child: GestureDetector(
               onTap: (){
                 SystemChrome.setPreferredOrientations([
                   DeviceOrientation.portraitUp,
                 ]);
                 SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: [
                   SystemUiOverlay.bottom,
                   SystemUiOverlay.top
                 ]);
                 delegate.popRoute();
               },
               child: const Icon(Icons.arrow_back,size: 30,color: Colors.white,),
             ),
           ),
         ],
       ),
    ));
  }

  Future<bool> _willPopOnTap() async {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: [
      SystemUiOverlay.bottom,
      SystemUiOverlay.top
    ]);
    delegate.popRoute();
    return false;
  }
}