import 'dart:developer';
import 'package:dotted_line/dotted_line.dart';
import 'package:flutter/material.dart';
import 'package:light_silver/language/language_constants.dart';
import 'package:light_silver/models/operation_item_model.dart';
import 'package:light_silver/models/patient.dart';
import 'package:light_silver/routes/route_name.dart';
import 'package:light_silver/utils/api.dart';
import 'package:light_silver/utils/utils.dart';
import 'package:light_silver/widgets/button.dart';
import 'package:light_silver/widgets/left_button.dart';
import 'package:provider/provider.dart';

import '../../../app.dart';
import '../../../view_model/operation_item_vm.dart';
import '../../../view_model/patient_vm.dart';
import '../../../widgets/title_widget.dart';
import '../video_palyer_screen.dart';

class LD01Routes extends StatefulWidget {
  @override
  State createState() => _LD01RoutesState();
}

class _LD01RoutesState extends State<LD01Routes> {
  PatientVm? _patientVm;
  OperationItemVm? _operationItemVm;

  final TextEditingController _lumbarLeftProblem = TextEditingController();
  final TextEditingController _lumbarRightProblem = TextEditingController();
  int _lumbarLeft = 0;
  int _lumbarRight = 0;
  OperationItemsModel? imageList;
  String netDownImageUrl = '';
  String netVideoUrl = '';


  _onLayoutDone(_) {
    List list =
    _operationItemVm!.operationItemsModelList!.where((element) => element.id == 7).toList();
    for (var element in list) {
      imageList = element;
      setState(() {});
    }
    _dataInit(_patientVm!.patient);
  }

  void _dataInit(Patient? patient) {
    if (patient == null) {
      return;
    }
    log('取得 側躺(6): $patient');
    Map<String, CompleteVideo> _leftVideoMap = imageList!.completeVideos
        .where((element) => element.containsKey('左0'))
        .first;
    Map<String, CompleteVideo> _rightVideoMap = imageList!.completeVideos
        .where((element) => element.containsKey('右0'))
        .first;
    switch (patient.lumbar_left) {
      case 0:
        _lumbarLeftProblem.text = '';
        _lumbarLeft = 0;
        netDownImageUrl = _leftVideoMap.values.first.thumbnail;
        netVideoUrl = _leftVideoMap.values.first.src;
        break;
      case 1:
        _lumbarLeftProblem.text = 'L5/S1';
        _lumbarLeft = 1;
        netDownImageUrl = _leftVideoMap.values.first.thumbnail;
        netVideoUrl = _leftVideoMap.values.first.src;
        break;
      case 2:
        _lumbarLeftProblem.text = 'L4/L5';
        _lumbarLeft = 2;
        netDownImageUrl = _leftVideoMap.values.first.thumbnail;
        netVideoUrl = _leftVideoMap.values.first.src;
        break;
      case 3:
        _lumbarLeftProblem.text = 'L3/L4';
        _lumbarLeft = 3;
        netDownImageUrl = _leftVideoMap.values.first.thumbnail;
        netVideoUrl = _leftVideoMap.values.first.src;
        break;
      case 4:
        _lumbarLeftProblem.text = 'L2/L3';
        _lumbarLeft = 4;
        netDownImageUrl = _leftVideoMap.values.first.thumbnail;
        netVideoUrl = _leftVideoMap.values.first.src;
        break;
      case 5:
        _lumbarLeftProblem.text = 'L1/L2';
        _lumbarLeft = 5;
        netDownImageUrl = _leftVideoMap.values.first.thumbnail;
        netVideoUrl = _leftVideoMap.values.first.src;
        break;
    }
    switch (patient.lumbar_right) {
      case 0:
        _lumbarRightProblem.text = '';
        _lumbarRight = 0;
        if (_lumbarLeft != 0) {
          netDownImageUrl = _rightVideoMap.values.first.thumbnail;
          netVideoUrl = _rightVideoMap.values.first.src;
        }
        break;
      case 1:
        _lumbarRightProblem.text = 'L5/S1';
        _lumbarRight = 1;
        break;
      case 2:
        _lumbarRightProblem.text = 'L4/L5';
        _lumbarRight = 2;
        break;
      case 3:
        _lumbarRightProblem.text = 'L3/L4';
        _lumbarRight = 3;
        break;
      case 4:
        _lumbarRightProblem.text = 'L2/L3';
        _lumbarRight = 4;
        break;
      case 5:
        _lumbarRightProblem.text = 'L1/L2';
        _lumbarRight = 5;
        break;
    }
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  Widget build(BuildContext context) {
    _patientVm = Provider.of<PatientVm>(context);
    _operationItemVm = Provider.of<OperationItemVm>(context);
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Column(
          children: [
            Expanded(flex: 1, child: _titleBar()),
            Expanded(flex: 10, child: _bodyWidget())
          ],
        ),
      ),
    );
  }

  //最上層標題與按鈕
  Widget _titleBar() {
    return LdTitleWidget(
      titleText: '${getTranslated(context, 'Lie_down')}(1)',
      leftButton: const LeftButton(),
      rightButton: _rightTopButton(),
    );
  }

  //右側按鈕
  Widget _rightTopButton() {
    return InkWell(
      onTap: () => delegate.popUtil(),
      child: const Icon(
        Icons.home,
        color: Colors.white,
        size: 45,
      ),
    );
  }

  //主體
  Widget _bodyWidget() {
    return Stack(
      children: [
        Container(
          padding: const EdgeInsets.only(top: 10, bottom: 10),
          decoration: Style.homeBg,
          alignment: Alignment.center,
          child: Container(
            width: MediaQuery.of(context).size.width / 1.1,
            decoration: Style.centerBG,
            child: Column(
              children: [
                //主體內部
                _bodyTopWidget(),
                //題目
                _topicWidget(),
                //顯示文字
                _showTextWidget(),
                const Spacer(),
                //顯示圖片
                _showImageWidget(),
                const Spacer(),
                //顯示影片
                _showVideoWidget(),
                const Spacer(),
                //按鈕
                _bottomButtonWidget(),
              ],
            ),
          ),
        ),
      ],
    );
  }

  //主體內部
  Widget _bodyTopWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 10, left: 50, right: 50),
      child: Column(
        children: [
          FittedBox(
            fit: BoxFit.scaleDown,
            child: Text(
              getTranslated(
                  context, 'Adjustment_of_lumbar_facet_joint_problems'),
              textScaleFactor: 1,
              style: const TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            height: 3,
            color: HexColor.fromHex('#521BAA'),
          )
        ],
      ),
    );
  }

  //題目
  Widget _topicWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 10, left: 25),
      child: Row(
        children: [
          Container(
            width: 40,
            height: 40,
            alignment: Alignment.center,
            decoration: BoxDecoration(
                color: HexColor.fromHex('#1E2C39'),
                borderRadius: const BorderRadius.all(Radius.circular(25))),
            child: const FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                '1',
                textScaleFactor: 1,
                style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    color: Colors.white),
              ),
            ),
          ),
          const SizedBox(
            width: 20,
          ),
          Expanded(
              child: FittedBox(
            fit: BoxFit.scaleDown,
            child: Text(
              getTranslated(
                  context, 'Treatment_skills_of_lumbar_facet_joint_problems'),
              textScaleFactor: 1,
              style: const TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            ),
          )),
        ],
      ),
    );
  }

  //顯示文字
  Widget _showTextWidget() {
    return Row(
      children: [
        Expanded(
            flex: 1,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                //左側
                FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    getTranslated(context, 'Left_side'),
                    textScaleFactor: 1,
                    style: const TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                    ),
                  ),
                ),
                //文字輸入框
                Container(
                  margin: const EdgeInsets.all(8),
                  width: 80,
                  child: TextField(
                    readOnly: true,
                    textAlign: TextAlign.center,
                    style: TextStyle(color: HexColor.fromHex('#096BC7')),
                    decoration: const InputDecoration(
                      contentPadding: EdgeInsets.all(5),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.black)),
                      filled: true,
                      fillColor: Colors.white,
                    ),
                    keyboardType: TextInputType.text,
                    textInputAction: TextInputAction.done,
                    controller: _lumbarLeftProblem,
                  ),
                ),
                //節
                FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    getTranslated(context, 'Section'),
                    textScaleFactor: 1,
                    style: const TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 20),
                  ),
                )
              ],
            )),
        Expanded(
            flex: 1,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                //右側
                FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    getTranslated(context, 'Right_side'),
                    textScaleFactor: 1,
                    style: const TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 20),
                  ),
                ),
                //文字輸入框
                Container(
                  margin: const EdgeInsets.all(8),
                  width: 80,
                  child: TextField(
                    readOnly: true,
                    textAlign: TextAlign.center,
                    style: TextStyle(color: HexColor.fromHex('#096BC7')),
                    decoration: const InputDecoration(
                      contentPadding: EdgeInsets.all(5),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.black)),
                      filled: true,
                      fillColor: Colors.white,
                    ),
                    keyboardType: TextInputType.text,
                    textInputAction: TextInputAction.done,
                    controller: _lumbarRightProblem,
                  ),
                ),
                //節
                FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    getTranslated(context, 'Section'),
                    textScaleFactor: 1,
                    style: const TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 20),
                  ),
                ),
              ],
            ))
      ],
    );
  }

  static const double circleSize = 20;

  //顯示圖片
  Widget _showImageWidget() {
    return Container(
      width: 300,
      height: 200,
      decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(Radius.circular(10)),
          border: Border.all(color: Colors.black, width: 2.0)),
      margin: const EdgeInsets.only(left: 25, right: 25),
      child: Stack(
        alignment: Alignment.center,
        children: [
          Container(
              padding: const EdgeInsets.symmetric(vertical: 10),
              child: Image.asset('assets/images/6back.png')),
          Container(
            margin: const EdgeInsets.only(bottom: 15),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                //L1/L2
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    //L1/L2 Text
                    const FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Text(
                        'L1/L2',
                        textScaleFactor: 1,
                      ),
                    ),
                    //L1/L2 圓
                    Container(
                      width: circleSize,
                      height: circleSize,
                      decoration: BoxDecoration(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(50)),
                        border: Border.all(width: 5.0, color: Colors.black),
                        color: _lumbarLeft == 5 ? Colors.red : Colors.white,
                      ),
                    ),
                    //L1/L2 線
                    SizedBox(
                      width: 140,
                      child: DottedLine(
                        lineThickness: 3.0,
                        dashColor: HexColor.fromHex('#24AF88'),
                      ),
                    ),
                    //L1/L2 圓
                    Container(
                      width: circleSize,
                      height: circleSize,
                      decoration: BoxDecoration(
                          borderRadius:
                              const BorderRadius.all(Radius.circular(50)),
                          border: Border.all(width: 5.0, color: Colors.black),
                          color: _lumbarRight == 5 ? Colors.red : Colors.white),
                    ),
                    //L1/L2 Text
                    const FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Text(
                        'L1/L2',
                        textScaleFactor: 1,
                      ),
                    ),
                  ],
                ),
                //L2/L3
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    //L2/L3 Text
                    const FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Text(
                        'L2/L3',
                        textScaleFactor: 1,
                      ),
                    ),
                    //L2/L3 圓
                    Container(
                      width: circleSize,
                      height: circleSize,
                      decoration: BoxDecoration(
                          borderRadius:
                              const BorderRadius.all(Radius.circular(50)),
                          border: Border.all(width: 5.0, color: Colors.black),
                          color: _lumbarLeft == 4 ? Colors.red : Colors.white),
                    ),
                    //L2/L3 線
                    SizedBox(
                      width: 140,
                      child: DottedLine(
                        lineThickness: 3.0,
                        dashColor: HexColor.fromHex('#24AF88'),
                      ),
                    ),
                    //L2/L3 圓
                    Container(
                      width: circleSize,
                      height: circleSize,
                      decoration: BoxDecoration(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(50)),
                        border: Border.all(width: 5.0, color: Colors.black),
                        color: _lumbarRight == 4 ? Colors.red : Colors.white,
                      ),
                    ),
                    //L2/L3 Text
                    const FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Text(
                        'L2/L3',
                        textScaleFactor: 1,
                      ),
                    ),
                  ],
                ),
                //L3/L4
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    //L3/L4 Text
                    const FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Text(
                        'L3/L4',
                        textScaleFactor: 1,
                      ),
                    ),
                    //L3/L4 圓
                    Container(
                      width: circleSize,
                      height: circleSize,
                      decoration: BoxDecoration(
                          borderRadius:
                              const BorderRadius.all(Radius.circular(50)),
                          border: Border.all(width: 5.0, color: Colors.black),
                          color: _lumbarLeft == 3 ? Colors.red : Colors.white),
                    ),
                    //L3/L4 線
                    SizedBox(
                      width: 140,
                      child: DottedLine(
                        lineThickness: 3.0,
                        dashColor: HexColor.fromHex('#24AF88'),
                      ),
                    ),
                    //L3/L4 圓
                    Container(
                      width: circleSize,
                      height: circleSize,
                      decoration: BoxDecoration(
                          borderRadius:
                              const BorderRadius.all(Radius.circular(50)),
                          border: Border.all(width: 5.0, color: Colors.black),
                          color: _lumbarRight == 3 ? Colors.red : Colors.white),
                    ),
                    //L3/L4 Text
                    const FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Text(
                        'L3/L4',
                        textScaleFactor: 1,
                      ),
                    ),
                  ],
                ),
                //L4/L5
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    //L4/L5 Text
                    const FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Text(
                        'L4/L5',
                        textScaleFactor: 1,
                      ),
                    ),
                    //L4/L5 圓
                    Container(
                      width: circleSize,
                      height: circleSize,
                      decoration: BoxDecoration(
                          borderRadius:
                              const BorderRadius.all(Radius.circular(50)),
                          border: Border.all(width: 5.0, color: Colors.black),
                          color: _lumbarLeft == 2 ? Colors.red : Colors.white),
                    ),
                    //L4/L5 線
                    SizedBox(
                      width: 140,
                      child: DottedLine(
                        lineThickness: 3.0,
                        dashColor: HexColor.fromHex('#24AF88'),
                      ),
                    ),
                    //L4/L5 圓
                    Container(
                      width: circleSize,
                      height: circleSize,
                      decoration: BoxDecoration(
                          borderRadius:
                              const BorderRadius.all(Radius.circular(50)),
                          border: Border.all(width: 5.0, color: Colors.black),
                          color: _lumbarRight == 2 ? Colors.red : Colors.white),
                    ),
                    //L4/L5 Text
                    const FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Text(
                        'L4/L5',
                        textScaleFactor: 1,
                      ),
                    ),
                  ],
                ),
                //L5/S1
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    //L5/S1 Text
                    const FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Text(
                        'L5/S1',
                        textScaleFactor: 1,
                      ),
                    ),
                    //L5/S1 圓
                    Container(
                      width: circleSize,
                      height: circleSize,
                      decoration: BoxDecoration(
                          borderRadius:
                              const BorderRadius.all(Radius.circular(50)),
                          border: Border.all(width: 5.0, color: Colors.black),
                          color: _lumbarLeft == 1 ? Colors.red : Colors.white),
                    ),
                    //L5/S1 線
                    SizedBox(
                      width: 140,
                      child: DottedLine(
                        lineThickness: 3.0,
                        dashColor: HexColor.fromHex('#24AF88'),
                      ),
                    ),
                    //L5/S1 圓
                    Container(
                      width: circleSize,
                      height: circleSize,
                      decoration: BoxDecoration(
                          borderRadius:
                              const BorderRadius.all(Radius.circular(50)),
                          border: Border.all(width: 5.0, color: Colors.black),
                          color: _lumbarRight == 1 ? Colors.red : Colors.white),
                    ),
                    //L5/S1 Text
                    const FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Text(
                        'L5/S1',
                        textScaleFactor: 1,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Positioned(
              right: -10,
              bottom: -10,
              child: IconButton(
                icon: const Icon(Icons.zoom_in),
                iconSize: 30,
                onPressed: _imageOnTap,
              ))
        ],
      ),
    );
  }

  //下面影片
  void _videoOnTap() {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) {
      return VideoPlayerScreen(
        videoUrl: '$imageUrlOffline$imageUrlPublic$netVideoUrl',
      );
    }));
  }

  //上面圖片放大
  void _upImageOnTap() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => HeroViewRouteWrapper(
                  lumbarRight: _lumbarRight,
                  lumbarLeft: _lumbarLeft,
                )));
  }

  void _imageOnTap() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => HeroViewRouteWrapper(
                  lumbarRight: _lumbarRight,
                  lumbarLeft: _lumbarLeft,
                )));
  }

  //顯示影片
  Widget _showVideoWidget() {
    return Container(
      width: 300,
      height: 180,
      decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(Radius.circular(10)),
          image: DecorationImage(
            image:
                NetworkImage('$imageUrlOffline$imageUrlPublic$netDownImageUrl'),
            fit: BoxFit.cover,
          ),
          border: Border.all(color: Colors.black, width: 2.0)),
      margin: const EdgeInsets.only(left: 25, right: 25),
      child: Stack(
        alignment: Alignment.center,
        children: [
          Positioned(
              right: 0,
              bottom: 0,
              child: Row(
                children: [
                  InkWell(
                    onTap: _videoOnTap,
                    child: Container(
                      decoration: const BoxDecoration(
                          color: Colors.black,
                          borderRadius: BorderRadius.all(Radius.circular(25))),
                      child: const Icon(
                        Icons.arrow_right,
                        size: 30,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 5,
                  ),
                  InkWell(
                    onTap: _upImageOnTap,
                    child: const Icon(Icons.zoom_in, size: 45),
                  )
                ],
              ))
        ],
      ),
    );
  }

  //按鈕
  Widget _bottomButtonWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 5, right: 25, bottom: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          //下一步
          NextButton(
              click: _nextOnTap,
              buttonText: getTranslated(context, 'Next_step'),
              decoration: Style.ldNextBG),
        ],
      ),
    );
  }

  void _nextOnTap() {
    delegate.push(name: RouteName.lbk11);
  }
}

class HeroViewRouteWrapper extends StatelessWidget {
  const HeroViewRouteWrapper(
      {required this.lumbarRight, required this.lumbarLeft});

  final int lumbarRight;
  final int lumbarLeft;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Column(
          children: [
            Expanded(flex: 1, child: _titleBar(context)),
            Expanded(flex: 10, child: _bodyWidget(context)),
          ],
        ),
      ),
    );
  }

  //最上層標題與按鈕
  Widget _titleBar(context) {
    return Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(colors: [
        HexColor.fromHex('#4809AB'),
        HexColor.fromHex('#7E66AA'),
        HexColor.fromHex('#4B18A3'),
      ], begin: Alignment.topCenter, end: Alignment.bottomCenter)),
      child: Row(
        children: [
          const Expanded(flex: 1, child: LeftButton()),
          Expanded(
              flex: 3,
              child: Container()),
          Expanded(flex: 1, child: Container())
        ],
      ),
    );
  }

  Widget _bodyWidget(context) {
    return Container(
      decoration: const BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/images/home_bg.png'),
              fit: BoxFit.fill)),
      child: Container(
        margin: const EdgeInsets.only(top: 0, left: 10, right: 10),
        child: Stack(
          alignment: Alignment.center,
          children: [
            Container(
              height: MediaQuery.of(context).size.height / 1.5,
              margin: const EdgeInsets.only(top: 50),
              child: Image.asset(
                'assets/images/6back.png',
                fit: BoxFit.fill,
              ),
            ),
            SizedBox(
                height: MediaQuery.of(context).size.height / 2,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    //L1/L2
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        //L1/L2 Text
                        const FittedBox(
                          fit: BoxFit.scaleDown,
                          child: Text(
                            'L1/L2',
                            textScaleFactor: 1,
                          ),
                        ),
                        //L1/L2 圓
                        Container(
                          width: 40,
                          height: 40,
                          decoration: BoxDecoration(
                            borderRadius:
                                const BorderRadius.all(Radius.circular(50)),
                            border: Border.all(width: 7.0, color: Colors.black),
                            color: lumbarLeft == 5 ? Colors.red : Colors.white,
                          ),
                        ),
                        //L1/L2 線
                        SizedBox(
                          width: 180,
                          child: DottedLine(
                            lineThickness: 3.0,
                            dashColor: HexColor.fromHex('#24AF88'),
                          ),
                        ),
                        //L1/L2 圓
                        Container(
                          width: 40,
                          height: 40,
                          decoration: BoxDecoration(
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(50)),
                              border:
                                  Border.all(width: 7.0, color: Colors.black),
                              color:
                                  lumbarRight == 5 ? Colors.red : Colors.white),
                        ),
                        //L1/L2 Text
                        const FittedBox(
                          fit: BoxFit.scaleDown,
                          child: Text(
                            'L1/L2',
                            textScaleFactor: 1,
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    //L2/L3
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        //L2/L3 Text
                        const FittedBox(
                          fit: BoxFit.scaleDown,
                          child: Text(
                            'L2/L3',
                            textScaleFactor: 1,
                          ),
                        ),
                        //L2/L3 圓
                        Container(
                          width: 40,
                          height: 40,
                          decoration: BoxDecoration(
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(50)),
                              border:
                                  Border.all(width: 7.0, color: Colors.black),
                              color:
                                  lumbarLeft == 4 ? Colors.red : Colors.white),
                        ),
                        //L2/L3 線
                        SizedBox(
                          width: 180,
                          child: DottedLine(
                            lineThickness: 3.0,
                            dashColor: HexColor.fromHex('#24AF88'),
                          ),
                        ),
                        //L2/L3 圓
                        Container(
                          width: 40,
                          height: 40,
                          decoration: BoxDecoration(
                            borderRadius:
                                const BorderRadius.all(Radius.circular(50)),
                            border: Border.all(width: 7.0, color: Colors.black),
                            color: lumbarRight == 4 ? Colors.red : Colors.white,
                          ),
                        ),
                        //L2/L3 Text
                        const FittedBox(
                          fit: BoxFit.scaleDown,
                          child: Text(
                            'L2/L3',
                            textScaleFactor: 1,
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    //L3/L4
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        //L3/L4 Text
                        const FittedBox(
                          fit: BoxFit.scaleDown,
                          child: Text(
                            'L3/L4',
                            textScaleFactor: 1,
                          ),
                        ),
                        //L3/L4 圓
                        Container(
                          width: 40,
                          height: 40,
                          decoration: BoxDecoration(
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(50)),
                              border:
                                  Border.all(width: 7.0, color: Colors.black),
                              color:
                                  lumbarLeft == 3 ? Colors.red : Colors.white),
                        ),
                        //L3/L4 線
                        SizedBox(
                          width: 180,
                          child: DottedLine(
                            lineThickness: 3.0,
                            dashColor: HexColor.fromHex('#24AF88'),
                          ),
                        ),
                        //L3/L4 圓
                        Container(
                          width: 40,
                          height: 40,
                          decoration: BoxDecoration(
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(50)),
                              border:
                                  Border.all(width: 7.0, color: Colors.black),
                              color:
                                  lumbarRight == 3 ? Colors.red : Colors.white),
                        ),
                        //L3/L4 Text
                        const FittedBox(
                          fit: BoxFit.scaleDown,
                          child: Text(
                            'L3/L4',
                            textScaleFactor: 1,
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    //L4/L5
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        //L4/L5 Text
                        const FittedBox(
                          fit: BoxFit.scaleDown,
                          child: Text(
                            'L4/L5',
                            textScaleFactor: 1,
                          ),
                        ),
                        //L4/L5 圓
                        Container(
                          width: 40,
                          height: 40,
                          decoration: BoxDecoration(
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(50)),
                              border:
                                  Border.all(width: 7.0, color: Colors.black),
                              color:
                                  lumbarLeft == 2 ? Colors.red : Colors.white),
                        ),
                        //L4/L5 線
                        SizedBox(
                          width: 180,
                          child: DottedLine(
                            lineThickness: 3.0,
                            dashColor: HexColor.fromHex('#24AF88'),
                          ),
                        ),
                        //L4/L5 圓
                        Container(
                          width: 40,
                          height: 40,
                          decoration: BoxDecoration(
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(50)),
                              border:
                                  Border.all(width: 7.0, color: Colors.black),
                              color:
                                  lumbarRight == 2 ? Colors.red : Colors.white),
                        ),
                        //L4/L5 Text
                        const FittedBox(
                          fit: BoxFit.scaleDown,
                          child: Text(
                            'L4/L5',
                            textScaleFactor: 1,
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    //L5/S1
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        //L5/S1 Text
                        const FittedBox(
                          fit: BoxFit.scaleDown,
                          child: Text(
                            'L5/S1',
                            textScaleFactor: 1,
                          ),
                        ),
                        //L5/S1 圓
                        Container(
                          width: 40,
                          height: 40,
                          decoration: BoxDecoration(
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(50)),
                              border:
                                  Border.all(width: 7.0, color: Colors.black),
                              color:
                                  lumbarLeft == 1 ? Colors.red : Colors.white),
                        ),
                        //L5/S1 線
                        SizedBox(
                          width: 180,
                          child: DottedLine(
                            lineThickness: 3.0,
                            dashColor: HexColor.fromHex('#24AF88'),
                          ),
                        ),
                        //L5/S1 圓
                        Container(
                          width: 40,
                          height: 40,
                          decoration: BoxDecoration(
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(50)),
                              border:
                                  Border.all(width: 7.0, color: Colors.black),
                              color:
                                  lumbarRight == 1 ? Colors.red : Colors.white),
                        ),
                        //L5/S1 Text
                        const FittedBox(
                          fit: BoxFit.scaleDown,
                          child: Text(
                            'L5/S1',
                            textScaleFactor: 1,
                          ),
                        ),
                      ],
                    ),
                  ],
                ))
          ],
        ),
      ),
    );
  }
}
