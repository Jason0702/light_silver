import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:light_silver/database/database_helper.dart';
import 'package:light_silver/language/language_constants.dart';
import 'package:light_silver/models/operation_item_model.dart';
import 'package:light_silver/models/patient.dart';
import 'package:light_silver/routes/route_name.dart';
import 'package:light_silver/utils/api.dart';
import 'package:light_silver/utils/shared_preference_util.dart';

import 'package:light_silver/utils/utils.dart';
import 'package:light_silver/widgets/button.dart';
import 'package:light_silver/widgets/left_button.dart';
import 'package:provider/provider.dart';

import '../../../app.dart';
import '../../../view_model/operation_item_vm.dart';
import '../../../widgets/hero_photo_view_wrapper.dart';

class SPs04Routes extends StatefulWidget {
  @override
  State createState() => _SPs04RoutesState();
}

class _SPs04RoutesState extends State<SPs04Routes> {
  OperationItemVm? _operationItemVm;
  late DatabaseHelper dbHelper;
  late SharedPreferenceUtil _sharedPreferenceUtil;
  late Patient patient;

  int _PSISLeft = 0;
  int _PSISRight = 0;
  int _ILALeft = 0;
  int _ILARight = 0;
  int _PropLeft = 0;
  int _PropRight = 0;
  OperationItemsModel? imageList;

  //_titleBarBG
  final _titleBarBG = BoxDecoration(
      gradient: LinearGradient(colors: [
    HexColor.fromHex('#1DAE85'),
    HexColor.fromHex('#71C2AB'),
    HexColor.fromHex('#1DAE85'),
  ], begin: Alignment.topCenter, end: Alignment.bottomCenter));

  //leftAndRight
  final _textBG = BoxDecoration(
      borderRadius: const BorderRadius.all(Radius.circular(10)),
      gradient: LinearGradient(colors: [
        HexColor.fromHex('#1DAE85'),
        HexColor.fromHex('#71C2AB'),
        HexColor.fromHex('#1DAE85'),
      ], begin: Alignment.topCenter, end: Alignment.bottomCenter));

  //漸層背景
  final _homeBg = const BoxDecoration(
      image: DecorationImage(
          image: AssetImage('assets/images/home_bg.png'), fit: BoxFit.fill));

  //中間背景
  final _centerBG = const BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.all(Radius.circular(10)),
      boxShadow: [
        BoxShadow(
            color: Colors.black54,
            offset: Offset(5.0, 5.0),
            blurRadius: 10.0,
            spreadRadius: 2.0)
      ]);

  //圖片按鈕背景
  final _imageButtonBG = BoxDecoration(
      borderRadius: const BorderRadius.only(
          topRight: Radius.circular(10.0), bottomRight: Radius.circular(10.0)),
      gradient: LinearGradient(colors: [
        HexColor.fromHex('#0067B4'),
        HexColor.fromHex('#003457'),
      ], begin: Alignment.topCenter, end: Alignment.bottomCenter));

  //選項背景
  final _selectBG = BoxDecoration(
      color: HexColor.fromHex('#24AF88'),
      borderRadius: const BorderRadius.all(Radius.circular(10)));

  _onLayoutDone(_) {
    List list = _operationItemVm!.operationItemsModelList!.where((element) => element.id == 5).toList();
    for (var element in list) {
      imageList = element;
      setState(() {});
    }
    _sharedPreferenceUtil.getPatientName().then((value) => {
          if (value.isNotEmpty)
            {
              dbHelper.getPatient(value).then((value) => {_dataInit(value!)})
            }
        });
  }

  void _dataInit(Patient _patient) {
    patient = _patient;
    log('取得 俯臥(4): $patient');
    _PSISLeft = patient.PSIS_left;
    _PSISRight = patient.PSIS_right;
    _ILALeft = patient.ILA_left;
    _ILARight = patient.ILA_right;
    _PropLeft = patient.Prop_left;
    _PropRight = patient.Prop_right;
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    _sharedPreferenceUtil = SharedPreferenceUtil();
    dbHelper = DatabaseHelper();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  Widget build(BuildContext context) {
    _operationItemVm = Provider.of<OperationItemVm>(context);
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Column(
          children: [
            Expanded(flex: 1, child: _titleBar()),
            Expanded(flex: 10, child: _bodyWidget())
          ],
        ),
      ),
    );
  }

  //最上層標題與按鈕
  Widget _titleBar() {
    return Container(
      decoration: _titleBarBG,
      child: Row(
        children: [
          Expanded(flex: 1, child: LeftSaveButton(func: editPatient)),
          Expanded(
              flex: 3,
              child: Container(
                alignment: Alignment.center,
                child: FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    '${getTranslated(context, 'Prone')}(4)',
                    textScaleFactor: 1,
                    style: const TextStyle(fontSize: 25, color: Colors.white),
                  ),
                ),
              )),
          Expanded(flex: 1, child: _rightTopButton())
        ],
      ),
    );
  }

  //右側按鈕
  Widget _rightTopButton() {
    return InkWell(
      onTap: () {
        delPatient();
        delegate.popUtil();
      },
      child: const Icon(
        Icons.home,
        color: Colors.white,
        size: 45,
      ),
    );
  }

  //主體
  Widget _bodyWidget() {
    return Stack(
      children: [
        Container(
          padding: const EdgeInsets.only(top: 10, bottom: 10),
          decoration: _homeBg,
          alignment: Alignment.center,
          child: Container(
            width: MediaQuery.of(context).size.width / 1.1,
            decoration: _centerBG,
            child: Column(
              children: [
                //主體內部
                _bodyTopWidget(),
                //題目
                _topicWidget(),
                //左或右
                _leftOrRightWidget(),
                const Spacer(),
                //選項
                _optionsWidget(),
                const Spacer(),
                //按鈕
                _bottomButtonWidget(),
              ],
            ),
          ),
        ),
        //身體圖片按鈕
        imageList != null &&
                imageList!.pics.isNotEmpty &&
                imageList!.pics[3].isNotEmpty
            ? InkWell(
                onTap: _bodyImageOnTap,
                child: Container(
                  margin: const EdgeInsets.only(top: 15),
                  decoration: _imageButtonBG,
                  child: Container(
                    margin: const EdgeInsets.all(10),
                    child: Image.asset(
                      'assets/images/people.png',
                      height: 50,
                    ),
                  ),
                ),
              )
            : Container(),
      ],
    );
  }

  //身體圖片按鈕
  void _bodyImageOnTap() {
    showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return HeroPhotoViewRouteWrapper(
            title: '${getTranslated(context, 'Prone')}(4)',
            imageProvider: NetworkImage(
                '$imageUrlOffline$imageUrlPrivate${imageList!.pics[3]}',
                headers: {"Authorization": "Bearer $accessToken"}),
          );
        });
  }

  //主體內部
  Widget _bodyTopWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 10, left: 50, right: 50),
      child: Column(
        children: [
          FittedBox(
            fit: BoxFit.scaleDown,
            child: Text(
              getTranslated(
                  context, 'Evaluation_and_treatment_of_sacral_problems'),
              textScaleFactor: 1,
              style: const TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Container(
            height: 3,
            color: HexColor.fromHex('#4A991E'),
          )
        ],
      ),
    );
  }

  //題目
  Widget _topicWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 20, left: 25),
      child: Row(
        children: [
          Container(
            width: 40,
            height: 40,
            alignment: Alignment.center,
            decoration: BoxDecoration(
                color: HexColor.fromHex('#1E2C39'),
                borderRadius: const BorderRadius.all(Radius.circular(25))),
            child: const FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                '4',
                textScaleFactor: 1,
                style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    color: Colors.white),
              ),
            ),
          ),
          const SizedBox(
            width: 20,
          ),
          FittedBox(
            fit: BoxFit.scaleDown,
            child: Text(
              getTranslated(context, 'Sacral_feature_point_palpation'),
              textScaleFactor: 1,
              style: const TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            ),
          ),
        ],
      ),
    );
  }

  //左或右
  Widget _leftOrRightWidget() {
    return Container(
      height: 50,
      margin: const EdgeInsets.only(top: 10, left: 25, right: 25),
      child: Row(
        children: [
          Expanded(
              flex: 1,
              child: Container(
                alignment: Alignment.center,
                decoration: _textBG,
                child: FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    getTranslated(context, 'Left_side'),
                    textScaleFactor: 1,
                    style: const TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                ),
              )),
          const SizedBox(
            width: 10,
          ),
          Expanded(
              flex: 1,
              child: Container(
                alignment: Alignment.center,
                decoration: _textBG,
                child: FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    getTranslated(context, 'Right_side'),
                    textScaleFactor: 1,
                    style: const TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                ),
              ))
        ],
      ),
    );
  }

  //選項
  Widget _optionsWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 10, left: 25, right: 25),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          //PSIS
          Row(
            children: [
              Expanded(
                  flex: 1,
                  child: Column(
                    children: [
                      Container(
                        height: 60,
                        decoration: BoxDecoration(
                            borderRadius:
                                const BorderRadius.all(Radius.circular(10)),
                            color: Colors.grey[200],
                            boxShadow: const [
                              BoxShadow(
                                  color: Colors.black38,
                                  offset: Offset(5.0, 5.0),
                                  blurRadius: 10.0,
                                  spreadRadius: 2.0)
                            ]),
                        child: Row(
                          children: [
                            Expanded(
                                flex: 1,
                                child: InkWell(
                                  onTap: _PSISLeft1OnTap,
                                  child: Container(
                                    alignment: Alignment.center,
                                    margin: const EdgeInsets.all(5),
                                    decoration:
                                        _PSISLeft == 1 ? _selectBG : null,
                                    child: InkWell(
                                      onTap: _PSISLeft1OnTap,
                                      child: FittedBox(
                                        fit: BoxFit.scaleDown,
                                        child: Text(
                                          getTranslated(context, 'high'),
                                          textScaleFactor: 1,
                                          style: TextStyle(
                                              color: _PSISLeft == 1
                                                  ? Colors.white
                                                  : Colors.black),
                                        ),
                                      ),
                                    ),
                                  ),
                                )),
                            Expanded(
                                flex: 1,
                                child: InkWell(
                                  onTap: _PSISLeft2OnTap,
                                  child: Container(
                                    alignment: Alignment.center,
                                    margin: const EdgeInsets.all(5),
                                    decoration:
                                        _PSISLeft == 2 ? _selectBG : null,
                                    child: FittedBox(
                                      fit: BoxFit.scaleDown,
                                      child: Text(
                                        getTranslated(context, 'low'),
                                        textScaleFactor: 1,
                                        style: TextStyle(
                                            color: _PSISLeft == 2
                                                ? Colors.white
                                                : Colors.black),
                                      ),
                                    ),
                                  ),
                                )),
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      const FittedBox(
                        fit: BoxFit.scaleDown,
                        child: Text(
                          'PSIS',
                          textScaleFactor: 1,
                          style: TextStyle(
                              color: Colors.black, fontWeight: FontWeight.bold),
                        ),
                      )
                    ],
                  )),
              const SizedBox(
                width: 10,
              ),
              Expanded(
                  flex: 1,
                  child: Column(
                    children: [
                      Container(
                        height: 60,
                        decoration: BoxDecoration(
                            borderRadius:
                                const BorderRadius.all(Radius.circular(10)),
                            color: Colors.grey[200],
                            boxShadow: const [
                              BoxShadow(
                                  color: Colors.black38,
                                  offset: Offset(5.0, 5.0),
                                  blurRadius: 10.0,
                                  spreadRadius: 2.0)
                            ]),
                        child: Row(
                          children: [
                            Expanded(
                                flex: 1,
                                child: InkWell(
                                  onTap: _PSISRight1OnTap,
                                  child: Container(
                                    alignment: Alignment.center,
                                    margin: const EdgeInsets.all(5),
                                    decoration:
                                        _PSISRight == 1 ? _selectBG : null,
                                    child: FittedBox(
                                      fit: BoxFit.scaleDown,
                                      child: Text(
                                        getTranslated(context, 'high'),
                                        textScaleFactor: 1,
                                        style: TextStyle(
                                            color: _PSISRight == 1
                                                ? Colors.white
                                                : Colors.black),
                                      ),
                                    ),
                                  ),
                                )),
                            Expanded(
                                flex: 1,
                                child: InkWell(
                                  onTap: _PSISRight2OnTap,
                                  child: Container(
                                    alignment: Alignment.center,
                                    margin: const EdgeInsets.all(5),
                                    decoration:
                                        _PSISRight == 2 ? _selectBG : null,
                                    child: FittedBox(
                                      fit: BoxFit.scaleDown,
                                      child: Text(
                                        getTranslated(context, 'low'),
                                        textScaleFactor: 1,
                                        style: TextStyle(
                                            color: _PSISRight == 2
                                                ? Colors.white
                                                : Colors.black),
                                      ),
                                    ),
                                  ),
                                )),
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      const FittedBox(
                        fit: BoxFit.scaleDown,
                        child: Text(
                          'PSIS',
                          textScaleFactor: 1,
                          style: TextStyle(
                              color: Colors.black, fontWeight: FontWeight.bold),
                        ),
                      )
                    ],
                  )),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          //ILT
          Row(
            children: [
              Expanded(
                  flex: 1,
                  child: Column(
                    children: [
                      Container(
                        height: 60,
                        decoration: BoxDecoration(
                            borderRadius:
                                const BorderRadius.all(Radius.circular(10)),
                            color: Colors.grey[200],
                            boxShadow: const [
                              BoxShadow(
                                  color: Colors.black38,
                                  offset: Offset(5.0, 5.0),
                                  blurRadius: 10.0,
                                  spreadRadius: 2.0)
                            ]),
                        child: Row(
                          children: [
                            Expanded(
                                flex: 1,
                                child: InkWell(
                                  onTap: _ILALeft1OnTap,
                                  child: Container(
                                    alignment: Alignment.center,
                                    margin: const EdgeInsets.all(5),
                                    decoration:
                                        _ILALeft == 1 ? _selectBG : null,
                                    child: FittedBox(
                                      fit: BoxFit.scaleDown,
                                      child: Text(
                                        getTranslated(context, 'high'),
                                        textScaleFactor: 1,
                                        style: TextStyle(
                                            color: _ILALeft == 1
                                                ? Colors.white
                                                : Colors.black),
                                      ),
                                    ),
                                  ),
                                )),
                            Expanded(
                                flex: 1,
                                child: InkWell(
                                  onTap: _ILALeft2OnTap,
                                  child: Container(
                                    alignment: Alignment.center,
                                    margin: const EdgeInsets.all(5),
                                    decoration:
                                        _ILALeft == 2 ? _selectBG : null,
                                    child: FittedBox(
                                      fit: BoxFit.scaleDown,
                                      child: Text(
                                        getTranslated(context, 'low'),
                                        textScaleFactor: 1,
                                        style: TextStyle(
                                            color: _ILALeft == 2
                                                ? Colors.white
                                                : Colors.black),
                                      ),
                                    ),
                                  ),
                                )),
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      const FittedBox(
                        fit: BoxFit.scaleDown,
                        child: Text(
                          'ILA',
                          textScaleFactor: 1,
                          style: TextStyle(
                              color: Colors.black, fontWeight: FontWeight.bold),
                        ),
                      )
                    ],
                  )),
              const SizedBox(
                width: 10,
              ),
              Expanded(
                  flex: 1,
                  child: Column(
                    children: [
                      Container(
                        height: 60,
                        decoration: BoxDecoration(
                            borderRadius:
                                const BorderRadius.all(Radius.circular(10)),
                            color: Colors.grey[200],
                            boxShadow: const [
                              BoxShadow(
                                  color: Colors.black38,
                                  offset: Offset(5.0, 5.0),
                                  blurRadius: 10.0,
                                  spreadRadius: 2.0)
                            ]),
                        child: Row(
                          children: [
                            Expanded(
                                flex: 1,
                                child: InkWell(
                                  onTap: _ILTRight1OnTap,
                                  child: Container(
                                    alignment: Alignment.center,
                                    margin: const EdgeInsets.all(5),
                                    decoration:
                                        _ILARight == 1 ? _selectBG : null,
                                    child: FittedBox(
                                      fit: BoxFit.scaleDown,
                                      child: Text(
                                        getTranslated(context, 'high'),
                                        textScaleFactor: 1,
                                        style: TextStyle(
                                            color: _ILARight == 1
                                                ? Colors.white
                                                : Colors.black),
                                      ),
                                    ),
                                  ),
                                )),
                            Expanded(
                                flex: 1,
                                child: InkWell(
                                  onTap: _ILTRight2OnTap,
                                  child: Container(
                                    alignment: Alignment.center,
                                    margin: const EdgeInsets.all(5),
                                    decoration:
                                        _ILARight == 2 ? _selectBG : null,
                                    child: FittedBox(
                                      fit: BoxFit.scaleDown,
                                      child: Text(
                                        getTranslated(context, 'low'),
                                        textScaleFactor: 1,
                                        style: TextStyle(
                                            color: _ILARight == 2
                                                ? Colors.white
                                                : Colors.black),
                                      ),
                                    ),
                                  ),
                                )),
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      const FittedBox(
                        fit: BoxFit.scaleDown,
                        child: Text(
                          'ILA',
                          textScaleFactor: 1,
                          style: TextStyle(
                              color: Colors.black, fontWeight: FontWeight.bold),
                        ),
                      )
                    ],
                  )),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          //Prop
          Column(
            children: [
              Container(
                alignment: Alignment.centerLeft,
                child: FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    'Prone Prop(${getTranslated(context, 'Beauty')})',
                    textScaleFactor: 1,
                    style: const TextStyle(
                        color: Colors.black, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              Row(
                children: [
                  Expanded(
                      flex: 1,
                      child: Column(
                        children: [
                          Container(
                            height: 60,
                            decoration: BoxDecoration(
                                borderRadius:
                                    const BorderRadius.all(Radius.circular(10)),
                                color: _PSISLeft == 0 ||
                                        _PSISRight == 0 ||
                                        _ILALeft == 0 ||
                                        _ILARight == 0 ||
                                        (_PSISLeft == 1 && _PSISRight == 1) ||
                                        (_PSISLeft == 2 && _PSISRight == 2) ||
                                        (_ILALeft == 1 && _ILARight == 1) ||
                                        (_ILALeft == 2 && _ILARight == 2)
                                    ? Colors.grey
                                    : Colors.grey[200],
                                boxShadow: const [
                                  BoxShadow(
                                      color: Colors.black38,
                                      offset: Offset(5.0, 5.0),
                                      blurRadius: 10.0,
                                      spreadRadius: 2.0)
                                ]),
                            child: Row(
                              children: [
                                Expanded(
                                    flex: 1,
                                    child: InkWell(
                                      onTap: _PSISLeft == 0 ||
                                              _PSISRight == 0 ||
                                              _ILALeft == 0 ||
                                              _ILARight == 0 ||
                                              (_PSISLeft == 1 &&
                                                  _PSISRight == 1) ||
                                              (_PSISLeft == 2 &&
                                                  _PSISRight == 2) ||
                                              (_ILALeft == 1 &&
                                                  _ILARight == 1) ||
                                              (_ILALeft == 2 && _ILARight == 2)
                                          ? null
                                          : _PropLeft1OnTap,
                                      child: Container(
                                        alignment: Alignment.center,
                                        margin: const EdgeInsets.all(5),
                                        decoration:
                                            _PropLeft == 1 ? _selectBG : null,
                                        child: FittedBox(
                                          fit: BoxFit.scaleDown,
                                          child: Text(
                                            getTranslated(context, 'high'),
                                            textScaleFactor: 1,
                                            style: TextStyle(
                                                color: _PropLeft == 1
                                                    ? Colors.white
                                                    : Colors.black),
                                          ),
                                        ),
                                      ),
                                    )),
                                Expanded(
                                    flex: 1,
                                    child: InkWell(
                                      onTap: _PSISLeft == 0 ||
                                              _PSISRight == 0 ||
                                              _ILALeft == 0 ||
                                              _ILARight == 0 ||
                                              (_PSISLeft == 1 &&
                                                  _PSISRight == 1) ||
                                              (_PSISLeft == 2 &&
                                                  _PSISRight == 2) ||
                                              (_ILALeft == 1 &&
                                                  _ILARight == 1) ||
                                              (_ILALeft == 2 && _ILARight == 2)
                                          ? null
                                          : _PropLeft2OnTap,
                                      child: Container(
                                        alignment: Alignment.center,
                                        margin: const EdgeInsets.all(5),
                                        decoration:
                                            _PropLeft == 2 ? _selectBG : null,
                                        child: FittedBox(
                                          fit: BoxFit.scaleDown,
                                          child: Text(
                                            getTranslated(context, 'low'),
                                            textScaleFactor: 1,
                                            style: TextStyle(
                                                color: _PropLeft == 2
                                                    ? Colors.white
                                                    : Colors.black),
                                          ),
                                        ),
                                      ),
                                    )),
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          const FittedBox(
                            fit: BoxFit.scaleDown,
                            child: Text(
                              'PSIS',
                              textScaleFactor: 1,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold),
                            ),
                          )
                        ],
                      )),
                  const SizedBox(
                    width: 10,
                  ),
                  Expanded(
                      flex: 1,
                      child: Column(
                        children: [
                          Container(
                            height: 60,
                            decoration: BoxDecoration(
                                borderRadius:
                                    const BorderRadius.all(Radius.circular(10)),
                                color: _PSISLeft == 0 ||
                                        _PSISRight == 0 ||
                                        _ILALeft == 0 ||
                                        _ILARight == 0 ||
                                        (_PSISLeft == 1 && _PSISRight == 1) ||
                                        (_PSISLeft == 2 && _PSISRight == 2) ||
                                        (_ILALeft == 1 && _ILARight == 1) ||
                                        (_ILALeft == 2 && _ILARight == 2)
                                    ? Colors.grey
                                    : Colors.grey[200],
                                boxShadow: const [
                                  BoxShadow(
                                      color: Colors.black38,
                                      offset: Offset(5.0, 5.0),
                                      blurRadius: 10.0,
                                      spreadRadius: 2.0)
                                ]),
                            child: Row(
                              children: [
                                Expanded(
                                    flex: 1,
                                    child: InkWell(
                                      onTap: _PSISLeft == 0 ||
                                              _PSISRight == 0 ||
                                              _ILALeft == 0 ||
                                              _ILARight == 0 ||
                                              (_PSISLeft == 1 &&
                                                  _PSISRight == 1) ||
                                              (_PSISLeft == 2 &&
                                                  _PSISRight == 2) ||
                                              (_ILALeft == 1 &&
                                                  _ILARight == 1) ||
                                              (_ILALeft == 2 && _ILARight == 2)
                                          ? null
                                          : _PropRight1OnTap,
                                      child: Container(
                                        alignment: Alignment.center,
                                        margin: const EdgeInsets.all(5),
                                        decoration:
                                            _PropRight == 1 ? _selectBG : null,
                                        child: FittedBox(
                                          fit: BoxFit.scaleDown,
                                          child: Text(
                                            getTranslated(context, 'high'),
                                            textScaleFactor: 1,
                                            style: TextStyle(
                                                color: _PropRight == 1
                                                    ? Colors.white
                                                    : Colors.black),
                                          ),
                                        ),
                                      ),
                                    )),
                                Expanded(
                                    flex: 1,
                                    child: InkWell(
                                      onTap: _PSISLeft == 0 ||
                                              _PSISRight == 0 ||
                                              _ILALeft == 0 ||
                                              _ILARight == 0 ||
                                              (_PSISLeft == 1 &&
                                                  _PSISRight == 1) ||
                                              (_PSISLeft == 2 &&
                                                  _PSISRight == 2) ||
                                              (_ILALeft == 1 &&
                                                  _ILARight == 1) ||
                                              (_ILALeft == 2 && _ILARight == 2)
                                          ? null
                                          : _PropRight2OnTap,
                                      child: Container(
                                        alignment: Alignment.center,
                                        margin: const EdgeInsets.all(5),
                                        decoration:
                                            _PropRight == 2 ? _selectBG : null,
                                        child: FittedBox(
                                          fit: BoxFit.scaleDown,
                                          child: Text(
                                            getTranslated(context, 'low'),
                                            textScaleFactor: 1,
                                            style: TextStyle(
                                                color: _PropRight == 2
                                                    ? Colors.white
                                                    : Colors.black),
                                          ),
                                        ),
                                      ),
                                    )),
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          const FittedBox(
                            fit: BoxFit.scaleDown,
                            child: Text(
                              'PSIS',
                              textScaleFactor: 1,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold),
                            ),
                          )
                        ],
                      )),
                ],
              )
            ],
          )
        ],
      ),
    );
  }

  //按鈕
  Widget _bottomButtonWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 10, right: 10, bottom: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          //清除
          ClearButton(click: _clearOnTap, buttonText: getTranslated(context, 'Clear')),
          const SizedBox(
            width: 25,
          ),
          //下一步
          NextButton(
              click: _PSISLeft == 0 ||
                      _PSISRight == 0 ||
                      _ILALeft == 0 ||
                      _ILARight == 0
                  ? null
                  : _nextOnTap,
              buttonText: getTranslated(context, 'Next_step'),
              decoration: _PSISLeft == 0 ||
                      _PSISRight == 0 ||
                      _ILALeft == 0 ||
                      _ILARight == 0
                  ? null
                  : Style.spsNextBG)
        ],
      ),
    );
  }

  //清除
  void _clearOnTap() {
    delPatient();
    setState(() {
      _PSISLeft = 0;
      _PSISRight = 0;
      _ILALeft = 0;
      _ILARight = 0;
      _PropLeft = 0;
      _PropRight = 0;
    });
  }

  //下一步
  void _nextOnTap() {
    editPatient();
    delegate.push(name: RouteName.sps05);
  }

  void editPatient() {
    Patient _patient = Patient(
        name: patient.name,
        gender: patient.gender,
        age: patient.age,
        phone: patient.phone,
        problem: patient.problem,
        relieveSymptomsOfDiscomfort: patient.relieveSymptomsOfDiscomfort,
        postureCorrection: patient.postureCorrection,
        informationHealthEducation: patient.informationHealthEducation,
        jointReduction: patient.jointReduction,
        coreMuscleTraining: patient.coreMuscleTraining,
        healthPromotion: patient.healthPromotion,
        GT: patient.GT,
        pad: patient.pad,
        padHigh: patient.padHigh.isEmpty ? '' : patient.padHigh,
        IC: patient.IC,
        ASIS: patient.ASIS,
        PSIS: patient.PSIS,
        flexion: patient.flexion,
        floor: patient.floor,
        gillet: patient.gillet,
        Extension: patient.Extension,
        rotation_right: patient.rotation_right,
        rotation_left: patient.rotation_left,
        side_left: patient.side_left,
        side_right: patient.side_right,
        side_knee_left: patient.side_knee_left,
        side_knee_right: patient.side_knee_right,
        supine_leg_length: patient.supine_leg_length,
        adductor: patient.adductor,
        inguinal: patient.inguinal,
        prone_leg_length: patient.prone_leg_length,
        add_ROM: patient.add_ROM,
        ext_rotator: patient.ext_rotator,
        PSIS_left: _PSISLeft,
        PSIS_right: _PSISRight,
        ILA_left: _ILALeft,
        ILA_right: _ILARight,
        Prop_left: _PropLeft,
        Prop_right: _PropRight,
        lumbar_left: patient.lumbar_left,
        lumbar_right: patient.lumbar_right,
        supine_ilium_leg_length: patient.supine_ilium_leg_length,
        Long_sit: patient.Long_sit,
        SLR: patient.SLR,
        Patrick: patient.Patrick,
        Knee_flexor: patient.Knee_flexor,
        ASIS_umbilical: patient.ASIS_umbilical,
        IC_recheck: patient.IC_recheck,
        flexion_recheck: patient.flexion_recheck,
        floor_recheck: patient.floor_recheck,
        gillet_recheck: patient.gillet_recheck,
        extension_recheck: patient.extension_recheck,
        rotation_left_recheck: patient.rotation_left_recheck,
        rotation_right_recheck: patient.rotation_right_recheck,
        side_left_recheck: patient.side_left_recheck,
        side_right_recheck: patient.side_right_recheck,
        side_knee_left_recheck: patient.side_knee_left_recheck,
        side_knee_right_recheck: patient.side_knee_right_recheck);
    dbHelper.updatePatient(_patient);
  }

  void delPatient() {
    Patient _patient = Patient(
        name: patient.name,
        gender: patient.gender,
        age: patient.age,
        phone: patient.phone,
        problem: patient.problem,
        relieveSymptomsOfDiscomfort: patient.relieveSymptomsOfDiscomfort,
        postureCorrection: patient.postureCorrection,
        informationHealthEducation: patient.informationHealthEducation,
        jointReduction: patient.jointReduction,
        coreMuscleTraining: patient.coreMuscleTraining,
        healthPromotion: patient.healthPromotion,
        GT: patient.GT,
        pad: patient.pad,
        padHigh: patient.padHigh.isEmpty ? '' : patient.padHigh,
        IC: patient.IC,
        ASIS: patient.ASIS,
        PSIS: patient.PSIS,
        flexion: patient.flexion,
        floor: patient.floor,
        gillet: patient.gillet,
        Extension: patient.Extension,
        rotation_right: patient.rotation_right,
        rotation_left: patient.rotation_left,
        side_left: patient.side_left,
        side_right: patient.side_right,
        side_knee_left: patient.side_knee_left,
        side_knee_right: patient.side_knee_right,
        supine_leg_length: patient.supine_leg_length,
        adductor: patient.adductor,
        inguinal: patient.inguinal,
        prone_leg_length: patient.prone_leg_length,
        add_ROM: patient.add_ROM,
        ext_rotator: patient.ext_rotator,
        PSIS_left: 0,
        PSIS_right: 0,
        ILA_left: 0,
        ILA_right: 0,
        Prop_left: 0,
        Prop_right: 0,
        lumbar_left: patient.lumbar_left,
        lumbar_right: patient.lumbar_right,
        supine_ilium_leg_length: patient.supine_ilium_leg_length,
        Long_sit: patient.Long_sit,
        SLR: patient.SLR,
        Patrick: patient.Patrick,
        Knee_flexor: patient.Knee_flexor,
        ASIS_umbilical: patient.ASIS_umbilical,
        IC_recheck: patient.IC_recheck,
        flexion_recheck: patient.flexion_recheck,
        floor_recheck: patient.floor_recheck,
        gillet_recheck: patient.gillet_recheck,
        extension_recheck: patient.extension_recheck,
        rotation_left_recheck: patient.rotation_left_recheck,
        rotation_right_recheck: patient.rotation_right_recheck,
        side_left_recheck: patient.side_left_recheck,
        side_right_recheck: patient.side_right_recheck,
        side_knee_left_recheck: patient.side_knee_left_recheck,
        side_knee_right_recheck: patient.side_knee_right_recheck);
    dbHelper
        .updatePatient(_patient)
        .then((value) => {EasyLoading.showToast('清除完成: $value')});
  }

  void delAllPatient() {
    Patient _patient = Patient(
        name: patient.name,
        gender: patient.gender,
        age: patient.age,
        phone: patient.phone,
        problem: patient.problem,
        relieveSymptomsOfDiscomfort: patient.relieveSymptomsOfDiscomfort,
        postureCorrection: patient.postureCorrection,
        informationHealthEducation: patient.informationHealthEducation,
        jointReduction: patient.jointReduction,
        coreMuscleTraining: patient.coreMuscleTraining,
        healthPromotion: patient.healthPromotion,
        GT: patient.GT,
        pad: patient.pad,
        padHigh: patient.padHigh,
        IC: patient.IC,
        ASIS: patient.ASIS,
        PSIS: patient.PSIS,
        flexion: patient.flexion,
        floor: patient.floor,
        gillet: patient.gillet,
        Extension: patient.Extension,
        rotation_right: patient.rotation_right,
        rotation_left: patient.rotation_left,
        side_left: patient.side_left,
        side_right: patient.side_right,
        side_knee_left: patient.side_knee_left,
        side_knee_right: patient.side_knee_right,
        supine_leg_length: patient.supine_leg_length,
        adductor: patient.adductor,
        inguinal: patient.inguinal,
        prone_leg_length: 0,
        add_ROM: 0,
        ext_rotator: 0,
        PSIS_left: 0,
        PSIS_right: 0,
        ILA_left: 0,
        ILA_right: 0,
        Prop_left: 0,
        Prop_right: 0,
        lumbar_left: patient.lumbar_left,
        lumbar_right: patient.lumbar_right,
        supine_ilium_leg_length: patient.supine_ilium_leg_length,
        Long_sit: patient.Long_sit,
        SLR: patient.SLR,
        Patrick: patient.Patrick,
        Knee_flexor: patient.Knee_flexor,
        ASIS_umbilical: patient.ASIS_umbilical,
        IC_recheck: patient.IC_recheck,
        flexion_recheck: patient.flexion_recheck,
        floor_recheck: patient.floor_recheck,
        gillet_recheck: patient.gillet_recheck,
        extension_recheck: patient.extension_recheck,
        rotation_left_recheck: patient.rotation_left_recheck,
        rotation_right_recheck: patient.rotation_right_recheck,
        side_left_recheck: patient.side_left_recheck,
        side_right_recheck: patient.side_right_recheck,
        side_knee_left_recheck: patient.side_knee_left_recheck,
        side_knee_right_recheck: patient.side_knee_right_recheck);
    dbHelper.updatePatient(_patient);
  }

  void _PSISLeft1OnTap() {
    setState(() {
      _PSISLeft = 1;
      if (_PropRight != 0 || _PropLeft != 0) {
        _PropRight = 0;
        _PropLeft = 0;
      }
    });
  }

  void _PSISLeft2OnTap() {
    setState(() {
      _PSISLeft = 2;
      if (_PropRight != 0 || _PropLeft != 0) {
        _PropRight = 0;
        _PropLeft = 0;
      }
    });
  }

  void _PSISRight1OnTap() {
    setState(() {
      _PSISRight = 1;
      if (_PropRight != 0 || _PropLeft != 0) {
        _PropRight = 0;
        _PropLeft = 0;
      }
    });
  }

  void _PSISRight2OnTap() {
    setState(() {
      _PSISRight = 2;
      if (_PropRight != 0 || _PropLeft != 0) {
        _PropRight = 0;
        _PropLeft = 0;
      }
    });
  }

  void _ILALeft1OnTap() {
    setState(() {
      _ILALeft = 1;
      if (_PropRight != 0 || _PropLeft != 0) {
        _PropRight = 0;
        _PropLeft = 0;
      }
    });
  }

  void _ILALeft2OnTap() {
    setState(() {
      _ILALeft = 2;
      if (_PropRight != 0 || _PropLeft != 0) {
        _PropRight = 0;
        _PropLeft = 0;
      }
    });
  }

  void _ILTRight1OnTap() {
    setState(() {
      _ILARight = 1;
      if (_PropRight != 0 || _PropLeft != 0) {
        _PropRight = 0;
        _PropLeft = 0;
      }
    });
  }

  void _ILTRight2OnTap() {
    setState(() {
      _ILARight = 2;
      if (_PropRight != 0 || _PropLeft != 0) {
        _PropRight = 0;
        _PropLeft = 0;
      }
    });
  }

  void _PropLeft1OnTap() {
    setState(() {
      _PropLeft = 1;
    });
  }

  void _PropLeft2OnTap() {
    setState(() {
      _PropLeft = 2;
    });
  }

  void _PropRight1OnTap() {
    setState(() {
      _PropRight = 1;
    });
  }

  void _PropRight2OnTap() {
    setState(() {
      _PropRight = 2;
    });
  }
}
