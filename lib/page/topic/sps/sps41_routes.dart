import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:light_silver/language/language_constants.dart';
import 'package:light_silver/models/operation_item_model.dart';
import 'package:light_silver/models/patient.dart';
import 'package:light_silver/routes/route_name.dart';
import 'package:light_silver/utils/api.dart';
import 'package:light_silver/utils/utils.dart';
import 'package:light_silver/widgets/left_button.dart';
import 'package:light_silver/widgets/noShadowScrollbehavior.dart';
import 'package:provider/provider.dart';

import '../../../app.dart';
import '../../../view_model/operation_item_vm.dart';
import '../../../view_model/patient_vm.dart';
import '../../../widgets/button.dart';
import '../../../widgets/title_widget.dart';
import '../../network_image_page.dart';
import '../video_palyer_screen.dart';

class SPs41Routes extends StatefulWidget {
  @override
  State createState() => _SPs41RoutesState();
}

class _SPs41RoutesState extends State<SPs41Routes> {
  PatientVm? _patientVm;
  OperationItemVm? _operationItemVm;

  OperationItemsModel? imageList;
  String netUpImageUrl = '';
  String netDownImageUrl = '';
  String netVideoUrl = '';

  final TextEditingController _iliumSide = TextEditingController();

  _onLayoutDone(_) {
    List list =
    _operationItemVm!.operationItemsModelList!.where((element) => element.id == 4).toList();
    for (var element in list) {
      imageList = element;
      setState(() {});
    }
    _dataInit(_patientVm!.patient);
  }

  void _dataInit(Patient? patient) {
    if (patient == null) {
      return;
    }
    log('取得 俯臥(4-1): $patient');
    switch (patient.gillet) {
      case 1:
        _iliumSide.text = getTranslated(context, 'left');
        if (imageList != null) {
          debugPrint(imageList!.completePics
              .where((element) => element.containsKey('左'))
              .toString());
          debugPrint(imageList!.completeVideos
              .where((element) => element.containsKey('左'))
              .first
              .toString());
          Map<String, dynamic> map = imageList!.completePics
              .where((element) => element.containsKey('左'))
              .first;
          Map<String, CompleteVideo> videoMap = imageList!.completeVideos
              .where((element) => element.containsKey('左'))
              .first;
          netUpImageUrl = map.values.first;
          netDownImageUrl = videoMap.values.first.thumbnail;
          netVideoUrl = videoMap.values.first.src;
        }
        break;
      case 2:
        _iliumSide.text = getTranslated(context, 'right');
        if (imageList != null) {
          debugPrint(imageList!.completePics
              .where((element) => element.containsKey('右'))
              .toString());
          debugPrint(imageList!.completeVideos
              .where((element) => element.containsKey('右'))
              .first
              .toString());
          Map<String, dynamic> map = imageList!.completePics
              .where((element) => element.containsKey('右'))
              .first;
          Map<String, CompleteVideo> videoMap = imageList!.completeVideos
              .where((element) => element.containsKey('右'))
              .first;
          netUpImageUrl = map.values.first;
          netDownImageUrl = videoMap.values.first.thumbnail;
          netVideoUrl = videoMap.values.first.src;
        }
        break;
    }
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  Widget build(BuildContext context) {
    _patientVm = Provider.of<PatientVm>(context);
    _operationItemVm = Provider.of<OperationItemVm>(context);
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Column(
          children: [
            Expanded(flex: 1, child: _titleBar()),
            Expanded(flex: 10, child: _bodyWidget())
          ],
        ),
      ),
    );
  }

  //最上層標題與按鈕
  Widget _titleBar() {
    return SpsTitleWidget(
      titleText: '${getTranslated(context, 'Prone')}(1)',
      leftButton: const LeftButton(),
      rightButton: _rightTopButton(),
    );
  }

  //右側按鈕
  Widget _rightTopButton() {
    return InkWell(
      onTap: () => delegate.popUtil(),
      child: const Icon(
        Icons.home,
        color: Colors.white,
        size: 45,
      ),
    );
  }

//主體
  Widget _bodyWidget() {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 10),
      decoration: Style.homeBg,
      alignment: Alignment.center,
      child: Container(
        width: MediaQuery.of(context).size.width / 1.1,
        decoration: Style.centerBG,
        child: ScrollConfiguration(
          behavior: NoShadowScrollBehavior(),
          child: SingleChildScrollView(
            child: Column(
              children: [
                //主體內部
                _bodyTopWidget(),
                //題目
                _topicWidget(),
                //顯示數值
                _showTextWidget(),
                //圖片
                _imageWidget(),
                const SizedBox(
                  height: 10,
                ),
                //影片與圖片
                _videoWidget(),
                //按鈕
                _bottomButtonWidget(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  //主體內部
  Widget _bodyTopWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 20, left: 50, right: 50),
      child: Column(
        children: [
          FittedBox(
            fit: BoxFit.scaleDown,
            child: Text(
              getTranslated(context,
                  'Adjustment_of_the_upward_movement_of_the_iliac_bone'),
              textScaleFactor: 1,
              style: const TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Container(
            height: 3,
            color: HexColor.fromHex('#216AB2'),
          )
        ],
      ),
    );
  }

  //題目
  Widget _topicWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 10, left: 25),
      child: Row(
        children: [
          Container(
            width: 40,
            height: 40,
            alignment: Alignment.center,
            decoration: BoxDecoration(
                color: HexColor.fromHex('#1E2C39'),
                borderRadius: BorderRadius.circular(25)),
            child: const FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                '1',
                textScaleFactor: 1,
                style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    color: Colors.white),
              ),
            ),
          ),
          const SizedBox(
            width: 10,
          ),
          Expanded(
            child: FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                getTranslated(context,
                    'Skills_to_deal_with_the_problem_of_upward_movement_of_the_iliac_bone'),
                textScaleFactor: 1,
                style:
                    const TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
              ),
            ),
          )
        ],
      ),
    );
  }

  //顯示數值
  Widget _showTextWidget() {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 25),
      child: Row(
        children: [
          Container(
            margin: const EdgeInsets.all(8),
            width: 100,
            child: TextField(
              readOnly: true,
              textAlign: TextAlign.center,
              style: TextStyle(color: HexColor.fromHex('#096BC7')),
              decoration: const InputDecoration(
                contentPadding: EdgeInsets.all(5),
                enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black)),
                filled: true,
                fillColor: Colors.white,
              ),
              keyboardType: TextInputType.text,
              textInputAction: TextInputAction.done,
              controller: _iliumSide,
            ),
          ),
          //側
          FittedBox(
            fit: BoxFit.scaleDown,
            child: Text(
              getTranslated(context, 'Lateral_iliac_shift_up'),
              textScaleFactor: 1,
              style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
            ),
          ),
        ],
      ),
    );
  }

  //圖片
  Widget _imageWidget() {
    if (_patientVm!.patient == null) {
      return Container();
    }
    debugPrint('圖片網址: $imageUrlOffline$imageUrlPublic$netUpImageUrl');
    return Container(
      width: 300,
      height: 200,
      decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(Radius.circular(15)),
          border: Border.all(color: Colors.black, width: 1)),
      child: Stack(
        alignment: Alignment.center,
        children: [
          netUpImageUrl.isNotEmpty
              ? Image(
                  image: NetworkImage(
                      '$imageUrlOffline$imageUrlPublic$netUpImageUrl'),
                )
              : Container(),
          Positioned(
              bottom: 10,
              right: 10,
              child: InkWell(
                onTap: () => netUpImageUrl.isNotEmpty ? _upImageOnTap() : null,
                child: const Icon(
                  Icons.zoom_in,
                  size: 45,
                ),
              ))
        ],
      ),
    );
  }

  //影片與圖片
  Widget _videoWidget() {
    return Container(
      width: 300,
      height: 180,
      decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(Radius.circular(15)),
          image: netDownImageUrl.isNotEmpty
              ? DecorationImage(
                  image: NetworkImage(
                      '$imageUrlOffline$imageUrlPublic$netDownImageUrl'),
                  fit: BoxFit.cover,
                )
              : null,
          border: Border.all(color: Colors.black, width: 1)),
      child: Stack(
        alignment: Alignment.center,
        children: [
          Positioned(
              bottom: 0,
              right: 0,
              child: Row(
                children: [
                  InkWell(
                    onTap: () =>
                        netDownImageUrl.isNotEmpty ? _videoOnTap() : null,
                    child: Container(
                      decoration: const BoxDecoration(
                          color: Colors.black,
                          borderRadius: BorderRadius.all(Radius.circular(25))),
                      child: const Icon(
                        Icons.arrow_right,
                        size: 30,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 5,
                  ),
                  InkWell(
                    onTap: () =>
                        netDownImageUrl.isNotEmpty ? _downImageOnTap() : null,
                    child: const Icon(
                      Icons.zoom_in,
                      size: 45,
                    ),
                  )
                ],
              ))
        ],
      ),
    );
  }

  //下面影片
  void _videoOnTap() {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) {
      return VideoPlayerScreen(
        videoUrl: '$imageUrlOffline$imageUrlPublic$netVideoUrl',
      );
    }));
  }

  //下面圖片放大
  void _downImageOnTap() {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) {
      return NetWorkImagePage(
          imageUrl: '$imageUrlOffline$imageUrlPublic$netDownImageUrl');
    }));
  }

  //上面圖片放大
  void _upImageOnTap() {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) {
      return NetWorkImagePage(
          imageUrl: '$imageUrlOffline$imageUrlPublic$netUpImageUrl');
    }));
  }

  //按鈕
  Widget _bottomButtonWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 10, right: 25, bottom: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          //下一步
          NextButton(
            click: _nextOnTap,
            decoration: Style.spsNextBG,
            buttonText: getTranslated(context, 'Next_step'),
          )
        ],
      ),
    );
  }

  //下一步
  void _nextOnTap() {
    delegate.push(name: RouteName.sps01);
  }
}
