import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:light_silver/language/language_constants.dart';
import 'package:light_silver/models/operation_item_model.dart';
import 'package:light_silver/models/patient.dart';
import 'package:light_silver/routes/route_name.dart';
import 'package:light_silver/utils/api.dart';

import 'package:light_silver/utils/utils.dart';
import 'package:light_silver/widgets/button.dart';
import 'package:light_silver/widgets/noShadowScrollbehavior.dart';
import 'package:provider/provider.dart';

import '../../../app.dart';
import '../../../view_model/operation_item_vm.dart';
import '../../../view_model/patient_vm.dart';
import '../../../widgets/title_widget.dart';
import '../../network_image_page.dart';
import '../video_palyer_screen.dart';

class SPs05Routes extends StatefulWidget{

  @override
  State createState() => _SPs05RoutesState();
}
class _SPs05RoutesState extends State<SPs05Routes>{
  PatientVm? _patientVm;
  OperationItemVm? _operationItemVm;

  final TextEditingController _sacrumProblem = TextEditingController();
  OperationItemsModel? imageList;
  String netUpImageUrl = '';
  String netDownImageUrl = '';
  String netVideoUrl = '';

  _onLayoutDone(_) {
    List list = _operationItemVm!.operationItemsModelList!.where((element) => element.id == 5).toList();
    for (var element in list) {
      imageList = element;
      setState(() {});
    }
    _dataInit(_patientVm!.patient);
  }

  void _dataInit(Patient? patient) {
    if (patient == null) {
      return;
    }
    log('取得 俯臥(5): $patient');
    //雙側薦椎屈曲(點頭)
    if(patient.PSIS_left == 1 && patient.PSIS_right == 1 && patient.ILA_left == 1 && patient.ILA_right == 1){
        _sacrumProblem.text = '${getTranslated(context, 'Bilateral_sacral_flexion')}(${getTranslated(context, 'nod')})';
    }
    //雙側薦椎伸直(抬頭)
    if(patient.PSIS_left == 2 && patient.PSIS_right == 2 && patient.ILA_left == 2 && patient.ILA_right == 2){
        _sacrumProblem.text = '${getTranslated(context, 'Bilateral_sacral_extension')}(${getTranslated(context, 'look_up')})';
    }
    //左側薦椎屈曲(點頭)
    if(patient.PSIS_left == 1 && patient.PSIS_right == 2 && patient.ILA_left == 1 && patient.ILA_right == 2 && patient.Prop_left == 1){
        _sacrumProblem.text = '${getTranslated(context, 'Left_sacral_flexion')}(${getTranslated(context, 'nod')})';
    }
    //右側薦椎伸直(抬頭)
    if(patient.PSIS_left == 1 && patient.PSIS_right == 2 && patient.ILA_left == 1 && patient.ILA_right == 2 && patient.Prop_left == 2){
        _sacrumProblem.text = '${getTranslated(context, 'Right_sacral_extension')}(${getTranslated(context, 'look_up')})';
    }
    //左側薦椎伸直(抬頭)
    if(patient.PSIS_left == 2 && patient.PSIS_right == 1 && patient.ILA_left == 2 && patient.ILA_right == 1 && patient.Prop_left == 1){
        _sacrumProblem.text = '${getTranslated(context, 'Left_sacral_extension')}(${getTranslated(context, 'look_up')})';
    }
    //右側薦椎屈曲(點頭)
    if(patient.PSIS_left == 2 && patient.PSIS_right == 1 && patient.ILA_left == 2 && patient.ILA_right == 1 && patient.Prop_left == 2){
        _sacrumProblem.text = '${getTranslated(context, 'Right_sacral_flexion')}(${getTranslated(context, 'nod')})';
    }
    //左側向前扭轉(R/R forward torsion)
    if(patient.PSIS_left == 1 && patient.PSIS_right == 2 && patient.ILA_left == 2 && patient.ILA_right == 1 && patient.Prop_left == 1){
        _sacrumProblem.text = '${getTranslated(context, 'Twisted_left_side_forward')}(R/R forward torsion)';
    }
    //右側向後扭轉(R/Ｌ backward torsion)
    if(patient.PSIS_left == 1 && patient.PSIS_right == 2 && patient.ILA_left == 2 && patient.ILA_right == 1 && patient.Prop_left == 2){
        _sacrumProblem.text = '${getTranslated(context, 'Twisted_right_back')}(R/L backward torsion)';
    }
    //左側向後扭轉(L/R backward torsion)
    if(patient.PSIS_left == 2 && patient.PSIS_right == 1 && patient.ILA_left == 1 && patient.ILA_right == 2 && patient.Prop_left == 1){
        _sacrumProblem.text = '${getTranslated(context, 'Twisted_left_side_backward')}(L/R backward torsion)';
    }
    //右側向前扭轉(L/L forward torsion)
    if(patient.PSIS_left == 2 && patient.PSIS_right == 1 && patient.ILA_left == 1 && patient.ILA_right == 2 && patient.Prop_left == 2){
        _sacrumProblem.text = '${getTranslated(context, 'Twisted_right_side_forward')}(L/L forward torsion)';
    }
    //region 圖片部分
    //雙側薦椎屈曲(點頭)
    if(patient.PSIS_left == 1 && patient.PSIS_right == 1 && patient.ILA_left == 1 && patient.ILA_right == 1){
      debugPrint(imageList!.completePics.where((element) => element.containsKey('雙側薦椎屈曲')).toString());
      debugPrint(imageList!.completeVideos.where((element) => element.containsKey('雙側薦椎屈曲')).toString());
      Map<String, dynamic> map = imageList!.completePics.where((element) => element.containsKey('雙側薦椎屈曲')).first;
      Map<String, CompleteVideo> videoMap = imageList!.completeVideos.where((element) => element.containsKey('雙側薦椎屈曲')).first;
      netUpImageUrl = map.values.first;
      netDownImageUrl = videoMap.values.first.thumbnail;
      netVideoUrl = videoMap.values.first.src;
    }
    //雙側薦椎伸直(抬頭)
    if(patient.PSIS_left == 2 && patient.PSIS_right == 2 && patient.ILA_left == 2 && patient.ILA_right == 2){
      debugPrint(imageList!.completePics.where((element) => element.containsKey('雙側薦椎伸直')).toString());
      debugPrint(imageList!.completeVideos.where((element) => element.containsKey('雙側薦椎伸直')).toString());
      Map<String, dynamic> map = imageList!.completePics.where((element) => element.containsKey('雙側薦椎伸直')).first;
      Map<String, CompleteVideo> videoMap = imageList!.completeVideos.where((element) => element.containsKey('雙側薦椎伸直')).first;
      netUpImageUrl = map.values.first;
      netDownImageUrl = videoMap.values.first.thumbnail;
      netVideoUrl = videoMap.values.first.src;
    }
    //左側薦椎屈曲(點頭)
    if(patient.PSIS_left == 1 && patient.PSIS_right == 2 && patient.ILA_left == 1 && patient.ILA_right == 2 && patient.Prop_left == 1){
      debugPrint(imageList!.completePics.where((element) => element.containsKey('左側薦椎屈曲')).toString());
      debugPrint(imageList!.completeVideos.where((element) => element.containsKey('左側薦椎屈曲')).toString());
      Map<String, dynamic> map = imageList!.completePics.where((element) => element.containsKey('左側薦椎屈曲')).first;
      Map<String, CompleteVideo> videoMap = imageList!.completeVideos.where((element) => element.containsKey('左側薦椎屈曲')).first;
      netUpImageUrl = map.values.first;
      netDownImageUrl = videoMap.values.first.thumbnail;
      netVideoUrl = videoMap.values.first.src;
    }
    //右側薦椎伸直(抬頭)
    if(patient.PSIS_left == 1 && patient.PSIS_right == 2 && patient.ILA_left == 1 && patient.ILA_right == 2 && patient.Prop_left == 2){
      debugPrint(imageList!.completePics.where((element) => element.containsKey('右側薦椎伸直')).toString());
      debugPrint(imageList!.completeVideos.where((element) => element.containsKey('右側薦椎伸直')).toString());
      Map<String, dynamic> map = imageList!.completePics.where((element) => element.containsKey('右側薦椎伸直')).first;
      Map<String, CompleteVideo> videoMap = imageList!.completeVideos.where((element) => element.containsKey('右側薦椎伸直')).first;
      netUpImageUrl = map.values.first;
      netDownImageUrl = videoMap.values.first.thumbnail;
      netVideoUrl = videoMap.values.first.src;
    }
    //左側薦椎伸直(抬頭)
    if(patient.PSIS_left == 2 && patient.PSIS_right == 1 && patient.ILA_left == 2 && patient.ILA_right == 1 && patient.Prop_left == 1){
      debugPrint(imageList!.completePics.where((element) => element.containsKey('左側薦椎伸直')).toString());
      debugPrint(imageList!.completeVideos.where((element) => element.containsKey('左側薦椎伸直')).toString());
      Map<String, dynamic> map = imageList!.completePics.where((element) => element.containsKey('左側薦椎伸直')).first;
      Map<String, CompleteVideo> videoMap = imageList!.completeVideos.where((element) => element.containsKey('左側薦椎伸直')).first;
      netUpImageUrl = map.values.first;
      netDownImageUrl = videoMap.values.first.thumbnail;
      netVideoUrl = videoMap.values.first.src;
    }
    //右側薦椎屈曲(點頭)
    if(patient.PSIS_left == 2 && patient.PSIS_right == 1 && patient.ILA_left == 2 && patient.ILA_right == 1 && patient.Prop_left == 2){
      debugPrint(imageList!.completePics.where((element) => element.containsKey('右側薦椎屈曲')).toString());
      debugPrint(imageList!.completeVideos.where((element) => element.containsKey('右側薦椎屈曲')).toString());
      Map<String, dynamic> map = imageList!.completePics.where((element) => element.containsKey('右側薦椎屈曲')).first;
      Map<String, CompleteVideo> videoMap = imageList!.completeVideos.where((element) => element.containsKey('右側薦椎屈曲')).first;
      netUpImageUrl = map.values.first;
      netDownImageUrl = videoMap.values.first.thumbnail;
      netVideoUrl = videoMap.values.first.src;
    }
    //左側向前扭轉(R/R forward torsion)
    if(patient.PSIS_left == 1 && patient.PSIS_right == 2 && patient.ILA_left == 2 && patient.ILA_right == 1 && patient.Prop_left == 1){
      debugPrint(imageList!.completePics.where((element) => element.containsKey('左側向前扭轉')).toString());
      debugPrint(imageList!.completeVideos.where((element) => element.containsKey('左側向前扭轉')).toString());
      Map<String, dynamic> map = imageList!.completePics.where((element) => element.containsKey('左側向前扭轉')).first;
      Map<String, CompleteVideo> videoMap = imageList!.completeVideos.where((element) => element.containsKey('左側向前扭轉')).first;
      netUpImageUrl = map.values.first;
      netDownImageUrl = videoMap.values.first.thumbnail;
      netVideoUrl = videoMap.values.first.src;
    }
    //右側向後扭轉(R/Ｌ backward torsion)
    if(patient.PSIS_left == 1 && patient.PSIS_right == 2 && patient.ILA_left == 2 && patient.ILA_right == 1 && patient.Prop_left == 2){
      debugPrint(imageList!.completePics.where((element) => element.containsKey('右側向後扭轉')).toString());
      debugPrint(imageList!.completeVideos.where((element) => element.containsKey('右側向後扭轉')).toString());
      Map<String, dynamic> map = imageList!.completePics.where((element) => element.containsKey('右側向後扭轉')).first;
      Map<String, CompleteVideo> videoMap = imageList!.completeVideos.where((element) => element.containsKey('右側向後扭轉')).first;
      netUpImageUrl = map.values.first;
      netDownImageUrl = videoMap.values.first.thumbnail;
      netVideoUrl = videoMap.values.first.src;
    }
    //左側向後扭轉(L/R backward torsion)
    if(patient.PSIS_left == 2 && patient.PSIS_right == 1 && patient.ILA_left == 1 && patient.ILA_right == 2 && patient.Prop_left == 1){
      debugPrint(imageList!.completePics.where((element) => element.containsKey('左側向後扭轉')).toString());
      debugPrint(imageList!.completeVideos.where((element) => element.containsKey('左側向後扭轉')).toString());
      Map<String, dynamic> map = imageList!.completePics.where((element) => element.containsKey('左側向後扭轉')).first;
      Map<String, CompleteVideo> videoMap = imageList!.completeVideos.where((element) => element.containsKey('左側向後扭轉')).first;
      netUpImageUrl = map.values.first;
      netDownImageUrl = videoMap.values.first.thumbnail;
      netVideoUrl = videoMap.values.first.src;
    }
    //右側向前扭轉(L/L forward torsion)
    if(patient.PSIS_left == 2 && patient.PSIS_right == 1 && patient.ILA_left == 1 && patient.ILA_right == 2 && patient.Prop_left == 2){
      debugPrint(imageList!.completePics.where((element) => element.containsKey('右側向前扭轉')).toString());
      debugPrint(imageList!.completeVideos.where((element) => element.containsKey('右側向前扭轉')).toString());
      Map<String, dynamic> map = imageList!.completePics.where((element) => element.containsKey('右側向前扭轉')).first;
      Map<String, CompleteVideo> videoMap = imageList!.completeVideos.where((element) => element.containsKey('右側向前扭轉')).first;
      netUpImageUrl = map.values.first;
      netDownImageUrl = videoMap.values.first.thumbnail;
      netVideoUrl = videoMap.values.first.src;
    }
    //endregion
    setState(() {});

  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  Widget build(BuildContext context) {
    _patientVm = Provider.of<PatientVm>(context);
    _operationItemVm = Provider.of<OperationItemVm>(context);
    return WillPopScope(child: Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Column(
          children: [
            Expanded(flex: 1, child: _titleBar()),
            Expanded(flex: 10, child: _bodyWidget())
          ],
        ),
      ),
    ), onWillPop: () async{
      showBackDialog(false);
      return false;
    });
  }

  //最上層標題與按鈕
  Widget _titleBar() {
    return SpsTitleWidget(
      titleText: '${getTranslated(context, 'Prone')}(5)',
      leftButton: _leftTopButton(),
      rightButton: _rightTopButton(),
    );
  }

  //左側按鈕
  Widget _leftTopButton() {
    return InkWell(
      onTap: () {
        showBackDialog(false);
      },
      child: const Icon(
        Icons.arrow_back_rounded,
        color: Colors.white,
        size: 45,
      ),
    );
  }

  //右側按鈕
  Widget _rightTopButton() {
    return InkWell(
      onTap: () {
        showBackDialog(true);
      },
      child: const Icon(
        Icons.home,
        color: Colors.white,
        size: 45,
      ),
    );
  }

  void showBackDialog(bool isBackHome) async {
    bool isSelect = await showDialog(
        context: context,
        builder: (context){
          return AlertDialog(
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                const FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    '可能會變更之前的評估結果，是否確定\n更改先前評估記錄?',
                    textAlign: TextAlign.left,
                    textScaleFactor: 1,
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 24,
                        fontWeight: FontWeight.bold
                    ),
                  ),
                ),
                const SizedBox(height: 10,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    //是
                    ButtonTheme(
                      height: 80,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            foregroundColor: Colors.white,
                            backgroundColor: Colors.transparent,
                            shape: const RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(Radius.circular(10.0))),
                            padding: const EdgeInsets.all(0.0)
                        ),
                        onPressed: () {
                          Navigator.of(context).pop(true);
                        },
                        child: Container(
                          height: 60,
                          width: 120,
                          alignment: Alignment.center,
                          decoration: Style.imageButtonBG,
                          child: const FittedBox(
                            fit: BoxFit.scaleDown,
                            child: Text(
                              '是',
                              textScaleFactor: 1,
                              style: TextStyle(color: Colors.white, fontSize: 20),
                            ),
                          ),
                        ),
                      ),
                    ),
                    //否
                    ButtonTheme(
                      height: 80,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            foregroundColor: Colors.white,
                            backgroundColor: Colors.transparent,
                            shape: const RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(Radius.circular(10.0))),
                            padding: const EdgeInsets.all(0.0)
                        ),
                        onPressed: () {
                          delegate.popRoute();
                        },
                        child: Container(
                          height: 60,
                          width: 120,
                          alignment: Alignment.center,
                          decoration: Style.spsNextBG,
                          child: const FittedBox(
                            fit: BoxFit.scaleDown,
                            child: Text(
                              '否',
                              textScaleFactor: 1,
                              style: TextStyle(color: Colors.white, fontSize: 20),
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                )
              ],
            ),
          );
        }
    );
    if(isSelect){
      if(isBackHome){
        delegate.popUtil();
      }else{
        delegate.popRoute();
      }
    }
  }

  //主體
  Widget _bodyWidget() {
    return Stack(
      children: [
        Container(
          padding: const EdgeInsets.only(top: 10, bottom: 10),
          decoration: Style.homeBg,
          alignment: Alignment.center,
          child: Container(
            width: MediaQuery.of(context).size.width / 1.1,
            decoration: Style.centerBG,
            child: ScrollConfiguration(
              behavior: NoShadowScrollBehavior(),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    //主體內部
                    _bodyTopWidget(),
                    //題目
                    _topicWidget(),
                    //問題
                    _problemWidget(),
                    //圖片
                    _imageWidget(),
                    const SizedBox(height: 10,),
                    //影片與圖片
                    _videoWidget(),
                    //按鈕
                    _bottomButtonWidget(),
                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  //主體內部
  Widget _bodyTopWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 10, left: 50, right: 50),
      child: Column(
        children: [
          FittedBox(
            fit: BoxFit.scaleDown,
            child: Text(
              getTranslated(
                  context, 'Evaluation_and_adjustment_of_sacral_problems'),
              textScaleFactor: 1,
              style: const TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Container(
            height: 3,
            color: HexColor.fromHex('#4A991E'),
          )
        ],
      ),
    );
  }

  //題目
  Widget _topicWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 20, left: 25),
      child: Row(
        children: [
          Container(
            width: 40,
            height: 40,
            alignment: Alignment.center,
            decoration: BoxDecoration(
                color: HexColor.fromHex('#1E2C39'),
                borderRadius: const BorderRadius.all(Radius.circular(25))),
            child: const FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                '5',
                textScaleFactor: 1,
                style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    color: Colors.white),
              ),
            ),
          ),
          const SizedBox(
            width: 20,
          ),
          FittedBox(
            fit: BoxFit.scaleDown,
            child: Text(
              getTranslated(context, 'Sacral_problems_handling_skills'),
              textScaleFactor: 1,
              style: const TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            ),
          ),
        ],
      ),
    );
  }

  //問題
  Widget _problemWidget(){
    return Container(
      alignment: Alignment.centerLeft,
      margin: const EdgeInsets.only(top: 5,left: 20),
      child: Container(
        margin: const EdgeInsets.all(8),
        width: 300,
        child: TextField(
          readOnly: true,
          textAlign: TextAlign.center,
          style: TextStyle(color: HexColor.fromHex('#096BC7')),
          decoration: const InputDecoration(
            contentPadding: EdgeInsets.all(5),
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.black)
            ),
            filled: true,
            fillColor: Colors.white,
          ),
          keyboardType: TextInputType.text,
          textInputAction: TextInputAction.done,
          controller: _sacrumProblem,
        ),
      ),
    );
  }

  //圖片
  Widget _imageWidget(){
    debugPrint('圖片網址: $imageUrlOffline$imageUrlPublic$netUpImageUrl');
    return Container(
      width: 300,
      height: 200,
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.all(Radius.circular(15)),
        border: Border.all(color: Colors.black, width: 1)),
      child: Stack(
        alignment: Alignment.center,
        children: [
          Image(
            image: NetworkImage('$imageUrlOffline$imageUrlPublic$netUpImageUrl'),
            fit: BoxFit.fill,
          ),
          Positioned(bottom: 10, right: 10, child: InkWell(
              onTap: _upImageOnTap,
              child: const Icon(Icons.zoom_in, size: 45,)
          ),)
        ],
      ),
    );
  }
  //影片與圖片
  Widget _videoWidget(){
    return Container(
      width: 300,
      height: 180,
      decoration: BoxDecoration(
          image: DecorationImage(
            image:
            NetworkImage('$imageUrlOffline$imageUrlPublic$netDownImageUrl'),
            fit: BoxFit.cover,
          ),
        borderRadius: const BorderRadius.all(Radius.circular(15)),
        border: Border.all(color: Colors.black, width: 1)),
      child: Stack(
        alignment: Alignment.center,
        children: [
          Positioned(bottom: 0, right: 0, child: Row(
            children: [
              InkWell(
                onTap: _videoOnTap,
                child: Container(
                  decoration: const BoxDecoration(
                    color: Colors.black,
                    borderRadius: BorderRadius.all(Radius.circular(25))
                  ),
                  child: const Icon(Icons.arrow_right, size: 30, color: Colors.white,),
                ),
              ),
              const SizedBox(width: 5,),
              InkWell(
                onTap: _downImageOnTap,
                child: const Icon(Icons.zoom_in, size: 45,),
              )
            ],
          ))
        ],
      ),
    );
  }
  //下面影片
  void _videoOnTap(){
    Navigator.of(context).push(MaterialPageRoute(builder: (context){
      return VideoPlayerScreen(videoUrl: '$imageUrlOffline$imageUrlPublic$netVideoUrl',);
    }));
  }
  //下面圖片放大
  void _downImageOnTap(){
    Navigator.of(context).push(MaterialPageRoute(builder:(context){
      return NetWorkImagePage(imageUrl: '$imageUrlOffline$imageUrlPublic$netDownImageUrl');
    }));
  }
  //上面圖片放大
  void _upImageOnTap(){
    Navigator.of(context).push(MaterialPageRoute(builder:(context){
      return NetWorkImagePage(imageUrl: '$imageUrlOffline$imageUrlPublic$netUpImageUrl');
    }));
  }

  //按鈕
  Widget _bottomButtonWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 30, right: 25, bottom: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          //下一步
          NextButton(
              click: _nextOnTap,
              buttonText: getTranslated(context, 'Next_step'),
              decoration: Style.spsNextBG)
        ],
      ),
    );
  }

  void _nextOnTap(){
      delegate.push(name: RouteName.sps06);
  }

}