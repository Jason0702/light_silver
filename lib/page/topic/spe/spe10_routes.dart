import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:light_silver/language/language_constants.dart';
import 'package:light_silver/models/patient.dart';
import 'package:light_silver/utils/utils.dart';
import 'package:light_silver/widgets/button.dart';
import 'package:light_silver/widgets/noShadowScrollbehavior.dart';
import 'package:provider/provider.dart';

import '../../../app.dart';
import '../../../routes/route_name.dart';
import '../../../view_model/patient_vm.dart';
import '../../../widgets/title_widget.dart';

class SPe10Routes extends StatefulWidget {
  @override
  State createState() => _SPe10RoutesState();
}

class _SPe10RoutesState extends State<SPe10Routes>
    with WidgetsBindingObserver {
  PatientVm? _patientVm;

  final GlobalKey<_GTLeftResultTextWidgetState> _gTLeftResultKey = GlobalKey();
  final GlobalKey<_GTRightResultTextWidgetState> _gTRightResultKey =
      GlobalKey();
  final GlobalKey<_ICLeftResultTextWidgetState> _iCLeftResultKey = GlobalKey();
  final GlobalKey<_ICRightResultTextWidgetState> _iCRightResultKey =
      GlobalKey();
  final GlobalKey<_ASISLeftResultTextWidgetState> _aSISLeftResultKey =
      GlobalKey();
  final GlobalKey<_ASISRightResultTextWidgetState> _aSISRightResultKey =
      GlobalKey();
  final GlobalKey<_PSISLeftResultTextWidgetState> _pSISLeftResultKey =
      GlobalKey();
  final GlobalKey<_PSISRightResultTextWidgetState> _pSISRightResultKey =
      GlobalKey();
  final GlobalKey<_FlexionLeftResultTextWidgetState> _flexionLeftResultKey =
      GlobalKey();
  final GlobalKey<_FlexionRightResultTextWidgetState> _flexionRightResultKey =
      GlobalKey();
  final GlobalKey<_GilletLeftResultTextWidgetState> _gilletLeftResultKey =
      GlobalKey();
  final GlobalKey<_GilletRightResultTextWidgetState> _gilletRightResultKey =
      GlobalKey();
  final GlobalKey<_IliumSideResultTextWidgetState> _iliumSideResultKey =
      GlobalKey();

  _onLayoutDone(_) {
    _dataInit(_patientVm!.patient);
  }

  void _dataInit(Patient? patient) {
    if (patient == null) {
      return;
    }
    log('取得 站姿結論(1): $patient');
    _gTLeftResultKey.currentState?.onRefresh(patient.GT);
    _gTRightResultKey.currentState?.onRefresh(patient.GT);
    _iCLeftResultKey.currentState?.onRefresh(patient.IC);
    _iCRightResultKey.currentState?.onRefresh(patient.IC);
    _aSISLeftResultKey.currentState?.onRefresh(patient.ASIS);
    _aSISRightResultKey.currentState?.onRefresh(patient.ASIS);
    _pSISLeftResultKey.currentState?.onRefresh(patient.PSIS);
    _pSISRightResultKey.currentState?.onRefresh(patient.PSIS);
    _flexionLeftResultKey.currentState?.onRefresh(patient.flexion);
    _flexionRightResultKey.currentState?.onRefresh(patient.flexion);
    _gilletLeftResultKey.currentState?.onRefresh(patient.gillet);
    _gilletRightResultKey.currentState?.onRefresh(patient.gillet);
    _iliumSideResultKey.currentState?.onRefresh(patient.gillet);
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  Widget build(BuildContext context) {
    _patientVm = Provider.of<PatientVm>(context);
    return WillPopScope(
        child: Scaffold(
          backgroundColor: Colors.white,
          resizeToAvoidBottomInset: false,
          body: SafeArea(
            child: Column(
              children: [
                Expanded(flex: 1, child: _titleBar()),
                Expanded(flex: 10, child: _bodyWidget())
              ],
            ),
          ),
        ),
        onWillPop: () async {
          showBackDialog(false);
          return false;
        });
  }

  //最上層標題與按鈕
  Widget _titleBar() {
    return SpeTitleWidget(
      titleText: getTranslated(context, 'Standing_posture'),
      leftButton: _leftTopButton(),
      rightButton: _rightTopButton(),
    );
  }

  //左側按鈕
  Widget _leftTopButton() {
    return InkWell(
      onTap: () {
        showBackDialog(false);
      },
      child: const Icon(
        Icons.arrow_back_rounded,
        color: Colors.white,
        size: 45,
      ),
    );
  }

  //右側按鈕
  Widget _rightTopButton() {
    return InkWell(
      onTap: () {
        showBackDialog(true);
      },
      child: const Icon(
        Icons.home,
        color: Colors.white,
        size: 45,
      ),
    );
  }

  void showBackDialog(bool isBackHome) async {
    bool isSelect = await showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                const FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    '可能會變更之前的評估結果，是否確定\n更改先前評估記錄?',
                    textAlign: TextAlign.left,
                    textScaleFactor: 1,
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 24,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    //是
                    ButtonTheme(
                      height: 80,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            foregroundColor: Colors.white,
                            backgroundColor: Colors.transparent,
                            shape: const RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0))),
                            padding: const EdgeInsets.all(0.0)),
                        onPressed: () {
                          Navigator.of(context).pop(true);
                        },
                        child: Container(
                          height: 60,
                          width: 120,
                          alignment: Alignment.center,
                          decoration: Style.imageButtonBG,
                          child: const FittedBox(
                            fit: BoxFit.scaleDown,
                            child: Text(
                              '是',
                              textScaleFactor: 1,
                              style:
                                  TextStyle(color: Colors.white, fontSize: 20),
                            ),
                          ),
                        ),
                      ),
                    ),
                    //否
                    ButtonTheme(
                      height: 80,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            foregroundColor: Colors.white,
                            backgroundColor: Colors.transparent,
                            shape: const RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0))),
                            padding: const EdgeInsets.all(0.0)),
                        onPressed: () => delegate.popRoute(),
                        child: Container(
                          height: 60,
                          width: 120,
                          alignment: Alignment.center,
                          decoration: Style.speNextBG,
                          child: const FittedBox(
                            fit: BoxFit.scaleDown,
                            child: Text(
                              '否',
                              textScaleFactor: 1,
                              style:
                                  TextStyle(color: Colors.white, fontSize: 20),
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                )
              ],
            ),
          );
        });
    if (!isSelect) {
      return;
    }
    if (isBackHome) {
      delegate.popUtil();
    } else {
      delegate.popRoute();
    }
  }

  //主體
  Widget _bodyWidget() {
    return Stack(
      children: [
        Container(
          padding: const EdgeInsets.symmetric(vertical: 10),
          decoration: Style.homeBg,
          alignment: Alignment.center,
          child: Container(
            width: MediaQuery.of(context).size.width / 1.1,
            decoration: Style.centerBG,
            child: ScrollConfiguration(
              behavior: NoShadowScrollBehavior(),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    //主體內部
                    _bodyTopWidget(),
                    //題目
                    _topicWidget(),
                    //表格
                    _tableWidget(),
                    //按鈕
                    _bottomButtonWidget(),
                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  //主體內部
  Widget _bodyTopWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 20, left: 50, right: 50),
      child: Column(
        children: [
          FittedBox(
            fit: BoxFit.scaleDown,
            child: Text(
              getTranslated(
                  context, 'Evaluation_of_iliac_and_lumbar_spine_problems'),
              textScaleFactor: 1,
              style: const TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Container(
            height: 3,
            color: HexColor.fromHex('#4A991E'),
          )
        ],
      ),
    );
  }

  //題目
  Widget _topicWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 10, left: 25),
      child: Row(
        children: [
          FittedBox(
            fit: BoxFit.scaleDown,
            child: Text(
              '${getTranslated(context, 'Summary_of_the_problems_in_the_standing_posture')}(1)',
              textScaleFactor: 1,
              style: const TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            ),
          ),
        ],
      ),
    );
  }

  static const double tableHeight = 50;
  //表格
  Widget _tableWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 20, left: 20, right: 20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          //標題
          Row(
            children: [
              Expanded(
                  flex: 1,
                  child: Container(
                    height: tableHeight,
                    alignment: Alignment.center,
                    decoration: Style.speTitleBarBG,
                    child: Container(
                      margin: const EdgeInsets.only(
                          top: 5, bottom: 5, left: 15, right: 15),
                      child: FittedBox(
                        fit: BoxFit.scaleDown,
                        child: Text(
                          getTranslated(context, 'Item'),
                          textScaleFactor: 1,
                          style: const TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 20),
                        ),
                      ),
                    ),
                  )),
              const SizedBox(
                width: 3,
              ),
              Expanded(
                  flex: 1,
                  child: Container(
                    height: tableHeight,
                    alignment: Alignment.center,
                    decoration: Style.speTitleBarBG,
                    child: Container(
                      margin: const EdgeInsets.only(
                          top: 5, bottom: 5, left: 15, right: 15),
                      child: FittedBox(
                        fit: BoxFit.scaleDown,
                        child: Text(
                          getTranslated(context, 'Left_side'),
                          textScaleFactor: 1,
                          style: const TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 20),
                        ),
                      ),
                    ),
                  )),
              const SizedBox(
                width: 3,
              ),
              Expanded(
                  flex: 1,
                  child: Container(
                    height: tableHeight,
                    alignment: Alignment.center,
                    decoration: Style.speTitleBarBG,
                    child: Container(
                      margin: const EdgeInsets.only(
                          top: 5, bottom: 5, left: 15, right: 15),
                      child: FittedBox(
                        fit: BoxFit.scaleDown,
                        child: Text(
                          getTranslated(context, 'Right_side'),
                          textScaleFactor: 1,
                          style: const TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 20),
                        ),
                      ),
                    ),
                  ))
            ],
          ),
          //股骨大轉子GT
          Row(
            children: [
              Expanded(
                  flex: 1,
                  child: Container(
                    height: tableHeight,
                    decoration: BoxDecoration(
                        border: Border.all(color: HexColor.fromHex('#4A991E'))),
                    child: Container(
                      alignment: Alignment.center,
                      child: FittedBox(
                        fit: BoxFit.scaleDown,
                        child: Text(
                          getTranslated(context, 'Greater_trochanter_GT'),
                          textScaleFactor: 1,
                          style: const TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color: Colors.black),
                        ),
                      ),
                    ),
                  )),
              Expanded(
                  flex: 1,
                  child: Container(
                    height: tableHeight,
                    decoration: BoxDecoration(
                        border: Border.all(color: HexColor.fromHex('#4A991E'))),
                    child: Container(
                      alignment: Alignment.center,
                      child: GTLeftResultTextWidget(key: _gTLeftResultKey),
                    ),
                  )),
              Expanded(
                  flex: 1,
                  child: Container(
                    height: tableHeight,
                    decoration: BoxDecoration(
                        border: Border.all(color: HexColor.fromHex('#4A991E'))),
                    child: Container(
                      alignment: Alignment.center,
                      child: GTRightResultTextWidget(key: _gTRightResultKey),
                    ),
                  ))
            ],
          ),
          //髂嵴
          Row(
            children: [
              Expanded(
                  flex: 1,
                  child: Container(
                    height: tableHeight,
                    decoration: BoxDecoration(
                        border: Border.all(color: HexColor.fromHex('#4A991E'))),
                    child: Container(
                      alignment: Alignment.center,
                      child: FittedBox(
                        fit: BoxFit.scaleDown,
                        child: Text(
                          getTranslated(context, 'Iliac_crest'),
                          textScaleFactor: 1,
                          style: const TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color: Colors.black),
                        ),
                      ),
                    ),
                  )),
              Expanded(
                  flex: 1,
                  child: Container(
                    height: tableHeight,
                    decoration: BoxDecoration(
                        border: Border.all(color: HexColor.fromHex('#4A991E'))),
                    child: Container(
                      alignment: Alignment.center,
                      child: ICLeftResultTextWidget(key: _iCLeftResultKey),
                    ),
                  )),
              Expanded(
                  flex: 1,
                  child: Container(
                    height: tableHeight,
                    decoration: BoxDecoration(
                        border: Border.all(color: HexColor.fromHex('#4A991E'))),
                    child: Container(
                      alignment: Alignment.center,
                      child: ICRightResultTextWidget(key: _iCRightResultKey),
                    ),
                  ))
            ],
          ),
          //ASIS
          Row(
            children: [
              Expanded(
                  flex: 1,
                  child: Container(
                    height: tableHeight,
                    decoration: BoxDecoration(
                        border: Border.all(color: HexColor.fromHex('#4A991E'))),
                    child: Container(
                      alignment: Alignment.center,
                      child: FittedBox(
                        fit: BoxFit.scaleDown,
                        child: Text(
                          getTranslated(context, 'ASIS'),
                          textScaleFactor: 1,
                          style: const TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color: Colors.black),
                        ),
                      ),
                    ),
                  )),
              Expanded(
                  flex: 1,
                  child: Container(
                    height: tableHeight,
                    decoration: BoxDecoration(
                        border: Border.all(color: HexColor.fromHex('#4A991E'))),
                    child: Container(
                      alignment: Alignment.center,
                      child: ASISLeftResultTextWidget(key: _aSISLeftResultKey),
                    ),
                  )),
              Expanded(
                  flex: 1,
                  child: Container(
                    height: tableHeight,
                    decoration: BoxDecoration(
                        border: Border.all(color: HexColor.fromHex('#4A991E'))),
                    child: Container(
                      alignment: Alignment.center,
                      child:
                          ASISRightResultTextWidget(key: _aSISRightResultKey),
                    ),
                  ))
            ],
          ),
          //PSIS
          Row(
            children: [
              Expanded(
                  flex: 1,
                  child: Container(
                    height: tableHeight,
                    decoration: BoxDecoration(
                        border: Border.all(color: HexColor.fromHex('#4A991E'))),
                    child: Container(
                      alignment: Alignment.center,
                      child: FittedBox(
                        fit: BoxFit.scaleDown,
                        child: Text(
                          getTranslated(context, 'PSIS'),
                          textScaleFactor: 1,
                          style: const TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color: Colors.black),
                        ),
                      ),
                    ),
                  )),
              Expanded(
                  flex: 1,
                  child: Container(
                    height: tableHeight,
                    decoration: BoxDecoration(
                        border: Border.all(color: HexColor.fromHex('#4A991E'))),
                    child: Container(
                      alignment: Alignment.center,
                      child: PSISLeftResultTextWidget(key: _pSISLeftResultKey),
                    ),
                  )),
              Expanded(
                  flex: 1,
                  child: Container(
                    height: tableHeight,
                    decoration: BoxDecoration(
                        border: Border.all(color: HexColor.fromHex('#4A991E'))),
                    child: Container(
                      alignment: Alignment.center,
                      child:
                          PSISRightResultTextWidget(key: _pSISRightResultKey),
                    ),
                  ))
            ],
          ),
          //站立前彎測試
          Row(
            children: [
              Expanded(
                  flex: 1,
                  child: Container(
                    height: tableHeight,
                    decoration: BoxDecoration(
                        border: Border.all(color: HexColor.fromHex('#4A991E'))),
                    child: Container(
                      alignment: Alignment.center,
                      child: FittedBox(
                        fit: BoxFit.scaleDown,
                        child: Text(
                          getTranslated(context, 'Standing_forward_bend_test'),
                          textScaleFactor: 1,
                          style: const TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color: Colors.black),
                        ),
                      ),
                    ),
                  )),
              Expanded(
                  flex: 1,
                  child: Container(
                    height: tableHeight,
                    decoration: BoxDecoration(
                        border: Border.all(color: HexColor.fromHex('#4A991E'))),
                    child: Container(
                      alignment: Alignment.center,
                      child: FlexionLeftResultTextWidget(
                          key: _flexionLeftResultKey),
                    ),
                  )),
              Expanded(
                  flex: 1,
                  child: Container(
                    height: tableHeight,
                    decoration: BoxDecoration(
                        border: Border.all(color: HexColor.fromHex('#4A991E'))),
                    child: Container(
                      alignment: Alignment.center,
                      child: FlexionRightResultTextWidget(
                          key: _flexionRightResultKey),
                    ),
                  ))
            ],
          ),
          //吉利測試
          Row(
            children: [
              Expanded(
                  flex: 1,
                  child: Container(
                    height: tableHeight,
                    decoration: BoxDecoration(
                        border: Border.all(color: HexColor.fromHex('#4A991E'))),
                    child: Container(
                      alignment: Alignment.center,
                      child: FittedBox(
                        fit: BoxFit.scaleDown,
                        child: Text(
                          getTranslated(context, 'Geely_test'),
                          textScaleFactor: 1,
                          style: const TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color: Colors.black),
                        ),
                      ),
                    ),
                  )),
              Expanded(
                  flex: 1,
                  child: Container(
                    height: tableHeight,
                    decoration: BoxDecoration(
                        border: Border.all(color: HexColor.fromHex('#4A991E'))),
                    child: Container(
                      alignment: Alignment.center,
                      child:
                          GilletLeftResultTextWidget(key: _gilletLeftResultKey),
                    ),
                  )),
              Expanded(
                  flex: 1,
                  child: Container(
                    height: tableHeight,
                    decoration: BoxDecoration(
                        border: Border.all(color: HexColor.fromHex('#4A991E'))),
                    child: Container(
                      alignment: Alignment.center,
                      child: GilletRightResultTextWidget(
                          key: _gilletRightResultKey),
                    ),
                  ))
            ],
          ),
          //髂骨問題位置
          Row(
            children: [
              Expanded(
                  flex: 1,
                  child: Container(
                    height: tableHeight,
                    decoration: BoxDecoration(
                        border: Border.all(color: HexColor.fromHex('#4A991E'))),
                    child: Container(
                      alignment: Alignment.center,
                      child: FittedBox(
                        fit: BoxFit.scaleDown,
                        child: Text(
                          getTranslated(context, 'Ilium_problem_location'),
                          textScaleFactor: 1,
                          style: const TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color: Colors.black),
                        ),
                      ),
                    ),
                  )),
              Expanded(
                  flex: 2,
                  child: Container(
                    height: tableHeight,
                    decoration: BoxDecoration(
                        border: Border.all(color: HexColor.fromHex('#4A991E'))),
                    child: Container(
                      alignment: Alignment.center,
                      child:
                          IliumSideResultTextWidget(key: _iliumSideResultKey),
                    ),
                  ))
            ],
          ),
        ],
      ),
    );
  }

  //按鈕
  Widget _bottomButtonWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 10, right: 25, bottom: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          //下一步
          NextButton(
              click: _nextOnTap,
              buttonText: getTranslated(context, 'Next_step'),
              decoration: Style.speNextBG)
        ],
      ),
    );
  }

  //下一步
  void _nextOnTap() {
    delegate.push(name: RouteName.spe11);
  }
}

//GTLeftResult
class GTLeftResultTextWidget extends StatefulWidget {
  const GTLeftResultTextWidget({Key? key}) : super(key: key);

  @override
  State createState() => _GTLeftResultTextWidgetState();
}

class _GTLeftResultTextWidgetState extends State<GTLeftResultTextWidget> {
  String text = ' ';

  void onRefresh(int value) {
    switch (value) {
      case 0:
        text = getTranslated(context, 'default_value');
        break;
      case 1:
        text = getTranslated(context, 'low');
        break;
      case 2:
        text = getTranslated(context, 'contour');
        break;
      case 3:
        text = getTranslated(context, 'high');
        break;
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return FittedBox(
      fit: BoxFit.scaleDown,
      child: Text(
        text,
        textScaleFactor: 1,
        style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
            color: HexColor.fromHex('#096BC7')),
      ),
    );
  }
}

//GTRightResult
class GTRightResultTextWidget extends StatefulWidget {
  const GTRightResultTextWidget({Key? key}) : super(key: key);

  @override
  State createState() => _GTRightResultTextWidgetState();
}

class _GTRightResultTextWidgetState extends State<GTRightResultTextWidget> {
  String text = ' ';

  void onRefresh(int value) {
    switch (value) {
      case 0:
        text = getTranslated(context, 'default_value');
        break;
      case 1:
        text = getTranslated(context, 'high');
        break;
      case 2:
        text = getTranslated(context, 'contour');
        break;
      case 3:
        text = getTranslated(context, 'low');
        break;
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return FittedBox(
      fit: BoxFit.scaleDown,
      child: Text(
        text,
        textScaleFactor: 1,
        style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
            color: HexColor.fromHex('#096BC7')),
      ),
    );
  }
}

//ICLeftResult
class ICLeftResultTextWidget extends StatefulWidget {
  const ICLeftResultTextWidget({Key? key}) : super(key: key);

  @override
  State createState() => _ICLeftResultTextWidgetState();
}

class _ICLeftResultTextWidgetState extends State<ICLeftResultTextWidget> {
  String text = ' ';

  void onRefresh(int value) {
    switch (value) {
      case 0:
        text = getTranslated(context, 'default_value');
        break;
      case 1:
        text = getTranslated(context, 'low');
        break;
      case 2:
        text = getTranslated(context, 'contour');
        break;
      case 3:
        text = getTranslated(context, 'high');
        break;
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return FittedBox(
      fit: BoxFit.scaleDown,
      child: Text(
        text,
        textScaleFactor: 1,
        style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
            color: HexColor.fromHex('#096BC7')),
      ),
    );
  }
}

//ICRightResult
class ICRightResultTextWidget extends StatefulWidget {
  const ICRightResultTextWidget({Key? key}) : super(key: key);

  @override
  State createState() => _ICRightResultTextWidgetState();
}

class _ICRightResultTextWidgetState extends State<ICRightResultTextWidget> {
  String text = ' ';

  void onRefresh(int value) {
    switch (value) {
      case 0:
        text = getTranslated(context, 'default_value');
        break;
      case 1:
        text = getTranslated(context, 'high');
        break;
      case 2:
        text = getTranslated(context, 'contour');
        break;
      case 3:
        text = getTranslated(context, 'low');
        break;
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return FittedBox(
      fit: BoxFit.scaleDown,
      child: Text(
        text,
        textScaleFactor: 1,
        style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
            color: HexColor.fromHex('#096BC7')),
      ),
    );
  }
}

//ASISLeftResult
class ASISLeftResultTextWidget extends StatefulWidget {
  const ASISLeftResultTextWidget({Key? key}) : super(key: key);

  @override
  State createState() => _ASISLeftResultTextWidgetState();
}

class _ASISLeftResultTextWidgetState extends State<ASISLeftResultTextWidget> {
  String text = ' ';

  void onRefresh(int value) {
    switch (value) {
      case 0:
        text = getTranslated(context, 'default_value');
        break;
      case 1:
        text = getTranslated(context, 'low');
        break;
      case 2:
        text = getTranslated(context, 'contour');
        break;
      case 3:
        text = getTranslated(context, 'high');
        break;
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return FittedBox(
      fit: BoxFit.scaleDown,
      child: Text(
        text,
        textScaleFactor: 1,
        style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
            color: HexColor.fromHex('#096BC7')),
      ),
    );
  }
}

//ASISRightResult
class ASISRightResultTextWidget extends StatefulWidget {
  const ASISRightResultTextWidget({Key? key}) : super(key: key);

  @override
  State createState() => _ASISRightResultTextWidgetState();
}

class _ASISRightResultTextWidgetState extends State<ASISRightResultTextWidget> {
  String text = ' ';

  void onRefresh(int value) {
    switch (value) {
      case 0:
        text = getTranslated(context, 'default_value');
        break;
      case 1:
        text = getTranslated(context, 'high');
        break;
      case 2:
        text = getTranslated(context, 'contour');
        break;
      case 3:
        text = getTranslated(context, 'low');
        break;
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return FittedBox(
      fit: BoxFit.scaleDown,
      child: Text(
        text,
        textScaleFactor: 1,
        style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
            color: HexColor.fromHex('#096BC7')),
      ),
    );
  }
}

//PSISLeftResult
class PSISLeftResultTextWidget extends StatefulWidget {
  const PSISLeftResultTextWidget({Key? key}) : super(key: key);

  @override
  State createState() => _PSISLeftResultTextWidgetState();
}

class _PSISLeftResultTextWidgetState extends State<PSISLeftResultTextWidget> {
  String text = ' ';

  void onRefresh(int value) {
    switch (value) {
      case 0:
        text = getTranslated(context, 'default_value');
        break;
      case 1:
        text = getTranslated(context, 'low');
        break;
      case 2:
        text = getTranslated(context, 'contour');
        break;
      case 3:
        text = getTranslated(context, 'high');
        break;
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return FittedBox(
      fit: BoxFit.scaleDown,
      child: Text(
        text,
        textScaleFactor: 1,
        style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
            color: HexColor.fromHex('#096BC7')),
      ),
    );
  }
}

//PSISRightResult
class PSISRightResultTextWidget extends StatefulWidget {
  const PSISRightResultTextWidget({Key? key}) : super(key: key);

  @override
  State createState() => _PSISRightResultTextWidgetState();
}

class _PSISRightResultTextWidgetState extends State<PSISRightResultTextWidget> {
  String text = ' ';

  void onRefresh(int value) {
    switch (value) {
      case 0:
        text = getTranslated(context, 'default_value');
        break;
      case 1:
        text = getTranslated(context, 'high');
        break;
      case 2:
        text = getTranslated(context, 'contour');
        break;
      case 3:
        text = getTranslated(context, 'low');
        break;
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return FittedBox(
      fit: BoxFit.scaleDown,
      child: Text(
        text,
        textScaleFactor: 1,
        style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
            color: HexColor.fromHex('#096BC7')),
      ),
    );
  }
}

//FlexionLeftResult
class FlexionLeftResultTextWidget extends StatefulWidget {
  const FlexionLeftResultTextWidget({Key? key}) : super(key: key);

  @override
  State createState() => _FlexionLeftResultTextWidgetState();
}

class _FlexionLeftResultTextWidgetState
    extends State<FlexionLeftResultTextWidget> {
  String text = ' ';

  void onRefresh(int value) {
    switch (value) {
      case 1:
        text = getTranslated(context, 'abnormal');
        break;
      case 2:
        text = getTranslated(context, 'normal');
        break;
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return FittedBox(
      fit: BoxFit.scaleDown,
      child: Text(
        text,
        textScaleFactor: 1,
        style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
            color: HexColor.fromHex('#096BC7')),
      ),
    );
  }
}

//FlexionRightResult
class FlexionRightResultTextWidget extends StatefulWidget {
  const FlexionRightResultTextWidget({Key? key}) : super(key: key);

  @override
  State createState() => _FlexionRightResultTextWidgetState();
}

class _FlexionRightResultTextWidgetState
    extends State<FlexionRightResultTextWidget> {
  String text = ' ';

  void onRefresh(int value) {
    switch (value) {
      case 1:
        text = getTranslated(context, 'normal');
        break;
      case 2:
        text = getTranslated(context, 'abnormal');
        break;
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return FittedBox(
      fit: BoxFit.scaleDown,
      child: Text(
        text,
        textScaleFactor: 1,
        style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
            color: HexColor.fromHex('#096BC7')),
      ),
    );
  }
}

//GilletLeftResult
class GilletLeftResultTextWidget extends StatefulWidget {
  const GilletLeftResultTextWidget({Key? key}) : super(key: key);

  @override
  State createState() => _GilletLeftResultTextWidgetState();
}

class _GilletLeftResultTextWidgetState
    extends State<GilletLeftResultTextWidget> {
  String text = ' ';

  void onRefresh(int value) {
    switch (value) {
      case 1:
        text = getTranslated(context, 'abnormal');
        break;
      case 2:
        text = getTranslated(context, 'normal');
        break;
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return FittedBox(
      fit: BoxFit.scaleDown,
      child: Text(
        text,
        textScaleFactor: 1,
        style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
            color: HexColor.fromHex('#096BC7')),
      ),
    );
  }
}

//GilletRightResult
class GilletRightResultTextWidget extends StatefulWidget {
  const GilletRightResultTextWidget({Key? key}) : super(key: key);

  @override
  State createState() => _GilletRightResultTextWidgetState();
}

class _GilletRightResultTextWidgetState
    extends State<GilletRightResultTextWidget> {
  String text = ' ';

  void onRefresh(int value) {
    switch (value) {
      case 1:
        text = getTranslated(context, 'normal');
        break;
      case 2:
        text = getTranslated(context, 'abnormal');
        break;
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return FittedBox(
      fit: BoxFit.scaleDown,
      child: Text(
        text,
        textScaleFactor: 1,
        style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
            color: HexColor.fromHex('#096BC7')),
      ),
    );
  }
}

//IliumSideResult
class IliumSideResultTextWidget extends StatefulWidget {
  const IliumSideResultTextWidget({Key? key}) : super(key: key);

  @override
  State createState() => _IliumSideResultTextWidgetState();
}

class _IliumSideResultTextWidgetState extends State<IliumSideResultTextWidget> {
  String text = ' ';

  void onRefresh(int value) {
    switch (value) {
      case 1:
        text = getTranslated(context, 'The_problem_side_is_the_left_side');
        break;
      case 2:
        text = getTranslated(context, 'The_problem_side_is_the_right_side');
        break;
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return FittedBox(
      fit: BoxFit.scaleDown,
      child: Text(
        text,
        textScaleFactor: 1,
        style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
            color: HexColor.fromHex('#096BC7')),
      ),
    );
  }
}
