import 'dart:developer';

import 'package:dotted_line/dotted_line.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:light_silver/language/language_constants.dart';
import 'package:light_silver/models/operation_item_model.dart';
import 'package:light_silver/models/patient.dart';
import 'package:light_silver/routes/route_name.dart';
import 'package:light_silver/utils/api.dart';
import 'package:light_silver/utils/utils.dart';
import 'package:light_silver/widgets/left_button.dart';
import 'package:provider/provider.dart';
import 'dart:math' as math;

import '../../../app.dart';
import '../../../view_model/operation_item_vm.dart';
import '../../../view_model/patient_vm.dart';
import '../../../widgets/button.dart';
import '../../../widgets/hero_photo_view_wrapper.dart';
import '../../../widgets/title_widget.dart';

class SPe08Routes extends StatefulWidget {
  @override
  State createState() => _SPe08RoutesState();
}

class _SPe08RoutesState extends State<SPe08Routes>
    with WidgetsBindingObserver {
  PatientVm? _patientVm;
  OperationItemVm? _operationItemVm;

  int _rotationRight = 3;
  int _rotationLeft = 3;
  OperationItemsModel? imageList;

  _onLayoutDone(_) {
    List list = _operationItemVm!.operationItemsModelList!.where((element) => element.id == 2).toList();
    for (var element in list) {
      imageList = element;
      setState(() {});
    }
    _dataInit(_patientVm!.patient);
  }

  void _dataInit(Patient? patient) {
    if (patient == null) {
      return;
    }
    log('取得 站姿(8): $patient');
    _rotationRight = _patientVm!.patient!.rotation_right;
    _rotationLeft = _patientVm!.patient!.rotation_left;
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  Widget build(BuildContext context) {
    _patientVm = Provider.of<PatientVm>(context);
    _operationItemVm = Provider.of<OperationItemVm>(context);
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Column(
          children: [
            Expanded(flex: 1, child: _titleBar()),
            Expanded(flex: 10, child: _bodyWidget())
          ],
        ),
      ),
    );
  }

  //最上層標題與按鈕
  Widget _titleBar() {
    return SpeTitleWidget(
      titleText: '${getTranslated(context, 'Standing_posture')}(8)',
      leftButton: LeftSaveButton(func: editPatient),
      rightButton: _rightTopButton(),
    );
  }

  //右側按鈕
  Widget _rightTopButton() {
    return InkWell(
      onTap: () {
        delAllPatient();
        delegate.popUtil();
      },
      child: const Icon(
        Icons.home,
        color: Colors.white,
        size: 45,
      ),
    );
  }

  //主體
  Widget _bodyWidget() {
    return Stack(
      children: [
        Container(
          padding: const EdgeInsets.only(top: 10, bottom: 10),
          decoration: Style.homeBg,
          alignment: Alignment.center,
          child: Container(
            width: MediaQuery.of(context).size.width / 1.1,
            decoration: Style.centerBG,
            child: Column(
              children: [
                //主體內部
                _bodyTopWidget(),
                //題目
                _topicWidget(),
                //左或右
                _leftOrRightWidget(),
                //提示
                _promptWidget(),
                const Spacer(),
                //選項
                _optionsWidget(),
                const Spacer(),
                //按鈕
                _bottomButtonWidget(),
              ],
            ),
          ),
        ),
        //身體圖片按鈕
        imageList != null && imageList!.pics.isNotEmpty && imageList!.pics[7].isNotEmpty ? InkWell(
          onTap: _bodyImageOnTap,
          child: Container(
            margin: const EdgeInsets.only(top: 15),
            decoration: Style.imageButtonBG,
            child: Container(
              margin: const EdgeInsets.all(10),
              child: Image.asset(
                'assets/images/people.png',
                height: 50,
              ),
            ),
          ),
        ) : Container(),
      ],
    );
  }

  //主體內部
  Widget _bodyTopWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 20, left: 50, right: 50),
      child: Column(
        children: [
          FittedBox(
            fit: BoxFit.scaleDown,
            child: Text(
              getTranslated(
                  context, 'Evaluation_of_iliac_and_lumbar_spine_problems'),
              textScaleFactor: 1,
              style: const TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Container(
            height: 3,
            color: HexColor.fromHex('#4A991E'),
          )
        ],
      ),
    );
  }

  //題目
  Widget _topicWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 10, left: 25),
      child: Row(
        children: [
          Container(
            width: 40,
            height: 40,
            alignment: Alignment.center,
            decoration: BoxDecoration(
                color: HexColor.fromHex('#1E2C39'),
                borderRadius: const BorderRadius.all(Radius.circular(25))),
            child: const FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                '8',
                textScaleFactor: 1,
                style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    color: Colors.white),
              ),
            ),
          ),
          const SizedBox(
            width: 20,
          ),
          FittedBox(
            fit: BoxFit.scaleDown,
            child: Text(
              getTranslated(context, 'Rotation_test'),
              textScaleFactor: 1,
              style: const TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            ),
          ),
        ],
      ),
    );
  }

  //左或右
  Widget _leftOrRightWidget() {
    return Container(
      height: 50,
      margin: const EdgeInsets.only(top: 10, left: 25, right: 25),
      child: Row(
        children: [
          Expanded(
              flex: 1,
              child: Container(
                alignment: Alignment.center,
                decoration: Style.speTextBG,
                child: FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    getTranslated(context, 'Left_side'),
                    textScaleFactor: 1,
                    style: const TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                ),
              )),
          const SizedBox(
            width: 10,
          ),
          Expanded(
              flex: 1,
              child: Container(
                alignment: Alignment.center,
                decoration: Style.speTextBG,
                child: FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    getTranslated(context, 'Right_side'),
                    textScaleFactor: 1,
                    style: const TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                ),
              ))
        ],
      ),
    );
  }

  //提示
  Widget _promptWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            'assets/images/tips.png',
            width: 30,
          ),
          FittedBox(
            fit: BoxFit.scaleDown,
            child: Text(
              getTranslated(context, 'Both_left_and_right_need_to_be_clicked'),
              textScaleFactor: 1,
              style: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
          )
        ],
      ),
    );
  }

  //選項
  Widget _optionsWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
              width: 320,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  //圓
                  InkWell(
                    onTap: rotationLeft0OnTap,
                    child: Container(
                      width: 40,
                      height: 40,
                      decoration: BoxDecoration(
                          borderRadius: const BorderRadius.all(Radius.circular(50)),
                          border: Border.all(
                              width: 7.0,
                              color: _rotationLeft != 0
                                  ? Colors.black
                                  : Colors.red)),
                    ),
                  ),
                  //線
                  InkWell(
                    onTap: rotationLeft1OnTap,
                    child: Row(
                      children: [
                        Transform.rotate(
                          angle: math.pi / 1,
                          child: Icon(
                            Icons.arrow_forward_ios_outlined,
                            color: _rotationLeft != 0 && _rotationLeft != 1
                                ? Colors.black
                                : Colors.red,
                          ),
                        ),
                        SizedBox(
                            width: 30,
                            child: DottedLine(
                              lineThickness: 3.0,
                              dashColor:
                                  _rotationLeft != 0 && _rotationLeft != 1
                                      ? Colors.black
                                      : Colors.red,
                            )),
                      ],
                    ),
                  ),
                  //中途
                  InkWell(
                    onTap: rotationLeft2OnTap,
                    child: Row(
                      children: [
                        Container(
                          margin: const EdgeInsets.only(bottom: 5),
                          alignment: Alignment.bottomCenter,
                          height: 30,
                          child: const DottedLine(
                            direction: Axis.vertical,
                            lineThickness: 3.0,
                            dashColor: Colors.black,
                            dashGapColor: Colors.black,
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.only(left: 5),
                          alignment: Alignment.bottomCenter,
                          height: 30,
                          child: const DottedLine(
                            direction: Axis.vertical,
                            lineThickness: 3.0,
                            dashColor: Colors.black,
                            dashGapColor: Colors.black,
                          ),
                        ),
                        SizedBox(
                          width: 30,
                          child: DottedLine(
                            lineThickness: 3.0,
                            dashColor: _rotationLeft != 0 &&
                                    _rotationLeft != 1 &&
                                    _rotationLeft != 2
                                ? Colors.black
                                : Colors.red,
                          ),
                        ),
                      ],
                    ),
                  ),
                  //圓
                  Container(
                    width: 40,
                    height: 40,
                    decoration: BoxDecoration(
                        borderRadius: const BorderRadius.all(Radius.circular(50)),
                        border: Border.all(width: 7.0, color: Colors.black)),
                  ),
                  //中途
                  InkWell(
                    onTap: rotationRight2OnTap,
                    child: Row(
                      children: [
                        SizedBox(
                          width: 30,
                          child: DottedLine(
                            lineThickness: 3.0,
                            dashColor: _rotationRight != 0 &&
                                    _rotationRight != 1 &&
                                    _rotationRight != 2
                                ? Colors.black
                                : Colors.red,
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.only(bottom: 5),
                          alignment: Alignment.bottomCenter,
                          height: 30,
                          child: const DottedLine(
                            direction: Axis.vertical,
                            lineThickness: 3.0,
                            dashColor: Colors.black,
                            dashGapColor: Colors.black,
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.only(left: 5),
                          alignment: Alignment.bottomCenter,
                          height: 30,
                          child: const DottedLine(
                            direction: Axis.vertical,
                            lineThickness: 3.0,
                            dashColor: Colors.black,
                            dashGapColor: Colors.black,
                          ),
                        ),
                      ],
                    ),
                  ),
                  //線
                  InkWell(
                    onTap: rotationRight1OnTap,
                    child: Row(
                      children: [
                        SizedBox(
                          width: 30,
                          child: DottedLine(
                            lineThickness: 3.0,
                            dashColor:
                                _rotationRight != 0 && _rotationRight != 1
                                    ? Colors.black
                                    : Colors.red,
                          ),
                        ),
                        Icon(
                          Icons.arrow_forward_ios_outlined,
                          color: _rotationRight != 0 && _rotationRight != 1
                              ? Colors.black
                              : Colors.red,
                        ),
                      ],
                    ),
                  ),
                  //圓
                  InkWell(
                    onTap: rotationRight0OnTap,
                    child: Container(
                      width: 40,
                      height: 40,
                      decoration: BoxDecoration(
                          borderRadius: const BorderRadius.all(Radius.circular(50)),
                          border: Border.all(
                              width: 7.0,
                              color: _rotationRight != 0
                                  ? Colors.black
                                  : Colors.red)),
                    ),
                  ),
                ],
              ))
        ],
      ),
    );
  }

  //按鈕
  Widget _bottomButtonWidget() {
    return Container(
      margin: const EdgeInsets.only(right: 10, bottom: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          //清除
          ClearButton(
              click: _clearOnTap,
              buttonText: getTranslated(context, 'Clear')),
          const SizedBox(
            width: 25,
          ),
          //下一步
          NextButton(
              click: _rotationLeft == 3 || _rotationRight == 3
                  ? null
                  : _nextOnTap,
              buttonText: getTranslated(context, 'Next_step'),
              decoration: _rotationLeft == 3 || _rotationRight == 3
                  ? null
                  : Style.speNextBG)
        ],
      ),
    );
  }

  //身體圖片按鈕
  void _bodyImageOnTap() {
    showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2){
          return HeroPhotoViewRouteWrapper(
            title: '${getTranslated(context, 'Standing_posture')}(8)',
            imageProvider: NetworkImage('$imageUrlOffline$imageUrlPublic${imageList!.pics[7]}'),
          );
        });
  }

  //清除
  void _clearOnTap() {
    delPatient();
    setState(() {
      _rotationLeft = 3;
      _rotationRight = 3;
    });
  }

  //下一步
  void _nextOnTap() {
    editPatient();
    delegate.push(name: RouteName.spe09);
  }

  void editPatient() {
    Patient patient = Patient(
        name: _patientVm!.patient!.name,
        gender: _patientVm!.patient!.gender,
        age: _patientVm!.patient!.age,
        phone: _patientVm!.patient!.phone,
        problem: _patientVm!.patient!.problem,
        relieveSymptomsOfDiscomfort: _patientVm!.patient!.relieveSymptomsOfDiscomfort,
        postureCorrection: _patientVm!.patient!.postureCorrection,
        informationHealthEducation: _patientVm!.patient!.informationHealthEducation,
        jointReduction: _patientVm!.patient!.jointReduction,
        coreMuscleTraining: _patientVm!.patient!.coreMuscleTraining,
        healthPromotion: _patientVm!.patient!.healthPromotion,
        GT: _patientVm!.patient!.GT,
        pad: _patientVm!.patient!.pad,
        padHigh: _patientVm!.patient!.padHigh.isEmpty ? '' : _patientVm!.patient!.padHigh,
        IC: _patientVm!.patient!.IC,
        ASIS: _patientVm!.patient!.ASIS,
        PSIS: _patientVm!.patient!.PSIS,
        flexion: _patientVm!.patient!.flexion,
        floor: _patientVm!.patient!.floor,
        gillet: _patientVm!.patient!.gillet,
        Extension: _patientVm!.patient!.Extension,
        rotation_left: _rotationRight,
        rotation_right: _rotationLeft,
        side_left: _patientVm!.patient!.side_left,
        side_right: _patientVm!.patient!.side_right,
        side_knee_left: _patientVm!.patient!.side_knee_left,
        side_knee_right: _patientVm!.patient!.side_knee_right,
        supine_leg_length: _patientVm!.patient!.supine_leg_length,
        adductor: _patientVm!.patient!.adductor,
        inguinal: _patientVm!.patient!.inguinal,
        prone_leg_length: _patientVm!.patient!.prone_leg_length,
        add_ROM: _patientVm!.patient!.add_ROM,
        ext_rotator: _patientVm!.patient!.ext_rotator,
        PSIS_left: _patientVm!.patient!.PSIS_left,
        PSIS_right: _patientVm!.patient!.PSIS_right,
        ILA_left: _patientVm!.patient!.ILA_left,
        ILA_right: _patientVm!.patient!.ILA_right,
        Prop_left: _patientVm!.patient!.Prop_left,
        Prop_right: _patientVm!.patient!.Prop_right,
        lumbar_left: _patientVm!.patient!.lumbar_left,
        lumbar_right: _patientVm!.patient!.lumbar_right,
        supine_ilium_leg_length: _patientVm!.patient!.supine_ilium_leg_length,
        Long_sit: _patientVm!.patient!.Long_sit,
        SLR: _patientVm!.patient!.SLR,
        Patrick: _patientVm!.patient!.Patrick,
        Knee_flexor: _patientVm!.patient!.Knee_flexor,
        ASIS_umbilical: _patientVm!.patient!.ASIS_umbilical,
        IC_recheck: _patientVm!.patient!.IC_recheck,
        flexion_recheck: _patientVm!.patient!.flexion_recheck,
        floor_recheck: _patientVm!.patient!.floor_recheck,
        gillet_recheck: _patientVm!.patient!.gillet_recheck,
        extension_recheck: _patientVm!.patient!.extension_recheck,
        rotation_left_recheck: _patientVm!.patient!.rotation_left_recheck,
        rotation_right_recheck: _patientVm!.patient!.rotation_right_recheck,
        side_left_recheck: _patientVm!.patient!.side_left_recheck,
        side_right_recheck: _patientVm!.patient!.side_right_recheck,
        side_knee_left_recheck: _patientVm!.patient!.side_knee_left_recheck,
        side_knee_right_recheck: _patientVm!.patient!.side_knee_right_recheck);
    _patientVm!.editPatient(patient);
  }

  void delPatient() async {
    Patient patient = Patient(
        name: _patientVm!.patient!.name,
        gender: _patientVm!.patient!.gender,
        age: _patientVm!.patient!.age,
        phone: _patientVm!.patient!.phone,
        problem: _patientVm!.patient!.problem,
        relieveSymptomsOfDiscomfort: _patientVm!.patient!.relieveSymptomsOfDiscomfort,
        postureCorrection: _patientVm!.patient!.postureCorrection,
        informationHealthEducation: _patientVm!.patient!.informationHealthEducation,
        jointReduction: _patientVm!.patient!.jointReduction,
        coreMuscleTraining: _patientVm!.patient!.coreMuscleTraining,
        healthPromotion: _patientVm!.patient!.healthPromotion,
        GT: _patientVm!.patient!.GT,
        pad: _patientVm!.patient!.pad,
        padHigh: _patientVm!.patient!.padHigh.isEmpty ? '' : _patientVm!.patient!.padHigh,
        IC: _patientVm!.patient!.IC,
        ASIS: _patientVm!.patient!.ASIS,
        PSIS: _patientVm!.patient!.PSIS,
        flexion: _patientVm!.patient!.flexion,
        floor: _patientVm!.patient!.floor,
        gillet: _patientVm!.patient!.gillet,
        Extension: _patientVm!.patient!.Extension,
        rotation_right: 3,
        rotation_left: 3,
        side_left: _patientVm!.patient!.side_left,
        side_right: _patientVm!.patient!.side_right,
        side_knee_left: _patientVm!.patient!.side_knee_left,
        side_knee_right: _patientVm!.patient!.side_knee_right,
        supine_leg_length: _patientVm!.patient!.supine_leg_length,
        adductor: _patientVm!.patient!.adductor,
        inguinal: _patientVm!.patient!.inguinal,
        prone_leg_length: _patientVm!.patient!.prone_leg_length,
        add_ROM: _patientVm!.patient!.add_ROM,
        ext_rotator: _patientVm!.patient!.ext_rotator,
        PSIS_left: _patientVm!.patient!.PSIS_left,
        PSIS_right: _patientVm!.patient!.PSIS_right,
        ILA_left: _patientVm!.patient!.ILA_left,
        ILA_right: _patientVm!.patient!.ILA_right,
        Prop_left: _patientVm!.patient!.Prop_left,
        Prop_right: _patientVm!.patient!.Prop_right,
        lumbar_left: _patientVm!.patient!.lumbar_left,
        lumbar_right: _patientVm!.patient!.lumbar_right,
        supine_ilium_leg_length: _patientVm!.patient!.supine_ilium_leg_length,
        Long_sit: _patientVm!.patient!.Long_sit,
        SLR: _patientVm!.patient!.SLR,
        Patrick: _patientVm!.patient!.Patrick,
        Knee_flexor: _patientVm!.patient!.Knee_flexor,
        ASIS_umbilical: _patientVm!.patient!.ASIS_umbilical,
        IC_recheck: _patientVm!.patient!.IC_recheck,
        flexion_recheck: _patientVm!.patient!.flexion_recheck,
        floor_recheck: _patientVm!.patient!.floor_recheck,
        gillet_recheck: _patientVm!.patient!.gillet_recheck,
        extension_recheck: _patientVm!.patient!.extension_recheck,
        rotation_left_recheck: _patientVm!.patient!.rotation_left_recheck,
        rotation_right_recheck: _patientVm!.patient!.rotation_right_recheck,
        side_left_recheck: _patientVm!.patient!.side_left_recheck,
        side_right_recheck: _patientVm!.patient!.side_right_recheck,
        side_knee_left_recheck: _patientVm!.patient!.side_knee_left_recheck,
        side_knee_right_recheck: _patientVm!.patient!.side_knee_right_recheck);
    await _patientVm!.editPatient(patient);
    EasyLoading.showToast('清除完成');
  }

  void delAllPatient(){
    Patient patient = Patient(
        name: _patientVm!.patient!.name,
        gender: _patientVm!.patient!.gender,
        age: _patientVm!.patient!.age,
        phone: _patientVm!.patient!.phone,
        problem: _patientVm!.patient!.problem,
        relieveSymptomsOfDiscomfort: _patientVm!.patient!.relieveSymptomsOfDiscomfort,
        postureCorrection: _patientVm!.patient!.postureCorrection,
        informationHealthEducation: _patientVm!.patient!.informationHealthEducation,
        jointReduction: _patientVm!.patient!.jointReduction,
        coreMuscleTraining: _patientVm!.patient!.coreMuscleTraining,
        healthPromotion: _patientVm!.patient!.healthPromotion,
        GT: 0,
        pad: 0,
        padHigh: '',
        IC: 0,
        ASIS: 0,
        PSIS: 0,
        flexion: 0,
        floor: '',
        gillet: 0,
        Extension: 3,
        rotation_right: 3,
        rotation_left: 3,
        side_left: _patientVm!.patient!.side_left,
        side_right: _patientVm!.patient!.side_right,
        side_knee_left: _patientVm!.patient!.side_knee_left,
        side_knee_right: _patientVm!.patient!.side_knee_right,
        supine_leg_length: _patientVm!.patient!.supine_leg_length,
        adductor: _patientVm!.patient!.adductor,
        inguinal: _patientVm!.patient!.inguinal,
        prone_leg_length: _patientVm!.patient!.prone_leg_length,
        add_ROM: _patientVm!.patient!.add_ROM,
        ext_rotator: _patientVm!.patient!.ext_rotator,
        PSIS_left: _patientVm!.patient!.PSIS_left,
        PSIS_right: _patientVm!.patient!.PSIS_right,
        ILA_left: _patientVm!.patient!.ILA_left,
        ILA_right: _patientVm!.patient!.ILA_right,
        Prop_left: _patientVm!.patient!.Prop_left,
        Prop_right: _patientVm!.patient!.Prop_right,
        lumbar_left: _patientVm!.patient!.lumbar_left,
        lumbar_right: _patientVm!.patient!.lumbar_right,
        supine_ilium_leg_length: _patientVm!.patient!.supine_ilium_leg_length,
        Long_sit: _patientVm!.patient!.Long_sit,
        SLR: _patientVm!.patient!.SLR,
        Patrick: _patientVm!.patient!.Patrick,
        Knee_flexor: _patientVm!.patient!.Knee_flexor,
        ASIS_umbilical: _patientVm!.patient!.ASIS_umbilical,
        IC_recheck: _patientVm!.patient!.IC_recheck,
        flexion_recheck: _patientVm!.patient!.flexion_recheck,
        floor_recheck: _patientVm!.patient!.floor_recheck,
        gillet_recheck: _patientVm!.patient!.gillet_recheck,
        extension_recheck: _patientVm!.patient!.extension_recheck,
        rotation_left_recheck: _patientVm!.patient!.rotation_left_recheck,
        rotation_right_recheck: _patientVm!.patient!.rotation_right_recheck,
        side_left_recheck: _patientVm!.patient!.side_left_recheck,
        side_right_recheck: _patientVm!.patient!.side_right_recheck,
        side_knee_left_recheck: _patientVm!.patient!.side_knee_left_recheck,
        side_knee_right_recheck: _patientVm!.patient!.side_knee_right_recheck);
    _patientVm!.editPatient(patient);
  }

  //rotationLeft0
  void rotationLeft0OnTap() {
    setState(() {
      _rotationLeft = 0;
    });
  }

  //rotationLeft1
  void rotationLeft1OnTap() {
    setState(() {
      _rotationLeft = 1;
    });
  }

  //rotationLeft2
  void rotationLeft2OnTap() {
    setState(() {
      _rotationLeft = 2;
    });
  }

  //rotationRight0
  void rotationRight0OnTap() {
    setState(() {
      _rotationRight = 0;
    });
  }

  //rotationRight1
  void rotationRight1OnTap() {
    setState(() {
      _rotationRight = 1;
    });
  }

  //rotationRight2
  void rotationRight2OnTap() {
    setState(() {
      _rotationRight = 2;
    });
  }
}