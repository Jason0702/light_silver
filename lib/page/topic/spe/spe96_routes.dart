import 'dart:developer';

import 'package:dotted_line/dotted_line.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:light_silver/language/language_constants.dart';
import 'package:light_silver/models/operation_item_model.dart';
import 'package:light_silver/models/patient.dart';
import 'package:light_silver/utils/api.dart';
import 'package:light_silver/utils/utils.dart';
import 'package:light_silver/widgets/button.dart';
import 'package:light_silver/widgets/left_button.dart';
import 'dart:math' as math;

import 'package:light_silver/widgets/noShadowScrollbehavior.dart';
import 'package:provider/provider.dart';

import '../../../app.dart';
import '../../../view_model/operation_item_vm.dart';
import '../../../view_model/patient_vm.dart';
import '../../../widgets/hero_photo_view_wrapper.dart';
import '../../../widgets/title_widget.dart';

class SPe96Routes extends StatefulWidget {
  @override
  State createState() => _SPe96RoutesState();
}

class _SPe96RoutesState extends State<SPe96Routes> {
  PatientVm? _patientVm;
  OperationItemVm? _operationItemVm;

  int _sideLeftRecheck = 3;
  int _sideRightRecheck = 3;

  final TextEditingController _sideKneeLeftRecheck = TextEditingController();
  final TextEditingController _sideKneeRightRecheck = TextEditingController();

  OperationItemsModel? imageList;

  _onLayoutDone(_) {
    List list =
    _operationItemVm!.operationItemsModelList!.where((element) => element.id == 9).toList();
    for (var element in list) {
      imageList = element;
      setState(() {});
    }
    _sideKneeLeftRecheck.text = '';
    _sideKneeRightRecheck.text = '';
    _dataInit(_patientVm!.patient);
  }

  void _dataInit(Patient? patient) {
    if (patient == null) {
      return;
    }
    log('取得 站姿(9-6): $patient');
    _sideLeftRecheck = patient.side_left_recheck;
    _sideRightRecheck = patient.side_right_recheck;

    _sideKneeLeftRecheck.text = patient.side_knee_left_recheck;
    _sideKneeRightRecheck.text = patient.side_knee_right_recheck;

    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  Widget build(BuildContext context) {
    _patientVm = Provider.of<PatientVm>(context);
    _operationItemVm = Provider.of<OperationItemVm>(context);
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: true,
      body: GestureDetector(
        onTap: _clickOnTap,
        child: SafeArea(
          child: Column(
            children: [
              Expanded(flex: 1, child: _titleBar()),
              Expanded(flex: 10, child: _bodyWidget())
            ],
          ),
        ),
      ),
    );
  }

  void _clickOnTap() {
    debugPrint('點擊其他區域');
    FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus && currentFocus.focusedChild != null) {
      FocusManager.instance.primaryFocus!.unfocus();
    }
  }

  //最上層標題與按鈕
  Widget _titleBar() {
    return SpeTitleWidget(
      titleText: '${getTranslated(context, 'Standing_posture_reconfirm')}(6)',
      leftButton: LeftSaveButton(func: editPatient),
      rightButton: _rightTopButton(),
    );
  }

  //右側按鈕
  Widget _rightTopButton() {
    return InkWell(
      onTap: () {
        delAllPatient();
        delegate.popUtil();
      },
      child: const Icon(
        Icons.home,
        color: Colors.white,
        size: 45,
      ),
    );
  }

  //主體
  Widget _bodyWidget() {
    return Stack(
      children: [
        Container(
          padding: const EdgeInsets.only(top: 10, bottom: 10),
          decoration: Style.homeBg,
          alignment: Alignment.center,
          child: Container(
            width: MediaQuery.of(context).size.width / 1.1,
            decoration: Style.centerBG,
            child: ScrollConfiguration(
              behavior: NoShadowScrollBehavior(),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    //主體內部
                    _bodyTopWidget(),
                    //題目
                    _topicWidget(),
                    //左或右
                    _leftOrRightWidget(),
                    //選項
                    _optionsWidget(),
                    //需求
                    _demandWidget(),
                    const SizedBox(
                      height: 20,
                    ),
                    //按鈕
                    _bottomButtonWidget()
                  ],
                ),
              ),
            ),
          ),
        ),
        //身體圖片按鈕
        imageList != null &&
                imageList!.pics.isNotEmpty &&
                imageList!.pics[5].isNotEmpty
            ? InkWell(
                onTap: _bodyImageOnTap,
                child: Container(
                  margin: const EdgeInsets.only(top: 15),
                  decoration: Style.imageButtonBG,
                  child: Container(
                    margin: const EdgeInsets.all(10),
                    child: Image.asset(
                      'assets/images/people.png',
                      height: 50,
                    ),
                  ),
                ),
              )
            : Container(),
      ],
    );
  }

  //主體內部
  Widget _bodyTopWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 20, left: 50, right: 50),
      child: Column(
        children: [
          FittedBox(
            fit: BoxFit.scaleDown,
            child: Text(
              getTranslated(
                  context, 'Reassessment_of_iliac_and_lumbar_spine_problems'),
              textScaleFactor: 1,
              style: const TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Container(
            height: 3,
            color: HexColor.fromHex('#4A991E'),
          )
        ],
      ),
    );
  }

  //題目
  Widget _topicWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 10, left: 25),
      child: Row(
        children: [
          Container(
            width: 40,
            height: 40,
            alignment: Alignment.center,
            decoration: BoxDecoration(
                color: HexColor.fromHex('#1E2C39'),
                borderRadius: const BorderRadius.all(Radius.circular(25))),
            child: const FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                '6',
                textScaleFactor: 1,
                style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    color: Colors.white),
              ),
            ),
          ),
          const SizedBox(
            width: 20,
          ),
          FittedBox(
            fit: BoxFit.scaleDown,
            child: Text(
              getTranslated(context, 'Scoliosis_test'),
              textScaleFactor: 1,
              style: const TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            ),
          ),
        ],
      ),
    );
  }

  //左或右
  Widget _leftOrRightWidget() {
    return Container(
      height: 50,
      margin: const EdgeInsets.only(top: 10, left: 25, right: 25),
      child: Row(
        children: [
          Expanded(
              flex: 1,
              child: Container(
                alignment: Alignment.center,
                decoration: Style.speTextBG,
                child: FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    getTranslated(context, 'Left_side'),
                    textScaleFactor: 1,
                    style: const TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                ),
              )),
          const SizedBox(
            width: 10,
          ),
          Expanded(
              flex: 1,
              child: Container(
                alignment: Alignment.center,
                decoration: Style.speTextBG,
                child: FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    getTranslated(context, 'Right_side'),
                    textScaleFactor: 1,
                    style: const TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                ),
              ))
        ],
      ),
    );
  }

  //選項
  Widget _optionsWidget() {
    return Container(
      margin: const EdgeInsets.only(left: 5, right: 5, top: 70),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Transform.rotate(
                    angle: math.pi / 4,
                    child: _leftWidget(),
                  ),
                  Transform.rotate(
                    angle: math.pi / -4,
                    child: _rightWidget(),
                  )
                ],
              ),
              //圓
              Container(
                margin: const EdgeInsets.only(top: 20),
                width: 40,
                height: 40,
                decoration: BoxDecoration(
                    borderRadius: const BorderRadius.all(Radius.circular(50)),
                    border: Border.all(width: 7.0, color: Colors.black)),
              ),
            ],
          )
        ],
      ),
    );
  }

  Widget _leftWidget() {
    return Row(
      children: [
        //圓
        InkWell(
          onTap: _sideLeftRecheck0OnTap,
          child: Container(
            width: 40,
            height: 40,
            decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(Radius.circular(50)),
                border: Border.all(
                    width: 7.0,
                    color: _sideLeftRecheck != 0 ? Colors.black : Colors.red)),
          ),
        ),
        //線
        InkWell(
          onTap: _sideLeftRecheck1OnTap,
          child: Row(
            children: [
              Transform.rotate(
                angle: math.pi / 1,
                child: Icon(
                  Icons.arrow_forward_ios_outlined,
                  color: _sideLeftRecheck != 0 && _sideLeftRecheck != 1
                      ? Colors.black
                      : Colors.red,
                ),
              ),
              SizedBox(
                  width: 45,
                  child: DottedLine(
                    lineThickness: 3.0,
                    dashColor: _sideLeftRecheck != 0 && _sideLeftRecheck != 1
                        ? Colors.black
                        : Colors.red,
                  )),
            ],
          ),
        ),
        //中途
        InkWell(
          onTap: _sideLeftRecheck2OnTap,
          child: Row(
            children: [
              Container(
                margin: const EdgeInsets.only(bottom: 5),
                alignment: Alignment.bottomCenter,
                height: 50,
                child: const DottedLine(
                  direction: Axis.vertical,
                  lineThickness: 3.0,
                  dashColor: Colors.black,
                  dashGapColor: Colors.black,
                ),
              ),
              Container(
                margin: const EdgeInsets.only(left: 5),
                alignment: Alignment.bottomCenter,
                height: 50,
                child: const DottedLine(
                  direction: Axis.vertical,
                  lineThickness: 3.0,
                  dashColor: Colors.black,
                  dashGapColor: Colors.black,
                ),
              ),
              SizedBox(
                width: 45,
                child: DottedLine(
                  lineThickness: 3.0,
                  dashColor: _sideLeftRecheck != 0 &&
                          _sideLeftRecheck != 1 &&
                          _sideLeftRecheck != 2
                      ? Colors.black
                      : Colors.red,
                ),
              )
            ],
          ),
        ),
      ],
    );
  }

  Widget _rightWidget() {
    return Row(
      children: [
        //中途
        InkWell(
          onTap: _sideRightRecheck2OnTap,
          child: Row(
            children: [
              SizedBox(
                width: 45,
                child: DottedLine(
                  lineThickness: 3.0,
                  dashColor: _sideRightRecheck != 0 &&
                          _sideRightRecheck != 1 &&
                          _sideRightRecheck != 2
                      ? Colors.black
                      : Colors.red,
                ),
              ),
              Container(
                margin: const EdgeInsets.only(bottom: 5),
                alignment: Alignment.bottomCenter,
                height: 50,
                child: const DottedLine(
                  direction: Axis.vertical,
                  lineThickness: 3.0,
                  dashColor: Colors.black,
                  dashGapColor: Colors.black,
                ),
              ),
              Container(
                margin: const EdgeInsets.only(left: 5),
                alignment: Alignment.bottomCenter,
                height: 50,
                child: const DottedLine(
                  direction: Axis.vertical,
                  lineThickness: 3.0,
                  dashColor: Colors.black,
                  dashGapColor: Colors.black,
                ),
              ),
            ],
          ),
        ),
        //線
        InkWell(
          onTap: _sideRightRecheck1OnTap,
          child: Row(
            children: [
              SizedBox(
                width: 45,
                child: DottedLine(
                  lineThickness: 3.0,
                  dashColor: _sideRightRecheck != 0 && _sideRightRecheck != 1
                      ? Colors.black
                      : Colors.red,
                ),
              ),
              Icon(
                Icons.arrow_forward_ios_outlined,
                color: _sideRightRecheck != 0 && _sideRightRecheck != 1
                    ? Colors.black
                    : Colors.red,
              ),
            ],
          ),
        ),
        //圓
        InkWell(
          onTap: _sideRightRecheck0OnTap,
          child: Container(
            width: 40,
            height: 40,
            decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(Radius.circular(50)),
                border: Border.all(
                    width: 7.0,
                    color: _sideRightRecheck != 0 ? Colors.black : Colors.red)),
          ),
        ),
      ],
    );
  }

  //需求
  Widget _demandWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 10, left: 25, right: 25),
      color: Colors.white,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                alignment: Alignment.center,
                margin: const EdgeInsets.all(5),
                child: FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    getTranslated(context, 'Distance_from_fingertip_to_knee'),
                    textScaleFactor: 1,
                    style: const TextStyle(
                        fontSize: 20,
                        color: Colors.black,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              Row(
                children: [
                  Container(
                    margin: const EdgeInsets.all(8),
                    width: 80,
                    child: TextField(
                      textAlign: TextAlign.center,
                      style: const TextStyle(color: Colors.black),
                      decoration: const InputDecoration(
                        contentPadding: EdgeInsets.all(5),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.black)),
                        filled: true,
                        fillColor: Colors.white,
                      ),
                      keyboardType: TextInputType.number,
                      textInputAction: TextInputAction.done,
                      controller: _sideKneeLeftRecheck,
                    ),
                  ),
                  FittedBox(
                    fit: BoxFit.scaleDown,
                    child: Text(
                      getTranslated(context, 'Cm'),
                      textScaleFactor: 1,
                      style: const TextStyle(
                          fontSize: 20,
                          color: Colors.black,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
            ],
          ),
          //右邊
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                alignment: Alignment.centerLeft,
                margin: const EdgeInsets.all(5),
                child: FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    getTranslated(context, 'Distance_from_fingertip_to_knee'),
                    textScaleFactor: 1,
                    style: const TextStyle(
                        fontSize: 20,
                        color: Colors.black,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              Row(
                children: [
                  Container(
                    margin: const EdgeInsets.all(8),
                    width: 80,
                    child: TextField(
                      textAlign: TextAlign.center,
                      style: const TextStyle(color: Colors.black),
                      decoration: const InputDecoration(
                        contentPadding: EdgeInsets.all(5),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.black)),
                        filled: true,
                        fillColor: Colors.white,
                      ),
                      keyboardType: TextInputType.number,
                      textInputAction: TextInputAction.done,
                      controller: _sideKneeRightRecheck,
                    ),
                  ),
                  FittedBox(
                    fit: BoxFit.scaleDown,
                    child: Text(
                      getTranslated(context, 'Cm'),
                      textScaleFactor: 1,
                      style: const TextStyle(
                          fontSize: 20,
                          color: Colors.black,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
            ],
          )
        ],
      ),
    );
  }

  //按鈕
  Widget _bottomButtonWidget() {
    return Container(
      margin: const EdgeInsets.only(right: 10, bottom: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          //清除
          ClearButton(
              click: _clearOnTap, buttonText: getTranslated(context, 'Clear')),
          const SizedBox(
            width: 25,
          ),
          //下一步
          NextButton(
              click: _sideLeftRecheck == 3 || _sideRightRecheck == 3
                  ? null
                  : _nextOnTap,
              buttonText: getTranslated(context, 'Next_step'),
              decoration: _sideLeftRecheck == 3 || _sideRightRecheck == 3
                  ? null
                  : Style.speNextBG),
        ],
      ),
    );
  }

  //身體圖片按鈕
  void _bodyImageOnTap() {
    showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return HeroPhotoViewRouteWrapper(
            title: '${getTranslated(context, 'Standing_posture')}(6)',
            imageProvider: NetworkImage(
                '$imageUrlOffline$imageUrlPrivate${imageList!.pics[5]}',
                headers: {"Authorization": "Bearer $accessToken"}),
          );
        });
  }

  //清除
  void _clearOnTap() {
    delPatient();
    setState(() {
      _sideLeftRecheck = 3;
      _sideRightRecheck = 3;
      _sideKneeLeftRecheck.text = '100';
      _sideKneeRightRecheck.text = '100';
    });
  }

  //下一步
  void _nextOnTap() {
    if ((_sideKneeLeftRecheck.text.isEmpty &&
            _sideKneeRightRecheck.text.isEmpty) ||
        !int.parse(_sideKneeLeftRecheck.text).isNegative &&
            !int.parse(_sideKneeRightRecheck.text).isNegative) {
      editPatient();
      delegate.popUtil();
    } else {
      EasyLoading.showToast('不可輸入負值');
    }
  }

  void editPatient() {
    Patient patient = Patient(
        name: _patientVm!.patient!.name,
        gender: _patientVm!.patient!.gender,
        age: _patientVm!.patient!.age,
        phone: _patientVm!.patient!.phone,
        problem: _patientVm!.patient!.problem,
        relieveSymptomsOfDiscomfort: _patientVm!.patient!.relieveSymptomsOfDiscomfort,
        postureCorrection: _patientVm!.patient!.postureCorrection,
        informationHealthEducation: _patientVm!.patient!.informationHealthEducation,
        jointReduction: _patientVm!.patient!.jointReduction,
        coreMuscleTraining: _patientVm!.patient!.coreMuscleTraining,
        healthPromotion: _patientVm!.patient!.healthPromotion,
        GT: _patientVm!.patient!.GT,
        pad: _patientVm!.patient!.pad,
        padHigh: _patientVm!.patient!.padHigh.isEmpty ? '' : _patientVm!.patient!.padHigh,
        IC: _patientVm!.patient!.IC,
        ASIS: _patientVm!.patient!.ASIS,
        PSIS: _patientVm!.patient!.PSIS,
        flexion: _patientVm!.patient!.flexion,
        floor: _patientVm!.patient!.floor,
        gillet: _patientVm!.patient!.gillet,
        Extension: _patientVm!.patient!.Extension,
        rotation_right: _patientVm!.patient!.rotation_right,
        rotation_left: _patientVm!.patient!.rotation_left,
        side_left: _patientVm!.patient!.side_left,
        side_right: _patientVm!.patient!.side_right,
        side_knee_left: _patientVm!.patient!.side_knee_left,
        side_knee_right: _patientVm!.patient!.side_knee_right,
        supine_leg_length: _patientVm!.patient!.supine_leg_length,
        adductor: _patientVm!.patient!.adductor,
        inguinal: _patientVm!.patient!.inguinal,
        prone_leg_length: _patientVm!.patient!.prone_leg_length,
        add_ROM: _patientVm!.patient!.add_ROM,
        ext_rotator: _patientVm!.patient!.ext_rotator,
        PSIS_left: _patientVm!.patient!.PSIS_left,
        PSIS_right: _patientVm!.patient!.PSIS_right,
        ILA_left: _patientVm!.patient!.ILA_left,
        ILA_right: _patientVm!.patient!.ILA_right,
        Prop_left: _patientVm!.patient!.Prop_left,
        Prop_right: _patientVm!.patient!.Prop_right,
        lumbar_left: _patientVm!.patient!.lumbar_left,
        lumbar_right: _patientVm!.patient!.lumbar_right,
        supine_ilium_leg_length: _patientVm!.patient!.supine_ilium_leg_length,
        Long_sit: _patientVm!.patient!.Long_sit,
        SLR: _patientVm!.patient!.SLR,
        Patrick: _patientVm!.patient!.Patrick,
        Knee_flexor: _patientVm!.patient!.Knee_flexor,
        ASIS_umbilical: _patientVm!.patient!.ASIS_umbilical,
        IC_recheck: _patientVm!.patient!.IC_recheck,
        flexion_recheck: _patientVm!.patient!.flexion_recheck,
        floor_recheck: _patientVm!.patient!.floor_recheck,
        gillet_recheck: _patientVm!.patient!.gillet_recheck,
        extension_recheck: _patientVm!.patient!.extension_recheck,
        rotation_left_recheck: _patientVm!.patient!.rotation_left_recheck,
        rotation_right_recheck: _patientVm!.patient!.rotation_right_recheck,
        side_left_recheck: _sideLeftRecheck,
        side_right_recheck: _sideRightRecheck,
        side_knee_left_recheck: _sideKneeLeftRecheck.text,
        side_knee_right_recheck: _sideKneeRightRecheck.text);
    _patientVm!.editPatient(patient);
  }

  void delPatient() async {
    Patient patient = Patient(
        name: _patientVm!.patient!.name,
        gender: _patientVm!.patient!.gender,
        age: _patientVm!.patient!.age,
        phone: _patientVm!.patient!.phone,
        problem: _patientVm!.patient!.problem,
        relieveSymptomsOfDiscomfort: _patientVm!.patient!.relieveSymptomsOfDiscomfort,
        postureCorrection: _patientVm!.patient!.postureCorrection,
        informationHealthEducation: _patientVm!.patient!.informationHealthEducation,
        jointReduction: _patientVm!.patient!.jointReduction,
        coreMuscleTraining: _patientVm!.patient!.coreMuscleTraining,
        healthPromotion: _patientVm!.patient!.healthPromotion,
        GT: _patientVm!.patient!.GT,
        pad: _patientVm!.patient!.pad,
        padHigh: _patientVm!.patient!.padHigh.isEmpty ? '' : _patientVm!.patient!.padHigh,
        IC: _patientVm!.patient!.IC,
        ASIS: _patientVm!.patient!.ASIS,
        PSIS: _patientVm!.patient!.PSIS,
        flexion: _patientVm!.patient!.flexion,
        floor: _patientVm!.patient!.floor,
        gillet: _patientVm!.patient!.gillet,
        Extension: _patientVm!.patient!.Extension,
        rotation_right: _patientVm!.patient!.rotation_right,
        rotation_left: _patientVm!.patient!.rotation_left,
        side_left: _patientVm!.patient!.side_left,
        side_right: _patientVm!.patient!.side_right,
        side_knee_left: _patientVm!.patient!.side_knee_left,
        side_knee_right: _patientVm!.patient!.side_knee_right,
        supine_leg_length: _patientVm!.patient!.supine_leg_length,
        adductor: _patientVm!.patient!.adductor,
        inguinal: _patientVm!.patient!.inguinal,
        prone_leg_length: _patientVm!.patient!.prone_leg_length,
        add_ROM: _patientVm!.patient!.add_ROM,
        ext_rotator: _patientVm!.patient!.ext_rotator,
        PSIS_left: _patientVm!.patient!.PSIS_left,
        PSIS_right: _patientVm!.patient!.PSIS_right,
        ILA_left: _patientVm!.patient!.ILA_left,
        ILA_right: _patientVm!.patient!.ILA_right,
        Prop_left: _patientVm!.patient!.Prop_left,
        Prop_right: _patientVm!.patient!.Prop_right,
        lumbar_left: _patientVm!.patient!.lumbar_left,
        lumbar_right: _patientVm!.patient!.lumbar_right,
        supine_ilium_leg_length: _patientVm!.patient!.supine_ilium_leg_length,
        Long_sit: _patientVm!.patient!.Long_sit,
        SLR: _patientVm!.patient!.SLR,
        Patrick: _patientVm!.patient!.Patrick,
        Knee_flexor: _patientVm!.patient!.Knee_flexor,
        ASIS_umbilical: _patientVm!.patient!.ASIS_umbilical,
        IC_recheck: _patientVm!.patient!.IC_recheck,
        flexion_recheck: _patientVm!.patient!.flexion_recheck,
        floor_recheck: _patientVm!.patient!.floor_recheck,
        gillet_recheck: _patientVm!.patient!.gillet_recheck,
        extension_recheck: _patientVm!.patient!.extension_recheck,
        rotation_left_recheck: _patientVm!.patient!.rotation_left_recheck,
        rotation_right_recheck: _patientVm!.patient!.rotation_right_recheck,
        side_left_recheck: 3,
        side_right_recheck: 3,
        side_knee_left_recheck: '',
        side_knee_right_recheck: '');
    await _patientVm!.editPatient(patient);
    EasyLoading.showToast('清除完成');
  }

  void delAllPatient() {
    Patient patient = Patient(
        name: _patientVm!.patient!.name,
        gender: _patientVm!.patient!.gender,
        age: _patientVm!.patient!.age,
        phone: _patientVm!.patient!.phone,
        problem: _patientVm!.patient!.problem,
        relieveSymptomsOfDiscomfort: _patientVm!.patient!.relieveSymptomsOfDiscomfort,
        postureCorrection: _patientVm!.patient!.postureCorrection,
        informationHealthEducation: _patientVm!.patient!.informationHealthEducation,
        jointReduction: _patientVm!.patient!.jointReduction,
        coreMuscleTraining: _patientVm!.patient!.coreMuscleTraining,
        healthPromotion: _patientVm!.patient!.healthPromotion,
        GT: _patientVm!.patient!.GT,
        pad: _patientVm!.patient!.pad,
        padHigh: _patientVm!.patient!.padHigh,
        IC: _patientVm!.patient!.IC,
        ASIS: _patientVm!.patient!.ASIS,
        PSIS: _patientVm!.patient!.PSIS,
        flexion: _patientVm!.patient!.flexion,
        floor: _patientVm!.patient!.floor,
        gillet: _patientVm!.patient!.gillet,
        Extension: _patientVm!.patient!.Extension,
        rotation_right: _patientVm!.patient!.rotation_right,
        rotation_left: _patientVm!.patient!.rotation_left,
        side_left: _patientVm!.patient!.side_left,
        side_right: _patientVm!.patient!.side_right,
        side_knee_left: _patientVm!.patient!.side_knee_left,
        side_knee_right: _patientVm!.patient!.side_knee_right,
        supine_leg_length: _patientVm!.patient!.supine_leg_length,
        adductor: _patientVm!.patient!.adductor,
        inguinal: _patientVm!.patient!.inguinal,
        prone_leg_length: _patientVm!.patient!.prone_leg_length,
        add_ROM: _patientVm!.patient!.add_ROM,
        ext_rotator: _patientVm!.patient!.ext_rotator,
        PSIS_left: _patientVm!.patient!.PSIS_left,
        PSIS_right: _patientVm!.patient!.PSIS_right,
        ILA_left: _patientVm!.patient!.ILA_left,
        ILA_right: _patientVm!.patient!.ILA_right,
        Prop_left: _patientVm!.patient!.Prop_left,
        Prop_right: _patientVm!.patient!.Prop_right,
        lumbar_left: _patientVm!.patient!.lumbar_left,
        lumbar_right: _patientVm!.patient!.lumbar_right,
        supine_ilium_leg_length: _patientVm!.patient!.supine_ilium_leg_length,
        Long_sit: _patientVm!.patient!.Long_sit,
        SLR: _patientVm!.patient!.SLR,
        Patrick: _patientVm!.patient!.Patrick,
        Knee_flexor: _patientVm!.patient!.Knee_flexor,
        ASIS_umbilical: _patientVm!.patient!.ASIS_umbilical,
        IC_recheck: 0,
        flexion_recheck: 0,
        floor_recheck: '',
        gillet_recheck: 0,
        extension_recheck: 3,
        rotation_left_recheck: 3,
        rotation_right_recheck: 3,
        side_left_recheck: 3,
        side_right_recheck: 3,
        side_knee_left_recheck: '',
        side_knee_right_recheck: '');
    _patientVm!.editPatient(patient);
  }

  //sideLeft0
  void _sideLeftRecheck0OnTap() {
    setState(() {
      _sideLeftRecheck = 0;
      debugPrint('Left: $_sideLeftRecheck');
    });
  }

  //sideLeft1
  void _sideLeftRecheck1OnTap() {
    setState(() {
      _sideLeftRecheck = 1;
      debugPrint('Left: $_sideLeftRecheck');
    });
  }

  //sideLeft2
  void _sideLeftRecheck2OnTap() {
    setState(() {
      _sideLeftRecheck = 2;
      debugPrint('Left: $_sideLeftRecheck');
    });
  }

  //sideRight0
  void _sideRightRecheck0OnTap() {
    setState(() {
      _sideRightRecheck = 0;
      debugPrint('Right: $_sideRightRecheck');
    });
  }

  //sideRight1
  void _sideRightRecheck1OnTap() {
    setState(() {
      _sideRightRecheck = 1;
      debugPrint('Right: $_sideRightRecheck');
    });
  }

  //sideRight2
  void _sideRightRecheck2OnTap() {
    setState(() {
      _sideRightRecheck = 2;
      debugPrint('Right: $_sideRightRecheck');
    });
  }
}
