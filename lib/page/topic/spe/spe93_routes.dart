import 'dart:developer';

import 'package:dotted_line/dotted_line.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:light_silver/database/database_helper.dart';
import 'package:light_silver/language/language_constants.dart';
import 'package:light_silver/models/operation_item_model.dart';
import 'package:light_silver/models/patient.dart';
import 'package:light_silver/routes/route_name.dart';
import 'package:light_silver/utils/api.dart';
import 'package:light_silver/utils/shared_preference_util.dart';
import 'package:light_silver/utils/utils.dart';
import 'package:light_silver/widgets/left_button.dart';
import 'package:provider/provider.dart';
import 'dart:math' as math;

import '../../../app.dart';
import '../../../view_model/operation_item_vm.dart';
import '../../../view_model/patient_vm.dart';
import '../../../widgets/button.dart';
import '../../../widgets/hero_photo_view_wrapper.dart';
import '../../../widgets/title_widget.dart';

class SPe93Routes extends StatefulWidget {

  @override
  State createState() => _SPe93RoutesState();
}

class _SPe93RoutesState extends State<SPe93Routes> {
  PatientVm? _patientVm;
  OperationItemVm? _operationItemVm;
  int _gilletRecheck = 0;
  OperationItemsModel? imageList;

  _onLayoutDone(_) {
    List list = _operationItemVm!.operationItemsModelList!.where((element) => element.id == 9)
        .toList();
    for (var element in list) {
      imageList = element;
      setState(() {});
    }
    _dataInit(_patientVm!.patient);
  }

  void _dataInit(Patient? patient) {
    if (patient == null) {
      return;
    }
    log('取得 站姿(9-3): $patient');
    _gilletRecheck = patient.gillet_recheck;
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  Widget build(BuildContext context) {
    _patientVm = Provider.of<PatientVm>(context);
    _operationItemVm = Provider.of<OperationItemVm>(context);
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Column(
          children: [
            Expanded(flex: 1, child: _titleBar()),
            Expanded(flex: 10, child: _bodyWidget())
          ],
        ),
      ),
    );
  }

  //最上層標題與按鈕
  Widget _titleBar() {
    return SpeTitleWidget(
      titleText: '${getTranslated(
          context, 'Standing_posture_reconfirm')}(3)',
      leftButton: LeftSaveButton(func: editPatient),
      rightButton: _rightTopButton(),
    );
  }

  //右側按鈕
  Widget _rightTopButton() {
    return InkWell(
      onTap: () {
        delPatient();
        delegate.popUtil();
      },
      child: const Icon(
        Icons.home,
        color: Colors.white,
        size: 45,
      ),
    );
  }

  //主體
  Widget _bodyWidget() {
    return Stack(
      children: [
        Container(
          padding: const EdgeInsets.only(top: 10, bottom: 10),
          decoration: Style.homeBg,
          alignment: Alignment.center,
          child: Container(
            width: MediaQuery
                .of(context)
                .size
                .width / 1.1,
            decoration: Style.centerBG,
            child: Column(
              children: [
                //主體內部
                _bodyTopWidget(),
                //題目
                _topicWidget(),
                //左或右
                _leftOrRightWidget(),
                const Spacer(),
                //選項
                _optionsWidget(),
                const Spacer(),
                //按鈕
                _bottomButtonWidget(),
              ],
            ),
          ),
        ),
        //身體圖片按鈕
        imageList != null && imageList!.pics.isNotEmpty &&
            imageList!.pics[2].isNotEmpty ? InkWell(
          onTap: _bodyImageOnTap,
          child: Container(
            margin: const EdgeInsets.only(top: 15),
            decoration: Style.imageButtonBG,
            child: Container(
              margin: const EdgeInsets.all(10),
              child: Image.asset(
                'assets/images/people.png',
                height: 50,
              ),
            ),
          ),
        ) : Container(),
      ],
    );
  }

  //主體內部
  Widget _bodyTopWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 10, left: 50, right: 50),
      child: Column(
        children: [
          FittedBox(
            fit: BoxFit.scaleDown,
            child: Text(
              getTranslated(
                  context, 'Reassessment_of_iliac_and_lumbar_spine_problems'),
              textScaleFactor: 1,
              style: const TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Container(
            height: 3,
            color: HexColor.fromHex('#4A991E'),
          )
        ],
      ),
    );
  }

  //題目
  Widget _topicWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 20, left: 25),
      child: Row(
        children: [
          Container(
            width: 40,
            height: 40,
            alignment: Alignment.center,
            decoration: BoxDecoration(
                color: HexColor.fromHex('#1E2C39'),
                borderRadius: BorderRadius.circular(25)),
            child: const FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                '3',
                textScaleFactor: 1,
                style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    color: Colors.white),
              ),
            ),
          ),
          const SizedBox(
            width: 20,
          ),
          FittedBox(
            fit: BoxFit.scaleDown,
            child: Text(
              getTranslated(context, 'Geely_test'),
              textScaleFactor: 1,
              style: const TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            ),
          ),
        ],
      ),
    );
  }

  //左或右
  Widget _leftOrRightWidget() {
    return Container(
      height: 50,
      margin: const EdgeInsets.only(top: 10, left: 25, right: 25),
      child: Row(
        children: [
          Expanded(
              flex: 1,
              child: Container(
                alignment: Alignment.center,
                decoration: Style.speTextBG,
                child: FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    getTranslated(context, 'Left_side'),
                    textScaleFactor: 1,
                    style: const TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                ),
              )),
          const SizedBox(
            width: 10,
          ),
          Expanded(
              flex: 1,
              child: Container(
                alignment: Alignment.center,
                decoration: Style.speTextBG,
                child: FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    getTranslated(context, 'Right_side'),
                    textScaleFactor: 1,
                    style: const TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                ),
              ))
        ],
      ),
    );
  }

  //選項
  Widget _optionsWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 20, left: 25, right: 25),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          //flexion1
          InkWell(
            onTap: _gilletRecheck1OnTap,
            child: Column(
              children: [
                Container(
                  width: 40,
                  height: 40,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50),
                      border: Border.all(width: 7.0,
                          color: _gilletRecheck != 1 ? Colors.black : Colors
                              .red)),
                ),
                Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Transform.rotate(angle: math.pi / -2,
                      child: Icon(Icons.arrow_forward_ios_outlined,
                        color: _gilletRecheck != 1
                            ? HexColor.fromHex('#479719')
                            : Colors.red,),),
                    SizedBox(
                      width: 10,
                      height: 130,
                      child: DottedLine(
                        direction: Axis.vertical,
                        lineThickness: 3.0,
                        dashColor: _gilletRecheck != 1 ? HexColor.fromHex(
                            '#479719') : Colors.red,
                      ),
                    ),
                  ],
                ),
                Container(
                  width: 40,
                  height: 40,
                  decoration: BoxDecoration(
                      borderRadius: const BorderRadius.all(Radius.circular(50)),
                      border: Border.all(width: 7.0, color: Colors.black)),
                ),
              ],
            ),
          ),
          InkWell(
            onTap: _gilletRecheck3OnTap,
            child: Row(
              children: [
                Row(
                  children: [
                    Transform.rotate(angle: math.pi / -1,
                      child: Icon(Icons.arrow_forward_ios_outlined,
                        color: _gilletRecheck != 3
                            ? HexColor.fromHex('#479719')
                            : Colors.red,),),
                    SizedBox(
                      width: 65,
                      child: DottedLine(
                        lineThickness: 3.0,
                        dashColor: _gilletRecheck != 3 ? HexColor.fromHex(
                            '#479719') : Colors.red,
                      ),
                    ),
                  ],
                ),
                Container(
                  width: 40,
                  height: 40,
                  decoration: BoxDecoration(
                      borderRadius: const BorderRadius.all(Radius.circular(50)),
                      border: Border.all(width: 7.0,
                          color: _gilletRecheck != 3 ? Colors.black : Colors
                              .red)),
                ),
                Row(
                  children: [
                    SizedBox(
                      width: 65,
                      child: DottedLine(
                        lineThickness: 3.0,
                        dashColor: _gilletRecheck != 3 ? HexColor.fromHex(
                            '#479719') : Colors.red,
                      ),
                    ),
                    Icon(Icons.arrow_forward_ios_outlined,
                      color: _gilletRecheck != 3
                          ? HexColor.fromHex('#479719')
                          : Colors.red,)
                  ],
                )
              ],
            ),
          ),
          //flexion2
          InkWell(
            onTap: _gilletRecheck2OnTap,
            child: Column(
              children: [
                Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      width: 40,
                      height: 40,
                      decoration: BoxDecoration(
                          borderRadius: const BorderRadius.all(
                              Radius.circular(50)),
                          border: Border.all(width: 7.0,
                              color: _gilletRecheck != 2 ? Colors.black : Colors
                                  .red)),
                    ),
                    Transform.rotate(angle: math.pi / -2,
                      child: Icon(Icons.arrow_forward_ios_outlined,
                        color: _gilletRecheck != 2
                            ? HexColor.fromHex('#479719')
                            : Colors.red,),),
                    SizedBox(
                      width: 10,
                      height: 130,
                      child: DottedLine(
                        lineThickness: 3.0,
                        direction: Axis.vertical,
                        dashColor: _gilletRecheck != 2 ? HexColor.fromHex(
                            '#479719') : Colors.red,
                      ),
                    ),

                  ],
                ),
                Container(
                  width: 40,
                  height: 40,
                  decoration: BoxDecoration(
                      borderRadius: const BorderRadius.all(Radius.circular(50)),
                      border: Border.all(width: 7.0, color: Colors.black)),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  //按鈕
  Widget _bottomButtonWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 20, right: 10, bottom: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          //清除
          ClearButton(
              click: _clearOnTap,
              buttonText: getTranslated(context, 'Clear')),
          const SizedBox(
            width: 25,
          ),
          //下一步
          NextButton(
              click: _gilletRecheck == 0 ? null : _nextOnTap,
              buttonText: getTranslated(context, 'Next_step'),
              decoration: _gilletRecheck == 0 ? null : Style.speNextBG)
        ],
      ),
    );
  }

  //身體圖片按鈕
  void _bodyImageOnTap() {
    showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2){
          return HeroPhotoViewRouteWrapper(
            title: '${getTranslated(context, 'Standing_posture')}(3)',
            imageProvider: NetworkImage(
                '$imageUrlOffline$imageUrlPrivate${imageList!.pics[2]}',
                headers: {"Authorization": "Bearer $accessToken"}),
          );
        });
  }

  //清除
  void _clearOnTap() {
    delPatient();
    setState(() {
      _gilletRecheck = 0;
    });
  }

  //下一步
  void _nextOnTap() {
    editPatient();
    if (_patientVm!.patient!.flexion_recheck != _gilletRecheck) {
      delegate.push(name: RouteName.spe92And93);
    } else {
      delegate.push(name: RouteName.spe94);
    }
  }

  void editPatient() {
    Patient patient = Patient(
        name: _patientVm!.patient!.name,
        gender: _patientVm!.patient!.gender,
        age: _patientVm!.patient!.age,
        phone: _patientVm!.patient!.phone,
        problem: _patientVm!.patient!.problem,
        relieveSymptomsOfDiscomfort: _patientVm!.patient!.relieveSymptomsOfDiscomfort,
        postureCorrection: _patientVm!.patient!.postureCorrection,
        informationHealthEducation: _patientVm!.patient!.informationHealthEducation,
        jointReduction: _patientVm!.patient!.jointReduction,
        coreMuscleTraining: _patientVm!.patient!.coreMuscleTraining,
        healthPromotion: _patientVm!.patient!.healthPromotion,
        GT: _patientVm!.patient!.GT,
        pad: _patientVm!.patient!.pad,
        padHigh: _patientVm!.patient!.padHigh.isEmpty ? '' : _patientVm!.patient!.padHigh,
        IC: _patientVm!.patient!.IC,
        ASIS: _patientVm!.patient!.ASIS,
        PSIS: _patientVm!.patient!.PSIS,
        flexion: _patientVm!.patient!.flexion,
        floor: _patientVm!.patient!.floor,
        gillet: _patientVm!.patient!.gillet,
        Extension: _patientVm!.patient!.Extension,
        rotation_right: _patientVm!.patient!.rotation_right,
        rotation_left: _patientVm!.patient!.rotation_left,
        side_left: _patientVm!.patient!.side_left,
        side_right: _patientVm!.patient!.side_right,
        side_knee_left: _patientVm!.patient!.side_knee_left,
        side_knee_right: _patientVm!.patient!.side_knee_right,
        supine_leg_length: _patientVm!.patient!.supine_leg_length,
        adductor: _patientVm!.patient!.adductor,
        inguinal: _patientVm!.patient!.inguinal,
        prone_leg_length: _patientVm!.patient!.prone_leg_length,
        add_ROM: _patientVm!.patient!.add_ROM,
        ext_rotator: _patientVm!.patient!.ext_rotator,
        PSIS_left: _patientVm!.patient!.PSIS_left,
        PSIS_right: _patientVm!.patient!.PSIS_right,
        ILA_left: _patientVm!.patient!.ILA_left,
        ILA_right: _patientVm!.patient!.ILA_right,
        Prop_left: _patientVm!.patient!.Prop_left,
        Prop_right: _patientVm!.patient!.Prop_right,
        lumbar_left: _patientVm!.patient!.lumbar_left,
        lumbar_right: _patientVm!.patient!.lumbar_right,
        supine_ilium_leg_length: _patientVm!.patient!.supine_ilium_leg_length,
        Long_sit: _patientVm!.patient!.Long_sit,
        SLR: _patientVm!.patient!.SLR,
        Patrick: _patientVm!.patient!.Patrick,
        Knee_flexor: _patientVm!.patient!.Knee_flexor,
        ASIS_umbilical: _patientVm!.patient!.ASIS_umbilical,
        IC_recheck: _patientVm!.patient!.IC_recheck,
        flexion_recheck: _patientVm!.patient!.flexion_recheck,
        floor_recheck: _patientVm!.patient!.floor_recheck,
        gillet_recheck: _gilletRecheck,
        extension_recheck: _patientVm!.patient!.extension_recheck,
        rotation_left_recheck: _patientVm!.patient!.rotation_left_recheck,
        rotation_right_recheck: _patientVm!.patient!.rotation_right_recheck,
        side_left_recheck: _patientVm!.patient!.side_left_recheck,
        side_right_recheck: _patientVm!.patient!.side_right_recheck,
        side_knee_left_recheck: _patientVm!.patient!.side_knee_left_recheck,
        side_knee_right_recheck: _patientVm!.patient!.side_knee_right_recheck
    );
    _patientVm!.editPatient(patient);
  }

  void delPatient() async {
    Patient patient = Patient(
        name: _patientVm!.patient!.name,
        gender: _patientVm!.patient!.gender,
        age: _patientVm!.patient!.age,
        phone: _patientVm!.patient!.phone,
        problem: _patientVm!.patient!.problem,
        relieveSymptomsOfDiscomfort: _patientVm!.patient!.relieveSymptomsOfDiscomfort,
        postureCorrection: _patientVm!.patient!.postureCorrection,
        informationHealthEducation: _patientVm!.patient!.informationHealthEducation,
        jointReduction: _patientVm!.patient!.jointReduction,
        coreMuscleTraining: _patientVm!.patient!.coreMuscleTraining,
        healthPromotion: _patientVm!.patient!.healthPromotion,
        GT: _patientVm!.patient!.GT,
        pad: _patientVm!.patient!.pad,
        padHigh: _patientVm!.patient!.padHigh.isEmpty ? '' : _patientVm!.patient!.padHigh,
        IC: _patientVm!.patient!.IC,
        ASIS: _patientVm!.patient!.ASIS,
        PSIS: _patientVm!.patient!.PSIS,
        flexion: _patientVm!.patient!.flexion,
        floor: _patientVm!.patient!.floor,
        gillet: _patientVm!.patient!.gillet,
        Extension: _patientVm!.patient!.Extension,
        rotation_right: _patientVm!.patient!.rotation_right,
        rotation_left: _patientVm!.patient!.rotation_left,
        side_left: _patientVm!.patient!.side_left,
        side_right: _patientVm!.patient!.side_right,
        side_knee_left: _patientVm!.patient!.side_knee_left,
        side_knee_right: _patientVm!.patient!.side_knee_right,
        supine_leg_length: _patientVm!.patient!.supine_leg_length,
        adductor: _patientVm!.patient!.adductor,
        inguinal: _patientVm!.patient!.inguinal,
        prone_leg_length: _patientVm!.patient!.prone_leg_length,
        add_ROM: _patientVm!.patient!.add_ROM,
        ext_rotator: _patientVm!.patient!.ext_rotator,
        PSIS_left: _patientVm!.patient!.PSIS_left,
        PSIS_right: _patientVm!.patient!.PSIS_right,
        ILA_left: _patientVm!.patient!.ILA_left,
        ILA_right: _patientVm!.patient!.ILA_right,
        Prop_left: _patientVm!.patient!.Prop_left,
        Prop_right: _patientVm!.patient!.Prop_right,
        lumbar_left: _patientVm!.patient!.lumbar_left,
        lumbar_right: _patientVm!.patient!.lumbar_right,
        supine_ilium_leg_length: _patientVm!.patient!.supine_ilium_leg_length,
        Long_sit: _patientVm!.patient!.Long_sit,
        SLR: _patientVm!.patient!.SLR,
        Patrick: _patientVm!.patient!.Patrick,
        Knee_flexor: _patientVm!.patient!.Knee_flexor,
        ASIS_umbilical: _patientVm!.patient!.ASIS_umbilical,
        IC_recheck: _patientVm!.patient!.IC_recheck,
        flexion_recheck: _patientVm!.patient!.flexion_recheck,
        floor_recheck: _patientVm!.patient!.floor_recheck,
        gillet_recheck: 0,
        extension_recheck: _patientVm!.patient!.extension_recheck,
        rotation_left_recheck: _patientVm!.patient!.rotation_left_recheck,
        rotation_right_recheck: _patientVm!.patient!.rotation_right_recheck,
        side_left_recheck: _patientVm!.patient!.side_left_recheck,
        side_right_recheck: _patientVm!.patient!.side_right_recheck,
        side_knee_left_recheck: _patientVm!.patient!.side_knee_left_recheck,
        side_knee_right_recheck: _patientVm!.patient!.side_knee_right_recheck
    );
    await _patientVm!.editPatient(patient);
    EasyLoading.showToast('清除完成');
  }

  void delAllPatient() {
    Patient patient = Patient(
        name: _patientVm!.patient!.name,
        gender: _patientVm!.patient!.gender,
        age: _patientVm!.patient!.age,
        phone: _patientVm!.patient!.phone,
        problem: _patientVm!.patient!.problem,
        relieveSymptomsOfDiscomfort: _patientVm!.patient!.relieveSymptomsOfDiscomfort,
        postureCorrection: _patientVm!.patient!.postureCorrection,
        informationHealthEducation: _patientVm!.patient!.informationHealthEducation,
        jointReduction: _patientVm!.patient!.jointReduction,
        coreMuscleTraining: _patientVm!.patient!.coreMuscleTraining,
        healthPromotion: _patientVm!.patient!.healthPromotion,
        GT: _patientVm!.patient!.GT,
        pad: _patientVm!.patient!.pad,
        padHigh: _patientVm!.patient!.padHigh,
        IC: _patientVm!.patient!.IC,
        ASIS: _patientVm!.patient!.ASIS,
        PSIS: _patientVm!.patient!.PSIS,
        flexion: _patientVm!.patient!.flexion,
        floor: _patientVm!.patient!.floor,
        gillet: _patientVm!.patient!.gillet,
        Extension: _patientVm!.patient!.Extension,
        rotation_right: _patientVm!.patient!.rotation_right,
        rotation_left: _patientVm!.patient!.rotation_left,
        side_left: _patientVm!.patient!.side_left,
        side_right: _patientVm!.patient!.side_right,
        side_knee_left: _patientVm!.patient!.side_knee_left,
        side_knee_right: _patientVm!.patient!.side_knee_right,
        supine_leg_length: _patientVm!.patient!.supine_leg_length,
        adductor: _patientVm!.patient!.adductor,
        inguinal: _patientVm!.patient!.inguinal,
        prone_leg_length: _patientVm!.patient!.prone_leg_length,
        add_ROM: _patientVm!.patient!.add_ROM,
        ext_rotator: _patientVm!.patient!.ext_rotator,
        PSIS_left: _patientVm!.patient!.PSIS_left,
        PSIS_right: _patientVm!.patient!.PSIS_right,
        ILA_left: _patientVm!.patient!.ILA_left,
        ILA_right: _patientVm!.patient!.ILA_right,
        Prop_left: _patientVm!.patient!.Prop_left,
        Prop_right: _patientVm!.patient!.Prop_right,
        lumbar_left: _patientVm!.patient!.lumbar_left,
        lumbar_right: _patientVm!.patient!.lumbar_right,
        supine_ilium_leg_length: _patientVm!.patient!.supine_ilium_leg_length,
        Long_sit: _patientVm!.patient!.Long_sit,
        SLR: _patientVm!.patient!.SLR,
        Patrick: _patientVm!.patient!.Patrick,
        Knee_flexor: _patientVm!.patient!.Knee_flexor,
        ASIS_umbilical: _patientVm!.patient!.ASIS_umbilical,
        IC_recheck: 0,
        flexion_recheck: 0,
        floor_recheck: '',
        gillet_recheck: 0,
        extension_recheck: _patientVm!.patient!.extension_recheck,
        rotation_left_recheck: _patientVm!.patient!.rotation_left_recheck,
        rotation_right_recheck: _patientVm!.patient!.rotation_right_recheck,
        side_left_recheck: _patientVm!.patient!.side_left_recheck,
        side_right_recheck: _patientVm!.patient!.side_right_recheck,
        side_knee_left_recheck: _patientVm!.patient!.side_knee_left_recheck,
        side_knee_right_recheck: _patientVm!.patient!.side_knee_right_recheck
    );
    _patientVm!.editPatient(patient);
  }

  //_gilletRecheck1
  void _gilletRecheck1OnTap() {
    setState(() {
      _gilletRecheck = 1;
    });
  }

  //_gilletRecheck2
  void _gilletRecheck2OnTap() {
    setState(() {
      _gilletRecheck = 2;
    });
  }

  //_gilletRecheck3
  void _gilletRecheck3OnTap() {
    setState(() {
      _gilletRecheck = 3;
    });
  }

}