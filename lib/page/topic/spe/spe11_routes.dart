import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:light_silver/database/database_helper.dart';
import 'package:light_silver/language/language_constants.dart';
import 'package:light_silver/models/patient.dart';
import 'package:light_silver/utils/shared_preference_util.dart';
import 'package:light_silver/utils/utils.dart';
import 'package:light_silver/widgets/button.dart';
import 'package:light_silver/widgets/left_button.dart';
import 'package:provider/provider.dart';

import '../../../app.dart';
import '../../../routes/route_name.dart';
import '../../../view_model/patient_vm.dart';
import '../../../widgets/title_widget.dart';

class SPe11Routes extends StatefulWidget {
  @override
  State createState() => _SPe11RoutesState();
}

class _SPe11RoutesState extends State<SPe11Routes> {
  PatientVm? _patientVm;

  final TextEditingController _floorResult = TextEditingController();
  final TextEditingController _sideKneeLeftResult = TextEditingController();
  final TextEditingController _sideKneeRightResult = TextEditingController();

  _onLayoutDone(_) {
    _dataInit(_patientVm!.patient);
  }

  void _dataInit(Patient? patient) {
    if (patient == null) {
      return;
    }
    log('取得 站姿結論(2): $patient');
    _floorResult.text = patient.floor == ''
        ? getTranslated(context, 'No_test')
        : patient.floor;
    _sideKneeLeftResult.text = patient.side_knee_left == ''
        ? getTranslated(context, 'No_test')
        : patient.side_knee_left;
    _sideKneeRightResult.text = patient.side_knee_right == ''
        ? getTranslated(context, 'No_test')
        : patient.side_knee_right;
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  Widget build(BuildContext context) {
    _patientVm = Provider.of<PatientVm>(context);
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Column(
          children: [
            Expanded(flex: 1, child: _titleBar()),
            Expanded(flex: 10, child: _bodyWidget())
          ],
        ),
      ),
    );
  }

  //最上層標題與按鈕
  Widget _titleBar() {
    return SpeTitleWidget(
      titleText: getTranslated(context, 'Standing_posture'),
      leftButton: const LeftButton(),
      rightButton: _rightTopButton(),
    );
  }

  //右側按鈕
  Widget _rightTopButton() {
    return InkWell(
      onTap: () => delegate.popUtil(),
      child: const Icon(
        Icons.home,
        color: Colors.white,
        size: 45,
      ),
    );
  }

  //主體
  Widget _bodyWidget() {
    return Stack(
      children: [
        Container(
          padding: const EdgeInsets.only(top: 10, bottom: 10),
          decoration: Style.homeBg,
          alignment: Alignment.center,
          child: Container(
            width: MediaQuery.of(context).size.width / 1.1,
            decoration: Style.centerBG,
            child: Column(
              children: [
                //主體內部
                _bodyTopWidget(),
                //題目
                _topicWidget(),
                //左或右
                _leftOrRightWidget(),
                //身體圖
                _bodyImageWidget(),
                const Spacer(),
                //按鈕
                _bottomButtonWidget(),
                const Spacer(),
              ],
            ),
          ),
        ),
      ],
    );
  }

  //主體內部
  Widget _bodyTopWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 20, left: 50, right: 50),
      child: Column(
        children: [
          FittedBox(
            fit: BoxFit.scaleDown,
            child: Text(
              getTranslated(
                  context, 'Evaluation_of_iliac_and_lumbar_spine_problems'),
              textScaleFactor: 1,
              style: const TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            height: 3,
            color: HexColor.fromHex('#4A991E'),
          )
        ],
      ),
    );
  }

  //題目
  Widget _topicWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 10, left: 25),
      child: Row(
        children: [
          FittedBox(
            fit: BoxFit.scaleDown,
            child: Text(
              '${getTranslated(context, 'Summary_of_the_problems_in_the_standing_posture')}(2)',
              textScaleFactor: 1,
              style: const TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            ),
          ),
        ],
      ),
    );
  }

  //左或右
  Widget _leftOrRightWidget() {
    return Container(
      height: 50,
      margin: const EdgeInsets.only(top: 10, left: 25, right: 25),
      child: Row(
        children: [
          Expanded(
              flex: 1,
              child: Container(
                alignment: Alignment.center,
                decoration: Style.speTextBG,
                child: FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    getTranslated(context, 'Left_side'),
                    textScaleFactor: 1,
                    style: const TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                ),
              )),
          const SizedBox(
            width: 10,
          ),
          Expanded(
              flex: 1,
              child: Container(
                alignment: Alignment.center,
                decoration: Style.speTextBG,
                child: FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    getTranslated(context, 'Right_side'),
                    textScaleFactor: 1,
                    style: const TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                ),
              ))
        ],
      ),
    );
  }

  //身體圖
  Widget _bodyImageWidget() {
    return Stack(
      children: [
        //文字
        Container(
          margin: const EdgeInsets.only(top: 5, left: 10, right: 10),
          child: Column(
            children: [
              //身體前驅
              Column(
                children: [
                  FittedBox(
                    fit: BoxFit.scaleDown,
                    child: Text(
                      getTranslated(context, 'Bend_forward'),
                      textScaleFactor: 1,
                      style: const TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      //矯正置中用
                      FittedBox(
                        fit: BoxFit.scaleDown,
                        child: Text(
                          getTranslated(context, 'Cm'),
                          textScaleFactor: 1,
                          style: const TextStyle(
                              fontSize: 20,
                              color: Colors.transparent,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                          margin: const EdgeInsets.all(8),
                          width: 70,
                          child: TextField(
                            readOnly: true,
                            textAlign: TextAlign.center,
                            style: const TextStyle(color: Colors.black),
                            decoration: const InputDecoration(
                              contentPadding: EdgeInsets.all(5),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black)),
                              filled: true,
                              fillColor: Colors.white,
                            ),
                            keyboardType: TextInputType.number,
                            textInputAction: TextInputAction.done,
                            controller: _floorResult,
                          )),
                      FittedBox(
                        fit: BoxFit.scaleDown,
                        child: Text(
                          getTranslated(context, 'Cm'),
                          textScaleFactor: 1,
                          style: const TextStyle(
                              fontSize: 16,
                              color: Colors.black,
                              fontWeight: FontWeight.bold),
                        ),
                      )
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  //左側彎
                  Column(
                    children: [
                      FittedBox(
                        fit: BoxFit.scaleDown,
                        child: Text(
                          getTranslated(context, 'Left_side_bend'),
                          textScaleFactor: 1,
                          style: const TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      FittedBox(
                        fit: BoxFit.scaleDown,
                        child: Text(
                          '(${getTranslated(context, 'Distance_from_fingertip_to_knee_side')})',
                          textScaleFactor: 1,
                          style: const TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            margin: const EdgeInsets.all(5),
                            width: 70,
                            child: TextField(
                              readOnly: true,
                              textAlign: TextAlign.center,
                              style: const TextStyle(color: Colors.black),
                              decoration: const InputDecoration(
                                  contentPadding: EdgeInsets.all(5),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Colors.black)),
                                  filled: true,
                                  fillColor: Colors.white),
                              keyboardType: TextInputType.number,
                              textInputAction: TextInputAction.done,
                              controller: _sideKneeLeftResult,
                            ),
                          ),
                          FittedBox(
                            fit: BoxFit.scaleDown,
                            child: Text(
                              getTranslated(context, 'Cm'),
                              textScaleFactor: 1,
                              style: const TextStyle(
                                  fontSize: 16,
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold),
                            ),
                          )
                        ],
                      ),
                      //左側彎XX
                      Container(
                        margin: const EdgeInsets.only(top: 10, left: 50),
                        child: Consumer<PatientVm>(builder: (context, vm, _){
                          return Row(
                            children: [
                              vm.patient != null &&
                                  (vm.patient!.side_left != 0 ||
                                      vm.patient!.side_left == 2)
                                  ? Image.asset(
                                'assets/images/pain_level.png',
                                width: 20,
                              )
                                  : Container(),
                              vm.patient != null && vm.patient!.side_left != 0
                                  ? Image.asset(
                                'assets/images/pain_level.png',
                                width: 20,
                              )
                                  : Container(),
                            ],
                          );
                        },),
                      ),
                    ],
                  ),
                  //身體前屈XX
                  Consumer<PatientVm>(builder: (context, vm, _){
                    return Row(
                      children: [
                        vm.patient != null &&
                            vm.patient!.floor != '' &&
                            int.parse(vm.patient!.floor) <= 5 &&
                            int.parse(vm.patient!.floor) != 100 &&
                            int.parse(vm.patient!.floor) != 0
                            ? Image.asset(
                          'assets/images/pain_level.png',
                          width: 20,
                        )
                            : Container(),
                        vm.patient != null &&
                            vm.patient!.floor != '' &&
                            int.parse(vm.patient!.floor) > 5 &&
                            int.parse(vm.patient!.floor) != 0 &&
                            int.parse(vm.patient!.floor) != 100
                            ? Image.asset(
                          'assets/images/pain_level.png',
                          width: 20,
                        )
                            : Container(),
                        vm.patient != null && vm.patient!.floor == ''
                            ? FittedBox(
                            fit: BoxFit.scaleDown,
                            child: Text(
                              getTranslated(context, 'default_value'),
                              textScaleFactor: 1,
                              style: const TextStyle(color: Colors.red),
                            ))
                            : Container(),
                      ],
                    );
                  }),
                  //右側彎
                  Column(
                    children: [
                      FittedBox(
                        fit: BoxFit.scaleDown,
                        child: Text(
                          getTranslated(context, 'Right_side_bend'),
                          textScaleFactor: 1,
                          style: const TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      FittedBox(
                        fit: BoxFit.scaleDown,
                        child: Text(
                          '(${getTranslated(context, 'Distance_from_fingertip_to_knee_side')})',
                          textScaleFactor: 1,
                          style: const TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            margin: const EdgeInsets.all(8),
                            width: 70,
                            child: TextField(
                              readOnly: true,
                              textAlign: TextAlign.center,
                              style: const TextStyle(color: Colors.black),
                              decoration: const InputDecoration(
                                  contentPadding: EdgeInsets.all(5),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Colors.black)),
                                  filled: true,
                                  fillColor: Colors.white),
                              keyboardType: TextInputType.number,
                              textInputAction: TextInputAction.done,
                              controller: _sideKneeRightResult,
                            ),
                          ),
                          FittedBox(
                            fit: BoxFit.scaleDown,
                            child: Text(
                              getTranslated(context, 'Cm'),
                              textScaleFactor: 1,
                              style: const TextStyle(
                                  fontSize: 16,
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold),
                            ),
                          )
                        ],
                      ),
                      //右側彎XX
                      Container(
                        margin: const EdgeInsets.only(top: 10, right: 50),
                        child: Consumer<PatientVm>(builder: (context, vm, _){
                          return Row(
                            children: [
                              vm.patient != null &&
                                  vm.patient!.side_right != 0 &&
                                  vm.patient!.side_right == 2
                                  ? Image.asset(
                                'assets/images/pain_level.png',
                                width: 20,
                              )
                                  : Container(),
                              vm.patient != null && vm.patient!.side_right != 0
                                  ? Image.asset(
                                'assets/images/pain_level.png',
                                width: 20,
                              )
                                  : Container(),
                            ],
                          );
                        },),
                      ),
                    ],
                  )
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Column(
                    children: [
                      //左旋轉XX
                      Consumer<PatientVm>(builder: (context, vm, _){
                        return Row(
                          children: [
                            vm.patient != null &&
                                vm.patient!.rotation_left != 0 &&
                                vm.patient!.rotation_left == 2
                                ? Image.asset(
                              'assets/images/pain_level.png',
                              width: 20,
                            )
                                : Container(),
                            vm.patient != null && vm.patient!.rotation_left != 0
                                ? Image.asset(
                              'assets/images/pain_level.png',
                              width: 20,
                            )
                                : Container(),
                          ],
                        );
                      }),
                      const SizedBox(
                        height: 10,
                      ),
                      Container(
                        alignment: Alignment.center,
                        child: FittedBox(
                          fit: BoxFit.scaleDown,
                          child: Text(
                            getTranslated(context, 'Rotate_left'),
                            textScaleFactor: 1,
                            style: const TextStyle(
                                fontWeight: FontWeight.bold,),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Column(
                    children: [
                      //右旋轉XX
                      Consumer<PatientVm>(builder: (context, vm, _){
                        return Row(
                          children: [
                            vm.patient != null &&
                                vm.patient!.rotation_right != 0 &&
                                vm.patient!.rotation_right == 2
                                ? Image.asset(
                              'assets/images/pain_level.png',
                              width: 20,
                            )
                                : Container(),
                            vm.patient != null && vm.patient!.rotation_right != 0
                                ? Image.asset(
                              'assets/images/pain_level.png',
                              width: 20,
                            )
                                : Container(),
                          ],
                        );
                      }),
                      const SizedBox(
                        height: 10,
                      ),
                      Container(
                        alignment: Alignment.center,
                        child: FittedBox(
                          fit: BoxFit.scaleDown,
                          child: Text(
                            getTranslated(context, 'Rotate_right'),
                            textScaleFactor: 1,
                            style: const TextStyle(
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
              Consumer<PatientVm>(builder: (context, vm, _){
                return Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    vm.patient != null &&
                        vm.patient!.Extension != 0 &&
                        vm.patient!.Extension == 2
                        ? Image.asset(
                      'assets/images/pain_level.png',
                      width: 20,
                    )
                        : Container(),
                    vm.patient != null && vm.patient!.Extension != 0
                        ? Image.asset(
                      'assets/images/pain_level.png',
                      width: 20,
                    )
                        : Container(),
                  ],
                );
              }),
              const SizedBox(
                height: 10,
              ),
              Container(
                alignment: Alignment.center,
                child: FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    getTranslated(context, 'Straighten_the_body'),
                    textScaleFactor: 1,
                    style: const TextStyle(
                        fontWeight: FontWeight.bold),
                  ),
                ),
              )
            ],
          ),
        ),
        //圖
        Positioned(
          bottom: 45,
          right: 20,
          left: 20,
          child: Image.asset(
            'assets/images/result_green.png',
            width: 330,
            height: 200,
            fit: BoxFit.fill,
          ),
        )
      ],
    );
  }

  //按鈕
  Widget _bottomButtonWidget() {
    return Container(
      margin: const EdgeInsets.only(right: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            margin: const EdgeInsets.only(left: 15),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Row(
                  children: [
                    Image.asset(
                      'assets/images/pain_level.png',
                      width: 20,
                    ),
                    FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Text(
                        ' : ${getTranslated(context, 'Slightly_restricted')}',
                        textScaleFactor: 1,
                        style: const TextStyle(fontWeight: FontWeight.bold),
                      ),
                    )
                  ],
                ),
                Row(
                  children: [
                    Row(
                      children: [
                        Image.asset(
                          'assets/images/pain_level.png',
                          width: 20,
                        ),
                        Image.asset(
                          'assets/images/pain_level.png',
                          width: 20,
                        ),
                      ],
                    ),
                    FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Text(
                        ' : ${getTranslated(context, 'Severely_restricted')}',
                        textScaleFactor: 1,
                        style: const TextStyle(fontWeight: FontWeight.bold),
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
          //下一步
          NextButton(
              click: _nextOnTap,
              buttonText: getTranslated(context, 'Next_step'),
              decoration: Style.speNextBG),
        ],
      ),
    );
  }

  //下一步
  void _nextOnTap() {
    delegate.push(name: RouteName.lbk02);
  }
}
