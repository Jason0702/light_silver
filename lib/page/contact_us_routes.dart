

import 'package:flutter/material.dart';
import 'package:light_silver/app.dart';
import 'package:light_silver/language/language_constants.dart';
import 'package:light_silver/utils/utils.dart';
import 'package:light_silver/view_model/company_info_vm.dart';
import 'package:light_silver/widgets/webview_widget.dart';
import 'package:provider/provider.dart';

class ContactUsRoutes extends StatefulWidget {
  @override
  State createState() => _ContactUsRoutesState();
}

class _ContactUsRoutesState extends State<ContactUsRoutes> {
  CompanyInfoVm? _companyInfoVm;
  //漸層背景
  final _homeBg = const BoxDecoration(
      image: DecorationImage(
          image: AssetImage('assets/images/home_bg.png'), fit: BoxFit.fill));

  final _titleBarBG = BoxDecoration(
      gradient: LinearGradient(
          colors: [HexColor.fromHex('#0067B4'), HexColor.fromHex('#003457')],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter));

  //中間背景
  final _centerBG = const BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.all(Radius.circular(10)),
      boxShadow: [
        BoxShadow(
            color: Colors.black54,
            offset: Offset(5.0, 5.0),
            blurRadius: 10.0,
            spreadRadius: 2.0)
      ]);

  @override
  Widget build(BuildContext context) {
    _companyInfoVm = Provider.of<CompanyInfoVm>(context);
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Column(
          children: [
            Expanded(flex: 1, child: _titleBar()),
            Expanded(flex: 10, child: _bodyWidget()),
          ],
        ),
      ),
    );
  }

  //最上層標題與按鈕
  Widget _titleBar() {
    return Container(
      decoration: _titleBarBG,
      child: Row(
        children: [
          Expanded(flex: 1, child: _leftTopButton()),
          Expanded(
              flex: 5,
              child: Container(
                alignment: Alignment.center,
                child: FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    getTranslated(context, 'Contact_us'),
                    textScaleFactor: 1,
                    style: const TextStyle(fontSize: 25, color: Colors.white),
                  ),
                ),
              )),
          const Expanded(flex: 1, child: SizedBox(width: 10))
        ],
      ),
    );
  }

  //左側按鈕
  Widget _leftTopButton() {
    return InkWell(
      onTap: () => delegate.popRoute(),
      child: const Icon(
        Icons.arrow_back_rounded,
        color: Colors.white,
        size: 45,
      ),
    );
  }

  //主體
  Widget _bodyWidget() {
    return Container(
      decoration: _homeBg,
      alignment: Alignment.center,
      child: Container(
        width: MediaQuery.of(context).size.width / 1.1,
        height: MediaQuery.of(context).size.height / 1.3,
        decoration: _centerBG,
        child: Column(
          children: [
            const SizedBox(
              height: 30,
            ),
            Container(
              alignment: Alignment.center,
              child: Image(
                image: const AssetImage('assets/images/logo.png'),
                width: MediaQuery.of(context).size.width / 2.5,
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            //公司名稱與統編
            if (_companyInfoVm!.companyInfoModel != null)
              Container(
                margin: const EdgeInsets.only(left: 23),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Icon(
                      Icons.apartment_rounded,
                      color: HexColor.fromHex('#1E2C39'),
                      size: 30,
                    ),
                    const SizedBox(
                      width: 15,
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          _companyInfoVm!.companyInfoModel?.name ?? ' ',
                          textScaleFactor: 1,
                          style: TextStyle(
                              color: HexColor.fromHex('#1E2C39'),
                              fontSize: 20,
                              fontWeight: FontWeight.bold),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Text(
                          _companyInfoVm!.companyInfoModel == null
                              ? ' '
                              : '${_companyInfoVm!.companyInfoModel?.subName} \n統編 ${_companyInfoVm!.companyInfoModel?.taxIdNumber}',
                          textScaleFactor: 1,
                          style: TextStyle(
                              color: HexColor.fromHex('#1E2C39'),
                              fontSize: 20,
                              fontWeight: FontWeight.bold),
                        )
                      ],
                    )
                  ],
                ),
              ),
            const SizedBox(
              height: 10,
            ),
            //公司電話
            if (_companyInfoVm!.companyInfoModel != null)
              Container(
                margin: const EdgeInsets.only(left: 23),
                child: Row(
                  children: [
                    Icon(
                      Icons.phone_in_talk,
                      color: HexColor.fromHex('#1E2C39'),
                      size: 30,
                    ),
                    const SizedBox(
                      width: 15,
                    ),
                    Text(
                      _companyInfoVm!.companyInfoModel?.phone ?? ' ',
                      textScaleFactor: 1,
                      style: const TextStyle(
                          color: Colors.blue,
                          decoration: TextDecoration.underline,
                          fontSize: 20),
                    )
                  ],
                ),
              ),
            const SizedBox(
              height: 10,
            ),
            //公司Mail
            if (_companyInfoVm!.companyInfoModel != null)
              Container(
                margin: const EdgeInsets.only(left: 23),
                child: Row(
                  children: [
                    Icon(
                      Icons.mail,
                      color: HexColor.fromHex('#1E2C39'),
                      size: 30,
                    ),
                    const SizedBox(
                      width: 15,
                    ),
                    Text(
                      _companyInfoVm!.companyInfoModel?.email ?? ' ',
                      textScaleFactor: 1,
                      style: const TextStyle(
                          color: Colors.blue,
                          decoration: TextDecoration.underline,
                          fontSize: 20),
                    )
                  ],
                ),
              ),
            const SizedBox(
              height: 10,
            ),
            //公司地址
            if (_companyInfoVm!.companyInfoModel != null)
              Container(
                margin: const EdgeInsets.only(left: 23),
                child: Row(
                  children: [
                    Image(
                      image: const AssetImage('assets/images/pin.png'),
                      color: HexColor.fromHex('#1E2C39'),
                      width: 30,
                    ),
                    const SizedBox(
                      width: 15,
                    ),
                    Text(
                      _companyInfoVm!.companyInfoModel?.address ?? ' ',
                      textScaleFactor: 1,
                      style: TextStyle(
                          color: HexColor.fromHex('#1E2C39'),
                          fontSize: 20,
                          fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ),
            const SizedBox(
              height: 10,
            ),
            //公司網頁
            if (_companyInfoVm!.companyInfoModel != null)
              Container(
                margin: const EdgeInsets.only(left: 23),
                child: Row(
                  children: [
                    Image(
                      image: const AssetImage('assets/images/url.png'),
                      color: HexColor.fromHex('#1E2C39'),
                      width: 30,
                    ),
                    const SizedBox(
                      width: 15,
                    ),
                    Text(
                      _companyInfoVm!.companyInfoModel?.webSite ?? ' ',
                      textScaleFactor: 1,
                      style: const TextStyle(
                          color: Colors.blue,
                          decoration: TextDecoration.underline,
                          fontSize: 20),
                    )
                  ],
                ),
              ),
            const SizedBox(
              height: 10,
            ),
            //FB LINE IG
            /*Expanded(
                child: Container(
              margin: const EdgeInsets.only(left: 10, right: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  InkWell(
                    onTap: _fBButtonOnTab,
                    child: const Image(
                      image: AssetImage('assets/images/fb.png'),
                      width: 50,
                    ),
                  ),
                  InkWell(
                    onTap: _lineButtonOnTab,
                    child: const Image(
                      image: AssetImage('assets/images/line.png'),
                      width: 50,
                    ),
                  ),
                  InkWell(
                    onTap: _iGButtonOnTab,
                    child: const Image(
                      image: AssetImage('assets/images/instagram.png'),
                      width: 50,
                    ),
                  )
                ],
              ),
            )),*/
            const SizedBox(
              height: 10,
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _fBButtonOnTab() async {
    if (_companyInfoVm!.companyInfoModel == null) {
      return;
    }
    bool? result = await showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return Container();
        },
        barrierColor: Colors.black54,
        barrierDismissible: false,
        transitionDuration: const Duration(milliseconds: 100),
        transitionBuilder: (context, anim1, anim2, child) {
          return Transform.scale(
            scale: anim1.value,
            child: WebViewWidget(
              title: 'FaceBook',
              url: _companyInfoVm!.companyInfoModel!.facebook,
            ),
          );
        });
  }

  Future<void> _lineButtonOnTab() async {
    if (_companyInfoVm!.companyInfoModel == null) {
      return;
    }
    bool? result = await showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return Container();
        },
        barrierColor: Colors.black54,
        barrierDismissible: false,
        transitionDuration: const Duration(milliseconds: 100),
        transitionBuilder: (context, anim1, anim2, child) {
          return Transform.scale(
            scale: anim1.value,
            child: WebViewWidget(
              title: 'Line',
              url: _companyInfoVm!.companyInfoModel!.line,
            ),
          );
        });
  }

  Future<void> _iGButtonOnTab() async {
    if (_companyInfoVm!.companyInfoModel == null) {
      return;
    }
    bool? result = await showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return Container();
        },
        barrierColor: Colors.black54,
        barrierDismissible: false,
        transitionDuration: const Duration(milliseconds: 100),
        transitionBuilder: (context, anim1, anim2, child) {
          return Transform.scale(
            scale: anim1.value,
            child: WebViewWidget(
              title: 'Instagram',
              url: _companyInfoVm!.companyInfoModel!.instagram,
            ),
          );
        });
  }
}
