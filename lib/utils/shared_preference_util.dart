//本地儲存工具
import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferenceUtil{
  static const String PATIENT_NAME = "patient_name";
  static const String ACCOUNT = "account";
  static const String PASSWORD = "password";
  static const String COMPANY_NAME = "company_name";
  static const String OPERATOR = "operator";
  static const String COMPANY_ADDRESS = "company_address";
  static const String COMPANY_PHONE = "company_phone";
  static const String COMPANY_LOGO = "company_logo";

  //region病人名稱
  //刪除
  void delPatientName() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.remove(PATIENT_NAME);
  }
  //保存
  void savePatientName(String name) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString(PATIENT_NAME, name);
  }
  //讀取
  Future<String> getPatientName()async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString(PATIENT_NAME) ?? "";
  }
  //endregion

  //region帳號
  //保存
  void saveAccount(String account) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString(ACCOUNT, account);
  }
  //刪除
  void delAccount() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.remove(ACCOUNT);
  }
  //讀取
  Future<String> getAccount() async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString(ACCOUNT) ?? "";
  }
  //endregion
  
  //region 密碼
  void savePassword(String password) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString(PASSWORD, password);
  }
  void delPassword() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.remove(PASSWORD);
  }
  Future<String> getPassword() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString(PASSWORD) ?? "";
  }
  //endregion

  //region公司名稱
  //保存
  void saveCompanyName(String name) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString(COMPANY_NAME, name);
  }
  //刪除
  void delCompanyName() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.remove(COMPANY_NAME);
  }
  //讀取
  Future<String> getCompanyName() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString(COMPANY_NAME) ?? "";
  }
  //endregion

  //region 操作人員
  //保存
  void saveOperator(String operator) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString(OPERATOR, operator);
  }
  //刪除
  void delOperator() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.remove(OPERATOR);
  }
  //讀取
  Future<String> getOperator() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString(OPERATOR) ?? '';
  }
  //endregion

  //region 公司地址
  //保存
  void saveCompanyAddress(String address) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString(COMPANY_ADDRESS, address);
  }
  //刪除
  void delCompanyAddress() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.remove(COMPANY_ADDRESS);
  }
  //讀取
  Future<String> getCompanyAddress() async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString(COMPANY_ADDRESS) ?? '';
  }
  //endregion

  //region 公司電話
  //保存
  void saveCompanyPhone(String phone) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString(COMPANY_PHONE, phone);
  }
  //刪除
  void delCompanyPhone() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.remove(COMPANY_PHONE);
  }
  //讀取
  Future<String> getCompanyPhone() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString(COMPANY_PHONE) ?? '';
  }
  //endregion

  //region 公司LOGO
  //保存
  void saveCompanyLOGO(String logo) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString(COMPANY_LOGO, logo);
  }
  //刪除
  void delCompanyLOGO() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.remove(COMPANY_LOGO);
  }
  //讀取
  Future<String> getCompanyLOGO() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString(COMPANY_LOGO) ?? '';
  }
  //endregion

  Future<String> getAccessToken() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString('') ?? '';
  }
}