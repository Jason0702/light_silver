import 'package:flutter/material.dart';

///日誌輸出
class LogUtil{
  static const _TAG_DEFAULT = "###WriteOff###";

  //是否 debug
  static bool debug = false; //是否是debug模式, true: log v 不輸出.

  static String tagDefault = _TAG_DEFAULT;

  static void init({bool isDebug = false, String tag = _TAG_DEFAULT}){
    debug = isDebug;
    tag = tag;
  }

  static void e(Object object, {String? tag}){
    _printLog(tag!, '  e  ', object);
  }
  static void v(Object object, {String? tag}){
    if(debug){
      _printLog(tag!, '  v  ', object);
    }
  }
  static void _printLog(String tag, String stag, Object object) {
    StringBuffer stringBuffer = StringBuffer();
    stringBuffer.write((tag.isEmpty) ? tagDefault : tag);
    stringBuffer.write(stag);
    stringBuffer.write(object);
    debugPrint(stringBuffer.toString());
  }
}