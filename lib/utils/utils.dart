import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../models/patient.dart';
import 'app_image.dart';

extension HexColor on Color{
  static Color fromHex(String hexString){
    final buffer = StringBuffer();
    if(hexString .length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }
  String toHex({bool leadingHashSign = true}) => '${leadingHashSign ? '#' : ''}'
      '${alpha.toRadixString(16).padLeft(2,'0')}'
      '${red.toRadixString(16).padLeft(2,'0')}'
      '${green.toRadixString(16).padLeft(2,'0')}'
      '${blue.toRadixString(16).padLeft(2,'0')}';
}
class Utils{
  //病例打勾判斷
  static bool getIsMedicalHistoryStatus(Patient? patient){
    if(patient == null){
      return false;
    }
    if(patient.name.isNotEmpty &&
        patient.gender.isNotEmpty &&
        !patient.age.isNaN &&
        patient.phone.isNotEmpty &&
        patient.problem.isNotEmpty) {
      return true;
    }
    return false;
  }

  //站姿打勾判斷
  static bool getIsStandingPosture1Status(Patient? patient){
    if(patient == null){
      return false;
    }
    if (patient.GT != 0 &&
        patient.IC != 0 &&
        patient.ASIS != 0 &&
        patient.PSIS != 0 &&
        !patient.flexion.isNaN &&
        patient.gillet != 0 &&
        !patient.Extension.isNaN &&
        !patient.rotation_left.isNaN &&
        !patient.rotation_right.isNaN &&
        !patient.side_left.isNaN &&
        !patient.side_right.isNaN) {
      return true;
    }
    return false;
  }

  //仰躺打勾判斷
  static bool getIsLieOnYourBack1Status(Patient? patient){
    if(patient == null){
      return false;
    }
    if (patient.supine_leg_length != 0 &&
        patient.adductor != 0 &&
        patient.inguinal != 0) {
      return true;
    }
    return false;
  }

  //俯臥_髂骨打勾判斷
  static bool getIsProne1Status(Patient? patient){
    if(patient == null){
      return false;
    }
    if (patient.gillet != 0 &&
        patient.IC != 0 &&
        patient.ASIS != 0 &&
        patient.PSIS != 0) {
      return true;
    }
    return false;
  }

  //俯臥_薦椎 打勾判斷
  static bool getIsProne2Status(Patient? patient){
    if(patient == null){
      return false;
    }
    if (patient.prone_leg_length != 0 &&
        patient.add_ROM != 0 &&
        patient.ext_rotator != 0 &&
        patient.PSIS_left != 0 &&
        patient.PSIS_right != 0 &&
        patient.ILA_left != 0 &&
        patient.ILA_right != 0) {
      return true;
    }
    return false;
  }

  //俯臥_腰椎 打勾判斷
  static bool getIsProne3Status(Patient? patient){
    if(patient == null){
      return false;
    }
    if (patient.lumbar_left != 0 &&
        patient.lumbar_right != 0) {
      return true;
    }
    return false;
  }

  //側躺
  static bool getIsLieDownStatus(Patient? patient){
    if(patient == null){
      return false;
    }
    if (patient.lumbar_left != 0 &&
        patient.lumbar_right != 0) {
      return true;
    }
    return false;
  }

  //仰躺
  static bool getIsLieOnYourBack2Status(Patient? patient){
    if(patient == null){
      return false;
    }
    if (patient.supine_ilium_leg_length != 0 &&
        patient.SLR != 0 &&
        patient.Patrick != 0 &&
        patient.Knee_flexor != 0 &&
        patient.ASIS_umbilical != 0) {
      return true;
    }
    return false;
  }

  //站姿
  static bool getIsStandingPosture2Status(Patient? patient){
    if(patient == null){
      return false;
    }
    if (patient.extension_recheck != 3 &&
        patient.rotation_left_recheck != 3 &&
        patient.rotation_right_recheck != 3 &&
        patient.side_left_recheck != 3 &&
        patient.side_right_recheck != 3 &&
        patient.side_knee_left_recheck != '100' &&
        patient.side_knee_right_recheck != '100') {
      return true;
    }
    return false;
  }

  //預覽
  static bool getIsPreview(Patient? patient){
    if(patient == null){
      return false;
    }
    if(getIsMedicalHistoryStatus(patient) &&
        getIsStandingPosture1Status(patient) &&
        getIsLieOnYourBack1Status(patient) &&
        getIsProne1Status(patient) &&
        getIsProne2Status(patient) &&
        getIsProne3Status(patient) &&
        getIsLieDownStatus(patient) &&
        getIsLieOnYourBack2Status(patient) &&
        getIsStandingPosture2Status(patient)
    ){
      return true;
    }
    return false;
  }
}

class Style{
  static const homeBg = BoxDecoration(
      image: DecorationImage(
          image: AssetImage(AppImage.homeBg), fit: BoxFit.fill));

  static const centerBG = BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.all(Radius.circular(10)),
      boxShadow: [
        BoxShadow(
            color: Colors.black54,
            offset: Offset(5.0, 5.0),
            blurRadius: 10.0,
            spreadRadius: 2.0)
      ]);

  static final imageButtonBG = BoxDecoration(
      borderRadius: const BorderRadius.only(
          topRight: Radius.circular(10.0), bottomRight: Radius.circular(10.0)),
      gradient: LinearGradient(colors: [
        HexColor.fromHex('#0067B4'),
        HexColor.fromHex('#003457'),
      ], begin: Alignment.topCenter, end: Alignment.bottomCenter));

  static final medicalHistoryBG = BoxDecoration(
      borderRadius: BorderRadius.circular(10),
      gradient: LinearGradient(colors: [
        HexColor.fromHex('#CB2020'),
        HexColor.fromHex('#DC6363'),
        HexColor.fromHex('#CB2020'),
      ], begin: Alignment.topCenter, end: Alignment.bottomCenter));

  static final spsNextBG = BoxDecoration(
      borderRadius: BorderRadius.circular(10),
      gradient: LinearGradient(colors: [
        HexColor.fromHex('#1DAE85'),
        HexColor.fromHex('#71C2AB'),
        HexColor.fromHex('#1DAE85'),
      ], begin: Alignment.topCenter, end: Alignment.bottomCenter));

  static final speNextBG = BoxDecoration(
      borderRadius: BorderRadius.circular(10),
      gradient: LinearGradient(colors: [
        HexColor.fromHex('#3D9208'),
        HexColor.fromHex('#6DAC55'),
        HexColor.fromHex('#1C7B00'),
      ], begin: Alignment.topCenter, end: Alignment.bottomCenter));

  static final ldNextBG = BoxDecoration(
      borderRadius: BorderRadius.circular(10),
      gradient: LinearGradient(colors: [
        HexColor.fromHex('#4809AB'),
        HexColor.fromHex('#7E66AA'),
        HexColor.fromHex('#4B18A3'),
      ], begin: Alignment.topCenter, end: Alignment.bottomCenter));

  static final lbkNextBG = BoxDecoration(
      borderRadius: BorderRadius.circular(10),
      gradient: LinearGradient(colors: [
        HexColor.fromHex('#085CAF'),
        HexColor.fromHex('#618FBC'),
        HexColor.fromHex('#0057AE'),
      ], begin: Alignment.topCenter, end: Alignment.bottomCenter));

  static final lbkTenNextBG = BoxDecoration(
      borderRadius: BorderRadius.circular(10),
      gradient: LinearGradient(colors: [
        HexColor.fromHex('#D45B11'),
        HexColor.fromHex('#DC9F79'),
        HexColor.fromHex('#DE530B'),
      ], begin: Alignment.topCenter, end: Alignment.bottomCenter));

  static final speTitleBarBG = BoxDecoration(
      gradient: LinearGradient(colors: [
        HexColor.fromHex('#3D9208'),
        HexColor.fromHex('#6DAC55'),
        HexColor.fromHex('#1C7B00'),
      ], begin: Alignment.topCenter, end: Alignment.bottomCenter));

  static final speTextBG = BoxDecoration(
      borderRadius: const BorderRadius.all(Radius.circular(10)),
      gradient: LinearGradient(colors: [
        HexColor.fromHex('#3D9208'),
        HexColor.fromHex('#6DAC55'),
        HexColor.fromHex('#1C7B00'),
      ], begin: Alignment.topCenter, end: Alignment.bottomCenter));

  static final ldTitleBarBG = BoxDecoration(
      gradient: LinearGradient(colors: [
        HexColor.fromHex('#4809AB'),
        HexColor.fromHex('#7E66AA'),
        HexColor.fromHex('#4B18A3'),
      ], begin: Alignment.topCenter, end: Alignment.bottomCenter));

  static final lbkTitleBarBG = BoxDecoration(
      gradient: LinearGradient(colors: [
        HexColor.fromHex('#085CAF'),
        HexColor.fromHex('#618FBC'),
        HexColor.fromHex('#0057AE'),
      ], begin: Alignment.topCenter, end: Alignment.bottomCenter));

  static final lbkTenTitleBarBG = BoxDecoration(
      gradient: LinearGradient(colors: [
        HexColor.fromHex('#D45B11'),
        HexColor.fromHex('#DC9F79'),
        HexColor.fromHex('#DE530B'),
      ], begin: Alignment.topCenter, end: Alignment.bottomCenter));

  static final lbkTextBG = BoxDecoration(
      borderRadius: BorderRadius.circular(10),
      gradient: LinearGradient(colors: [
        HexColor.fromHex('#085CAF'),
        HexColor.fromHex('#618FBC'),
        HexColor.fromHex('#0057AE'),
      ], begin: Alignment.topCenter, end: Alignment.bottomCenter));

  static final lbkTenTextBG = BoxDecoration(
      borderRadius: const BorderRadius.all(Radius.circular(10)),
      gradient: LinearGradient(colors: [
        HexColor.fromHex('#D45B11'),
        HexColor.fromHex('#DC9F79'),
        HexColor.fromHex('#DE530B'),
      ], begin: Alignment.topCenter, end: Alignment.bottomCenter));

  static final spsTitleBarBG = BoxDecoration(
      gradient: LinearGradient(colors: [
        HexColor.fromHex('#1DAE85'),
        HexColor.fromHex('#71C2AB'),
        HexColor.fromHex('#1DAE85'),
      ], begin: Alignment.topCenter, end: Alignment.bottomCenter));

  static final spsTextBG = BoxDecoration(
      borderRadius: const BorderRadius.all(Radius.circular(10)),
      gradient: LinearGradient(colors: [
        HexColor.fromHex('#1DAE85'),
        HexColor.fromHex('#71C2AB'),
        HexColor.fromHex('#1DAE85'),
      ], begin: Alignment.topCenter, end: Alignment.bottomCenter));

  static final pvTitleBarBG = BoxDecoration(
      gradient: LinearGradient(colors: [
        HexColor.fromHex('#0067B4'),
        HexColor.fromHex('#003457'),
      ], begin: Alignment.topCenter, end: Alignment.bottomCenter));
}
