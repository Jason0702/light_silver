//Token
import 'dart:convert';

int id = 0;
String accessToken = '';
String refreshToken = '';

//本地
//String api = 'http://192.168.1.117:5017/api/v1';
String api = 'https://jg-hp.com/gin-kuanyin-api/api/v1';

String auth = base64Encode(utf8.encode('guest:guest'));

//String imageUrlOffline = 'http://192.168.1.117:5017';
String imageUrlOffline = 'https://jg-hp.com/gin-kuanyin-api';
String imageUrlPublic = '/public/';
String imageUrlPrivate = '/files/';

//註冊發送驗證
String postSingUp = '/members-signup';
//驗證碼確認
String putVreify = '/pending-members-verify/';
//登入
String postAuth = '/members/auth';
//會員資料
String getMember = '/members/';
//用戶資料
String getUserData = '/users/';
String putNewPassword = '/members';
//公司資訊
String getCompanyInfo = '/guest/company-info/1';

//操作項目
String getOperationItems = '/guest/operation-items';

class Api{
  static const String notification = '';
  static const String notificationUnreadCount = '';
  static const String notificationReadAll = '';
  static const String carousel = '';
  static const String repairType = '';
  static const String repairPlace = '';
  static const String repairFloor = '';
  static const String repairRecord = '';
  static const String companyInfo = '';
  static const String guestStatus = '';
}