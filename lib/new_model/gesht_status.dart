class GuestStatus{
  int? guestStatus;

  GuestStatus.fromJson(Map<String, dynamic> json){
    guestStatus = json['guest_status'];
  }

  Map<String, dynamic> toJson() => {
    'guestStatus': guestStatus
  };
}