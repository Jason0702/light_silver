class CompanyInfoModel{
  String? facebook;
  String? ig;

  CompanyInfoModel.fromJson(Map<String, dynamic>json){
    facebook = json['facebook'] ?? '';
    ig = json['ig'] ?? '';
  }

  Map<String, dynamic> toJson() => {
    'facebook':facebook,
    'ig':ig
  };
}