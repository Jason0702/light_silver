class MaintenanceTimeModel{
  int? id;
  String? period;
  bool? isCheck;

  MaintenanceTimeModel({
    required this.id,
    required this.period,
    required this.isCheck,
  });

  Map<String, dynamic> toJson() => {
    'id': id,
    'period': period,
    'isCheck': isCheck
  };
}