class NoticeModel{
  int? id;
  int? membersId;
  String? title;
  int? type;
  String? body;
  String? success;
  int? status;
  String? createdAt;

  NoticeModel.fromJson(Map<String, dynamic> json){
    id = json['id'];
    membersId = json['members_id'];
    title = json['title'];
    type = json['type'];
    body = json['body'];
    success = json['success'];
    status = json['status'];
    createdAt = json['created_at'];
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'membersId': membersId,
    'title': title,
    'type': type,
    'body': body,
    'success': success,
    'status': status,
    'createdAt': createdAt
  };
}