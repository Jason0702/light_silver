class CarouselModel{
  int? id;
  String? pic;
  int? sort;
  int? status;
  String? createdAt;
  String? updatedAt;

  CarouselModel.fromJson(Map<String, dynamic> json){
    id = json['id'];
    pic = json['pic'];
    sort = json['sort'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() => {
    'id':id,
    'pic':pic,
    'sort':sort,
    'status':status,
    'createdAt':createdAt,
    'updatedAt':updatedAt
  };
}