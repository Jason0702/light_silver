import 'dart:convert';

class RepairTypeModel{
  int? id;
  String? name;
  int? sort;
  int? status;

  RepairTypeModel.fromJson(Map<String, dynamic> json){
    id = json['id'];
    name = json['name'];
    sort = json['sort'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'name': name,
    'sort': sort,
    'status': status
  };
}

class RepairPlaceModel{
  int? id;
  String? name;
  int? sort;
  int? status;

  RepairPlaceModel.fromJson(Map<String, dynamic> json){
    id = json['id'];
    name = json['name'];
    sort = json['sort'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'name': name,
    'sort': sort,
    'status': status
  };
}

class RepairFloorModel{
  int? id;
  String? name;
  int? sort;
  int? status;

  RepairFloorModel.fromJson(Map<String, dynamic> json){
    id = json['id'];
    name = json['name'];
    sort = json['sort'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'name': name,
    'sort': sort,
    'status': status
  };
}

class AddRepairRecordDTO{
  int membersId;
  String buildName;
  String contactTime;
  List<RepairRecordItem> repairRecordItem;

  AddRepairRecordDTO({
    required this.membersId,
    required this.buildName,
    required this.contactTime,
    required this.repairRecordItem
  });

  Map<String, dynamic> toJson() => {
    'members_id': membersId,
    'build_name': buildName,
    'contact_time': contactTime,
    'repair_record_item': repairRecordItem
  };
}

class RepairRecordItem{
  String? repairTypeName;
  String? position;
  String? pic;
  String? description;

  RepairRecordItem({
    required this.repairTypeName,
    required this.position,
    required this.pic,
    required this.description
  });

  Map<String, dynamic> toJson() => {
    'repair_type_name': repairTypeName,
    'position': position,
    'pic': pic,
    'description': description
  };
}
/*
{
  id: 41,
  members_id: 34,                  //會員ID
  build_name: 清閤苑Ⅲ,             // 建案名稱
  order_number: 202208011,         // 訂單號碼
  contact_time: [                  // 聯絡時段
    "星期四 下午 (13:00~17:00)",
    "星期五 上午 (09:00~12:00)",
    "星期五 下午 (13:00~17:00)"
    ],
  survey_employee_id: 0,
  survey_pic: null,               // 勘查照片
  survey_content: null,           // 勘查狀況
  statement_file: null,           // 維修聲明書
  quotation_file: null,           // 報價單檔案
  is_statement_sign: 0,           // 聲明書簽核狀態
  is_quotation_sign: 0,           // 報價單簽核狀態
  show_quotation: 0,          // 是否保固外 (為1才顯示報價單)
  construction_date: null,        // 施工日期
  working_hours_estimate: null,   // 預計工時
  working_hours_actual: null,     // 實際工時
  type_of_work: null,             // 工種
  working_status: 0,              // 派工狀態 0:安排中 1:確認派工
  working_remark: null,           // 派工備註
  construction_employee_id: 0,
  pre_construction_pic: null,     // 施工前照片
  under_construction_pic: null,   // 施工中照片
  after_construction_pic: null,   // 施工後照片
  satisfaction_score: null,       // 滿意度分數
  satisfaction_remark: null,      // 滿意度意見
  process_step: 0,                // 流程步驟 0~4
  status: 0,                      // 報修狀態
  created_at: 2022-08-08 11:22:28,
  updated_at: 2022-08-08 11:22:28,
  status_text: 安排中,
  repair_type_names: []           // 報修項目名稱
  repair_record_item: [
    {
      id: 34,
      repair_record_id: 41,
      repair_type_name: 門窗,
      position: ["1F-前陽台"],
      pic: ["62f0816bb8355.png"],
      description: 2,
      created_at: 2022-08-08 11:22:28,
      updated_at: 2022-08-08 11:22:28
      }
    ]
  }
 */
class RepairRecordModel{
  int? id;
  int? membersId;
  String? buildName;
  String? orderNumber;
  List<String>? contactTime;
  int? surveyEmployeeId;
  List<String> surveyPic = [];
  String? surveyContent;
  String? statementFile;
  String? quotationFile;
  int? isStatementSign;
  int? isQuotationSign;
  int? showQuotation;
  String? constructionDate;
  String? workingHoursEstimate;
  String? workingHoursActual;
  String? typeOfWork;
  int? workingStatus;
  String? workingRemark;
  int? constructionEmployeeId;
  List<String> preConstructionPic = [];
  List<String> underConstructionPic = [];
  List<String> afterConstructionPic = [];
  String? satisfactionScore;
  String? satisfactionRemark;
  int? processStep;
  int? status;
  String? createdAt;
  String? updatedAt;
  String? closingDate;
  String? statusText;
  List<String> repairTypeNames = [];
  List<RepairRecordItemModel> repairRecordItem = [];

  RepairRecordModel.fromJson(Map<String, dynamic> response){
    id = response['id'];
    membersId = response['members_id'];
    buildName = response['build_name'];
    orderNumber = response['order_number'];
    contactTime = List<String>.from(json.decode(response['contact_time']));
    surveyEmployeeId = response['survey_employee_id'];
    surveyPic = response['survey_pic'] != null
        ? List<String>.from(json.decode(response['survey_pic']))
        : [''];
    surveyContent = response['survey_content'] ?? '';
    statementFile = response['statement_file'] ?? '';
    quotationFile = response['quotation_file'] ?? '';
    isStatementSign = response['is_statement_sign'];
    isQuotationSign = response['is_quotation_sign'];
    showQuotation = response['show_quotation'];
    constructionDate = response['construction_date'];
    workingHoursEstimate = response['working_hours_estimate'];
    workingHoursActual = response['working_hours_actual'];
    typeOfWork = response['type_of_work'];
    workingStatus = response['working_status'];
    workingRemark = response['working_remark'];
    constructionEmployeeId = response['construction_employee_id'];
    preConstructionPic = response['pre_construction_pic'] != null
        ? List<String>.from(json.decode(response['pre_construction_pic']))
        : [''];
    underConstructionPic = response['under_construction_pic'] != null
        ? List<String>.from(json.decode(response['under_construction_pic']))
        : [''];
    afterConstructionPic = response['after_construction_pic'] != null
        ? List<String>.from(json.decode(response['after_construction_pic']))
        : [''];
    satisfactionScore = response['satisfaction_score'];
    satisfactionRemark = response['satisfaction_remark'];
    processStep = response['process_step'];
    status = response['status'];
    createdAt = response['created_at'];
    updatedAt = response['updated_at'];
    closingDate = response['closing_date'];
    statusText = response['status_text'];
    repairTypeNames = List<String>.from(json.decode(json.encode(response['repair_type_names'])));
    List _list = json.decode(json.encode(response['repair_record_item']));
    repairRecordItem = _list.map((e) => RepairRecordItemModel.fromJson(e)).toList();
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'membersId': membersId,
    'buildName': buildName,
    'orderNumber': orderNumber,
    'contactTime': contactTime,
    'surveyEmployeeId': surveyEmployeeId,
    'surveyPic': surveyPic,
    'surveyContent': surveyContent,
    'statementFile': statementFile,
    'quotationFile': quotationFile,
    'isStatementSign': isStatementSign,
    'isQuotationSign': isQuotationSign,
    'showQuotation': showQuotation,
    'constructionDate': constructionDate,
    'workingHoursEstimate': workingHoursEstimate,
    'workingHoursActual': workingHoursActual,
    'typeOfWork': typeOfWork,
    'workingStatus': workingStatus,
    'workingRemark': workingRemark,
    'constructionEmployeeId': constructionEmployeeId,
    'preConstructionPic': preConstructionPic,
    'underConstructionPic': underConstructionPic,
    'afterConstructionPic': afterConstructionPic,
    'satisfactionScore': satisfactionScore,
    'satisfactionRemark': satisfactionRemark,
    'processStep': processStep,
    'status': status,
    'createdAt': createdAt,
    'updatedAt': updatedAt,
    'statusText': statusText,
    'repairTypeNames': repairTypeNames,
    'repairRecordItem': repairRecordItem,
  };
}

class RepairRecordItemModel{
  int? id;
  int? repairRecordId;
  String? repairTypeName;
  List<String>? position;
  List<String>? pic;
  String? description;
  String? createdAt;
  String? updatedAt;

  RepairRecordItemModel.fromJson(Map<String, dynamic> response){
    id = response['id'];
    repairRecordId = response['repair_record_id'];
    repairTypeName = response['repair_type_name'];
    position = List<String>.from(json.decode(response['position']));
    pic = List<String>.from(json.decode(response['pic']));
    description = response['description'];
    createdAt = response['created_at'];
    updatedAt = response['updated_at'];
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'repairRecordId': repairRecordId,
    'repairTypeName': repairTypeName,
    'position': position,
    'pic': pic,
    'description': description,
    'createdAt': createdAt,
    'updatedAt': updatedAt,
  };
}

class RepairReviewDTO{
  int id;
  String satisfactionScore;
  String satisfactionRemark;

  RepairReviewDTO({
    required this.id,
    required this.satisfactionScore,
    required this.satisfactionRemark
  });

  Map<String, dynamic> toJson() => {
    'id': id,
    'satisfaction_score': satisfactionScore,
    'satisfaction_remark': satisfactionRemark
  };
}