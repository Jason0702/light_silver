import 'package:flutter/material.dart';
import 'package:light_silver/view_model/set_up_vm.dart';
import 'package:provider/provider.dart';

class TitleTextWidget extends StatelessWidget{
  const TitleTextWidget({Key? key}): super(key: key);
  @override
  Widget build(BuildContext context) {
    return Consumer<SetUpVm>(builder: (context, setUp, _){
      return Text(
        setUp.title,
        textScaleFactor: 1,
        style: const TextStyle(fontSize: 25, color: Colors.white,fontWeight: FontWeight.bold),
      );
    },);
  }
}