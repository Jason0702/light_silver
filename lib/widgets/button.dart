import 'package:flutter/material.dart';

import '../utils/utils.dart';
//清除按鈕
class ClearButton extends StatelessWidget{
  final VoidCallback click;
  final String buttonText;
  const ClearButton({Key? key, required this.click, required this.buttonText}):super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
        style: ElevatedButton.styleFrom(
            foregroundColor: Colors.white,
            backgroundColor: Colors.transparent,
            shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0))),
            padding: const EdgeInsets.all(0.0)),
        onPressed: () => click(),
        child: Container(
          width: 120,
          height: 50,
          alignment: Alignment.center,
          decoration: BoxDecoration(
              borderRadius: const BorderRadius.all(Radius.circular(10)),
              color: HexColor.fromHex('#0061AA')),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              FittedBox(
                fit: BoxFit.scaleDown,
                child: Text(
                  buttonText,
                  textScaleFactor: 1,
                  style: const TextStyle(fontSize: 20),
                ),
              ),
              const Icon(
                Icons.close,
                color: Colors.white,
                size: 30,
              )
            ],
          ),
        )
    );
  }
}
//下一步按鈕
class NextButton extends StatelessWidget{
  final VoidCallback? click;
  final String buttonText;
  final Decoration? decoration;
  const NextButton({Key? key, required this.click, required this.buttonText, required this.decoration}):super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
        style: ElevatedButton.styleFrom(
            foregroundColor: Colors.white,
            backgroundColor: Colors.transparent,
            shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0))),
            padding: const EdgeInsets.all(0.0)),
        onPressed: click,
        child: Container(
          width: 120,
          height: 50,
          alignment: Alignment.center,
          decoration: decoration,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              FittedBox(
                fit: BoxFit.scaleDown,
                child: Text(
                  buttonText,
                  textScaleFactor: 1,
                  style: const TextStyle(fontSize: 20),
                ),
              ),
              const Icon(
                Icons.arrow_forward_sharp,
                color: Colors.white,
                size: 30,
              )
            ],
          ),
        )
    );
  }
}