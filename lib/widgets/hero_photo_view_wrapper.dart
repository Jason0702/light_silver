import 'package:flutter/material.dart';

import '../app.dart';
import '../utils/utils.dart';

class HeroPhotoViewRouteWrapper extends StatelessWidget {
  final ImageProvider imageProvider;
  final String title;
  const HeroPhotoViewRouteWrapper({
    Key? key,
    required this.imageProvider,
    required this.title,
  }): super(key: key);


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Column(
          children: [
            Expanded(flex: 1, child: _titleBar(context)),
            Expanded(flex: 10, child: _bodyWidget(context)),
          ],
        ),
      ),
    );
  }

  //最上層標題與按鈕
  Widget _titleBar(context) {
    return Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(colors: [
            HexColor.fromHex('#3D9208'),
            HexColor.fromHex('#6DAC55'),
            HexColor.fromHex('#1C7B00'),
          ], begin: Alignment.topCenter, end: Alignment.bottomCenter)),
      child: Row(
        children: [
          Expanded(flex: 1, child: _leftTopButton(context)),
          Expanded(
              flex: 3,
              child: Container(
                alignment: Alignment.center,
                child: FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(title,
                    textScaleFactor: 1,
                    style: const TextStyle(fontSize: 25, color: Colors.white),
                  ),
                ),
              )),
          Expanded(flex: 1, child: _rightTopButton(context))
        ],
      ),
    );
  }
  //左側按鈕
  Widget _leftTopButton(context) {
    return GestureDetector(
      onTap: () => Navigator.of(context).pop(),
      child: const Icon(
        Icons.arrow_back_rounded,
        color: Colors.white,
        size: 45,
      ),
    );
  }



  //右側按鈕
  Widget _rightTopButton(context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).pop();
        delegate.popUtil();
      },
      child: const Icon(
        Icons.home,
        color: Colors.white,
        size: 45,
      ),
    );
  }

  Widget _bodyWidget(context) {
    return Stack(
      children: [
        Container(
          constraints: BoxConstraints.expand(
            height: MediaQuery.of(context).size.height,
          ),
          decoration: const BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/images/home_bg.png'),
                  fit: BoxFit.fill)),
          child: Image(
            image: imageProvider,
          ),
        ),
        InkWell(
          onTap: () => Navigator.of(context).pop(),
          child: Container(
            margin: const EdgeInsets.only(top: 15),
            decoration: BoxDecoration(
                borderRadius: const BorderRadius.only(
                    topRight: Radius.circular(10.0),
                    bottomRight: Radius.circular(10.0)),
                gradient: LinearGradient(colors: [
                  HexColor.fromHex('#0067B4'),
                  HexColor.fromHex('#003457'),
                ], begin: Alignment.topCenter, end: Alignment.bottomCenter)),
            child: Container(
              margin: const EdgeInsets.all(10),
              child: Image.asset('assets/images/form.png', height: 50),
            ),
          ),
        ),
      ],
    );
  }
}