import 'package:flutter/material.dart';

import '../app.dart';

class LeftButton extends StatelessWidget{
  const LeftButton({Key? key}): super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => delegate.popRoute(),
      child: const Icon(
        Icons.arrow_back_rounded,
        color: Colors.white,
        size: 45,
      ),
    );
  }
}

class LeftSaveButton extends StatelessWidget{
  final VoidCallback func;
  const LeftSaveButton({Key? key, required this.func}): super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        func();
        delegate.popRoute();
      },
      child: const Icon(
        Icons.arrow_back_rounded,
        color: Colors.white,
        size: 45,
      ),
    );
  }
}