import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../view_model/login_register_vm.dart';

class TitleTextWidget extends StatelessWidget {
  const TitleTextWidget({Key? key}): super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<LoginRegisterVm>(builder: (context, vm, _){
      return Text(
      vm.title,
      textScaleFactor: 1,
      style: const TextStyle(fontSize: 25, color: Colors.white,fontWeight: FontWeight.bold),
      );
    },);
  }
}