import 'package:flutter/material.dart';

import '../utils/utils.dart';

class TitleWidget extends StatelessWidget{
  final String titleText;
  final Decoration decoration;
  final Widget? leftButton;
  final Widget? rightButton;

  const TitleWidget({Key? key, required this.titleText, required this.decoration, required this.leftButton, required this.rightButton});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: decoration,
      child: Row(
        children: [
          if(leftButton != null)
            Expanded(flex: 1, child: leftButton!),
          Expanded(
              flex: 3,
              child: Container(
                alignment: Alignment.center,
                child: FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    titleText,
                    textScaleFactor: 1,
                    style: const TextStyle(fontSize: 25, color: Colors.white),
                  ),
                ),
              )),
          if(rightButton != null)
            Expanded(flex: 1, child: rightButton!)
        ],
      ),
    );
  }
}

class SpeTitleWidget extends StatelessWidget{
  final String titleText;
  final Widget? leftButton;
  final Widget? rightButton;
  const SpeTitleWidget({Key? key, required this.titleText, this.leftButton, this.rightButton}): super(key: key);

  @override
  Widget build(BuildContext context) {
    return TitleWidget(
      titleText: titleText,
      decoration: Style.speTitleBarBG,
      leftButton: leftButton,
      rightButton: rightButton,
    );
  }
}

class LdTitleWidget extends StatelessWidget{
  final String titleText;
  final Widget? leftButton;
  final Widget? rightButton;
  const LdTitleWidget({Key? key, required this.titleText, this.leftButton, this.rightButton}): super(key: key);

  @override
  Widget build(BuildContext context) {
    return TitleWidget(
        titleText: titleText,
        decoration: Style.ldTitleBarBG,
        leftButton: leftButton,
        rightButton: rightButton,
    );
  }
}

class LbkTitleWidget extends StatelessWidget{
  final String titleText;
  final Widget? leftButton;
  final Widget? rightButton;
  const LbkTitleWidget({Key? key, required this.titleText, this.leftButton, this.rightButton}): super(key: key);

  @override
  Widget build(BuildContext context) {
    return TitleWidget(
        titleText: titleText,
        decoration: Style.lbkTitleBarBG,
        leftButton: leftButton,
        rightButton: rightButton,
    );
  }
}

class LbkTenTitleWidget extends StatelessWidget{
  final String titleText;
  final Widget? leftButton;
  final Widget? rightButton;
  const LbkTenTitleWidget({Key? key, required this.titleText, this.leftButton, this.rightButton}): super(key: key);

  @override
  Widget build(BuildContext context) {
    return TitleWidget(
      titleText: titleText,
      decoration: Style.lbkTenTitleBarBG,
      leftButton: leftButton,
      rightButton: rightButton,
    );
  }
}

class SpsTitleWidget extends StatelessWidget{
  final String titleText;
  final Widget? leftButton;
  final Widget? rightButton;
  const SpsTitleWidget({Key? key, required this.titleText, this.leftButton, this.rightButton}): super(key: key);

  @override
  Widget build(BuildContext context) {
    return TitleWidget(
      titleText: titleText,
      decoration: Style.spsTitleBarBG,
      leftButton: leftButton,
      rightButton: rightButton,
    );
  }
}

class PvTitleWidget extends StatelessWidget{
  final String titleText;
  final Widget? leftButton;
  final Widget? rightButton;
  const PvTitleWidget({Key? key, required this.titleText, this.leftButton, this.rightButton}): super(key: key);

  @override
  Widget build(BuildContext context) {
    return TitleWidget(
      titleText: titleText,
      decoration: Style.pvTitleBarBG,
      leftButton: leftButton,
      rightButton: rightButton,
    );
  }
}